package ru.rosst.extensions.kotlin

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope

/**
 * Метод-расширение коллекций - параллельная итерация.
 *
 * @param block блок кода.
 */
suspend fun <T> Iterable<T>.parallelForEach(block: suspend (T) -> Unit) =
    coroutineScope { map { async { block(it) } } }.awaitAll()

/**
 * Метод-расширение коллекций - параллельный маппинг.
 *
 * @param function блок кода.
 *
 * @return [List] коллекция с элементами типа [R].
 */
suspend fun <T, R> Iterable<T>.parallelMap(function: suspend (T) -> R) =
    coroutineScope { map { async { function(it) } } }.awaitAll()

suspend fun <T, R> Iterable<T>.parallelFlatMap(function: suspend (T) -> Iterable<R>) =
    coroutineScope { map { async { function(it) } }.flatMap { it.await() } }