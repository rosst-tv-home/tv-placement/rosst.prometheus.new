package ru.rosst.extensions.spring

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Абстрактный класс, описывающий Spring Framework Service с логгером.
 *
 * @property logger логгер.
 */
abstract class AbstractService {
    private val logger: Logger = LoggerFactory.getLogger(javaClass.simpleName)

    /**
     * Метод логгирования с тегом ["INFO"]. Форматирует многострочный текст в одну строку.
     * Метод можно использовать в классах - наследниках.
     *
     * @param message [String] сообщение.
     */
    protected fun info(message: String) = logger.info(formatMessage(message))

    /**
     * Метод логгирования с тегом ["WARNING"]. Форматирует многострочный текст в одну строку.
     * Метод можно использовать в классах - наследниках.
     *
     * @param message [String] сообщение.
     */
    protected fun warning(message: String) = logger.warn(formatMessage(message))

    /**
     * Метод логгирования с тегом ["ERROR"]. Форматирует многострочный текст в одну строку.
     * Метод можно использовать в классах - наследниках.
     *
     * @param message [String] сообщение.
     */
    protected fun error(message: String) = logger.error(formatMessage(message))

    /**
     * Метод форматирования текста. Если это многострочный текст, то текст формируется в одну строку.
     *
     * @param text  [String] текст.
     *
     * @return      [String] форматированный текст.
     */
    private fun formatMessage(text: String): String = text
        .trimMargin()
        .trimIndent()
        .replace("\n", " ")
}