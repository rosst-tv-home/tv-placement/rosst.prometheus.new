rootProject.name = "prometheus"

include("spring")
include("spring-data")
include("vimb")
include("extensions")
include("tvr")
