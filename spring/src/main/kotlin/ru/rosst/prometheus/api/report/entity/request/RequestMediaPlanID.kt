package ru.rosst.prometheus.api.report.entity.request

import com.fasterxml.jackson.annotation.JsonProperty

class RequestMediaPlanID(@JsonProperty(value = "id", required = true) val id: Int)