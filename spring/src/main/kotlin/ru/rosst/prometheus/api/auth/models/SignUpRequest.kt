package ru.rosst.prometheus.api.auth.models

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

class SignUpRequest(
    @JsonProperty("name")
    val name: String,
    @JsonProperty("surname")
    val surname: String,
    @JsonProperty("login")
    val login: String,
    @JsonProperty("password")
    val password: String
) : Serializable