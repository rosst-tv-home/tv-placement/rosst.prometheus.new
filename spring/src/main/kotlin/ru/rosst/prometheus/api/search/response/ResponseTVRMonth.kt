package ru.rosst.prometheus.api.search.response

import java.time.LocalDate

class ResponseTVRMonth(
    var date: LocalDate,
    var name: String
) {


    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + date.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResponseTVRMonth

        if (name != other.name) return false
        if (date != other.date) return false

        return true
    }


}



