package ru.rosst.prometheus.api.parameters

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.prometheus.api.parameters.request.*
import ru.rosst.prometheus.api.search.response.ResponseIsPlacementActive
import ru.rosst.prometheus.api.search.response.ResponseMediaPlan
import ru.rosst.prometheus.api.search.response.ResponseTVRMonth
import ru.rosst.prometheus.api.search.response.ResponseTvrDate
import ru.rosst.prometheus.entity.AutoPlacementFilm
import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.AutoPlacementPeriod
import ru.rosst.prometheus.entity.AutoPlacementTime
import ru.rosst.prometheus.enumerated.AutoPlacementAlgorithm
import ru.rosst.prometheus.service.ServiceAudiences
import ru.rosst.prometheus.service.ServiceAutoPlacementParameters
import ru.rosst.prometheus.service.ServiceMediaPlans
import ru.rosst.prometheus.service.ServicePalomarsChannelsRatings
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * Контроллер представления параметров автоматического размещения медиаплана.
 *
 * Сервисы управления данными таблиц базы данных:
 * @property srvMediaPlans [ServiceMediaPlans]              сервис управления данными таблицы ["media_plans"] "медиапланы".
 * @property srvParameters [ServiceAutoPlacementParameters] сервис управления данными таблицы ["auto_placement_parameters"] "параметры автоматического размещения".
 */
@RestController
@RequestMapping(value = ["media-plan-parameters"])
class MediaPlanParametersViewController() {
    @Autowired
    private lateinit var srvAudiences: ServiceAudiences

    @Autowired
    private lateinit var srvMediaPlans: ServiceMediaPlans

    @Autowired
    private lateinit var srvParameters: ServiceAutoPlacementParameters

    @Autowired
    private lateinit var srvPalomarsChannelsRatings: ServicePalomarsChannelsRatings

    /**
     * Метод запроса медиаплана.
     *
     * @param mediaPlanID [Int] идентификатор медиаплана.
     *
     * @return [ResponseEntity] ответ с медиапланом или ошибкой.
     */
    @GetMapping(value = ["get-media-plan"])
    @Transactional
    fun getMediaPlan(@RequestParam mediaPlanID: Int): ResponseEntity<*> {
        // Связи роликов и медиаплана - если связи не сущесвуют - выбрасываем ошибку
        val links = srvMediaPlans.findByMediaPlanId(mediaPlanID)
            ?: return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body("Медиаплан с идентификатором $mediaPlanID не найден.")

        // Находим в связях медиаплан или выбрасываем ошибку
        val mediaPlan = links.firstOrNull()?.mediaPlan
            ?: return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body("Медиаплан с идентификатором $mediaPlanID не найден.")


        // Отправляем медиаплан клиенту
        return ResponseEntity.ok(ResponseMediaPlan(mediaPlan))
    }

    /**
     * Метод запроса алгоритма
     */
    @GetMapping(value = ["get-placement-types"])
    @Transactional
    fun getAlgorithm(): ResponseEntity<Array<AutoPlacementAlgorithm>> {
        val algorithm = AutoPlacementAlgorithm.values()
        if (algorithm.isEmpty()) return ResponseEntity.status(HttpStatus.ACCEPTED).build()
        return ResponseEntity.ok(algorithm)
    }

    /**
     * Метод запроса признака "автоматическое размещение - включено".
     *
     * @param id [Int] индентификатор медиаплана.
     *
     * @return [ResponseEntity] ответ [ResponseIsPlacementActive] признак "автоматическое размещение".
     */
    @GetMapping(value = ["get-is-placement-active"])
    fun getIsPlacementActive(@RequestParam id: Int): ResponseEntity<Boolean> =
        ResponseEntity.ok(srvParameters.isActive(id))

    /**
     * Метод запроса коллекции целевых аудиторий, подходящих конкретному медиаплану.
     *
     * @param mediaPlanID   [Int] индентификатор медиаплана.
     *
     * @return              [ResponseEntity] ответ с [List] коллекции [String] наименований аудиторий, если они были найдены.
     */
    @GetMapping(value = ["get-audiences"])
    @Transactional
    fun getMediaPlanTVR(@RequestParam mediaPlanID: Int): ResponseEntity<List<ResponseTvrDate>>? {
        // Поиск медиаплана
        val mediaPlan = srvMediaPlans.findByMediaPlanId(mediaPlanID)?.first()?.mediaPlan
            ?: return ResponseEntity.status(HttpStatus.ACCEPTED).body(listOf<ResponseTvrDate>())
        // Поиск tvr аудиторий канала-месяца
        val audience = srvPalomarsChannelsRatings.findChannelAudiences(mediaPlan.channel)

        // Берём только названия аудиторий
        val tvrMonths = runBlocking {
            audience
                .parallelMap { ResponseTVRMonth(it.month, it.audience.name) }
                .toSet()
                .map { ResponseTvrDate(it.name, ZonedDateTime.of(it.date, LocalTime.now(), ZoneId.systemDefault())) }
        }
        // Если коллекция аудиторий пустая - отправляет HTTP_ACCEPTED 202 пользователю
        if (tvrMonths.isEmpty())
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(listOf<ResponseTvrDate>())

        // Пересылаем коллекцию аудиторий пользователю
        return ResponseEntity.ok(tvrMonths)
    }

    /**
     * Метод запроса параметров автоматического размещения медиаплана.
     *
     * @param mediaPlanID   [Int]               идентификатор медиаплана.
     *
     * @return              [ResponseEntity]    ответ с [AutoPlacementParameter] параметрами автоматического размещения медиаплана, если они были найдены.
     */
    @GetMapping(value = ["get-parameters"])
    fun getMediaPlanParameters(@RequestParam mediaPlanID: Int): ResponseEntity<RequestMediaPlanParameters> {
        // Поиск параметров автоматического размещения медиаплана
        val parameters = srvParameters.findByMediaPlanID(mediaPlanID)
        // Если параметры не пустые - отправляем клиенту
        if (parameters !== null) {
            val formattedParameters = RequestMediaPlanParameters(
                parameters.id,
                mediaPlanID,
                parameters.periods.map { period ->
                    RequestMediaPlanParametersPeriod(
                        period.id,
                        if (period.dateFrom !== null) ZonedDateTime.of(
                            period.dateFrom,
                            LocalTime.now(),
                            ZoneId.systemDefault()
                        ) else null,
                        if (period.dateBefore !== null) ZonedDateTime.of(
                            period.dateBefore,
                            LocalTime.now(),
                            ZoneId.systemDefault()
                        ) else null,
                        period.inventory,
                        period.percentPrime,
                        period.films.map { film ->
                            RequestMediaPlanParametersPeriodFilm(
                                film.id,
                                film.filmXMediaPlan.id,
                                film.inventoryPercent,
                                film.filmXMediaPlan.dateFrom,
                                film.filmXMediaPlan.dateBefore
                            )
                        }
                    )
                },
                parameters.times.map { time ->
                    RequestMediaPlanParametersTime(
                        time.id,
                        ZonedDateTime.of(LocalDate.now(), time.timeFrom, ZoneId.systemDefault()),
                        ZonedDateTime.of(LocalDate.now(), time.timeBefore, ZoneId.systemDefault()),
                        time.isWeekend
                    )
                },
                when (parameters.algorithm) {
                    AutoPlacementAlgorithm.AFFINITIES -> EnumPlacementChoice.AFFINITIES
                    AutoPlacementAlgorithm.EQUAL_DISTRIBUTION -> EnumPlacementChoice.EQUAL_DISTRIBUTION
                    else -> null
                },
                if (parameters.audience != null && parameters.audienceDate != null) {
                    RequestTvrDate(parameters.audience!!.name, parameters.audienceDate!!)
                } else {
                    null
                },
                parameters.inventoryPercentRequired,
                parameters.isPlacementActive,
                parameters.isAssignedRatingsPlacement
            )
            return ResponseEntity.ok(formattedParameters)
        }
        // Иначе, отправляем статус 202
        return ResponseEntity.status(HttpStatus.ACCEPTED).build()
    }

    /**
     * Метод запроса создания\обновления параметров автоматического размещения медиаплана.
     *
     * @param request [RequestMediaPlanParameters] параметры автоматического размещения медиаплана.
     */
    @PostMapping(value = ["set-parameters"])
    fun setMediaPlanParameters(@RequestBody request: RequestMediaPlanParameters): ResponseEntity<*> {
        return try {
            fromRequestToEntity(request)
            ResponseEntity.ok("Ok")
        } catch (exception: Exception) {
            when (exception) {
                is IllegalArgumentException -> ResponseEntity
                    .status(HttpStatus.EXPECTATION_FAILED)
                    .body("Медиаплан с идентификатором ${request.mediaPlanID} не найден.")
                else -> ResponseEntity
                    .status(HttpStatus.EXPECTATION_FAILED)
                    .body("Неизвестная ошибка.")
            }
        }
    }

    /**
     * Метод запроса создания\обновления признака "автоматическое размещение - включено".
     *
     * @param request [RequestIsPlacementActive] признак "автоматического размещение"
     */
    @PostMapping(value = ["set-is-placement-active"])
    fun setIsPlacementActive(@RequestBody request: RequestIsPlacementActive) {
        val parameters = srvParameters.findByMediaPlanID(request.id)
        parameters!!.isPlacementActive = request.isPlacementActive
        srvParameters.save(parameters)
    }


    /**
     * Метод удаления параметров автоматического размещения медиаплана по его идентификатору.
     *
     * @param mediaPlanID [Int] идентификатор медиаплана.
     */
    @GetMapping(value = ["delete-parameters"])
    fun deleteMediaPlanParameters(@RequestParam mediaPlanID: Int): ResponseEntity<String> {
        srvParameters.deleteByMediaPlanID(mediaPlanID)
        return ResponseEntity.ok("Параметры удалены")
    }

    private fun fromRequestToEntity(request: RequestMediaPlanParameters) = runBlocking {
        // Получаем связи роликов и медиаплана из базы данных - если связи не найдены - выбрасываем исключение
        val links = srvMediaPlans.findByMediaPlanId(
            request.mediaPlanID
        ) ?: throw IllegalArgumentException()

        // Поиск параметров автоматического размещения медиаплана
        val existParameters = srvParameters.findByMediaPlanID(request.mediaPlanID)

        // Если параметры есть в базе данных - удаляем
        if (existParameters != null) {
            srvParameters.deleteByID(existParameters.id)
        }

        // Конвертируем модели запроса временных интервалов в модели базы данных
        val times = request.times.parallelMap { time ->
            AutoPlacementTime(
                time.timeFrom.toLocalTime(),
                time.timeBefore.toLocalTime(),
                time.isWeekend
            ).apply { id = time.id }
        }

        // Конвертируем модели запроса периодов в модели базы данных
        val periods = request.periods.parallelMap { period ->
            AutoPlacementPeriod(
                period.fromDate?.toLocalDate(),
                period.beforeDate?.toLocalDate(),
                period.inventory,
                period.percentPrime,
                period.films.parallelMap { film ->
                    AutoPlacementFilm(
                        links.first { it.id == film.linkID },
                        film.inventoryPercent
                    ).apply { id = film.id }
                }.toSet()
            ).apply { id = period.id }
        }

        // Определяем указанный алгоритм размещения
        val algorithm = when (request.placement) {
            EnumPlacementChoice.AFFINITIES -> AutoPlacementAlgorithm.AFFINITIES
            EnumPlacementChoice.EQUAL_DISTRIBUTION -> AutoPlacementAlgorithm.EQUAL_DISTRIBUTION
            else -> throw IllegalArgumentException("Не удалось определить тип размещения медиаплана.")
        }
        // Определяем признак "автоматическое размещение"
        val placementActive = when (request.isPlacementActive) {
            true -> true
            false -> false
        }

        // Конвертируем модели запроса параметров в модели базы данных
        var parameters = AutoPlacementParameter(
            links.first().mediaPlan,
            algorithm,
            isPlacementActive = placementActive,
            isAssignedRatingsPlacement = request.isAssignedRatingsPlacement
        )

        if (algorithm == AutoPlacementAlgorithm.AFFINITIES) {
            if (request.audience == null) {
                throw IllegalArgumentException("Не указана целевая аудитория канала.")
            }

            val audience = srvAudiences.findByName(request.audience!!.name)
                ?: throw NoSuchElementException("Указанная аудитория не найдена в базе данных.")

            parameters.audience = audience
            parameters.audienceDate = request.audience?.date
        }
        parameters.inventoryPercentRequired = request.totalInventoryPercent

        // Сохраняем параметры в базу данных
        parameters = srvParameters.save(parameters)


        // Закидываем идентификатор параметров во временные параметры
        times.forEach { time -> time.parameter = parameters }

        // Сохраняем параметры временных периодов в базу данных
        srvParameters.saveTimes(times)

        // Закидываем идентификатор параметров в периоды
        periods.forEach { period ->
            period.parameter = parameters
            val persistedPeriod = srvParameters.savePeriod(period)
            period.films.forEach { film ->
                film.period = persistedPeriod
                srvParameters.saveFilm(film)
            }
        }
    }
}