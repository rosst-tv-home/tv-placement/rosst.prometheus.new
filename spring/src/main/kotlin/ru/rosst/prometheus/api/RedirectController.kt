package ru.rosst.prometheus.api

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Контроллер перенаправления запросов.
 */
@Controller
class RedirectController {
    /**
     * Метод - обработчик запросов. Обрабатывает запросы и перенаправляет запросы во frontend.
     */
    @RequestMapping(value = ["{_:^(?!index\\.html|api).*$}"])
    fun redirectApi(): String? = "forward:/"
}