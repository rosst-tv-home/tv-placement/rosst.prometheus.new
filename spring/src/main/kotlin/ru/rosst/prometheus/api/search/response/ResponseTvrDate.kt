package ru.rosst.prometheus.api.search.response

import java.time.ZonedDateTime

class ResponseTvrDate(
    val name: String,
    val date: ZonedDateTime
)

