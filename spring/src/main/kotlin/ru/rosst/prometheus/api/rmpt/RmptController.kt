package ru.rosst.prometheus.api.rmpt

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.rosst.prometheus.entity.Rating
import ru.rosst.prometheus.repository.RepoRatings

@RestController
@RequestMapping(value = ["rmpt"])
class RmptController {
    @Autowired
    private lateinit var repoRatings: RepoRatings

    @PostMapping(value = ["ratings"])
    fun getRatings(
        @RequestBody
        request: ModelRequestMonthChannelRatings
    ): ResponseEntity<Iterable<Rating>> = ResponseEntity.ok(
        repoRatings.getRatings(
            request.ids.toSet(),
            request.months.map { it.toLocalDate() }.toSet()
        )
    )
}