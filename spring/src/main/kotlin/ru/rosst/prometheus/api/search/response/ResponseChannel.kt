package ru.rosst.prometheus.api.search.response

import ru.rosst.prometheus.entity.Channel

/**
 * Модель канала.
 *
 * @param channel [Channel] модель канала.
 *
 * @property id [Int] идентификатор канала.
 * @property name [String] название канала.
 * @property palomarsChannelID [Int] идентификатор канала palomars.
 */
class ResponseChannel(channel: Channel) {
    val id: Int = channel.id
    val name: String = channel.name
    val palomarsChannelID: Int? = channel.palomarsChannel?.id
}