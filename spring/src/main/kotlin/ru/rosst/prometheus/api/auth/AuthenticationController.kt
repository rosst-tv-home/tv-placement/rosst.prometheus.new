package ru.rosst.prometheus.api.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.rosst.prometheus.api.ResponseMessage
import ru.rosst.prometheus.api.auth.models.SignInRequest
import ru.rosst.prometheus.api.auth.models.SignInResponse
import ru.rosst.prometheus.api.auth.models.SignUpRequest
import ru.rosst.prometheus.entity.User
import ru.rosst.prometheus.service.ServiceUsers
import ru.rosst.prometheus.service.spring.ServiceJWT

@RestController
@RequestMapping("api")
class AuthenticationController {
    @Autowired
    private lateinit var srvJWT: ServiceJWT

    @Autowired
    private lateinit var srvUsers: ServiceUsers

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    /**
     * Метод авторизации пользователя.
     * @param requestSignIn модель запроса авторизации.
     * @return модель ответа на запрос.
     */
    @PostMapping("sign-in")
    fun signIn(@RequestBody requestSignIn: SignInRequest): ResponseEntity<*> {
        val userCandidate = srvUsers.findByLogin(requestSignIn.login) ?: return ResponseEntity(
            ResponseMessage("Пользователь ${requestSignIn.login} не найден."),
            HttpStatus.BAD_REQUEST
        )

        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(
                requestSignIn.login, requestSignIn.password
            )
        )

        SecurityContextHolder.getContext().authentication = authentication

        val jwt = srvJWT.generate(userCandidate.login)

        return ResponseEntity.ok(
            SignInResponse(
                jwt,
                userCandidate.name,
                userCandidate.surname
            )
        )
    }

    @PostMapping("sign-up")
    fun singUp(@RequestBody requestSignUp: SignUpRequest): ResponseEntity<*> {
        val user = User(
            requestSignUp.login,
            requestSignUp.name,
            requestSignUp.surname,
            passwordEncoder.encode(requestSignUp.password)
        )
        srvUsers.save(user)
        return ResponseEntity.ok(ResponseMessage("Вы успешно зарегистрировались."))
    }
}