package ru.rosst.prometheus.api.auth.models

import com.fasterxml.jackson.annotation.JsonProperty

class SignInRequest(
    @JsonProperty("login")
    val login: String,
    @JsonProperty("password")
    val password: String
)