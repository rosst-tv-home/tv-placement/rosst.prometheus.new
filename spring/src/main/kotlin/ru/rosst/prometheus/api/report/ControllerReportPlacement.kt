package ru.rosst.prometheus.api.report

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.rosst.extensions.kotlin.parallelForEach
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.api.report.entity.request.RequestMediaPlanID
import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.enumerated.PalomarsTimeDayType
import ru.rosst.prometheus.service.ServiceAutoPlacementParameters
import ru.rosst.prometheus.service.ServiceBlocks
import ru.rosst.prometheus.service.autoplacement.models.Affinity
import service.report.ServiceReportPlacement
import service.report.entity.PlacementBlock
import service.report.entity.PlacementLine
import service.report.entity.PlacementMediaPlan
import java.time.DayOfWeek
import javax.transaction.Transactional

@RestController
@RequestMapping(value = ["report"])
class ControllerReportPlacement : AbstractService() {
    @Autowired
    private lateinit var srvBlocks: ServiceBlocks

    @Autowired
    private lateinit var srvParameters: ServiceAutoPlacementParameters

    @PostMapping(
        value = ["placement"],
        produces = ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
    )
    @Transactional
    fun reportMediaPlanPlacement(@RequestBody mediaPlans: List<RequestMediaPlanID>): ByteArray = runBlocking {
        ServiceReportPlacement.createBook()

        mediaPlans.forEach { mediaPlanID ->
            val parameters = srvParameters.findByMediaPlanID(mediaPlanID.id) ?: return@forEach
            val blocks = srvBlocks.findAllByMediaPlan(parameters.mediaPlan)
            val affinity = getAffinity(parameters)

            // Медиаплан
            val placementMediaPlan = PlacementMediaPlan(
                parameters.mediaPlan.name,
                if (parameters.mediaPlan.order.rank.id == 1.toByte()) "SUPER FIX" else "FIX",
                parameters.mediaPlan.date
            )

            val issues = blocks
                .map(Block::issue)

            // Линейки программ
            var placementLines = mutableListOf<PlacementLine>()

            blocks
                .forEach block@{ block ->
                    val line = placementLines.firstOrNull { line ->
                        val isName = line.programName == block.issue.program.name
                        val isTime = block.time.toLocalTime().withSecond(0) == line.time
                        isName && isTime
                    }
                    if (line == null) {
                        PlacementLine(
                            block.issue.program.name,
                            block.time.toLocalTime().withSecond(0)
                        ).apply {
                            this.issues.add(block.issue.id)
                            placementLines.add(this)
                        }
                        return@block
                    }
                    line.issues.add(block.issue.id)
                }
            placementLines = placementLines.filter {
                parameters.times.any { t -> it.time in t.timeFrom..t.timeBefore }
            }.toMutableList()


            val minDate = parameters.periods
                .filter { it.dateFrom != null }
                .minByOrNull { it.dateFrom!! }?.dateFrom
                ?: return@runBlocking byteArrayOf()
            val maxDate = parameters.periods
                .filter { it.dateBefore != null }
                .maxByOrNull { it.dateBefore!! }?.dateBefore
                ?: return@runBlocking byteArrayOf()
            // Дни линеек программ
            placementLines.forEach { line ->
                val days = (minDate.dayOfMonth..maxDate.dayOfMonth).map {
                    // Дата дня
                    val day = parameters.mediaPlan.date.withDayOfMonth(it)
                    val isWeekend = day.dayOfWeek == DayOfWeek.SATURDAY || day.dayOfWeek == DayOfWeek.SUNDAY
                    val bs = issues
                        .filter { iss -> line.issues.contains(iss.id) }
                        .flatMap { it.blocks }
                        .filter { b ->

                            val isTime = b.time.toLocalTime().withSecond(0) == line.time.withSecond(0)
                            val isDate = b.date == day
                            isTime && isDate
                        }
                    val isBlocks = bs.isNotEmpty()
                    val spots = bs.filter { b ->
                        b.spots.any { s -> s.filmXMediaPlan.mediaPlan.id == parameters.mediaPlan.id }
                    }.map { b ->
                        b.spots.filter { s -> s.filmXMediaPlan.mediaPlan.id == parameters.mediaPlan.id }
                    }.flatten()
                    val isSpots = !spots.isNullOrEmpty()
                    val calculatedAffinity = affinity.firstOrNull { aff ->
                        val inTime = line.time in aff.baseAudienceRating.timeFrom..aff.baseAudienceRating.timeBefore
                        val inDayType = (isWeekend && aff.baseAudienceRating.dayType == PalomarsTimeDayType.DAY_OFF) ||
                        !isWeekend && aff.baseAudienceRating.dayType == PalomarsTimeDayType.WORKDAY
                        inTime && inDayType
                    }?.affinity ?: 0.0
                    PlacementBlock(day, isBlocks, calculatedAffinity, isSpots, isWeekend).apply {
                        if (isSpots) {
                            spotsComment = spots.joinToString(separator = "\n") { s ->
                                val whoPlaced = if (s.isHumanPlacement) "Поставил человек:"
                                else "Поставил программатик:"
                                val theFilm = s.filmXMediaPlan.film.versionName
                                val filmDuration = s.filmXMediaPlan.film.duration
                                "$whoPlaced $theFilm ($filmDuration сек.)"
                            }
                        }
                    }
                }
                line.days.addAll(days)
            }

            placementMediaPlan.line.addAll(placementLines)

            return@forEach ServiceReportPlacement.createReport(placementMediaPlan)
        }

        val result = ServiceReportPlacement.getResult()
        ServiceReportPlacement.removeBook()

        return@runBlocking result
    }

    /**
     * Метод запроса affinity канала месяца.
     *
     * @param parameters    [AutoPlacementParameter]    параметры автоматического размещения медиаплана.
     *
     * @return              [Iterable]                  коллекция [Affinity] аффинити канала месяца.
     */
    private suspend fun getAffinity(parameters: AutoPlacementParameter): Iterable<Affinity> = coroutineScope {
        // Коллекция affinity получасовых периодов канала-месяца
        val affinity = mutableListOf<Affinity>()

        // Запрашиваем базовую аудиторию канала
        val baseAudience = parameters.mediaPlan.channel.palomarsChannel?.baseAudiences?.lastOrNull()
        if (baseAudience == null) {
            error("Не удалось получить базовую аудиторию канала: ${parameters.mediaPlan.channel.palomarsChannel?.name}")
            return@coroutineScope affinity
        }

        // Запрашиваем все известные рейтинги по всем известным аудиториям канала
        val ratings = parameters.mediaPlan.channel.palomarsChannel?.ratings
        if (ratings.isNullOrEmpty()) {
            error("Не удалось найти хоть какие-то рейтинги канала: ${parameters.mediaPlan.channel.palomarsChannel?.name}")
            return@coroutineScope affinity
        }

        // Делаем выборку рейтингов по целевой аудитории
        val targetAudienceRatings = ratings.filter { rating ->
            rating.audience == parameters.audience && rating.month == parameters.audienceDate
        }

        // Делаем выборку рейтингов по базовой аудитории
        val baseAudienceRatings = ratings.filter { rating ->
            rating.audience == baseAudience.audience && rating.month == parameters.audienceDate
        }

        // Считаем affinity получасовых периодов канала
        targetAudienceRatings.parallelForEach { targetRating ->
            val foundBaseAudience = baseAudienceRatings.firstOrNull { baseRating ->
                baseRating.timeFrom == targetRating.timeFrom &&
                baseRating.timeBefore == targetRating.timeBefore &&
                baseRating.dayType == targetRating.dayType
            } ?: return@parallelForEach warning("Не удалось найти БА канала к ЦА чтобы посчитать affinity")
            affinity.add(Affinity(foundBaseAudience, targetRating))
        }

        // Сортируем affinity по убыванию
        affinity.sortByDescending(Affinity::affinity)

        return@coroutineScope affinity
    }
}