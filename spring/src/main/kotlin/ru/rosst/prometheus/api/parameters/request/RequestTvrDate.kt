package ru.rosst.prometheus.api.parameters.request

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate

class RequestTvrDate(
    @JsonProperty(value = "name")
    val name: String,
    @JsonProperty(value = "date")
    val date: LocalDate
)
