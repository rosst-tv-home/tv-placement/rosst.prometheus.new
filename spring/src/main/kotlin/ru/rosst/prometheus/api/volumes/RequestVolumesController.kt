package ru.rosst.prometheus.api.volumes

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.rosst.prometheus.service.ServiceRequestVolumes

@RestController
@RequestMapping(value = ["request-volumes"])
class RequestVolumesController {
    @Autowired
    private lateinit var srvRequestVolumes: ServiceRequestVolumes

    @GetMapping(value = ["blocks"])
    fun getBlocksRequestVolumes() = ResponseEntity.ok(srvRequestVolumes.get("blocks"))

    @GetMapping(value = ["media-plans"])
    fun getMediaPlansRequestVolumes() = ResponseEntity.ok(srvRequestVolumes.get("media_plans"))
}