package ru.rosst.prometheus.api.search.response

import com.fasterxml.jackson.annotation.JsonProperty
import ru.rosst.prometheus.entity.SellingDirection

/**
 * Модель направления продаж для представления поиска медиапланов.
 *
 * @param sellingDirection [SellingDirection] модель направления продаж.
 *
 * @property id [Byte] идентификатор направления продаж.
 * @property name [String] название направления продаж.
 */
class ResponseSellingDirection(sellingDirection: SellingDirection) {
    @JsonProperty(value = "id")
    val id: Byte = sellingDirection.id

    @JsonProperty(value = "name")
    val name: String = sellingDirection.name
}