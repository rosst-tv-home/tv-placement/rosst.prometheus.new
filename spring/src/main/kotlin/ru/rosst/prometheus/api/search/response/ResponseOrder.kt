package ru.rosst.prometheus.api.search.response

import ru.rosst.prometheus.entity.Order

/**
 * Модель заказа.
 *
 * @param order [Order] заказ.
 *
 * @property id [Int] идентификатор заказа.
 * @property name [String] название заказа.
 * @property isSuperFix [Boolean] признак "super fix размещение".
 * @property agreement [ResponseAgreement] сделка заказа.
 */
class ResponseOrder(order: Order) {
    val id: Int = order.id
    val name: String = order.name
    val isSuperFix = order.rank.id == 1.toByte()
    val agreement: ResponseAgreement = ResponseAgreement(order.agreement)
}