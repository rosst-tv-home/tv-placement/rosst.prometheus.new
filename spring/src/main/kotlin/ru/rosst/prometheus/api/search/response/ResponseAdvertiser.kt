package ru.rosst.prometheus.api.search.response

import ru.rosst.prometheus.entity.Advertiser

/**
 * Модель рекламодателя.
 *
 * @param advertiser [Advertiser] модель рекламодателя.
 *
 * @property id [Int] идентификатор рекламодателя.
 * @property name [String] название рекламодателя.
 */
class ResponseAdvertiser(advertiser: Advertiser) {
    val id: Int = advertiser.id
    val name: String = advertiser.name
}