package ru.rosst.prometheus.api.channels

import ru.rosst.prometheus.entity.PalomarsChannel

/**
 * Модель канала Palomars.
 *
 * @param channel   [PalomarsChannel]   канал Palomars.
 *
 * @property id     [Int]               идентификатор канала Palomars.
 * @property name   [String]            название канала Palomars.
 */
class ResponsePalomarsChannel(channel: PalomarsChannel) {
    val id: Int = channel.id
    val name: String = channel.name
}