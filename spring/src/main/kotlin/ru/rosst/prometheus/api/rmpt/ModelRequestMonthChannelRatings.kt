package ru.rosst.prometheus.api.rmpt

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class ModelRequestMonthChannelRatings(
    @JsonProperty(value = "ids")
    val ids: List<Int>,
    @JsonProperty(value = "months")
    val months: List<LocalDateTime>
)