package ru.rosst.prometheus.api.channels

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.ZonedDateTime

class RequestPalomarsChannelXAudience(
    @JsonProperty(value = "palomars_channel_id")
    val palomarsChannelID: Int,
    @JsonProperty(value = "audience_id")
    val audienceID: Int,
    @JsonProperty(value = "date")
    val date: ZonedDateTime
)