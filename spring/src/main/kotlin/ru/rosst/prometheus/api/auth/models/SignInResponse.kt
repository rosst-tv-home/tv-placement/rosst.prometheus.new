package ru.rosst.prometheus.api.auth.models

import com.fasterxml.jackson.annotation.JsonProperty

class SignInResponse(
    @JsonProperty("token")
    val token: String,
    @JsonProperty("name")
    val name: String,
    @JsonProperty("surname")
    val surname: String
)