package ru.rosst.prometheus.api.parameters.request

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Модель признак "автоматическое размещение - включено".
 *
 * @param id [Int] идентификатор медиаплана.
 * @param isPlacementActive [Boolean] признак "автоматическое размещение".
 */
class RequestIsPlacementActive(
    @JsonProperty(value = "id")
    val id: Int = 0,
    @JsonProperty(value = "isPlacementActive")
    val isPlacementActive: Boolean
)