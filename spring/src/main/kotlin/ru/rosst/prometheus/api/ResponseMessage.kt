package ru.rosst.prometheus.api

import com.fasterxml.jackson.annotation.JsonProperty

class ResponseMessage(
    @JsonProperty("message")
    val message: String
)