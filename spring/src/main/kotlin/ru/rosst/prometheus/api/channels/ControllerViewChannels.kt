package ru.rosst.prometheus.api.channels

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.rosst.prometheus.api.search.response.ResponseSellingDirection
import ru.rosst.prometheus.entity.Audience
import ru.rosst.prometheus.entity.PalomarsChannelXBaseAudience
import ru.rosst.prometheus.service.*

/**
 * Контроллер представления базовых аудиторий palomars каналов.
 *
 * @property srvChannels                    [ServiceChannels]                   сервис управления данными таблицы ["channels"].
 * @property srvAudiences                   [ServiceAudiences]                  сервис управления данными таблицы ["audiences"].
 * @property srvPalomarsChannels            [ServicePalomarsChannels]           сервис управления данными таблицы ["palomars_channels"].
 * @property srvSellingDirections           [ServiceSellingDirections]          сервис управления данными таблицы ["selling_directions"].
 * @property srvPalomarsChannelsXBaseAudiences    [ServicePalomarsChannelsXBaseAudiences]   сервис управления данными таблицы ["palomars_channels_x_audiences"].
 */
@RestController
@RequestMapping(value = ["channels"])
class ControllerViewChannels {

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvAudiences: ServiceAudiences

    @Autowired
    private lateinit var srvPalomarsChannels: ServicePalomarsChannels

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    @Autowired
    private lateinit var srvPalomarsChannelsXBaseAudiences: ServicePalomarsChannelsXBaseAudiences


    /**
     * Метод запроса коллекции всех известных направлений продаж.
     *
     * @return [List] коллекция [ResponseSellingDirection] направлений продаж.
     */
    @GetMapping(value = ["get-selling-directions"])
    fun getSellingDirections(): ResponseEntity<List<ResponseSellingDirection>> {
        // Направления продаж
        val sellingDirections = srvSellingDirections
            // Поиск всех направлений в БД
            .findAll()
            // Конвертация в модель ответа
            .map { ResponseSellingDirection(it) }

        // Отправляем клиенту
        return ResponseEntity.ok(sellingDirections)
    }

    /**
     * Метод запроса Palomars каналов по направлению продаж.
     *
     * @param id    [Byte] идентификатор направления продаж.
     *
     * @return      [List] коллекция [ResponsePalomarsChannel] Palomars каналов.
     */
    @GetMapping(value = ["get-palomars-channels-by-selling-direction"])
    fun getChannelsBySellingDirection(@RequestParam id: Byte): ResponseEntity<List<ResponsePalomarsChannel>> {
        // Каналы
        val channels = srvChannels
            // Поиск каналов по направлению продаж в БД
            .findAllBySellingDirectionID(id)
            // Отбрасываем каналы, которые не связаны с Palomars каналами
            .filter { it.palomarsChannel != null }
            // Конвертация в модель ответа
            .map { ResponsePalomarsChannel(it.palomarsChannel!!) }

        // Отправляем клиенту
        return ResponseEntity.ok(channels)
    }

    /**
     * Метод запроса всех аудиторий.
     *
     * @return [Iterable] коллекция [Audience] аудиторий.
     */
    @GetMapping(value = ["get-audiences"])
    fun getAudiences(): ResponseEntity<Iterable<Audience>> = ResponseEntity.ok(srvAudiences.findAll())

    /**
     * Метод запроса всех сопоставлений.
     *
     * @return [Iterable] коллекция [PalomarsChannelXBaseAudience] сопоставлений каналов Palomars с базовыми аудиториями.
     */
    @GetMapping(value = ["get-palomars-channels-x-audiences"])
    fun getPalomarsChannelsXAudiences(): ResponseEntity<Iterable<PalomarsChannelXBaseAudience>> =
        ResponseEntity.ok(srvPalomarsChannelsXBaseAudiences.findAll())

    /**
     * Метод запроса сохранения сопоставления.
     *
     * @param request   [RequestPalomarsChannelXAudience]   модель запроса сопоставления.
     *
     * @return          [ResponseEntity]                    ответ [String] в строковом представлении.
     */
    @PostMapping(value = ["save-palomars-channel-x-audience"])
    fun savePalomarsChannelXAudience(@RequestBody request: RequestPalomarsChannelXAudience): ResponseEntity<String> {
        if (srvPalomarsChannelsXBaseAudiences.findOne(
                PalomarsChannelXBaseAudience.PrimaryKey(
                    request.palomarsChannelID,
                    request.audienceID,
                    request.date.toLocalDate()
                )
            ) != null
        ) {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Данная связь уже существует.")
        }

        val palomarsChannel = srvPalomarsChannels.findByID(request.palomarsChannelID)
            ?: return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body("Не удалось найти канал palomars с ID: ${request.palomarsChannelID}.")

        val audience = srvAudiences.findByID(request.audienceID)
            ?: return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body("Не удалось найти аудиторию с ID: ${request.palomarsChannelID}.")

        srvPalomarsChannelsXBaseAudiences.save(
            PalomarsChannelXBaseAudience(
                palomarsChannel,
                audience,
                request.date.toLocalDate()
            )
        )
        return ResponseEntity.status(HttpStatus.ACCEPTED).build()
    }

    @PostMapping(value = ["delete"])
    fun deletePalomarsChannelsXAudience(@RequestBody request: RequestPalomarsChannelXAudience): ResponseEntity<String> {
        val foundPalomarsChannelXAudience = srvPalomarsChannelsXBaseAudiences.findOne(
            PalomarsChannelXBaseAudience.PrimaryKey(
                request.palomarsChannelID,
                request.audienceID,
                request.date.toLocalDate()
            )
        )
        if (foundPalomarsChannelXAudience != null) {
            srvPalomarsChannelsXBaseAudiences.deleteByID(foundPalomarsChannelXAudience.primaryKey)
            return ResponseEntity.status(HttpStatus.ACCEPTED).build()
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Данное сопоставление не найдено в базе данных.")
    }
}