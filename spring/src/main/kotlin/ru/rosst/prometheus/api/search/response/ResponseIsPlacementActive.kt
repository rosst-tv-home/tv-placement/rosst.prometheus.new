package ru.rosst.prometheus.api.search.response

/**
 * Модель признак "автоматическое размещение - включено"
 *
 * @param id [Int] идентификатор медиаплана.
 * @param isPlacementActive [Boolean] признак "автоматического размещение".
 */
class ResponseIsPlacementActive(
    val id: Int,
    val isPlacementActive: Boolean
)