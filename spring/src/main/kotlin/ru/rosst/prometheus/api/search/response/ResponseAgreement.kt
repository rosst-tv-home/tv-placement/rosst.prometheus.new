package ru.rosst.prometheus.api.search.response

import ru.rosst.prometheus.entity.Agreement

/**
 * Модель сделки.
 *
 * @param agreement [Agreement] сделка.
 *
 * @property id [Int] идентификатор сделки.
 * @property name [String] название сделки.
 * @property advertiser [ResponseAdvertiser] рекламодатель сделки.
 * @property sellingDirection [ResponseSellingDirection] направление продаж сделки.
 */
class ResponseAgreement(agreement: Agreement) {
    val id: Int = agreement.id
    val name: String = agreement.name
    val advertiser: ResponseAdvertiser = ResponseAdvertiser(agreement.mapping.advertiser)
    val sellingDirection: ResponseSellingDirection = ResponseSellingDirection(agreement.mapping.sellingDirection)
}