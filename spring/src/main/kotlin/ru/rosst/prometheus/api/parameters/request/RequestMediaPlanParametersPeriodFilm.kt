package ru.rosst.prometheus.api.parameters.request

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDate

/**
 * Модель ролика параметров автоматического размещения медиаплана.
 *
 * @param id [Int] идентификатор.
 * @param linkID [Int] идентификатор ролика.
 * @param inventoryPercent [Double] процент инвентаря ролика от инвентаря периода.
 */
class RequestMediaPlanParametersPeriodFilm(
    @JsonProperty(value = "id")
    val id: Int = 0,
    @JsonProperty(value = "linkID")
    val linkID: Int,
    @JsonProperty(value = "inventoryPercent")
    val inventoryPercent: Double = 0.0,
    @JsonProperty(value = "dateFrom")
    val dateFrom: LocalDate,
    @JsonProperty(value = "dateBefore")
    val dateBefore: LocalDate
)