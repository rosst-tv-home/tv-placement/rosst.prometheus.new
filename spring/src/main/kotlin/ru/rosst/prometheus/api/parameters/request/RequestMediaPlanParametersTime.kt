package ru.rosst.prometheus.api.parameters.request

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.ZonedDateTime

/**
 * Модель времменного периода параметров автоматической расстановки медиаплана.
 *
 * @param id            [Int]           идентификатор.
 * @param timeFrom      [ZonedDateTime] время начала временного периода.
 * @param timeBefore    [ZonedDateTime] время окончания временного периода.
 * @param isWeekend     [Boolean]       признак "выходной день".
 */
class RequestMediaPlanParametersTime(
    @JsonProperty(value = "id")
    val id: Int = 0,
    @JsonProperty(value = "timeFrom")
    val timeFrom: ZonedDateTime,
    @JsonProperty(value = "timeBefore")
    val timeBefore: ZonedDateTime,
    @JsonProperty(value = "isWeekend")
    val isWeekend: Boolean
)