package ru.rosst.prometheus.api.tvr

import entity.EnumDayType
import entity.Region
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import ru.rosst.extensions.kotlin.parallelForEach
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.prometheus.entity.Audience
import ru.rosst.prometheus.entity.PalomarsChannelRating
import ru.rosst.prometheus.enumerated.PalomarsTimeDayType
import ru.rosst.prometheus.service.ServicePalomarsChannels
import ru.rosst.prometheus.service.ServicePalomarsChannelsRatings
import service.ReaderTVR
import java.time.LocalDate
import javax.transaction.Transactional

/**
 * Контроллер загрузки TVR выгрузки ["Excel file"] из Palomars.
 *
 * Сервисы управления данными таблиц базы данных:
 * @property srvPalomarsChannels    [ServicePalomarsChannels]   сервис управления данными таблицы ["palomars_channels"].
 * @property srvPalomarsChannelsRatings    [ServicePalomarsChannelsRatings]  сервис управление данными таблицы ["palomars_audiences"].
 */
@RestController
@RequestMapping(value = ["upload-tvr"])
class TVRUploadController {
    @Autowired
    private lateinit var srvPalomarsChannels: ServicePalomarsChannels

    @Autowired
    private lateinit var srvPalomarsChannelsRatings: ServicePalomarsChannelsRatings

    /**
     * Запрос загрузки TVR выгрузки ["Excel file"] из Palomars.
     *
     * @param file [MultipartFile] файл.
     */
    @PostMapping
    @Transactional
    fun upload(@RequestParam("file") file: MultipartFile) {
        // Коллекция регионов
        val regions = ReaderTVR.read(inputStream = file.inputStream)

        // Конвертируем модели в модели базы данных
        val audiences = convert(regions)

        // Пара коллекций каналов и месяцев.
        //        val pairChannelsMonths = getUnique(audiences)

        // Поиск дубликатов в базе данных
        //        val duplicates = srvPalomarsAudiences.findDuplicates(
        //            pairChannelsMonths.second,
        //            pairChannelsMonths.first
        //        )

        // Если дубликаты найдены - удалем из базы данных
        //        if (duplicates.count() >= 1) srvPalomarsAudiences.deleteMonths(duplicates)

        // Сохраняем данные в базу данных
        srvPalomarsChannelsRatings.save(audiences)
    }

    /**
     * Метод конвертации выгрузи TVR в xlsx ["Excel"] формате, из Palomars в модель базы данных.
     *
     * @param regions   [Iterable] коллекция [Region] регионов.
     *
     * @return          [Iterable] коллекция [PalomarsChannelRating] конвертированных TVR.
     */
    fun convert(regions: Iterable<Region>): Iterable<PalomarsChannelRating> = runBlocking {
        val ratings: MutableList<PalomarsChannelRating> = mutableListOf()
        val channelNames = regions.flatMap { it.channels }.parallelMap { it.name }.toSet()
        val channels = srvPalomarsChannels.findByNames(channelNames)
        if (channels.count() == 0) throw NoSuchElementException("Каналы Palomars не найдены в БД")

        regions.parallelForEach { excelRegion ->
            excelRegion.channels.parallelForEach channelForEach@{ excelChannel ->
                // Канал, который нашли в базе данных
                val palomarsChannel = channels.firstOrNull {
                    it.name == excelChannel.name
                } ?: return@channelForEach

                excelChannel.months.parallelForEach { excelMonth ->
                    excelMonth.dayTypes.parallelForEach { excelType ->
                        excelType.times.parallelForEach { excelTime ->
                            excelTime.audiencesRatings.parallelForEach { excelAudience ->
                                ratings.add(
                                    PalomarsChannelRating(
                                        palomarsChannel,
                                        Audience(excelAudience.name),
                                        LocalDate.of(excelMonth.date.year, excelMonth.date.month, 1),
                                        excelTime.timeFrom, excelTime.timeBefore,
                                        when (excelType.type) {
                                            EnumDayType.DAY_OFF -> PalomarsTimeDayType.DAY_OFF
                                            EnumDayType.WORKDAY -> PalomarsTimeDayType.WORKDAY
                                            EnumDayType.HOLIDAY -> PalomarsTimeDayType.HOLIDAY
                                        }, excelAudience.rating
                                    )
                                )
                            }
                        }
                    }
                }
            }
        }


        return@runBlocking ratings
    }
}