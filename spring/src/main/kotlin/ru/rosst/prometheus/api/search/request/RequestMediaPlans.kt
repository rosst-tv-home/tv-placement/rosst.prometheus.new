package ru.rosst.prometheus.api.search.request

import java.time.ZonedDateTime

/**
 * Модель запроса коллекции медиапланов по параметрам.
 *
 * @param sellingDirection [Byte] идентификатор направления продаж.
 * @param dateFrom [ZonedDateTime] дата начала периода медиапланов.
 * @param dateBefore [ZonedDateTime] дата окончания периода медиапланов.
 * @param channels [List] коллекция [Int] идентификаторов каналов медиапланов.
 * @param advertisers [List] коллекция [Int] идентификаторов рекламодателей медиапланов.
 */
class RequestMediaPlans(
    val sellingDirection: Byte,
    val dateFrom: ZonedDateTime,
    val dateBefore: ZonedDateTime,
    val channels: List<Int>,
    val advertisers: List<Int>
)