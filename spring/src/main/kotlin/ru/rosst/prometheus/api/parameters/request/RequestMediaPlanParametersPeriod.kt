package ru.rosst.prometheus.api.parameters.request

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.ZonedDateTime

/**
 * Модель периода параметров автоматического размещения медиаплана.
 *
 * @param id [Int] идентификатор периода.
 * @param fromDate [ZonedDateTime] дата начала периода (включительно).
 * @param beforeDate [ZonedDateTime] дата окончания периода (включительно).
 * @param inventory [Double] инвентарь периода.
 * @param percentPrime [Double] процент prime в периоде от инвентаря периода.
 * @param films [List] коллекция [RequestMediaPlanParametersPeriodFilm] роликов периода.
 */
class RequestMediaPlanParametersPeriod(
    @JsonProperty(value = "id", defaultValue = "0")
    val id: Int = 0,
    @JsonProperty(value = "fromDate", defaultValue = "null")
    val fromDate: ZonedDateTime? = null,
    @JsonProperty(value = "beforeDate", defaultValue = "null")
    val beforeDate: ZonedDateTime? = null,
    @JsonProperty(value = "inventory", defaultValue = "0.0")
    val inventory: Double = 0.0,
    @JsonProperty(value = "percentPrime", defaultValue = "0.0")
    val percentPrime: Double = 0.0,
    @JsonProperty(value = "films")
    val films: List<RequestMediaPlanParametersPeriodFilm> = listOf()
)