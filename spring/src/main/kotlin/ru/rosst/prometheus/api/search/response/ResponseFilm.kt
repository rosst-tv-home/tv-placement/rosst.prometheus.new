package ru.rosst.prometheus.api.search.response

import ru.rosst.prometheus.entity.FilmXMediaPlan
import java.time.LocalDate

/**
 * Модель ролика.
 *
 * @param link [FilmXMediaPlan] связь ролика и медиаплана.
 *
 * @property id [Int] идентификатор ролика.
 * @property linkID [Int] идентификатор связи ролика с медиапланом.
 * @property name [String] название ролика.
 * @property versionName [String] название версии ролика.
 * @property duration [Double] длительность ролика.
 * @property isInLaw [Boolean] признак ролика "соответсвует закону о рекламе".
 */
class ResponseFilm(link: FilmXMediaPlan) {
    val id: Int = link.film.id
    val linkID: Int = link.id
    val name: String = link.film.name
    val versionName: String = link.film.versionName
    val duration: Byte = link.film.duration
    val isInLaw: Boolean = link.film.isInLaw
    val dateFrom: LocalDate = link.dateFrom
    val dateBefore: LocalDate = link.dateBefore
}