package ru.rosst.prometheus.api.parameters.request

/**
 * Алгоритм автоматического размещения.
 *
 * @property AFFINITIES          по высокорейтинговым рекламным блокам.
 * @property EQUAL_DISTRIBUTION  равномерное распределение.
 */
enum class EnumPlacementChoice {
    AFFINITIES,
    EQUAL_DISTRIBUTION
}