package ru.rosst.prometheus.api.parameters.request

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.EnumType
import javax.persistence.Enumerated


/**
 * Модель параметров автоматической расстановки медиаплана.
 *
 * @param id                            [Int]       идентификатор.
 * @param mediaPlanID                   [Int]       идентификатор медиаплана.
 * @param periods                       [List]      коллекция [RequestMediaPlanParametersPeriod] периодов.
 * @param times                         [List]      коллекция [RequestMediaPlanParametersTime] временных периодов.
 * @param totalInventoryPercent         [Double]    процент планового инвентаря.
 * @param isPlacementActive             [Boolean]   признак "автоматическое размещение".
 * @param isAssignedRatingsPlacement    [Boolean]   признак "размещение по втянутым рейтингам".
 */
class RequestMediaPlanParameters(
    @JsonProperty(value = "id")
    val id: Int = 0,
    @JsonProperty(value = "mediaPlanID")
    val mediaPlanID: Int = 0,
    @JsonProperty(value = "periods")
    val periods: List<RequestMediaPlanParametersPeriod> = listOf(),
    @JsonProperty(value = "times")
    val times: List<RequestMediaPlanParametersTime> = listOf(),
    @JsonProperty(value = "placement")
    @Enumerated(EnumType.STRING)
    var placement: EnumPlacementChoice? = null,
    @JsonProperty(value = "audience")
    var audience: RequestTvrDate?,
    @JsonProperty(value = "percentInventoryNeed")
    var totalInventoryPercent: Double,
    @JsonProperty(value = "isPlacementActive")
    var isPlacementActive: Boolean,
    @JsonProperty(value = "isAssignedRatingsPlacement")
    var isAssignedRatingsPlacement: Boolean
)