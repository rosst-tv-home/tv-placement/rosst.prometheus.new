package ru.rosst.prometheus.api.search

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import ru.rosst.prometheus.api.search.request.RequestMediaPlans
import ru.rosst.prometheus.api.search.response.ResponseAdvertiser
import ru.rosst.prometheus.api.search.response.ResponseChannel
import ru.rosst.prometheus.api.search.response.ResponseMediaPlan
import ru.rosst.prometheus.api.search.response.ResponseSellingDirection
import ru.rosst.prometheus.entity.ChannelLight
import ru.rosst.prometheus.service.*
import ru.rosst.prometheus.service.vimb.VimbServiceMediaPlans
import javax.transaction.Transactional

/**
 * Контроллер представления поиска медиапланов.
 *
 * @property srvChannels [ServiceChannels] сервис управления каналами в базе данных.
 * @property srvMediaPlans [ServiceMediaPlans] сервис управления медиапланами в базе данных.
 * @property srvAdvertisers [ServiceAdvertisers] сервис управления рекламодателями в базе данных.
 * @property srvSellingDirections [ServiceSellingDirections] сервис управления направлениями продаж в базе данных.
 * @property vimbSrvMediaPlans [VimbServiceMediaPlans] сервис загрузки медиапланов из VIMB.
 */
@RestController
@RequestMapping(value = ["media-plan-search-view"])
class MediaPlanSearchViewController {
    @Autowired
    private lateinit var srvSpots: ServiceSpots

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvMediaPlans: ServiceMediaPlans

    @Autowired
    private lateinit var srvAdvertisers: ServiceAdvertisers

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    @Autowired
    private lateinit var srvAutoPlacementParameters: ServiceAutoPlacementParameters

    @Autowired
    private lateinit var vimbSrvMediaPlans: VimbServiceMediaPlans

    /**
     * Метод запроса коллекции всех известных направлений продаж.
     *
     * @return [List] коллекция [ResponseSellingDirection] направлений продаж.
     */
    @GetMapping(value = ["get-selling-directions"])
    fun getSellingDirections(): ResponseEntity<List<ResponseSellingDirection>> {
        // Направления продаж
        val sellingDirections = srvSellingDirections
            // Поиск всех направлений в БД
            .findAll()
            // Конвертация в модель ответа
            .map { ResponseSellingDirection(it) }

        // Отправляем клиенту
        return ResponseEntity.ok(sellingDirections)
    }

    /**
     * Метод запроса наличия параметров автоматического размещение по идентификатора медиа плана.
     *
     * @param id [Int] индетификатор медиа плана.
     *
     * @return [Boolean] признак "параметры автоматического размещение - есть ".
     */
    @GetMapping(value = ["get-parameters-mediaPlans"])
    fun getParametersMediaPlans(@RequestParam id: Int): ResponseEntity<Boolean> =
        ResponseEntity.ok(srvAutoPlacementParameters.isExists(id))

    /**
     * Метод запроса каналов по направлению продаж.
     *
     * @param id [Byte] идентификатор направления продаж.
     *
     * @return [List] коллекция [ResponseChannel] каналов.
     */
    @GetMapping(value = ["get-channels-by-selling-direction"])
    fun getChannelsBySellingDirection(@RequestParam id: Byte): ResponseEntity<List<ChannelLight>> =
        ResponseEntity.ok(srvChannels.findAllBySellingDirectionIDLight(id))

    /**
     * Метод запроса рекламодателей по направлению продаж.
     *
     * @param id [Byte] идентификатор направления продаж.
     *
     * @return [List] коллекция [ResponseAdvertiser] рекламодателей.
     */
    @GetMapping(value = ["get-advertisers-by-selling-direction"])
    fun getAdvertisersBySellingDirectionID(@RequestParam id: Byte): ResponseEntity<List<ResponseAdvertiser>> {
        // Рекламодателя
        val advertisers = srvAdvertisers
            // Поиск рекламодателей по направлению продаж
            .findAllBySellingDirectionID(id)
            // Конвертация в модель ответа
            .map { ResponseAdvertiser(it.advertiser) }

        // Отправляем клиенту
        return ResponseEntity.ok(advertisers)
    }

    /**
     * Метод запроса медиапланов по указанным параметрам.
     *
     * @param request [RequestMediaPlans] модель запроса медиапланов по указанным параметрам.
     *
     * @return [List] коллекция [ResponseMediaPlan] медиапланов.
     */
    @PostMapping(value = ["get-media-plans"])
    @Transactional
    fun getMediaPlans(@RequestBody request: RequestMediaPlans): ResponseEntity<List<ResponseMediaPlan>> {
        // Медиапланы
        val mediaPlans = srvMediaPlans
            // Поиск медиапланов по параметрам
            .findAllBySellingDirectionChannelsAdvertisersAndPeriod(
                request.sellingDirection,
                request.dateFrom.toLocalDate(),
                request.dateBefore.toLocalDate(),
                request.channels,
                request.advertisers
            )
            // Конвертация в модель ответа
            .map { ResponseMediaPlan(it) }

        // Отправляем клиенту
        return ResponseEntity.ok(mediaPlans)
    }

    /**
     * Метод запроса загрузки и обновления коллекции медиапланов по указанным параметрам.
     *
     * @param request [RequestMediaPlans] модель запроса медиапланов по заданным параметрам.
     *
     * @return коллекция медиапланов по указаным параметрам.
     */
    @PostMapping(value = ["download-media-plans"])
    @Transactional
    fun downloadMediaPlans(@RequestBody request: RequestMediaPlans): ResponseEntity<List<ResponseMediaPlan>> {
        // Обновляем данные по медиапланам, соответсвующим заданным параметрам
        vimbSrvMediaPlans.download(
            request.sellingDirection,
            request.dateFrom.toLocalDate(),
            request.dateBefore.toLocalDate(),
            request.channels.toSet(),
            request.advertisers.toSet()
        )

        // Медиапланы
        val mediaPlans = srvMediaPlans
            // Поиск медиапланов по параметрам
            .findAllBySellingDirectionChannelsAdvertisersAndPeriod(
                request.sellingDirection,
                request.dateFrom.toLocalDate(),
                request.dateBefore.toLocalDate(),
                request.channels,
                request.advertisers
            )
            // Конвертация в модель ответа
            .map { ResponseMediaPlan(it) }

        // Отправляем клиенту
        return ResponseEntity.ok(mediaPlans)
    }

    /**
     * Метод запроса инвентаря медиаплана.
     *
     * @param id [Int] идентификатор медиаплана.
     *
     * @return инвентарь медиаплана.
     */
    @Transactional
    @GetMapping(value = ["get-media-plan-current-inventory"])
    fun getMediaPlanInventory(@RequestParam id: Int): Double {
        val requiredInventory = srvAutoPlacementParameters.requiredInventory(id)

        val requiredInventoryPercent = srvAutoPlacementParameters.requiredInventoryPercent(id)
        val spotsLight = srvSpots.spotsLight(id)

        return spotsLight.sumOf { spot ->
            if (spot.isMinute) return@sumOf spot.duration.toDouble()
            (if (spot.isAssignedRatingsPlacement) spot.rating else spot.ratingForecast) * spot.duration / 20
        } / (requiredInventory * requiredInventoryPercent)
    }
}
