package ru.rosst.prometheus.api.search.response

import ru.rosst.prometheus.entity.MediaPlan
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime

/**
 * Модель медиаплана.
 *
 * @param mediaPlan [MediaPlan] медиаплан.
 *
 * @property id [Int] идентификатор медиаплана.
 * @property name [String] название медиаплана.
 * @property month [ZonedDateTime] месяц медиаплана.
 * @property order [ResponseOrder] заказ медиаплана.
 * @property films [List] коллекция [ResponseFilm] роликов медиаплана.
 * @property channel [ResponseChannel] канал медиаплана.
 */
class ResponseMediaPlan(mediaPlan: MediaPlan) {
    val id: Int = mediaPlan.id
    val name: String = mediaPlan.name
    val month: ZonedDateTime = ZonedDateTime.of(mediaPlan.date, LocalTime.now(), ZoneId.systemDefault())
    val order: ResponseOrder = ResponseOrder(mediaPlan.order)
    val films: List<ResponseFilm> = mediaPlan.mapping.map { ResponseFilm(it) }
    val channel: ResponseChannel = ResponseChannel(mediaPlan.channel)
    var difference: Double? = null
}