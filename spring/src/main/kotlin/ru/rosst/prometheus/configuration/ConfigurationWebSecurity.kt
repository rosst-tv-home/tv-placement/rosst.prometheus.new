package ru.rosst.prometheus.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import ru.rosst.prometheus.configuration.filters.JWTRequestFilter
import ru.rosst.prometheus.service.spring.ServiceUserDetails

@EnableWebSecurity
class ConfigurationWebSecurity : WebSecurityConfigurerAdapter() {
    @Autowired
    private lateinit var srvUserDetails: ServiceUserDetails

    @Autowired
    private lateinit var configAuthenticationEntryPoint: ConfigurationAuthenticationEntryPoint

    @Bean
    fun requestFilter(): JWTRequestFilter = JWTRequestFilter()

    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder = BCryptPasswordEncoder()

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    override fun configure(authenticationManagerBuilder: AuthenticationManagerBuilder) {
        authenticationManagerBuilder
            .userDetailsService(srvUserDetails)
            .passwordEncoder(passwordEncoder())
    }

    override fun configure(http: HttpSecurity) {
        http
            .httpBasic().disable()
            .csrf().disable()
            .cors().and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/api/sign-in", "/api/sign-up").permitAll()
            .antMatchers("/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(configAuthenticationEntryPoint)

        http.addFilterBefore(requestFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }
}