package ru.rosst.prometheus.configuration.filters

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.web.filter.OncePerRequestFilter
import ru.rosst.prometheus.service.spring.ServiceJWT
import ru.rosst.prometheus.service.spring.ServiceUserDetails
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * REST фильтр - срабатывает перед каждым запросом и проверяет наличие JSON WEB TOKEN.
 *
 * @property srvJWT         [ServiceJWT]            сервис взаимодействия с токенами.
 * @property srvUserDetails [ServiceUserDetails]    сервис управления пользовательскими данными.
 */
class JWTRequestFilter : OncePerRequestFilter() {
    @Autowired
    private lateinit var srvJWT: ServiceJWT

    @Autowired
    private lateinit var srvUserDetails: ServiceUserDetails

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        try {
            // Извлекаем токен из заголовка
            val token = getJsonWebToken(request)

            if (token != null) {
                val login = srvJWT.getLogin(token)
                val userDetails = srvUserDetails.loadUserByUsername(login)
                val userAuthentication = UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.authorities
                ).apply {
                    details = WebAuthenticationDetailsSource().buildDetails(request)
                }
                SecurityContextHolder.getContext().authentication = userAuthentication
            }
        } catch (exception: Exception) {
            println(exception.localizedMessage)
        }

        // Переход к следующему фильтру
        filterChain.doFilter(request, response)
    }

    /**
     * Метод извлечения JWT из заголовка запроса.
     *
     * @param request   [HttpServletRequest]    запрос.
     *
     * @return          [String]                JWT.
     */
    private fun getJsonWebToken(request: HttpServletRequest): String? {
        // Получаем значение заголовка авторизации
        val header = request.getHeader("Authorization")
        // Извлекаем JWT из заголовка
        return if (header.startsWith("Bearer ")) header.replace("Bearer ", "") else null
    }
}