package ru.rosst.prometheus.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry
import ru.rosst.prometheus.ws.log.WSController

@Configuration
@EnableWebSocket
class ConfigurationWebSocket : WebSocketConfigurer {
    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(WSController(), "/log").setAllowedOrigins("*").withSockJS()
    }
}