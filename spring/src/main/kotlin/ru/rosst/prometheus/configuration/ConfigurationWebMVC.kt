package ru.rosst.prometheus.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class ConfigurationWebMVC : WebMvcConfigurer {
    override fun addCorsMappings(registry: CorsRegistry) {
        registry
            .addMapping("/**")
            .allowedOrigins(
                "http://localhost:80",
                "http://localhost:8080",
                "http://192.168.0.151:80",
                "http://192.168.0.151:8080",
                "http://192.168.0.182:80",
                "http://192.168.0.182:8080",
                "http://192.168.0.14:81",
                "http://192.168.0.14:80",
                "http://192.168.0.14:8080",
                "http://palomars:81",
                "http://palomras:80",
                "http://palomars:8080"
            )
            .allowedMethods("GET", "POST")
    }
}