package ru.rosst.prometheus.ws.log

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.web.socket.*
import org.springframework.web.util.HtmlUtils
import ru.rosst.extensions.spring.AbstractService


class WSController : AbstractService(), WebSocketHandler, SubProtocolCapable {
    private val sessions: MutableSet<WebSocketSession> = mutableSetOf()

    /**
     * Метод обработки события подключения клиента к серверу WebSocket
     */
    override fun afterConnectionEstablished(session: WebSocketSession) {
        sessions.add(session)
        session.sendMessage(
            TextMessage(
                jacksonObjectMapper()
                    .registerModule(JavaTimeModule())
                    .writeValueAsString(ResponseLog("ololo log!"))
            )
        )
    }

    /**
     * Метод обработки события отключения клиента от сервера WebSocket
     */
    override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
        sessions.remove(session)
    }

    /**
     * Метод обработки события ошибки при передаче сообщения клиенту WebSocket
     */
    override fun handleTransportError(session: WebSocketSession, exception: Throwable) {
        error("Ошибка передачи сообщения WebSocket: ${exception.message}")
    }

    /**
     * Метод обработки события сообщения от клиента WebSocket
     */
    override fun handleMessage(session: WebSocketSession, message: WebSocketMessage<*>) {
        when (message) {
            is TextMessage -> handleTextMessage(session, message)
            is BinaryMessage -> {
                // handleBinaryMessage(session, message)
            }
            is PongMessage -> {
                // handlePongMessage(session, message)
            }
            else -> throw IllegalStateException("Неизвестный тип сообщения: $message")
        }
    }

    /**
     * Метод обработки текстовых сообщений от клиента WebSocket
     */
    fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        val request = message.payload
        println("Server received: $request")
        val response = String.format("response from server to '%s'", HtmlUtils.htmlEscape(request))
        println("Server sends: $response")
        session.sendMessage(TextMessage(response))
    }

    /**
     * Метод возвращает коллекцию поддерживаемых протоколов WebSocket
     */
    override fun getSubProtocols(): MutableList<String> = mutableListOf("prometheus")

    /**
     * Метод возвращает признак поддержки сообщений по частям
     */
    override fun supportsPartialMessages(): Boolean = false
}