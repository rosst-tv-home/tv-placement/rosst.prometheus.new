package ru.rosst.prometheus.ws.log

import java.time.LocalDateTime

class ResponseLog(
    val message: String
) {
    val time: LocalDateTime = LocalDateTime.now()
}