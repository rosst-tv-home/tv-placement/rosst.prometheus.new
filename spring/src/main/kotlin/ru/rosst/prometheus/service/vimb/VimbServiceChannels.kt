package ru.rosst.prometheus.service.vimb

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Channel
import ru.rosst.prometheus.entity.Region
import ru.rosst.prometheus.entity.SellingDirection
import ru.rosst.prometheus.models.channels.request.RequestChannels
import ru.rosst.prometheus.models.channels.response.ResponseChannel
import ru.rosst.prometheus.models.channels.response.ResponseChannels
import ru.rosst.prometheus.service.ServiceChannels
import ru.rosst.prometheus.service.ServiceSellingDirections

/**
 * Компонент загрузки каналов из VIMB API.
 *
 * Сервисы взаимодействия с VIMB API:
 * @property vimbSrvRequest         [VimbServiceChannels]       сервис отправки запросов к VIMB API.
 *
 * Сервисы управления данными в базе данных:
 * @property srvChannels            [ServiceChannels]           сервис управления каналами.
 * @property srvSellingDirections   [ServiceSellingDirections]  сервис управления направлениями продаж.
 */
@Component
class VimbServiceChannels : AbstractService() {
    @Autowired
    private lateinit var vimbSrvRequest: VimbServiceRequest

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    /**
     * Метод загрузки каналов из VIMB API.
     *
     * @param sellingDirectionID [Byte] идентификатор направления продаж.
     */
    fun download(sellingDirectionID: Byte) {
        info("Начало: загрузка каналов по направлению продаж ID: $sellingDirectionID.")

        // Проверяем, есть ли указанное направление продаж в базе данных
        val sellingDirection = srvSellingDirections.findById(sellingDirectionID)

        // Отправляем запрос
        val response = vimbSrvRequest.request(
            RequestChannels(sellingDirectionID),
            true
        ) as? ResponseChannels ?: return info(
            "Завершение: загрузка каналов по направлению продаж ID: $sellingDirectionID."
        )

        // Фильтруем каналы
        val filteredChannels = filter(response.channels)
        // Конвертируем каналы
        val convertedChannels = convert(filteredChannels, sellingDirection)

        // Сохраняем каналы в базу данных
        srvChannels.save(convertedChannels)

        info("Завершение: загрузка каналов по направлению продаж ID: $sellingDirectionID.")
    }

    /**
     * Метод фильтрации каналов, полученных из vimb api.
     * Удаляются каналы, название или название региона которых пустое.
     *
     * P.S. Было как-то что region_id он же bcpID или base_channel_placement_id мог прийти null,
     * хотя в докуентации этот тег вообще отсутсвует...
     *
     * @param channels         [Iterable] коллекция [ResponseChannel] каналов.
     *
     * @return отфильтрованная [Iterable] коллекция [ResponseChannel] каналов.
     */
    private fun filter(channels: Iterable<ResponseChannel>): Iterable<ResponseChannel> {
        info("Начало: фильтрация каналов.")
        info("Параметры: каналов до фильтрации: ${channels.count()} шт.")
        val filteredChannels = channels.filter { !it.name.isBlank() && !it.regionName.isBlank() }
        info("Параметры: каналов после фильтрации: ${filteredChannels.count()} шт.")
        info("Завершение: фильтрация каналов.")
        return filteredChannels
    }

    /**
     * Метод конвертации каналов в модели базы данных.
     *
     * @param channels          [Iterable]          коллекция [ResponseChannel] каналов.
     * @param sellingDirection  [SellingDirection]  направление продаж.
     *
     * @return                  [Iterable]          коллекция [Channel] каналов.
     */
    private fun convert(channels: Iterable<ResponseChannel>, sellingDirection: SellingDirection): Iterable<Channel> {
        info("Начало: конвертация каналов.")
        info("Параметры: каналов до конвертации ${channels.count()} шт.")
        val convertedChannels = runBlocking {
            channels.parallelMap {
                Channel(it.id, it.name, Region(it.regionID, it.regionName, sellingDirection), it.isDisabled)
            }
        }
        info("Параметры: каналов после конверации ${convertedChannels.count()}")
        info("Завершение: конвертация каналов.")
        return convertedChannels
    }
}