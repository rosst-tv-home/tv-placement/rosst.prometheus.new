package ru.rosst.prometheus.service.spring

import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class ServiceJWT {
    @Value("\${jwt.secret}")
    private lateinit var secretJWTString: String

    private val logger = LoggerFactory.getLogger(javaClass)

    fun generate(login: String): String = Jwts
        .builder()
        .setSubject(login)
        .signWith(SignatureAlgorithm.HS512, secretJWTString)
        .compact()

    fun validate(token: String): Boolean = try {
        Jwts.parser().setSigningKey(secretJWTString).parseClaimsJws(token)
        true
    } catch (exception: Exception) {
        when (exception) {
            is SignatureException -> {
                error("Неверная сигнатура JWT: ${exception.localizedMessage}")
                false
            }
            is MalformedJwtException -> {
                error("Неверный JWT: ${exception.localizedMessage}")
                false
            }
            is ExpiredJwtException -> {
                error("Время действия JWT истекло: ${exception.localizedMessage}")
                false
            }
            is UnsupportedJwtException -> {
                error("Ноподдерживаемый тип JWT: ${exception.localizedMessage}")
                false
            }
            is IllegalArgumentException -> {
                error("JWT не указан или неверного типа: ${exception.localizedMessage}")
                false
            }
            else -> false
        }
    }

    fun getLogin(token: String): String = Jwts
        .parser()
        .setSigningKey(secretJWTString)
        .parseClaimsJws(token)
        .body
        .subject

    private fun error(string: String) = logger.error(string.trimIndent().replace('\n', ' '))
}