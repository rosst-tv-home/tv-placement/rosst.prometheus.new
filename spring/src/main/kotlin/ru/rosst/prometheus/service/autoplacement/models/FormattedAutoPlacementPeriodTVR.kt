package ru.rosst.prometheus.service.autoplacement.models

import kotlinx.coroutines.runBlocking
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.AutoPlacementTime
import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.entity.MediaPlan
import java.time.LocalDate
import kotlin.math.ceil

class FormattedAutoPlacementPeriodTVR(
    override val parameters: AutoPlacementParameter,
    override val dateFrom: LocalDate,
    override val dateBefore: LocalDate,
    override val mediaPlan: MediaPlan,
    override val times: Iterable<AutoPlacementTime>,
    override val films: Iterable<FormattedAutoPlacementFilm>,
    private val tvrRatings: Iterable<Pair<Double, Iterable<Block>>>
) : InterfaceFormattedAutoPlacementPeriod {
    override val blocks: Iterable<Block>
        get() = tvrRatings.flatMap { it.second }

    override val segments: List<Map<Int, Iterable<Block>>> = emptyList()
        get() {
            // Если свойсво не пустое - не пересчитываем значение
            if (field.count() != 0 || blocks.count() == 0) return field

            // Отсортированные блоки по вермени выхода
            val sortedBlocks = tvrRatings.sortedByDescending { it.first }.flatMap { it.second }

            // Суммарный инвентарь роликов
            val filmsInventory = films.sumOf { it.inventory }

            // Суммарный инвентарь сетки
            val blocksInventory = blocks.sumOf { block ->
                val drawnInRating = block.assignedRatings.firstOrNull { it.order.id == mediaPlan.order.id }
                drawnInRating?.rating ?: block.ratingForecast
            }

            // Количество сегментов
            val segmentCount = getSegmentCount(filmsInventory, blocksInventory, blocks.count())
            // Количество блоков в одном сегменте
            val blockCountInSegment = getBlockCountInSegment(filmsInventory, blocksInventory)

            return runBlocking {
                sortedBlocks
                    .chunked(if (segmentCount > 1) segmentCount else sortedBlocks.size)
                    .parallelMap {
                        // Сортируем рекламные блоки по убыванию прогнозного рейтинга
                        val blocksByDescendingRating = it.sortedByDescending { block ->
                            tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first
                        }
                        // Ищем медиану рейтинга сегмента
                        val segmentRatingMedian = getSegmentRatingMedian(blocksByDescendingRating)
                        // Ищем медиану рейтинга выше медианы рейтинга сегмента
                        val segmentRatingMedianUpper = getSegmentRatingMedian(blocksByDescendingRating.filter { block ->
                            tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first >= segmentRatingMedian.second &&
                            blocksByDescendingRating.indexOf(block) <= segmentRatingMedian.first
                        })
                        // Ищем медиану рейтинга ниже медианы рейтинга сегмента
                        val segmentRatingMedianLower = getSegmentRatingMedian(blocksByDescendingRating.filter { block ->
                            tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first <= segmentRatingMedian.second &&
                            blocksByDescendingRating.indexOf(block) >= segmentRatingMedian.first
                        })

                        // Квантили сегмента
                        mapOf(
                            0 to blocksByDescendingRating.filter { block ->
                                tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first >= segmentRatingMedianUpper.second &&
                                blocksByDescendingRating.indexOf(block) <= segmentRatingMedianUpper.first
                            }.sortedByDescending { it.weekDay },
                            1 to blocksByDescendingRating.filter { block ->
                                tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first in segmentRatingMedian.second..segmentRatingMedianUpper.second &&
                                blocksByDescendingRating.indexOf(block) in segmentRatingMedianUpper.first..segmentRatingMedian.first
                            }.sortedByDescending { it.weekDay },
                            2 to blocksByDescendingRating.filter { block ->
                                tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first in segmentRatingMedianLower.second..segmentRatingMedian.second &&
                                blocksByDescendingRating.indexOf(block) in segmentRatingMedian.first..segmentRatingMedianLower.first + segmentRatingMedian.first
                            }.sortedByDescending { it.weekDay },
                            3 to blocksByDescendingRating.filter { block ->
                                tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first <= segmentRatingMedianLower.second &&
                                blocksByDescendingRating.indexOf(block) >= segmentRatingMedianLower.first + segmentRatingMedian.first
                            }.sortedByDescending { it.weekDay }
                        )
                    }
            }
        }

    override fun getSegmentCount(filmsInventory: Double, blocksInventory: Double, blocksCount: Int): Int {
        return ceil(filmsInventory * blocksCount / blocksInventory).toInt()
    }

    override fun getBlockCountInSegment(filmsInventory: Double, blocksInventory: Double): Int {
        return (blocksInventory / filmsInventory).toInt()
    }

    /**
     * Метод поиска медианы рейтинга в коллекции блоков.
     *
     * @param blocks [List] коллекция  [Block] рекламных блоков.
     * @return [Pair] пара из [Int] и [Double], где первый аргумент - индекс медианы, второй - рейтинг.
     */
    private fun getSegmentRatingMedian(blocks: List<Block>) =
        if (blocks.size % 2 == 0) {
            val nextBlock = blocks[blocks.size / 2]
            val previousBlock = blocks[blocks.size / 2 - 1]

            val nextBlockRating = tvrRatings.first { tvrRating -> tvrRating.second.contains(nextBlock) }.first
            val previousBlockRating = tvrRatings.first { tvrRating -> tvrRating.second.contains(previousBlock) }.first

            (blocks.size / 2) to (previousBlockRating + nextBlockRating) / 2
        } else {
            val block = blocks[(blocks.size.toDouble() / 2 - 0.5).toInt()]

            val blockRating = tvrRatings.first { tvrRating -> tvrRating.second.contains(block) }.first

            (blocks.size / 2 - 0.5).toInt() to blockRating
        }
}