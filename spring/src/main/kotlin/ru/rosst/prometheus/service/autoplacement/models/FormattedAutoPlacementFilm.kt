package ru.rosst.prometheus.service.autoplacement.models

/**
 * Модель параметров ролика.
 *
 * @param id идентификатор ролика.
 * @param inventory плановый инвентарь ролика в периоде.
 * @param inventoryDraft черновой инвентарь ролика в периоде. Сколько ролик уже набрал инвентаря в черновике.
 */
class FormattedAutoPlacementFilm(
    val id: Int,
    val inventory: Double,
    var inventoryDraft: Double = 0.0
)