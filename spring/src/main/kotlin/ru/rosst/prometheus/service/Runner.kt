package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Service
import ru.rosst.prometheus.service.vimb.VimbServiceBlocks
import ru.rosst.prometheus.service.vimb.VimbServiceMediaPlans
import ru.rosst.prometheus.service.vimb.VimbServiceSpots
import java.time.LocalDate

@Service
class Runner : CommandLineRunner {
    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvBlocks: ServiceBlocks

    @Autowired
    private lateinit var vimbSrvBlocks: VimbServiceBlocks

    @Autowired
    private lateinit var srvVimbSpots: VimbServiceSpots

    @Autowired
    private lateinit var srvVimbMP: VimbServiceMediaPlans

    @Autowired
    private lateinit var srvParameters: ServiceAutoPlacementParameters

    override fun run(vararg args: String?) {
//        vimbSrvBlocks.download(
//            21,
//            LocalDate.of(2021, 1, 1),
//            LocalDate.of(2022, 12, 29),
//            setOf(1017603)
//        )
//        srvVimbMP.download(
//            21,
//            LocalDate.of(2021, 1, 1),
//            LocalDate.of(2022, 1, 1)
//        )
    }
}