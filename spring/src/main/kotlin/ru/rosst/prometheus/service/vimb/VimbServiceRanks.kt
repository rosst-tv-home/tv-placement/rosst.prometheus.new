package ru.rosst.prometheus.service.vimb

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Rank
import ru.rosst.prometheus.models.ranks.request.RequestRanks
import ru.rosst.prometheus.models.ranks.response.ResponseRank
import ru.rosst.prometheus.models.ranks.response.ResponseRanks
import ru.rosst.prometheus.service.ServiceRanks

/**
 * Компонент взаимодействия с рангами через VIMB API.
 *
 * Сервисы взаимодействия с VIMB API:
 * @property vimbSrvRequest [VimbServiceRequest]    сервис отправки запросов к VIMB API.
 *
 * Компоненты управления данными базы данных:
 * @property srvRanks       [ServiceRanks]          компонент управления данными таблицы ["ranks"] "ранги".
 */
@Component
class VimbServiceRanks : AbstractService() {
    @Autowired
    private lateinit var srvRanks: ServiceRanks

    @Autowired
    private lateinit var vimbSrvRequest: VimbServiceRequest

    /**
     * Метод запроса рангов из VIMB API.
     */
    fun getRanks() {
        // Отправляем запрос
        val response = vimbSrvRequest.request(
            RequestRanks(), true
        ) as? ResponseRanks ?: return

        // Конвертируем модели
        val ranks = convert(response.ranks)

        // Сохраняем ранги в базу данных
        srvRanks.saveAll(ranks)
    }

    /**
     * Метод конверации моделей рангов.
     *
     * @param ranks [Iterable] коллекция [ResponseRank] моделей рангов.
     *
     * @return      [Iterable] коллекция [Rank] моделей рангов.
     */
    private fun convert(ranks: Iterable<ResponseRank>): Iterable<Rank> = runBlocking {
        ranks.parallelMap {
            val type = Rank.RankType.values().find { type ->
                type.placementTypeName == it.name
            }
            return@parallelMap Rank(
                it.id,
                type!!
            )
        }
    }
}