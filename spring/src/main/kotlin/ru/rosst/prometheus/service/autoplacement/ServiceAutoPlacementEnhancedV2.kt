package ru.rosst.prometheus.service.autoplacement

import kotlinx.coroutines.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.AutoPlacementPeriod
import ru.rosst.prometheus.entity.AutoPlacementRequestDate
import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.enumerated.PalomarsTimeDayType
import ru.rosst.prometheus.service.ServiceAutoPlacementParameters
import ru.rosst.prometheus.service.ServiceBlocks
import ru.rosst.prometheus.service.autoplacement.enchanced.*
import ru.rosst.prometheus.service.vimb.VimbServiceBlocks
import ru.rosst.prometheus.service.vimb.VimbServiceMediaPlans
import ru.rosst.prometheus.service.vimb.VimbServiceSpots
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.concurrent.Executors
import javax.transaction.Transactional
import kotlin.math.abs
import kotlin.math.floor

@Service
class ServiceAutoPlacementEnhancedV2 : AbstractService() {
    @Autowired
    private lateinit var srvBlocks: ServiceBlocks

    @Autowired
    private lateinit var srvParameters: ServiceAutoPlacementParameters

    @Autowired
    private lateinit var vimbSrvSpots: VimbServiceSpots

    @Autowired
    private lateinit var vimbSrvBlocks: VimbServiceBlocks

    @Autowired
    private lateinit var vimbSrvMediaPlans: VimbServiceMediaPlans

    fun initializeGroupPlacement(
        group: Map.Entry<Pair<Pair<LocalDate, Byte>, Byte>, List<AutoPlacementParameter>>
    ) = runBlocking {
        // Загружаем группы данных  из vimb api
        downloadGroupVimbData(
            group.key.first.second,
            group.key.first.first.atStartOfDay().toLocalDate(),
            group.key.first.first.atStartOfDay().plusMonths(1).minusDays(1).toLocalDate(),
            group.value.map { it.mediaPlan.channel.id }.toSet()
        )

        val dispatcher = Executors
            .newFixedThreadPool(floor(Runtime.getRuntime().availableProcessors() / 2.0).toInt())
            .asCoroutineDispatcher()

        group.value.map { async(dispatcher) { initializePlacement(it.id, true) } }.awaitAll()

        val requestDate = srvParameters.findRequestDate(
            group.key.first.second,
            group.key.second,
            group.key.first.first
        ) ?: AutoPlacementRequestDate(
            LocalDateTime.now(),
            group.key.first.first,
            group.value.first().mediaPlan.channel.region.sellingDirection,
            group.value.first().mediaPlan.order.rank
        )

        requestDate.date = LocalDateTime.now()
        srvParameters.saveRequestDate(requestDate)

        info("===============================================================")
        info("=================ГРУППОВОЕ РАЗМЕЩЕНИЕ ЗАВЕРШЕНО================")
        info("===============================================================")
    }

    /**
     * Метод инициализации алгоритма автоматического размещения медиаплана по аффинити.
     *
     * @param parametersID [Int] идентификатор параметров автоматического размещения медиаплана по аффинити.
     */
    @Transactional
    fun initializePlacement(parametersID: Int, isGroupPlacement: Boolean = false) {
        // Параметры автоматического размещения медиаплана
        val parameters = srvParameters.findById(parametersID)
            ?: return error("Параметры автоматического размещения ID: $parametersID не найдены.")

        info(
            """
            [Поток: ${Thread.currentThread().name}] медиаплан: ${parameters.mediaPlan.name}
        """
        )

        // Если инициализация не из группового размещения
        if (!isGroupPlacement) {
            // Загружаем данные из VIMB
            runBlocking { downloadVimbData(parameters) }
        }

        // Параметры автоматического размещения медиаплана со всеми данными (ролики, блоки, споты, периоды, affinity)
        val placementParameters: EnhancedPlacementParameters
        try {
            placementParameters = getPlacementParameters(parameters)
        } catch (exception: RuntimeException) {
            return
        }
        // if (placementParameters.currentInventoryTotal > placementParameters.requiredInventoryTotal) return
        var isContinuePlacement = true
        while (isContinuePlacement) {
            testPlacement(placementParameters)
            if (testShuffling(placementParameters)) continue
            if (testRising(placementParameters)) continue
            else {
                isContinuePlacement = false
                //                if (!placementParameters.isTotalInventory) {
                //                    placementParameters.isTotalInventory = true
                //                    continue
                //                } else isContinuePlacement = false
            }
        }

        // Формируем задачи запросов удаления и установки спотов
        val tasks = placementParameters.periods.flatMap { period ->
            period.getSortedAffinityHalfHourPeriods().flatMap { halfHourPeriod ->
                halfHourPeriod.getSortedBlocks().flatMap { block ->
                    block.getSpots().map { spot ->
                        if (!spot.inPlacement && !spot.requireRemoving) {
                            return@map GlobalScope.async {
                                spot.block.id to vimbSrvSpots.addSpot(
                                    spot.block.id,
                                    spot.film.id
                                )
                            }
                        }
                        if (spot.requireRemoving && spot.spotID != null) {
                            return@map GlobalScope.async { vimbSrvSpots.deleteSpot(spot.spotID!!) }
                        } else return@map null
                    }
                }
            }
        }
        // Коллекция выполненных задач
        val completeTasks = runBlocking { tasks.map { it?.await() } }

        // Итерируемся по выполненным задачам - получаем коллекцию заблокированных блоков
        val bannedBlocks = completeTasks
            // Берём только пары
            .filterIsInstance<Pair<*, *>>()
            // Берём только пары с Long
            .filter { it.first is Long && it.second is Long }
            // Получаем идентификаторы блоков с запретом на размещение
            .map { it.second as Long }
            .toSet()

        // Если есть идентификаторы блоков с запретом на размещение
        if (!bannedBlocks.isNullOrEmpty()) {
            // Отмечаем в базе данных эти рекламные блоки
            srvBlocks.markBanned(bannedBlocks)
        }

        // Устанавливаем новую дату последнего размещения медиаплана
        // parameters.lastPlacementDate = LocalDateTime.now()
        // Сохраняем новую дату последнего размещения медиаплана в базу данных
        // srvParameters.save(parameters)
    }

    private fun testPlacement(parameters: EnhancedPlacementParameters) {
        // Текущие время и дата
        val currentDateTime = LocalDateTime.now()

        // Признак "продолжать размещение"
        var isContinuePlacement = true

        while (isContinuePlacement) {
            // Признак "инвентарь был набран в этой итерации"
            var isInventoryClaimed = false

            run loop@{
                parameters.periods.forEach { period ->
                    period.getSortedAffinityHalfHourPeriods().forEach { halfHourPeriod ->
                        halfHourPeriod
                            .getSortedBlocks()
                            .filter {
                                // Валидный
                                !it.isInvalid &&
                                // Не имеет спотов из других медиапланов
                                !it.hasOtherMediaPlanSpots &&
                                // Объём больше 0
                                it.volumeFree != 0.toShort() &&
                                // Не в дедлайне
                                it.deadlineDateTime > currentDateTime &&
                                // Не имеет собственных спотов
                                it.getRelevantSpots().isNullOrEmpty() &&
                                // Не имеет соседних спотов
                                !parameters.hasNeighbourSpot(it)
                            }
                            .filter {
                                // Не прайм
                                !it.isPrime ||
                                // Или прайм, но в выходной
                                halfHourPeriod.isWeekend ||
                                // Или прайм в будний, но правило разрешает
                                parameters.isPrimeInWorkDays
                            }
                            .forEach block@{ block ->
                                // Дни и количество выходов в этих днях
                                val daysAndOutputsCount = parameters.getDatesAndOutputCounts()
                                // Минимальное количество выходов в дне периода
                                val minimalDayOutputCount = daysAndOutputsCount.minByOrNull { it.key }?.key ?: return

                                // Если блок не валиден - переходим к следующему блоку
                                if (period.getDateOutputCount(block.date)!! + 1 - minimalDayOutputCount >= 2) return@block

                                period.getSortedFilms()
                                    .filter {
                                        it.requiredInventoryTotal > 0.0 &&
                                        block.volumeFree >= it.timing.duration
                                    }.forEach film@{ film ->
                                        // Инвентарь
                                        val inventory = block.calculateInventory(film.timing.duration)

                                        // Если проверка по тоталу
                                        if (parameters.isTotalInventory) {
                                            if (parameters.isInventoryExceeded(block.isPrime, inventory)) return@film
                                        } else {
                                            // Если проверка по инвентарю
                                            if (parameters.isPeriodInventory) {
                                                if (period.isInventoryExceeded(block.isPrime, inventory)) return@film
                                            } else {
                                                // Если проверка по одной версии
                                                if (parameters.isOneVersionEmulation) {
                                                    if (film.timing.isInventoryExceeded(
                                                            block.isPrime,
                                                            inventory
                                                        )
                                                    ) return@film
                                                } else {
                                                    // Если проверка по ролику
                                                    if (film.isInventoryExceeded(block.isPrime, inventory)) return@film
                                                }
                                            }
                                        }
                                        // Добавлям ролик в рекламный блок
                                        block.addSpot(film)
                                        // Задаём признак "инвентарь был набран в этой итерации"
                                        isInventoryClaimed = true

                                        return@loop
                                    }
                            }
                    }
                }
            }

            // Если инвентарь не был набран в этой итерации
            if (!isInventoryClaimed) {
                // Если нет разрешения на набор инвентаря prime в будние дни
                if (!parameters.isPrimeInWorkDays) parameters.isPrimeInWorkDays = true
                // Если нет разрешения на набор инвентаря одной версией
                else if (
                    parameters.isPrimeInWorkDays &&
                    !parameters.isOneVersionEmulation
                ) {
                    parameters.isPrimeInWorkDays = false

                    parameters.isOneVersionEmulation = true
                } else if (
                    parameters.isPrimeInWorkDays &&
                    parameters.isOneVersionEmulation &&
                    !parameters.isPeriodInventory
                ) {
                    parameters.isPrimeInWorkDays = false
                    parameters.isOneVersionEmulation = false

                    parameters.isPeriodInventory = true
                }
                // Переключаем признак "продолжать размещение"
                else {
                    isContinuePlacement = false

                    parameters.isPrimeInWorkDays = false
                    parameters.isOneVersionEmulation = false
                    parameters.isPeriodInventory = false
                }
            }
        }
    }

    private fun testShuffling(parameters: EnhancedPlacementParameters): Boolean {
        // Текущие время и дата
        val currentDateTime = LocalDateTime.now()

        while (true) {
            // Признак "инвентарь был набран в этой итерации"
            var isInventoryClaimed = false

            run whileLoop@{
                // Ищем ролики с максимальным недобором инвентаря
                val films = parameters.periods
                    .flatMap { it.getSortedFilms() }
                    .filter {
                        it.requiredInventoryTotal > 0.0 && (
                        it.requiredInventoryPrime - it.currentInventoryPrime > 0.0 ||
                        it.requiredInventoryOffPrime - it.currentInventoryOffPrime > 0.0)
                    }

                films.forEach film@{ film ->
                    // Дни и количество выходов в этих днях
                    val daysAndOutputsCount = parameters.getDatesAndOutputCounts()
                    // Минимальное количество выходов в дне периода
                    val minimalDayOutputCount = daysAndOutputsCount.minByOrNull { it.key }?.key ?: return@film
                    // Максимальное количество выходов в дне периода
                    val maximalDayOutputCount = daysAndOutputsCount.maxByOrNull { it.key }?.key ?: return@film

                    // Определяем споты ролика с максимальным инвентарём
                    val spots = parameters.periods
                        .flatMap { it.getAffinityHalfHourPeriods() }
                        .flatMap { it.getBlocks() }
                        .flatMap { it.getRelevantSpots() }
                        .filter {
                            // Этот спот принадлежит ролику
                            it.film == film &&
                            // И не в дедлайне
                            it.block.deadlineDateTime > currentDateTime
                        }.filter {
                            // Не прайм
                            !it.block.isPrime ||
                            // Или прайм, но выходной
                            it.block.affinity.isWeekend ||
                            // Или прайм в будний, но правило разрешает
                            parameters.isPrimeInWorkDays
                        }.filter {
                            // Минимальное количество дней совпадает с максимальным
                            maximalDayOutputCount == minimalDayOutputCount ||
                            // Либо спот лежит в дне с максимальным количеством выходов
                            it.block.affinity.period.getDateOutputCount(it.block.date) == maximalDayOutputCount
                        }
                        .sortedByDescending { it.block.calculateInventory(it.film.timing.duration) }

                    spots.forEach spot@{ spot ->
                        // Инвентарь спота
                        val spotInventory = spot.block.calculateInventory(spot.film.timing.duration)
                        // Определяем блоки, в которые мы потенциально можем вставить два спота вместо одного
                        val blocks = parameters.periods
                            .flatMap { it.getAffinityHalfHourPeriods() }
                            .flatMap { it.getBlocks() }
                            .filter {
                                // Не является блоком спота
                                it != spot.block &&
                                // Блок совпадает по типу со спотом
                                it.isPrime == spot.block.isPrime &&
                                // Валидный
                                !it.isInvalid &&
                                // Не имеет других спотов из других медиапланов
                                !it.hasOtherMediaPlanSpots &&
                                // Не имеет спотов в себе
                                it.getRelevantSpots().isNullOrEmpty() &&
                                // Объёма достаточно
                                it.volumeFree >= spot.film.timing.duration &&
                                // Даёт прирост инвентаря
                                it.calculateInventory(spot.film.timing.duration) - spotInventory > 0.01 &&
                                it.deadlineDateTime > currentDateTime &&
                                // Дата рекламного блока должа входить в рассматриваемый период
                                it.dateTime.toLocalDate() in film.timing.period.dateFrom..film.timing.period.dateBefore &&
                                // Не имеет соседних спотов
                                !parameters.hasNeighbourSpot(it)
                            }.filter {
                                // Этот блок не прайм
                                !it.isPrime ||
                                // Или прайм, но выходной
                                it.affinity.isWeekend ||
                                // Или прайм в будни, но правило разрешает
                                parameters.isPrimeInWorkDays
                            }
                            .filter {
                                // Тот же день
                                it.date == spot.block.date || // или
                                // Максимальное количество выходов совпадает с минимальным
                                maximalDayOutputCount == minimalDayOutputCount || // или
                                // Блок имеет максимальное количество выходов
                                it.affinity.period.getDateOutputCount(it.date) == minimalDayOutputCount
                            }.sortedBy { abs(it.calculateInventory(spot.film.timing.duration) - spotInventory * 0.5) }

                        blocks
                            .sortedBy {
                                val nextBlock = blocks.getOrNull(blocks.indexOf(it) + 1)
                                val nextBlockInventory = nextBlock?.calculateInventory(spot.film.timing.duration) ?: 0.0
                                val currentBlockInventory = it.calculateInventory(spot.film.timing.duration)
                                abs((nextBlockInventory + currentBlockInventory) - spotInventory)
                            }
                            .forEachIndexed block@{ index, block ->
                                // Если следующего блока нет - переходим к следующему споту
                                var nextBlock = blocks.getOrNull(index + 1) ?: return@spot
                                // Если следующий блок по индексу в тот же день что и текущий - ищем другой
                                if (nextBlock.date == block.date) {
                                    nextBlock = blocks.firstOrNull { it.date != block.date } ?: return@block
                                }

                                if (block.date != spot.block.date && nextBlock.date != spot.block.date) return@block

                                // Считаем сколько наберёт спот в следующем блоке
                                val nextBlockInventory = nextBlock.calculateInventory(spot.film.timing.duration)

                                // Считаем сколько наберёт спот в текущем блоке
                                val currentBlockInventory = block.calculateInventory(spot.film.timing.duration)

                                // Разница между инвентарём спота и двумя блоками
                                val inventoryDifference = (nextBlockInventory + currentBlockInventory) - spotInventory

                                // Если разницы нет - переходим к следующему рекламному блоку
                                if (inventoryDifference <= 0.01) return@block

                                // Если количество выходов между максимальным и минимальным превысится - переходим к следующему блоку
                                if (
                                    (
                                    block.date != spot.block.date &&
                                    block.affinity.period.getDateOutputCount(block.date)!! + 1 - minimalDayOutputCount >= 2
                                    ) ||
                                    (
                                    nextBlock.date != spot.block.date &&
                                    nextBlock.affinity.period.getDateOutputCount(nextBlock.date)!! + 1 - minimalDayOutputCount >= 2
                                    )
                                ) return@block

                                // Если проверка по тоталу
                                if (parameters.isTotalInventory) {
                                    if (parameters.isInventoryExceeded(
                                            spot.block.isPrime,
                                            inventoryDifference
                                        )
                                    ) return@block
                                } else {
                                    // Если проверка по инвентарю
                                    if (parameters.isPeriodInventory) {
                                        if (spot.block.affinity.period.isInventoryExceeded(
                                                spot.block.isPrime,
                                                inventoryDifference
                                            )
                                        ) return@block
                                    } else {
                                        // Если проверка по одной версии
                                        if (parameters.isOneVersionEmulation) {
                                            if (spot.film.timing.isInventoryExceeded(
                                                    spot.block.isPrime,
                                                    inventoryDifference
                                                )
                                            ) return@block
                                        } else {
                                            // Если проверка по ролику
                                            if (spot.film.isInventoryExceeded(
                                                    spot.block.isPrime,
                                                    inventoryDifference
                                                )
                                            ) return@block
                                        }
                                    }
                                }

                                // Удаляем старый спот
                                spot.block.removeSpot(spot)
                                // Добавлям ролик в рекламный блок
                                block.addSpot(spot.film)
                                // Добавляем ролик в следующий рекламный блок
                                nextBlock.addSpot(spot.film)
                                // Задаём признак "инвентарь был набран в этой итерации"
                                isInventoryClaimed = true
                                // Переходим к следующему споту
                                return true
                            }
                    }
                }
            }

            // Если инвентарь не был набран в этой итерации
            if (!isInventoryClaimed) {
                // Если нет разрешения на набор инвентаря prime в будние дни
                if (!parameters.isPrimeInWorkDays) parameters.isPrimeInWorkDays = true
                // Если нет разрешения на набор инвентаря одной версией
                else if (
                    parameters.isPrimeInWorkDays &&
                    !parameters.isOneVersionEmulation
                ) {
                    parameters.isPrimeInWorkDays = false

                    parameters.isOneVersionEmulation = true
                } else if (
                    parameters.isPrimeInWorkDays &&
                    parameters.isOneVersionEmulation &&
                    !parameters.isPeriodInventory
                ) {
                    parameters.isPrimeInWorkDays = false
                    parameters.isOneVersionEmulation = false

                    parameters.isPeriodInventory = true
                }
                // Переключаем признак "продолжать размещение"
                else {
                    parameters.isPrimeInWorkDays = false
                    parameters.isOneVersionEmulation = false
                    parameters.isPeriodInventory = false

                    return false
                }
            }
        }
    }

    private fun testRising(parameters: EnhancedPlacementParameters): Boolean {
        // Текущие время и дата
        val currentDateTime = LocalDateTime.now()

        while (true) {
            // Признак "инвентарь был набран в этой итерации"
            var isInventoryClaimed = false

            run whileLoop@{
                // Ищем ролики с максимальным недобором инвентаря
                val films = parameters.periods
                    .flatMap { it.getSortedFilms() }
                    .filter {
                        it.requiredInventoryTotal > 0.0 && (
                        it.requiredInventoryPrime - it.currentInventoryPrime > 0.0 ||
                        it.requiredInventoryOffPrime - it.currentInventoryOffPrime > 0.0)
                    }

                films.forEach film@{ film ->
                    // Дни и количество выходов в этих днях
                    val daysAndOutputsCount = parameters.getDatesAndOutputCounts()
                    // Максимальное количество выходов в дне периода
                    val maximalDayOutputCount = daysAndOutputsCount.maxByOrNull { it.key }?.key ?: return@film
                    // Минимальное количество выходов в дне периода
                    val minimalDayOutputCount = daysAndOutputsCount.minByOrNull { it.key }?.key ?: return@film
                    // Определяем споты ролика с сортировкой возрастания инвентаря
                    val spots = parameters.periods
                        .flatMap { it.getAffinityHalfHourPeriods() }
                        .flatMap { it.getBlocks() }
                        .flatMap { it.getRelevantSpots() }
                        .filter {
                            // Этот спот принадлежит ролику
                            it.film == film &&
                            // И не в дедлайне
                            it.block.deadlineDateTime > currentDateTime
                        }.filter {
                            // Этот блок не прайм
                            !it.block.isPrime ||
                            // Или прайм и при этом выходной день
                            it.block.affinity.isWeekend ||
                            // Или прайм и будний день, но с выключеным правилом
                            parameters.isPrimeInWorkDays
                        }.filter {
                            // Минимальное количество дней совпадает с максимальным
                            maximalDayOutputCount == minimalDayOutputCount ||
                            // Либо спот лежит в дне с максимальным количеством выходов
                            it.block.affinity.period.getDateOutputCount(it.block.date) == maximalDayOutputCount
                        }
                        .sortedBy { it.block.calculateInventory(it.film.timing.duration) }

                    spots.forEach spot@{ spot ->
                        // Инвентарь спота
                        val spotInventory = spot.block.calculateInventory(spot.film.timing.duration)
                        // Определяем блоки, в которые мы потенциально можем переставить спот
                        val blocks = parameters.periods
                            .flatMap { it.getAffinityHalfHourPeriods() }
                            .flatMap { it.getBlocks() }
                            .filter {
                                // Не является блоком спота
                                it != spot.block &&
                                // Блок совпадает по типу со спотом
                                it.isPrime == spot.block.isPrime &&
                                // Валидный
                                !it.isInvalid &&
                                // Не имеет других спотов из других медиапланов
                                !it.hasOtherMediaPlanSpots &&
                                // Не имеет спотов в себе
                                it.getRelevantSpots().isNullOrEmpty() &&
                                // Объёма достаточно
                                it.volumeFree >= spot.film.timing.duration &&
                                // Даёт прирост инвентаря
                                it.calculateInventory(spot.film.timing.duration) - spotInventory > 0.01 &&
                                it.deadlineDateTime > currentDateTime &&
                                // Дата рекламного блока должа входить в рассматриваемый период
                                it.dateTime.toLocalDate() in film.timing.period.dateFrom..film.timing.period.dateBefore &&
                                // Не имеет соседних спотов
                                !parameters.hasNeighbourSpot(it)
                            }.filter {
                                // Этот блок не прайм
                                !it.isPrime ||
                                // Или прайм и при этом выходной день
                                it.affinity.isWeekend ||
                                // Или прайм и будний день, но с выключеным правилом
                                parameters.isPrimeInWorkDays
                            }
                            .filter {
                                // Тот же день
                                it.date == spot.block.date || // или
                                // Максимальное количество выходов совпадает с минимальным
                                maximalDayOutputCount == minimalDayOutputCount || // или
                                // Блок имеет максимальное количество выходов
                                it.affinity.period.getDateOutputCount(it.date) == minimalDayOutputCount
                            }
                            .sortedByDescending { it.calculateInventory(spot.film.timing.duration) - spotInventory }

                        blocks.forEach block@{ block ->
                            // Считаем сколько наберёт спот в текущем блоке
                            val currentBlockInventory = block.calculateInventory(spot.film.timing.duration)

                            // Разница между инвентарём спота и двумя блоками
                            val inventoryDifference = currentBlockInventory - spotInventory

                            // Если проверка по тоталу
                            if (parameters.isTotalInventory) {
                                if (parameters.isInventoryExceeded(
                                        spot.block.isPrime,
                                        inventoryDifference
                                    )
                                ) return@block
                            } else {
                                // Если проверка по инвентарю
                                if (parameters.isPeriodInventory) {
                                    if (spot.block.affinity.period.isInventoryExceeded(
                                            spot.block.isPrime,
                                            inventoryDifference
                                        )
                                    ) return@block
                                } else {
                                    // Если проверка по одной версии
                                    if (parameters.isOneVersionEmulation) {
                                        if (spot.film.timing.isInventoryExceeded(
                                                spot.block.isPrime,
                                                inventoryDifference
                                            )
                                        ) return@block
                                    } else {
                                        // Если проверка по ролику
                                        if (spot.film.isInventoryExceeded(
                                                spot.block.isPrime,
                                                inventoryDifference
                                            )
                                        ) return@block
                                    }
                                }
                            }

                            // Удаляем старый спот
                            spot.block.removeSpot(spot)
                            // Добавлям ролик в рекламный блок
                            block.addSpot(spot.film)
                            // Задаём признак "инвентарь был набран в этой итерации"
                            isInventoryClaimed = true
                            // Переходим к следующему споту
                            return true
                        }
                    }
                }
            }

            // Если инвентарь не был набран в этой итерации
            if (!isInventoryClaimed) {
                // Если нет разрешения на набор инвентаря prime в будние дни
                if (!parameters.isPrimeInWorkDays) parameters.isPrimeInWorkDays = true
                // Если нет разрешения на набор инвентаря одной версией
                else if (
                    parameters.isPrimeInWorkDays &&
                    !parameters.isOneVersionEmulation
                ) {
                    parameters.isPrimeInWorkDays = false

                    parameters.isOneVersionEmulation = true
                } else if (
                    parameters.isPrimeInWorkDays &&
                    parameters.isOneVersionEmulation &&
                    !parameters.isPeriodInventory
                ) {
                    parameters.isPrimeInWorkDays = false
                    parameters.isOneVersionEmulation = false

                    parameters.isPeriodInventory = true
                }
                // Переключаем признак "продолжать размещение"
                else {
                    parameters.isPrimeInWorkDays = false
                    parameters.isOneVersionEmulation = false
                    parameters.isPeriodInventory = false

                    return false
                }
            }
        }
    }

    /**
     * Метод формирования параметров автоматического размещения медиаплана и наполнения данными.
     * @param parameters    [AutoPlacementParameter]        параметры автоматического размещения медиаплана.
     * @return              [EnhancedPlacementParameters]   параметры автоматического размещения медиаплана с данными (ролики, споты, блоки, периоды, affinity).
     */
    private fun getPlacementParameters(parameters: AutoPlacementParameter): EnhancedPlacementParameters {
        // Параметры размещения
        return EnhancedPlacementParameters(
            parameters.mediaPlan.order.id,
            parameters.mediaPlan.id,
            parameters.id,
            parameters.inventoryPercentRequired,
            parameters.periods
                // Фильтруем периоды, которые не указаны (если такие есть)
                .filter { it.dateFrom != null && it.dateBefore != null }
                .map { period ->
                    EnhancedPlacementPeriod(
                        period.dateFrom!!,
                        period.dateBefore!!,
                        period.films
                            // Группируем ролики по хронометражу
                            .groupBy { it.filmXMediaPlan.film.duration }
                            .map {
                                EnhancedPlacementTiming(
                                    it.key,
                                    // Ролики хронометража
                                    it.value.map { film ->
                                        EnhancedPlacementFilm(
                                            film.filmXMediaPlan.id,
                                            period.percentPrime,
                                            if (parameters.mediaPlan.isMinute)
                                                period.inventory * 60 * film.inventoryPercent * parameters.inventoryPercentRequired
                                            else
                                                period.inventory * film.inventoryPercent * parameters.inventoryPercentRequired
                                        )
                                    }
                                )
                                    // Создаём обратную ссылку на хронометраж для всех роликов
                                    .apply { films.forEach { film -> film.timing = this } }
                            }
                    )
                        // Создаём обратную ссылку на период для всех хронометражей
                        .apply { timings.forEach { it.period = this } }
                        // Наполняем недельный период получасовыми периодами
                        .apply { getPlacementAffinityHalfHourPeriod(parameters, this) }
                }
        )
            // Создаём обратную ссылку на параметры для всех периодов
            .apply { periods.forEach { it.parameters = this } }
    }

    /**
     * Метод получения временных интервалов периода со значением аффинитивности, и принадлежащих
     * рекламных блоков.
     *
     * @param period [AutoPlacementPeriod] период, для которого нужно сформировать получасовые интервалы.
     */
    private fun getPlacementAffinityHalfHourPeriod(
        parameters: AutoPlacementParameter,
        period: EnhancedPlacementPeriod
    ) {
        // Базовая аудитория канала
        val audienceBase = parameters.mediaPlan.channel.palomarsChannel?.baseAudiences?.lastOrNull()
            ?: throw RuntimeException(
                "Не удалось определить базовую аудиторию канала: ${parameters.mediaPlan.channel.name}"
            )

        // Запрашиваем все известные рейтинги по всем известным аудиториям канала
        val ratings = parameters.mediaPlan.channel.palomarsChannel?.ratings
        if (ratings.isNullOrEmpty()) {
            throw RuntimeException(
                "Не удалось найти хоть какие-то рейтинги канала: ${parameters.mediaPlan.channel.palomarsChannel?.name}"
            )
        }

        // Делаем выборку рейтингов по целевой аудитории
        val targetAudienceRatings = ratings
            // Отбрасываем праздничные дни - не рассматриваем при размещении
            .filter { rating -> rating.dayType != PalomarsTimeDayType.HOLIDAY }
            // Отбрасываем аудитории, которые не выбраны в качесте целевой медиаплана
            .filter { rating -> rating.audience == parameters.audience }
            // Отбрасываем значения рейтингов других месячных периодов
            .filter { rating -> rating.month == parameters.audienceDate }

        // Делаем выборку рейтингов по базовой аудитории
        val baseAudienceRatings = ratings
            // Отбрасываем праздничные дни - не рассматриваем при размещении
            .filter { rating -> rating.dayType != PalomarsTimeDayType.HOLIDAY }
            // Отбрасываем аудитории, которые не выбраны в качесте базовой канала
            .filter { rating -> rating.audience == audienceBase.audience }
            // Отбрасываем значения рейтингов других месячных периодов
            .filter { rating -> rating.month == parameters.audienceDate }

        // Считаем affinity получасовых периодов канала
        val affinities = targetAudienceRatings.map { targetRating ->
            val foundBaseAudience = baseAudienceRatings.firstOrNull { baseRating ->
                baseRating.timeFrom == targetRating.timeFrom &&
                baseRating.timeBefore == targetRating.timeBefore &&
                baseRating.dayType == targetRating.dayType
            } ?: throw RuntimeException(
                "Не удалось рассчитать affinity времени [${targetRating.timeFrom}-${targetRating.timeBefore}]."
            )
            EnhancedPlacementAffinityHalfHourPeriod(
                foundBaseAudience.timeFrom,
                foundBaseAudience.timeBefore,
                foundBaseAudience.dayType == PalomarsTimeDayType.DAY_OFF,
                targetRating.rating / foundBaseAudience.rating * 100.0
            ).apply { this.period = period }
        }
            .sortedByDescending { it.affinity }

        period.addAffinityHalfHourPeriods(*affinities.toTypedArray())
        getPlacementAffinityHalfHourPeriodBlocksAndSpots(parameters, period.getSortedAffinityHalfHourPeriods())
    }

    /**
     * Метод заполнения получасовых периодов рекламными блоками и спотами.
     * @param parameters [AutoPlacementParameter]   параметры автоматического размещения медиаплана.
     * @param affinities [Iterable]                 коллекция [EnhancedPlacementAffinityHalfHourPeriod] получасовых периодов.
     */
    private fun getPlacementAffinityHalfHourPeriodBlocksAndSpots(
        parameters: AutoPlacementParameter,
        affinities: Iterable<EnhancedPlacementAffinityHalfHourPeriod>
    ) {
        // Рекламные блоки
        val blocks = getBlocks(parameters)

        affinities.forEach { affinityHalfHourPeriod ->
            // Определяем рекламные блоки этого получасового периода
            val halfHourPeriodBlocks = blocks
                .filter { block ->
                    // Рекламный блок в периоде
                    val isInPeriod =
                        block.date in affinityHalfHourPeriod.period.dateFrom..affinityHalfHourPeriod.period.dateBefore

                    // Рекламный блок в дне недели
                    val isInWeekDay =
                        block.weekDay in 1..5 && !affinityHalfHourPeriod.isWeekend ||
                        block.weekDay in 6..7 && affinityHalfHourPeriod.isWeekend

                    // Рекламный блок в получасовом интервале
                    val isInHalfHourPeriod =
                        block.time.toLocalTime() in affinityHalfHourPeriod.timeFrom..affinityHalfHourPeriod.timeBefore
                    isInPeriod && isInWeekDay && isInHalfHourPeriod
                }
                .map { block ->
                    EnhancedPlacementBlock(
                        parameters.isAssignedRatingsPlacement,
                        block.id,
                        block.date,
                        block.time,
                        block.deadlineDateTime,
                        block.isPrime,
                        block.isMinute,
                        block.volumeFree,
                        if (block.isMinute) block.volumeFree.toDouble() else block.ratingForecast,
                        block.assignedRatings.firstOrNull { assignedRating ->
                            assignedRating.order.id == parameters.mediaPlan.order.id
                        }?.rating,
                        isInvalid = !parameters.times.any {
                            block.weekDay in 1..5 && !it.isWeekend && block.time.toLocalTime() in it.timeFrom..it.timeBefore ||
                            block.weekDay in 6..7 && it.isWeekend && block.time.toLocalTime() in it.timeFrom..it.timeBefore
                        }
                    ).apply {
                        this.affinity = affinityHalfHourPeriod
                        this.hasOtherMediaPlanSpots =
                            block.spots.any { spot -> spot.filmXMediaPlan.mediaPlan.id != parameters.mediaPlan.id }
                        val spots =
                            block.spots.filter { spot -> spot.filmXMediaPlan.mediaPlan.id == parameters.mediaPlan.id }
                        if (spots.isNotEmpty()) {
                            spots.forEach { spot ->
                                val film = affinityHalfHourPeriod.period.timings.firstOrNull { timing ->
                                    timing.films.any { film -> film.id == spot.filmXMediaPlan.id }
                                }?.films?.firstOrNull { film -> film.id == spot.filmXMediaPlan.id }
                                    ?: throw RuntimeException("Не удалось сопоставить модель размещаемого ролика со спотом.")

                                this.addSpot(
                                    film,
                                    true,
                                    parameters.mediaPlan.order.isMultipleSpots ?: false,
                                    spotID = spot.id
                                )
                            }
                        }
                    }
                }

            // Добавляем определённые рекламные блоки в этот получасовой период
            affinityHalfHourPeriod.addBlocks(*halfHourPeriodBlocks.toTypedArray())
        }
    }

    /**
     * Метод получения рекламных блоков, подходящих для размещения медиаплана.
     *
     * @param parameters    [AutoPlacementParameter]    параметры автоматического размещения медиаплана.
     *
     * @return              [List]                      коллекция [Block] рекламных блоков.
     */
    private fun getBlocks(parameters: AutoPlacementParameter) = srvBlocks
        // Рекламные блоки канала - месяца
        .findAllByMediaPlan(parameters.mediaPlan)
        // Подходящие под параметры размещения
        .filter { block ->
            // Проверка "рекламный блок входит в один из временных периодов параметров автоматической расстановки"
            val isInTime = parameters.times.any {
                block.weekDay in 1..5 && !it.isWeekend && block.time.toLocalTime() in it.timeFrom..it.timeBefore ||
                block.weekDay in 6..7 && it.isWeekend && block.time.toLocalTime() in it.timeFrom..it.timeBefore
            } || block.spots.isNotEmpty()
            // Проверка "рекламный блок входит в один из дневных периодов параетров автоматической расстановки"
            val isInPeriod = parameters.periods
                .filter { it.dateFrom != null && it.dateBefore != null }
                .any { block.date in it.dateFrom!!..it.dateBefore!! }

            return@filter isInTime && isInPeriod && !block.isBanned
        }

    /**
     * Метод загрузки данных из vimb api.
     *
     * @param parameters [AutoPlacementParameter] параметры автоматического размещения медиаплан.
     */
    private suspend fun downloadVimbData(parameters: AutoPlacementParameter) {
        downloadMediaPlan(parameters)
        downloadBlocks(parameters)
        downloadSpots(parameters)
    }

    /**
     * Метод загрузки группы данных из vimb api.
     *
     * @param id            [Byte]      идентификатор направления продаж
     * @param dateFrom      [LocalDate] дата начала периода
     * @param dateBefore    [LocalDate] дата окончания периода
     */
    private suspend fun downloadGroupVimbData(
        id: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channelsID: Set<Int>
    ) {
        downloadMediaPlanGroup(id, dateFrom, dateBefore)
        downloadGroupBlocks(id, dateFrom, dateBefore, channelsID)
        downloadGroupSpots(id, dateFrom, dateBefore)
    }

    /**
     * Метод загрузки медиаплана из vimb api.
     *
     * @param parameters [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    private suspend fun downloadMediaPlan(parameters: AutoPlacementParameter) {
        vimbSrvMediaPlans.download(
            parameters.mediaPlan.channel.region.sellingDirection.id,
            parameters.mediaPlan.date.atStartOfDay().toLocalDate(),
            parameters.mediaPlan.date.atStartOfDay().plusMonths(1).minusDays(1).toLocalDate(),
            setOf(parameters.mediaPlan.channel.id),
            setOf(parameters.mediaPlan.order.agreement.mapping.advertiser.id)
        )
    }

    /**
     * Метод загрузки группы медиапланов из vimb api.
     *
     * @param id            [Byte]      идентификатор направления продаж
     * @param dateFrom      [LocalDate] дата начала периода
     * @param dateBefore    [LocalDate] дата окончания периода
     */
    private suspend fun downloadMediaPlanGroup(
        id: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate
    ) {
        vimbSrvMediaPlans.download(id, dateFrom, dateBefore)
    }

    /**
     * Метод загрузки рекламных блоков из vimb api.
     *
     * @param parameters [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    private suspend fun downloadBlocks(parameters: AutoPlacementParameter) {
        vimbSrvBlocks.download(
            parameters.mediaPlan.channel.region.sellingDirection.id,
            parameters.mediaPlan.date.atStartOfDay().toLocalDate(),
            parameters.mediaPlan.date.atStartOfDay().plusMonths(1).minusDays(1).toLocalDate(),
            setOf(parameters.mediaPlan.channel.id)
        )
    }

    /**
     * Метод загрузки группы рекламных блоков из vimb api.
     *
     * @param id            [Byte]      идентификатор направления продаж
     * @param dateFrom      [LocalDate] дата начала периода
     * @param dateBefore    [LocalDate] дата окончания периода
     */
    private suspend fun downloadGroupBlocks(
        id: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channelsID: Set<Int>
    ) {
        vimbSrvBlocks.download(id, dateFrom, dateBefore, channelsID)
    }

    /**
     * Метод загрузки спотов из vimb api.
     *
     * @param parameters [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    private suspend fun downloadSpots(parameters: AutoPlacementParameter) {
        vimbSrvSpots.downloadSpots(
            parameters.mediaPlan.channel.region.sellingDirection.id,
            parameters.mediaPlan.date.atStartOfDay().toLocalDate(),
            parameters.mediaPlan.date.atStartOfDay().plusMonths(1).minusDays(1).toLocalDate(),
            setOf(parameters.mediaPlan.channel.id),
            setOf(parameters.mediaPlan.order.agreement.mapping.advertiser.id)
        )
    }

    /**
     * Метод загрузки группы спотов из vimb api.
     *
     * @param id            [Byte]      идентификатор направления продаж
     * @param dateFrom      [LocalDate] дата начала периода
     * @param dateBefore    [LocalDate] дата окончания периода
     */
    private suspend fun downloadGroupSpots(
        id: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate
    ) {
        vimbSrvSpots.downloadSpots(id, dateFrom, dateBefore)
    }
}