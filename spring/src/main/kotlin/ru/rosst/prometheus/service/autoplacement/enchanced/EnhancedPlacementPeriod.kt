package ru.rosst.prometheus.service.autoplacement.enchanced

import kotlinx.coroutines.runBlocking
import ru.rosst.extensions.kotlin.parallelMap
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.math.ceil

/**
 * Модель периода.
 *
 * Данная модель хранит рекламные ролики и их инвентарь, который они набрали и должны ещё набрать (если требуется),
 * в данном периоде (обычно это недельный период). Также данная модель хранит все минимально подходящие рекламные блоки,
 * которые можно рассматривать для размещения в них роликов. Коллекция рекламных роликов сгруппирована по значениям
 * аффинити (больше - лучше), т.е. по некому коэффициенту приоритета выбора рекламного блока для размещения в него ролика.
 *
 * Предполагается следующая модель выбора рекламных блоков:
 * - рекламные блоки делятся на четыре квантиля:
 * -- первый:       самый лучший
 * -- второй:       выше среднего
 * -- третий:       ниже среднего
 * -- четвёртый:    самый плохой
 * - по-началу программатик рассматривает только первый и второй квантиль для размещения роликов в рекламные блоки, которые хранятся в этих квантилях.
 * - со временем (после нескольких итераций, или какое-либо другое условие по бизнес логике) добавляются рекламные блоки из третьего, а потом и из четвёртого квантиля для размещения в них роликов.
 * - правило выбора квантилей должно определяться в периоде или в параметрах (чтобы определять правило выбора сразу для всех периодов)
 *
 * @param       dateFrom                    [LocalDate]                     дата начала периода.
 * @param       dateBefore                  [LocalDate]                     дата окончания периода.
 * @param       timings                     [Iterable]                      коллекция [EnhancedPlacementTiming] хронометражей - групп роликов.
 * @param       affinityHalfHourPeriods     [Iterable]                      коллекция [EnhancedPlacementAffinityHalfHourPeriod] аффинитивности получасовых периодов.
 *
 * @property    parameters                  [EnhancedPlacementParameters]   параметры, к которым относится период.
 * @property    daysCount                   [Long]                          количество дней в периоде.
 *
 * @property    middleMedian                [Double]                        значение аффинитивности средней медианы.
 * @property    upperMedian                 [Double]                        значение аффинитивности высшей медианы.
 * @property    lowerMedian                 [Double]                        значение аффинитивности низшей медианы.
 *
 * @property    requiredInventoryPrime      [Double]                        плановый инвентарь роликов в периоде в prime.
 * @property    requiredInventoryOffPrime   [Double]                        плановый инвентарь роликов в периоде в off prime.
 * @property    requiredInventoryTotal      [Double]                        плановый инвентарь роликов в периоде всего.
 *
 * @property    currentInventoryTotal       [Double]                        текущий инвентарь роликов в периоде всего (вычисляемое).
 * @property    currentInventoryPrime       [Double]                        текущий инвентарь роликов в периоде в prime (вычисляемое).
 * @property    currentInventoryOffPrime    [Double]                        текущий инвентарь роликов в периоде в off prime (вычисляемое).
 */
class EnhancedPlacementPeriod(
    val dateFrom: LocalDate,
    val dateBefore: LocalDate,
    val timings: Iterable<EnhancedPlacementTiming>,
    private val affinityHalfHourPeriods: MutableList<EnhancedPlacementAffinityHalfHourPeriod> = mutableListOf()
) {

    lateinit var parameters: EnhancedPlacementParameters

    var isSecondQuantile: Boolean = false
    var isThirdQuantile: Boolean = false
    var isFourthQuantile: Boolean = false

    val daysCount = ChronoUnit.DAYS.between(this.dateFrom, this.dateBefore) + 1

    // region Вычисляемые свойства - медианы
    private val middleMedian get() = getMedian(getSortedAffinityHalfHourPeriods().map { it.affinity })
    private val upperMedian: Double
        get() {
            // Все получасовые аффинитивные интервалы периода
            val affinityHalfHourPeriods = getSortedAffinityHalfHourPeriods()
            // Индекс последнего большего либо равного средней медианы
            val indexOfLast = affinityHalfHourPeriods.indexOfLast { it.affinity >= middleMedian }
            // Получасовые аффинитивные интервалы периода, значение аффинити которых больше, либо равное средней медианы
            val upperMedianAffinityHalfHourPeriods = affinityHalfHourPeriods
                .subList(0, indexOfLast + 1)
            return getMedian(upperMedianAffinityHalfHourPeriods.map { it.affinity })
        }
    private val lowerMedian: Double
        get() {
            // Все получасовые аффинитивные интервалы периода
            val affinityHalfHourPeriods = getSortedAffinityHalfHourPeriods()
            // Индекс первого меньшего либо равного средней медианы
            val indexOfFirst = affinityHalfHourPeriods.indexOfFirst { it.affinity <= middleMedian }
            // Получасовые аффинитивные интервалы периода, значение аффинити которых меньше, либо равное средней медианы
            val lowerMedianAffinityHalfHourPeriods = affinityHalfHourPeriods
                .subList(indexOfFirst, affinityHalfHourPeriods.lastIndex + 1)
            return getMedian(lowerMedianAffinityHalfHourPeriods.map { it.affinity })
        }
    // endregion

    // region Свойства планового инвентаря
    val requiredInventoryPrime = timings
        .sumOf {
            it.films.sumOf { film ->
                film.requiredInventoryPrime
            }
        }
    val requiredInventoryOffPrime = timings.sumOf {
        it.films.sumOf { film ->
            film.requiredInventoryOffPrime
        }
    }
    val requiredInventoryTotal = requiredInventoryPrime + requiredInventoryOffPrime
    // endregion

    // region Вычисляемые свойства - размещённый инвентарь
    val currentInventoryTotal get() = timings.sumOf { it.films.sumOf { f -> f.currentInventoryTotal } }
    val currentInventoryPrime get() = timings.sumOf { it.films.sumOf { f -> f.currentInventoryPrime } }
    val currentInventoryOffPrime get() = timings.sumOf { it.films.sumOf { f -> f.currentInventoryOffPrime } }
    // endregion

    /**
     * Метод добавления получасовых аффинитивных интервалов в период.
     *
     * @param periods [Array] коллекция [EnhancedPlacementAffinityHalfHourPeriod] получасовых аффинитивных интервалов.
     */
    fun addAffinityHalfHourPeriods(vararg periods: EnhancedPlacementAffinityHalfHourPeriod) {
        affinityHalfHourPeriods.addAll(periods)
    }

    /**
     * Метод получения всех получасовых аффиниитивных интервалов в периоде.
     *
     * @return [List] коллекция всех [EnhancedPlacementAffinityHalfHourPeriod] получасовые аффинитивные интервалы в периоде.
     */
    fun getAffinityHalfHourPeriods(): List<EnhancedPlacementAffinityHalfHourPeriod> = affinityHalfHourPeriods

    /**
     * Метод получения всех получасовых аффинитивных интервалов в периоде отсортированных по значению аффинити.
     *
     * @return [List] коллекция всех [EnhancedPlacementAffinityHalfHourPeriod] получасовых аффинитивных интервалов в периоде.
     */
    fun getSortedAffinityHalfHourPeriods(): List<EnhancedPlacementAffinityHalfHourPeriod> =
        affinityHalfHourPeriods.sortedByDescending { it.affinity }

    /**
     * Метод получения получасовых аффинитивных интервалов, разрешённых квантилей.
     *
     * @return [List] коллекция [EnhancedPlacementAffinityHalfHourPeriod] получасовых аффинитивных интервалов, разрешённых квантилей.
     */
    fun getQuantifiedAffinityHalfHourPeriods(): List<EnhancedPlacementAffinityHalfHourPeriod> = when {
        isFourthQuantile || parameters.isFourthQuantile -> getSortedAffinityHalfHourPeriods()
        isThirdQuantile || parameters.isThirdQuantile -> getSortedAffinityHalfHourPeriods().filter { it.affinity >= lowerMedian }
        isSecondQuantile || parameters.isSecondQuantile -> getSortedAffinityHalfHourPeriods().filter { it.affinity >= middleMedian }
        else -> getSortedAffinityHalfHourPeriods().filter { it.affinity >= upperMedian }
    }

    /**
     * Метод получения коллекции роликов периода, отсортированных по набранному инвентарю в периоде по возрастанию.
     *
     * @return [List] коллекция [EnhancedPlacementFilm] роликов, отсортированных по набранному инвентарю в периоде по возрастанию.
     */
    fun getSortedFilms(): List<EnhancedPlacementFilm> = timings
        // Делаем единую коллекцию роликов из группы хронометражей
        .flatMap { it.films }
        // Сортируем ролики по набранному инвентарю по возрастанию
        .sortedWith(compareByDescending<EnhancedPlacementFilm> {
            it.timing.duration
        }.thenBy {
            it.currentInventoryTotal / it.requiredInventoryTotal * 100
        })

    // region Методы определения выходов

    /**
     * Метод получения рекламных блоков по указанной дате.
     * @param date [LocalDate] дата.
     * @return [Collection] коллекция [EnhancedPlacementBlock] рекламных блоков в указанной дате.
     */
    private fun getBlocksByDate(date: LocalDate) = runBlocking {
        affinityHalfHourPeriods
            .flatMap { it.getBlocks() }
            .filter { it.date == date }
            .ifEmpty { return@runBlocking null }
    }

    /**
     * Метод определения количества имеющихся выходов в указанной дате.
     * @param date  [LocalDate] дата, в которой нужно определить количество имеющихся выходов.
     * @return      [Int]       количество имеющихся выходов.
     */
    fun getDateOutputCount(date: LocalDate) = runBlocking {
        getBlocksByDate(date)?.sumOf { it.getRelevantSpots().size }
    }

    /**
     * Метод определения количества имеющихся выходов ролика в указанной дате.
     * @param date  [LocalDate]                 дата, в которой нужно определить количество имеющихся выходов ролика.
     * @param film  [EnhancedPlacementFilm]     ролик, выходы которого определяем.
     * @return      [Int]                       количество имеющихся выходов ролика в указанную дату.
     */
    fun getFilmOutputsCountInDate(date: LocalDate, film: EnhancedPlacementFilm) = runBlocking {
        getBlocksByDate(date)?.sumOf { it.getRelevantSpots().filter { spot -> spot.film == film }.size }
    }

    /**
     * Метод определения примерного количества выходов ролика в день.
     * @param film  [EnhancedPlacementFilm] ролик, выходы в день которого определяем.
     * @return      [Int]                   примерное количество выходов ролика в день.
     */
    fun getFilmOutputsCountPerDay(
        film: EnhancedPlacementFilm
    ): Int {
        // Текущие дата и время
        val currentDateTime = LocalDateTime.now()
        // Плановый инвентарь ролика в периоде
        val requiredFilmInventory = film.requiredInventoryTotal
        // Коллекция рекламных блоков, подходящих для размещения ролика
        val blocks = affinityHalfHourPeriods
            // Формируем единую коллекцию рекламных блоков всех получасовых аффинитивных интервалов
            .flatMap { it.getBlocks() }
            // Оставляем рекламные блоки, которые не в дедлайне
            .filter { it.deadlineDateTime > currentDateTime }
            // Оставляем рекламные блоки, обём которых подходит для размещения ролика
            .filter { it.volumeFree >= film.timing.duration }
            // Оставляем рекламные блоки без спотов
            .filter { it.getRelevantSpots().isEmpty() && !it.hasOtherMediaPlanSpots }
        // Количество рекламных блоков
        val blocksCount = blocks.size
        // Суммарный инвентарь рекламных блоков периода
        val blocksInventory = blocks.sumOf { it.calculateInventory(film.timing.duration) }

        // Считаем количество выходов ролика в день
        var outputsCount = ceil(requiredFilmInventory / (blocksInventory / blocksCount) / daysCount).toInt()
        // Если выходов расчиталось 0, делаем равным 1
        if (outputsCount == 0) outputsCount = 1

        return outputsCount
    }

    // endregion

    /**
     * Метод получения дней и количеств выходов в них.
     * @return [Collection] коллекция пар из [LocalDate] даты и [Int] количества выходов в эту дату.
     */
    fun getDaysAndOutputsCount() = runBlocking {
        (0 until daysCount).parallelMap { dateFrom.plusDays(it) to getDateOutputCount(dateFrom.plusDays(it)) }
    }

    /**
     * Метод определения медианы из коллекции значений.
     *
     * @param collection    [List]      коллекция [Double] значений с плавающей точкой.
     *
     * @return              [Double]    значение медианы.
     */
    private fun getMedian(collection: List<Double>): Double =
        // Если количество значений - нечётное число
        if (collection.size % 2 != 0) collection[collection.size / 2]
        // иначе, если количество значений - чётное число
        else (collection[collection.size / 2] + collection[collection.size / 2 - 1]) / 2

    /**
     * Метод проверки превышения инвентаря качества периода.
     *
     * @param isPrime   [Boolean]   признак "инвентарь prime".
     * @param inventory [Double]    инвентарь, который может набрать ролик.
     *
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    fun isInventoryExceeded(isPrime: Boolean, inventory: Double) =
        if (isPrime) getSortedFilms().sumOf {
            it.currentInventoryPrime
        } + inventory > getSortedFilms().sumOf {
            it.requiredInventoryPrime
        } || isTotalInventoryExceeded(inventory) ||
        parameters.isInventoryExceeded(isPrime, inventory)
        else getSortedFilms().sumOf {
            it.currentInventoryOffPrime
        } + inventory > getSortedFilms().sumOf {
            it.requiredInventoryOffPrime
        } || isTotalInventoryExceeded(inventory) ||
        parameters.isInventoryExceeded(isPrime, inventory)


    /**
     * Метод проверки превышения всего инвентаря периода.
     *
     * @param inventory [Double]    инвентарь, который может набрать ролик.
     *
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    private fun isTotalInventoryExceeded(inventory: Double) =
        getSortedFilms().sumOf {
            it.currentInventoryTotal
        } + inventory > getSortedFilms().sumOf {
            it.requiredInventoryTotal
        }
}