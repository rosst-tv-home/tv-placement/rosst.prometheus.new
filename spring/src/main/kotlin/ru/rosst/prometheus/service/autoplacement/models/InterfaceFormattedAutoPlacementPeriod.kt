package ru.rosst.prometheus.service.autoplacement.models

import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.AutoPlacementTime
import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.entity.MediaPlan
import java.time.LocalDate

/**
 * @property dateFrom   [LocalDate]     дата начала периода.
 * @property dateBefore [LocalDate]     дата окончания периода.
 * @property mediaPlan  [MediaPlan]     медиаплан.
 * @property times      [Iterable]      коллекция [AutoPlacementTime] временных интервалов периода.
 * @property films      [Iterable]      коллекция [FormattedAutoPlacementFilm] роликов периода.
 * @property blocks     [Iterable]      коллекция [Block] рекламных блоков.
 * @property segments   [Iterable]      коллекция сегментов с [Map] квантилями внутри.
 */
interface InterfaceFormattedAutoPlacementPeriod {
    val parameters: AutoPlacementParameter
    val dateFrom: LocalDate
    val dateBefore: LocalDate
    val mediaPlan: MediaPlan
    val times: Iterable<AutoPlacementTime>
    val films: Iterable<FormattedAutoPlacementFilm>
    val blocks: Iterable<Block>
    val segments: List<Map<Int, Iterable<Block>>>

    /**
     * Метод определения среднего количества сегментов, которые нужно создать в этом периоде.
     *
     * @param filmsInventory    [Double] суммарный плановый инвентарь роликов в периоде.
     * @param blocksInventory   [Double] суммарный свободный инвентарь рекламных блоков в периоде.
     * @param blocksCount       [Int]       количество рекламных блоков в периоде.
     *
     * @return [Int] среднее количество сегментов, которые нужно создать в этом периоде.
     */
    fun getSegmentCount(filmsInventory: Double, blocksInventory: Double, blocksCount: Int): Int

    /**
     * Метод определения среднего количества рекламных блоков, которые нужно поместить в каждый сегмент.
     *
     * @param filmsInventory    [Double]    суммарный плановый инвентарь роликов в периоде.
     * @param blocksInventory   [Double]    суммарный свободный инвентарь рекламных блоков в периоде.
     *
     * @return [Int] среднее количество рекламных блоков, которые нужно поместить в каждый сегмент.
     */
    fun getBlockCountInSegment(filmsInventory: Double, blocksInventory: Double): Int
}