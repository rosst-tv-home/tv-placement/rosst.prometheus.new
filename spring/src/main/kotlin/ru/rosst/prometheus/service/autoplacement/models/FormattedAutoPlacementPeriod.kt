package ru.rosst.prometheus.service.autoplacement.models

import kotlinx.coroutines.runBlocking
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.AutoPlacementTime
import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.entity.MediaPlan
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.math.ceil

/**
 * Модель периода для автоматического размещения.
 *
 * @param mediaPlan медиаплан.
 * @param dateFrom [LocalDate] дата начала периода размещения.
 * @param dateBefore [LocalDate] дата окончания периода размещения.
 * @param films [Iterable] коллекция [FormattedAutoPlacementFilm] роликов и их инвентаря в периоде.
 *
 * @property times вычисляемое свойство. [Iterable] коллекция [Pair] пар [LocalDateTime] периодов размещения.
 * @property blocks вычисляемое свойство. [Iterable] коллекция [Block] рекламных блоков.
 * @property segments вычисляемое свойство. коллекция рекламных блоков,
 * сгруппированных по сегментам и квантилям по прогнозному рейтингу.
 */
class FormattedAutoPlacementPeriod(
    override val parameters: AutoPlacementParameter,
    override val mediaPlan: MediaPlan,
    override val times: List<AutoPlacementTime>,
    override val dateFrom: LocalDate,
    override val dateBefore: LocalDate,
    override val films: List<FormattedAutoPlacementFilm>
) : InterfaceFormattedAutoPlacementPeriod {
    override var blocks: Iterable<Block> = listOf()

    override val segments: List<Map<Int, Iterable<Block>>> = emptyList()
        get() {
            // Если свойсво не пустое - не пересчитываем значение
            if (field.count() != 0 || blocks.count() == 0) return field

            // Отсортированные блоки по вермени выхода
            val sortedBlocks = blocks.sortedBy { it.time }

            // Суммарный инвентарь роликов
            val filmsInventory = films.sumOf { it.inventory }

            // Суммарный инвентарь сетки
            val blocksInventory = blocks.sumOf { block ->
                val drawnInRating = block.assignedRatings.firstOrNull { it.order.id == mediaPlan.order.id }
                return@sumOf drawnInRating?.rating ?: block.ratingForecast
            }

            // Количество сегментов
            val segmentCount = getSegmentCount(filmsInventory, blocksInventory, blocks.count())
            // Количество блоков в одном сегменте
            val blockCountInSegment = getBlockCountInSegment(filmsInventory, blocksInventory)

            // Смещение
            val offset =
                if (blockCountInSegment + blockCountInSegment / 2 >= sortedBlocks.count()) (sortedBlocks.count() / 2) - 1
                else blockCountInSegment + blockCountInSegment / 2

            val offsetList = sortedBlocks.subList(0, offset)
            val otherPartOfList = sortedBlocks.subList(offset, sortedBlocks.lastIndex + 1)
            val unionList = otherPartOfList.union(offsetList)

            return runBlocking {
                unionList
                    .chunked(if (segmentCount > 1) segmentCount else unionList.size)
                    .parallelMap {
                        // Сортируем рекламные блоки по убыванию прогнозного рейтинга
                        val blocksByDescendingRating = it.sortedByDescending { block ->
                            block.assignedRatings.firstOrNull { bdr ->
                                bdr.order.id == mediaPlan.order.id
                            }?.rating ?: block.ratingForecast
                        }
                        // Ищем медиану рейтинга сегмента
                        val segmentRatingMedian = getSegmentRatingMedian(blocksByDescendingRating)
                        // Ищем медиану рейтинга выше медианы рейтинга сегмента
                        val segmentRatingMedianUpper = getSegmentRatingMedian(blocksByDescendingRating.filter { block ->
                            block.assignedRatings.firstOrNull { bdr -> bdr.order.id == mediaPlan.order.id }?.rating ?: block.ratingForecast >= segmentRatingMedian.second &&
                            blocksByDescendingRating.indexOf(block) <= segmentRatingMedian.first
                        })
                        // Ищем медиану рейтинга ниже медианы рейтинга сегмента
                        val segmentRatingMedianLower = getSegmentRatingMedian(blocksByDescendingRating.filter { block ->
                            block.assignedRatings.firstOrNull { bdr -> bdr.order.id == mediaPlan.order.id }?.rating ?: block.ratingForecast <= segmentRatingMedian.second &&
                            blocksByDescendingRating.indexOf(block) >= segmentRatingMedian.first
                        })

                        // Квантили сегмента
                        mapOf(
                            0 to blocksByDescendingRating.filter { block ->
                                block.assignedRatings.firstOrNull { bdr -> bdr.order.id == mediaPlan.order.id }?.rating ?: block.ratingForecast >= segmentRatingMedianUpper.second &&
                                blocksByDescendingRating.indexOf(block) <= segmentRatingMedianUpper.first
                            },
                            1 to blocksByDescendingRating.filter { block ->
                                block.assignedRatings.firstOrNull { bdr -> bdr.order.id == mediaPlan.order.id }?.rating ?: block.ratingForecast in segmentRatingMedian.second..segmentRatingMedianUpper.second &&
                                blocksByDescendingRating.indexOf(block) in segmentRatingMedianUpper.first..segmentRatingMedian.first
                            },
                            2 to blocksByDescendingRating.filter { block ->
                                block.assignedRatings.firstOrNull { bdr -> bdr.order.id == mediaPlan.order.id }?.rating ?: block.ratingForecast in segmentRatingMedianLower.second..segmentRatingMedian.second &&
                                blocksByDescendingRating.indexOf(block) in segmentRatingMedian.first..segmentRatingMedianLower.first + segmentRatingMedian.first
                            },
                            3 to blocksByDescendingRating.filter { block ->
                                block.assignedRatings.firstOrNull { bdr -> bdr.order.id == mediaPlan.order.id }?.rating ?: block.ratingForecast <= segmentRatingMedianLower.second &&
                                blocksByDescendingRating.indexOf(block) >= segmentRatingMedianLower.first + segmentRatingMedian.first
                            }
                        )
                    }
            }
        }

    /**
     * Метод определения количества сегментов, на которые нужно разбить коллекцию рекламных блоков.
     *
     * @return [Int] количество сегментов.
     */
    override fun getSegmentCount(filmsInventory: Double, blocksInventory: Double, blocksCount: Int): Int {
        // Количество сегментов: инв.роликов * кол-во блоков в сетке / инвентарь сетки
        return ceil(filmsInventory * blocksCount / blocksInventory).toInt()
    }

    /**
     * Метод определения количества блоков в сегменте.
     *
     * @return [Int] количество рекламных блоков в одном сегменте.
     */
    override fun getBlockCountInSegment(filmsInventory: Double, blocksInventory: Double): Int {
        // Количество рекламныз блоков в сегменте: инв. сетки / инв. роликов
        return (blocksInventory / filmsInventory).toInt()
    }

    /**
     * Метод поиска медианы рейтинга в коллекции блоков.
     *
     * @param blocks [List] коллекция  [Block] рекламных блоков.
     * @return [Pair] пара из [Int] и [Double], где первый аргумент - индекс медианы, второй - рейтинг.
     */
    private fun getSegmentRatingMedian(blocks: List<Block>) =
        if (blocks.size % 2 == 0) {
            val nextBlock = blocks[blocks.size / 2]
            val previousBlock = blocks[blocks.size / 2 - 1]

            val previousBlockRating = previousBlock.assignedRatings.firstOrNull { bdr ->
                bdr.order.id == mediaPlan.order.id
            }?.rating ?: previousBlock.ratingForecast

            val nextBlockRating = nextBlock.assignedRatings.firstOrNull { bdr ->
                bdr.order.id == mediaPlan.order.id
            }?.rating ?: nextBlock.ratingForecast

            (blocks.size / 2) to (previousBlockRating + nextBlockRating) / 2
        } else {
            val block = blocks[(blocks.size.toDouble() / 2 - 0.5).toInt()]

            val blockRating = block.assignedRatings.firstOrNull { bdr ->
                bdr.order.id == mediaPlan.order.id
            }?.rating ?: block.ratingForecast

            (blocks.size / 2 - 0.5).toInt() to blockRating
        }
}