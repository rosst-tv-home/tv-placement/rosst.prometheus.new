package ru.rosst.prometheus.service.autoplacement.enchanced

/**
 * Модель ролика.
 *
 * @param id                            [Int]                       идентификатор связи ролика с медиапланом.
 * @param primePercent                  [Double]                    процент инвентаря в prime.
 * @param requiredInventoryTotal        [Double]                    плановый инвентарь ролика.
 *
 * @property timing                     [EnhancedPlacementTiming]   хронометраж - группа роликов, к которой принадлежит ролик.
 *
 * @property requiredInventoryPrime     [Double]                    плановый инвентарь ролика в prime.
 * @property requiredInventoryOffPrime  [Double]                    плановый инвентарь ролика в off prime.
 *
 * @property currentInventoryPrime      [Double]                    текущий инвентарь ролика в prime.
 * @property currentInventoryOffPrime   [Double]                    текущий инвентарь ролика в off prime.
 * @property currentInventoryTotal      [Double]                    текущий инвентарь ролика (вычисляемое).
 */
class EnhancedPlacementFilm(
    val id: Int,
    val primePercent: Double,
    val requiredInventoryTotal: Double
) {
    lateinit var timing: EnhancedPlacementTiming

    val requiredInventoryPrime = requiredInventoryTotal * primePercent
    val requiredInventoryOffPrime = requiredInventoryTotal - requiredInventoryPrime

    var currentInventoryPrime = 0.0
    var currentInventoryOffPrime = 0.0

    val currentInventoryTotal get() = currentInventoryPrime + currentInventoryOffPrime

    /**
     * Метод проверки превышения инвентаря качества ролика в периоде.
     * @param isPrime   [Boolean]   признак "инвентарь prime".
     * @param inventory [Double]    значение инвентаря качества ролика, который может набрать.
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    fun isInventoryExceeded(isPrime: Boolean, inventory: Double) =
        if (isPrime)
            currentInventoryPrime + inventory > requiredInventoryPrime ||
            isTotalInventoryExceeded(inventory) ||
            timing.isInventoryExceeded(isPrime, inventory)
        else
            currentInventoryOffPrime + inventory > requiredInventoryOffPrime ||
            isTotalInventoryExceeded(inventory) ||
            timing.isInventoryExceeded(isPrime, inventory)

    /**
     * Метод проверки превышения всего инвентаря ролика в периоде.
     * @param inventory [Double]    значение инвентаря, который может набрать ролик.
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    private fun isTotalInventoryExceeded(inventory: Double) = currentInventoryTotal + inventory > requiredInventoryTotal
}