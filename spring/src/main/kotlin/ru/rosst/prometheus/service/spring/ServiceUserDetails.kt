package ru.rosst.prometheus.service.spring

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import ru.rosst.prometheus.service.ServiceUsers

@Service
class ServiceUserDetails : UserDetailsService {
    @Autowired
    private lateinit var srvUsers: ServiceUsers

    override fun loadUserByUsername(login: String): UserDetails {
        val user = srvUsers.findByLogin(login)
            ?: throw UsernameNotFoundException("Пользователь с логином $login не найден.")
        return User
            .withUsername(user.login)
            .password(user.password)
            .authorities(SimpleGrantedAuthority("ROLE_ADMIN"))
            .build()
    }
}