package ru.rosst.prometheus.service.autoplacement.enchanced

/**
 * Модель хронометража - группы роликов.
 * @param duration  [Byte]                      значение хронометража - группы роликов.
 * @param films     [Iterable]                  коллекция [EnhancedPlacementFilm] роликов.
 * @property period [EnhancedPlacementPeriod]   период, которому принадлежит хронометраж - группа роликов.
 */
class EnhancedPlacementTiming(
    val duration: Byte,
    val films: Iterable<EnhancedPlacementFilm>
) {
    lateinit var period: EnhancedPlacementPeriod

    val requiredInventoryPrime get() = films.sumOf { it.requiredInventoryPrime }
    val requiredInventoryOffPrime get() = films.sumOf { it.requiredInventoryOffPrime }
    val requiredInventoryTotal get() = requiredInventoryPrime + requiredInventoryOffPrime

    val currentInventoryPrime get() = films.sumOf { it.currentInventoryPrime }
    val currentInventoryOffPrime get() = films.sumOf { it.currentInventoryOffPrime }
    val currentInventoryTotal get() = currentInventoryPrime + currentInventoryOffPrime

    /**
     * Метод проверки превышения инвентаря качества группы роликов.
     * @param isPrime   [Boolean]   признак "инвентарь prime".
     * @param inventory [Double]    значение инвентаря, которое может быть набрано.
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    fun isInventoryExceeded(isPrime: Boolean, inventory: Double) =
        if (isPrime) films.sumOf {
            it.currentInventoryPrime
        } + inventory > films.sumOf {
            it.requiredInventoryPrime
        } || isTotalInventoryExceeded(inventory) ||
        period.isInventoryExceeded(isPrime, inventory)
        else films.sumOf {
            it.currentInventoryOffPrime
        } + inventory > films.sumOf {
            it.requiredInventoryOffPrime
        } || isTotalInventoryExceeded(inventory) ||
        period.isInventoryExceeded(isPrime, inventory)

    /**
     * Метод проверки превышения всего инвентаря группы роликов.
     * @param inventory [Double]    значение инвентаря, которое может быть набрано.
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    private fun isTotalInventoryExceeded(inventory: Double) =
        films.sumOf {
            it.currentInventoryTotal
        } + inventory > films.sumOf {
            it.requiredInventoryTotal
        }
}