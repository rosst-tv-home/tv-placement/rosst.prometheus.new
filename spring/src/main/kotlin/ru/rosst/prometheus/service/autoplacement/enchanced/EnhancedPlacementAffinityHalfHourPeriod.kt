package ru.rosst.prometheus.service.autoplacement.enchanced

import java.time.LocalTime

/**
 * Модель временного интервала со значением аффинитивности и принадлежащими рекламными блоками.
 *
 * @param timeFrom      [LocalTime]                 время начала периода.
 * @param timeBefore    [LocalTime]                 время окончания периода.
 * @param isWeekend     [Boolean]                   признак "временной период выходного дня".
 * @param affinity      [Double]                    значение аффинитивности рекламных блоков в этом временном интервале.
 * @param blocks        [Iterable]                  коллекция [EnhancedPlacementBlock] рекламных блоков, в этом временном интервале.
 *
 * @property period     [EnhancedPlacementPeriod]   период, которому принадлежит этот временной интервал аффинитивности с рекламными блоками.
 */
class EnhancedPlacementAffinityHalfHourPeriod(
    val timeFrom: LocalTime,
    val timeBefore: LocalTime,
    val isWeekend: Boolean,
    val affinity: Double,
    private val blocks: MutableList<EnhancedPlacementBlock> = mutableListOf()
) {
    lateinit var period: EnhancedPlacementPeriod

    /**
     * Метод добавления рекламных блоков, которые пренадлежат этому получасовому периоду.
     * @param blocks [Array] коллекция [EnhancedPlacementBlock] рекламных блоков, пренадлежащих этому получасовому периоду.
     */
    fun addBlocks(vararg blocks: EnhancedPlacementBlock) {
        // Добавление рекламных блоков
        this.blocks.addAll(blocks)
    }

    /**
     * Метод получения рекламных блоков получасового периода.
     * @return [List] коллекция [EnhancedPlacementBlock] рекламных блоков.
     */
    fun getBlocks(): List<EnhancedPlacementBlock> = blocks

    /**
     * Метод получения рекламных блоков получасового периода, отсортированных по возрастанию количества выходов.
     * @return [List] коллекция [EnhancedPlacementBlock] рекламных блоков, отсортированных по возрастанию количества выходов.
     */
    fun getSortedBlocks(): List<EnhancedPlacementBlock> = blocks.sortedBy { period.getDateOutputCount(it.date) }
}