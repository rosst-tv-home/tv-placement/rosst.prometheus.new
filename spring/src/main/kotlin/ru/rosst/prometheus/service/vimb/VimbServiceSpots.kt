package ru.rosst.prometheus.service.vimb

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.models.errors.response.ResponseError
import ru.rosst.prometheus.models.spots.request.RequestSpotAdd
import ru.rosst.prometheus.models.spots.request.RequestSpotDelete
import ru.rosst.prometheus.models.spots.request.RequestSpots
import ru.rosst.prometheus.models.spots.response.ResponseAssignedRating
import ru.rosst.prometheus.models.spots.response.ResponseSpot
import ru.rosst.prometheus.models.spots.response.ResponseSpotAdd
import ru.rosst.prometheus.models.spots.response.ResponseSpots
import ru.rosst.prometheus.service.*
import java.time.LocalDate

/**
 * Сервис загрузки спотов vimb.
 *
 * Сервисы взаимодействия с VIMB API:
 * @property vimbSrvRequest         [VimbServiceRequest]        сервис отправки запросов к VIMB API.
 *
 * Сервисы управления данными базы данных:
 * @property srvRanks               [ServiceRanks]              сервис управления данными таблицы ["ranks"] "ранги".
 * @property srvSpots               [ServiceSpots]              сервис управления данными таблицы ["spots"] "споты".
 * @property srvBlocks              [ServiceBlocks]             сервис управления данными таблицы ["blocks"] "блоки".
 * @property srvChannels            [ServiceChannels]           сервис управления данными таблицы ["channels"] "каналы".
 * @property srvAdvertisers         [ServiceAdvertisers]        сервис управления данными таблицы ["advertisers"] "рекламодатели".
 * @property srvSellingDirections   [ServiceSellingDirections]  сервис управления данными таблицы ["selling_directions"] "направления продажи".
 */
@Component
class VimbServiceSpots : AbstractService() {
    @Autowired
    private lateinit var srvRanks: ServiceRanks

    @Autowired
    private lateinit var srvSpots: ServiceSpots

    @Autowired
    private lateinit var srvBlocks: ServiceBlocks

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvMediaPlans: ServiceMediaPlans

    @Autowired
    private lateinit var srvAdvertisers: ServiceAdvertisers

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    @Autowired
    private lateinit var vimbSrvRequest: VimbServiceRequest

    /**
     * Метод отправки запроса на размещение спота.
     *
     * @param blockID   [Long]  идентификатор рекламного блока.
     * @param linkID    [Int]   идентификатор ролика медиаплана.
     */
    fun addSpot(blockID: Long, linkID: Int): Any? {
        when (val response = vimbSrvRequest.request(RequestSpotAdd(blockID, linkID))) {
            is ResponseSpotAdd -> {
                info("Спот установлен: ${response.spotID}")
                return response
            }
            is ResponseError -> {
                warning("Не удалост установить спот")
                // 5710 - нарушение запрета на размещение
                // 101112 - пустые линейки и блоки "Нельзя размещать спот на данном канале этого медиаплана"
                return if (listOf(
                        101150,
                        101127,
                        5710,
                        101112,
                        8583,
                        11008,
                        8451
                    ).contains(response.details.description.code)
                )
                    blockID
                else null
            }
        }
        return null
    }

    /**
     * Метод запроса удаления спота.
     *
     * @param spotID [Long] идентификатор спота.
     */
    fun deleteSpot(spotID: Long): Any? {
        when (val response = vimbSrvRequest.request(RequestSpotDelete(spotID))) {
            is ResponseError -> {
                warning("Не удалось удалить спот ID $spotID.")
                return null
            }
        }
        return null
    }

    /**
     * Метод отправки запроса спотов.
     *
     * @param id            [Byte]      идентификатор направления продаж.
     * @param dateFrom      [LocalDate] дата начала периода.
     * @param dateBefore    [LocalDate] дата окончания периода.
     * @param channels      [Set]       коллекция [Int] идентификаторов каналов.
     * @param advertisers   [Set]       коллекция [Int] идентификаторов рекламодателей.
     */
    fun downloadSpots(
        id: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Set<Int>? = null,
        advertisers: Set<Int>? = null
    ) {
        info(
            """
            Начало: загрузка спотов:
            направление продаж [$id],
            дата начала периода [${dateFrom}],
            дата окончания периода [${dateBefore}],
            количество указанных каналов [${channels?.count() ?: 0}],
            количество указанных рекламодателей [${advertisers?.count() ?: 0}]...
        """
        )

        // Если дата окончания периода больше даты начала - выбрасываем исключение
        if (dateBefore < dateFrom) {
            throw IllegalArgumentException("Дата окончания периода не может быть больше даты начала периода.")
        }

        // Направление продаж
        val sellingDirection = srvSellingDirections.findById(id)

        // Каналы
        val channelsList =
            // Если каналы не указаны - значит запрашиваем по всем каналам
            if (channels.isNullOrEmpty()) srvChannels.findAllBySellingDirectionID(sellingDirection.id)
            // Иначе ищем те каналы, которые указаны
            else srvChannels.findAllByIDs(channels)

        // Рекламодатели
        val advertisersList =
            // Если рекламодатели не указаны - значит запрашиваем по всем рекламодателям
            if (advertisers.isNullOrEmpty()) srvAdvertisers.findAllBySellingDirectionID(sellingDirection.id)
                .map { it.advertiser }.toSet()
            // Иначе ищем тех рекламодателей, которые указаны
            else srvAdvertisers.findAllByIDs(advertisers).map { it.advertiser }.toSet()

        // Ранги размещения
        val ranks = srvRanks.findAll()

        // Отправляем запрос
        val response = vimbSrvRequest.request(
            RequestSpots(
                sellingDirection.id,
                dateFrom,
                dateBefore,
                channels = channels ?: emptySet(),
                advertisers = advertisers ?: emptySet()
            ), true
        ) as? ResponseSpots ?: return

        // Идентификаторы рекламных блоков
        val spotsBlocks = response.spots?.map { it.blockID }?.toSet() ?: setOf()
        // Идентифиаторы роликов в медиапланах
        val spotsMediaPlanFilmLinks = response.spots?.map { it.linkID }?.toSet() ?: setOf()

        // Ищем рекламные блоки в базе данных по их идентификаторам
        val blocks = srvBlocks.findAllByIds(spotsBlocks)
        // Ищем ролики в медиапланх по их идентификаторам
        val mediaPlanFilmLinks = srvMediaPlans.findLinksByIds(spotsMediaPlanFilmLinks)

        // Фильтруем споты
        val filteredSpots = filterSpots(
            response.spots ?: setOf(),
            blocks.map { it.id }.toSet(),
            mediaPlanFilmLinks.map { it.id }.toSet()
        )

        // Конвертируем споты
        val convertedSpots = convertSpots(
            ranks,
            filteredSpots,
            blocks,
            mediaPlanFilmLinks
        )

        // Сохраняем конвертированные споты в базу данных
        if (convertedSpots.isNullOrEmpty()) {
            srvSpots.save(null, dateFrom, dateBefore, channelsList, advertisersList)
        } else {
            // Сохраняем текущие споты, ищем и отмечаем недействительные записи о спотах в базе данных
            srvSpots.save(convertedSpots, dateFrom, dateBefore, channelsList, advertisersList)
        }

        // Проверяем, есть ли втянутые рейтинги рекламных блоков, если нет - завершаем выполнение метода
        if (response.assignedRatings.isNullOrEmpty()) {
            info("Данные втянутых рейтингов vimb пусты - завершение метода загрузки спотов vimb.")
            return
        }

        // Идентификаторы заказов
        val blockDrawnRatingOrders = response.assignedRatings?.map { it.orderID }?.toSet() ?: setOf()
        // Ищем заказы в базе данных по их идентификаторам
        val orders = srvMediaPlans.findAllOrdersByIds(blockDrawnRatingOrders)

        // Фильтруем втянутые рейтинги
        val filteredDrawnRatings = filterBlockDrawnRatings(
            response.assignedRatings ?: setOf(),
            blocks.map { it.id }.toSet(),
            orders.map { it.id }.toSet()
        )

        // Конвертируем втянутые рейтинги
        val convertedDrawnRatings = convertBlockDrawnRatings(
            filteredDrawnRatings,
            blocks, orders
        )

        // Сохраняем втянутые рейтинги в базу данных
        srvBlocks.saveDrawnRatings(convertedDrawnRatings, true, dateFrom, dateBefore, channelsList, advertisersList)
    }

    /**
     * Метод конвертации моделей втянутых рейтингов.
     *
     * @param ratings   [List]      коллекция [ResponseAssignedRating] втянутых рейтингов рекламных блоков в заказах.
     * @param blocks    [Iterable]  коллекция [Block] рекламных блоков.
     * @param orders    [Iterable]  коллекция [Order] заказов.
     *
     * @return          [List]      коллекция [BlockAssignedRating] втянутых рейтингов рекламных блоков в заказах.
     */
    private fun convertBlockDrawnRatings(
        ratings: List<ResponseAssignedRating>,
        blocks: Iterable<Block>,
        orders: Iterable<Order>
    ): List<BlockAssignedRating> = ratings.map {
        BlockAssignedRating(
            orders.first { order -> it.orderID == order.id },
            blocks.first { block -> it.blockID == block.id },
            it.rating
        )
    }

    /**
     * Метод фильтрации втянутых рейтингов. Сохраняются только те втянутые рейтинги, по которым есть данные о рекламном
     * блоке и заказе.
     *
     * @param ratings   [Iterable] коллекция [ResponseAssignedRating] втянутыхз рейтингов рекламных блоков в заказах.
     * @param blocks    [Iterable] коллекция [Long] известных идентификаторов рекламных блоков.
     * @param orders    [Iterable] коллекция [Int] известных индентификаторов заказов.
     *
     * @return [List] коллекция [ResponseAssignedRating] втянутых рейтингов.
     */
    private fun filterBlockDrawnRatings(
        ratings: Iterable<ResponseAssignedRating>,
        blocks: Iterable<Long>,
        orders: Iterable<Int>
    ): List<ResponseAssignedRating> = ratings.filter {
        blocks.contains(it.blockID) && orders.contains(it.orderID)
    }

    /**
     * Метод конвертации моделей спотов.
     *
     * @param ranks                 [List]      коллекция [Rank] рангов.
     * @param filteredSpots         [List]      коллекция [ResponseSpot] спотов.
     * @param blocks                [Iterable]  коллекция [Block] рекламных блоков.
     * @param filmsLinks    [Iterable]  коллекция [FilmXMediaPlan] роликов медиапланов.
     *
     * @return                      [List]      коллекция [Spot] спотов.
     */
    private fun convertSpots(
        ranks: List<Rank>,
        filteredSpots: List<ResponseSpot>,
        blocks: Iterable<Block>,
        filmsLinks: Iterable<FilmXMediaPlan>
    ): List<Spot> = filteredSpots.map {
        val block = blocks.first { block -> it.blockID == block.id }
        Spot(
            it.spotID,
            it.isHumanPlacement,
            block,
            filmsLinks.first { link -> it.linkID == link.id }
        )
    }

    /**
     * Метод фильтрации спотов. Сохраняются только те споты, по которым имеются данные о рекламном блоке и ролике.
     *
     * @param spots     [Iterable] коллекция [ResponseSpot] спотов.
     * @param blocks    [Iterable] коллекция [Long] известных идентификаторов блоков.
     * @param links     [Iterable] коллекция [Int] известных идентификаторов роликов в медиапланах.
     *
     * @return [List] коллекция [ResponseSpot] спотов.
     */
    private fun filterSpots(
        spots: Iterable<ResponseSpot>,
        blocks: Iterable<Long>,
        links: Iterable<Int>
    ): List<ResponseSpot> = spots.filter {
        it.rankID in 1..2 && blocks.contains(it.blockID) && links.contains(it.linkID)
    }
}
