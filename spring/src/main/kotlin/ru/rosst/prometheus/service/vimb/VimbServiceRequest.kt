package ru.rosst.prometheus.service.vimb

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.RequestDate
import ru.rosst.prometheus.models.advertisers.request.RequestAdvertisers
import ru.rosst.prometheus.models.blocks.request.RequestBlocks
import ru.rosst.prometheus.models.channels.request.RequestChannels
import ru.rosst.prometheus.models.errors.response.ResponseError
import ru.rosst.prometheus.models.mediaplans.request.RequestMediaPlans
import ru.rosst.prometheus.models.ranks.request.RequestRanks
import ru.rosst.prometheus.models.spots.request.RequestSpotAdd
import ru.rosst.prometheus.models.spots.request.RequestSpots
import ru.rosst.prometheus.service.ServiceRequestDates
import ru.rosst.prometheus.services.Soap
import java.time.LocalDateTime

/**
 * Сервис отправки запросов к VIMB API.
 *
 * Свойства программы:
 * @property crtPath            [String]                путь до файла сертификата VIMB API.
 * @property crtPassword        [String]                пароль от сертификата VIMB API.
 *
 * Сервисы управления данными в базе данных:
 * @property srvRequestDates   [ServiceRequestDates]  сервис управления записями о последних запросах к VIMB API.
 */
@Service
class VimbServiceRequest : AbstractService() {
    @Value("\${vimb.certificate.path}")
    private lateinit var crtPath: String

    @Value("\${vimb.certificate.password}")
    private lateinit var crtPassword: String

    @Autowired
    private lateinit var srvRequestDates: ServiceRequestDates

    /**
     * Метод получения экземпляра компонента [Soap] запроса к VIMB API.
     *
     * @return экземпляр компонента [Soap] запроса к VIMB API.
     */
    @Bean
    private fun soapInstance() = Soap(
        VIMB_CERTIFICATE_FILE_INPUT_STREAM = ClassPathResource(crtPath).inputStream,
        VIMB_CERTIFICATE_PASSWORD = crtPassword
    )

    /**
     * Метод отправки запроса к VIMB API.
     *
     * @param model [Any] экземпляр модели запроса VIMB API.
     *
     * @return [Any] экземпляр модели ответа на запрос VIMB API.
     */
    fun request(model: Any, isUsingJackson: Boolean = false): Any? {
        try {
            // Определяем "какой запрос отправляется"
            val what: String = defineRequestName(model)

            info("Начало: отправка запроса $what в VIMB API.")

            // Проверяем, доступна ли отправка запроса?
            if (!isRequestAllowed(model)) return null

            // Отправляем запрос
            val response = soapInstance().getVimbInfoStream(model)

            info("Завершение: отправка запроса $what в VIMB API.")

            return response
        } catch (exception: Exception) {
            when (exception) {
                is ClassCastException ->
                    error("Неудалось преобразовать ответ VIMB API в модель.")
                is ResponseError -> {
                    error(exception.details.description.message)
                    return exception
                }
                else ->
                    error(exception.localizedMessage)
            }
            return null
        }
    }

    /**
     * Метод проверки записи последнего запроса к VIMB API.
     *
     * @param model [Any] модель запроса VIMB API.
     *
     * @return [Boolean] признак загрузки "доступна".
     */
    private fun isRequestAllowed(model: Any): Boolean {
        if (model is RequestBlocks || model is RequestMediaPlans) return true
        info("Начало: проверка записи о последнем запросе [${model.javaClass.simpleName}] к VIMB API...")

        // Определяем название, по которому будем искать запись в базе данных,
        // если не удаётся определить - сразу разрешаем отправку запроса.
        val name = defineColumnName(model) ?: return true

        // Текущие дата и время
        val currentDate = LocalDateTime
            .now()
            .withSecond(0)
            .withNano(0)

        // Данные о последнем запросе из базы данных
        val recentRequestData = srvRequestDates.findFetchDateByName(name)

        // Если данные о последнем запросе не были найдены в базе данных
        // создаём новую запись и сразу разрешаем отправку запроса.
        if (recentRequestData == null) {
            warning("Запись последнего запроса [$name] отсутсвует в базе данных - создание новой.")

            // Определяем, сколько нужно накинуть минут (время, когда следующий запрос будет доступен)
            val nextDateAdditionMinutes = defineNextRequestDateAdditionMinutes(model)

            // Сохраняем новую запись последнего запроса
            srvRequestDates.saveAndFlush(
                RequestDate(
                    name,
                    currentDate,
                    currentDate.plusMinutes(nextDateAdditionMinutes)
                )
            )

            info("Новая запись о последнем запросе [$name] успешно создана в базе данных.")
            info("Завершение: проверка записи о последнем запросе [${model.javaClass.simpleName}] к VIMB API в базе данных.")
            return true
        }

        // Если текущее время и дата меньше чем дата следующего запроса к VIMB API - запрос ещё недоступен
        if (currentDate < recentRequestData.nextDate) {
            info(
                """
                Отправка запроса [${model.javaClass.simpleName}] ещё не доступна,
                будет доступна с: [${recentRequestData.nextDate}].
            """
            )
            info(
                """
                Завершение: проверка записи о последнем запросе 
                [${model.javaClass.simpleName}] к VIMB API в базе данных.
            """
            )
            return false
        }

        info("Отправка запроса доступна, обновление записи и сохранение в базу данных.")

        // Определяем, сколько нужно накинуть минут (время, когда следующий запрос будет доступен)
        val nextDateAdditionMinutes = defineNextRequestDateAdditionMinutes(model)

        recentRequestData.previousDate = currentDate
        recentRequestData.nextDate = currentDate.plusMinutes(nextDateAdditionMinutes)

        srvRequestDates.saveAndFlush(recentRequestData)

        info(
            """
            Завершение: проверка записи о последнем запросе 
            [${model.javaClass.simpleName}] к VIMB API в базе данных.
        """
        )

        return true
    }

    /**
     * Метод определения названия запроса к VIMB API для логгирования.
     *
     * @param model [Any]       модель запроса к VIMB API.
     *
     * @return      [String]    название запроса.
     */
    private fun defineRequestName(model: Any): String = when (model) {
        is RequestRanks -> "[коллекция рангов]"
        is RequestSpots -> "[коллекция спотов]"
        is RequestSpotAdd -> "[добавление спота]"
        is RequestBlocks -> "[коллекция рекламных блоков]"
        is RequestChannels -> "[коллекция каналов]"
        is RequestMediaPlans -> "[коллекция медиапланов]"
        is RequestAdvertisers -> "[коллекция рекламодателей]"
        else -> "[не определено]"
    }

    /**
     * Метод определения названия записи в базе данных, в которой хранятся данные о последнем запросе к VIMB API.
     *
     * @param model [Any] модель запроса к VIMB API.
     *
     * @return [String] название записи или null.
     */
    private fun defineColumnName(model: Any): String? = when (model) {
        is RequestSpots -> "spots"
        is RequestBlocks -> "blocks"
        is RequestChannels -> "channels"
        is RequestMediaPlans -> "media_plans"
        is RequestAdvertisers -> "advertisers"
        else -> {
            warning("Не удалось проверить запись последнего запроса [${model.javaClass.simpleName}] к VIMB API.")
            info("Завершение: проверка записи о последнем запросе [${model.javaClass.simpleName}] к VIMB API в базе данных.")
            null
        }
    }

    /**
     * Метод определения количества минут от текущего времени,
     * через которое будет доступен следующий запрос к VIMB API.
     *
     * @param model [Any] модель запроса к VIMB API.
     * @return количество минут [Long], через которое будет доступен следующий запрос.
     */
    private fun defineNextRequestDateAdditionMinutes(model: Any): Long = when (model) {
        is RequestSpots, is RequestBlocks -> 2
        is RequestChannels, is RequestAdvertisers -> 17
        is RequestMediaPlans -> 3
        else -> 0
    }
}