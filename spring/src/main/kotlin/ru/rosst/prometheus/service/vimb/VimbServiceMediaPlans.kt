package ru.rosst.prometheus.service.vimb

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.models.mediaplans.request.RequestMediaPlans
import ru.rosst.prometheus.models.mediaplans.response.ResponseMediaPlan
import ru.rosst.prometheus.models.mediaplans.response.ResponseMediaPlans
import ru.rosst.prometheus.service.*
import java.time.LocalDate
import java.time.temporal.ChronoUnit

/**
 * Сервис взаимодействия с медиапланами в vimb.
 *
 * @property vimbSrvRequest сервис отправки запросов к VIMB API
 *
 * @property srvRanks               сервис управления рангами размещения из базы данных
 * @property srvChannels            сервис управления каналами из базы данных
 * @property srvAdvertisers         сервису управления рекламодателями из базы данных
 * @property srvParameters          сервис управления параметрами автоматического размещения медиапланов из базы данных
 * @property srvMediaPlans          сервис управления медиапланами из базы данных
 * @property srvRequestDates        сервис управления записями о последних загрузках vimb из базы данных
 * @property srvSellingDirections   сервис управления направлениями продаж из базы данных
 */
@Component
class VimbServiceMediaPlans : AbstractService() {
    @Autowired
    private lateinit var srvRequestVolumes: ServiceRequestVolumes

    @Autowired
    private lateinit var vimbSrvRequest: VimbServiceRequest

    @Autowired
    private lateinit var srvRanks: ServiceRanks

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvAdvertisers: ServiceAdvertisers

    @Autowired
    private lateinit var srvParameters: ServiceAutoPlacementParameters

    @Autowired
    private lateinit var srvMediaPlans: ServiceMediaPlans

    @Autowired
    private lateinit var srvRequestDates: ServiceRequestDates

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    /**
     * Метод загрузки медиапланов и связанных с ними заказов, сделок и роликов по указанному направлению продаж,
     * рекламодателям, каналов и датам начала и окончания периода медиапланов.
     * Максимальный вес запроса: 12 месяцев
     * Время перезарядки: 600 секунд - 10 минут
     * Восстановление: 12 / 600 = 0.02 месяцев в секунду
     *
     * @param id идентификатор направления продаж.
     * @param dateFrom дата начала периода медиаплана.
     * @param dateBefore дата окончания периода медиаплана.
     * @param advertisers коллекция рекламодателей сделкок медиапланов (не обязательно).
     * @param channels коллекция каналов медиапланов (не обязательно).
     */
    fun download(
        id: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Set<Int>? = null,
        advertisers: Set<Int>? = null,
    ) {
        info(
            """
            Начало: загрузка медиапланов:
            направление продаж [${id}],
            дата начала периода медиапланов [${dateFrom}],
            дата окончания периода медиапланов [${dateBefore}],
            количество указанных каналов [${channels?.count() ?: 0}],
            количество указанных рекламодателей [${advertisers?.count() ?: 0}]
            """
        )

        info("Проверка правильности периодов медиапланов")
        if (dateBefore < dateFrom) {
            throw IllegalArgumentException(
                "Дата окончания периода медиаплана не может быть меньше даты начала периода медиаплана"
            )
        }
        info("Периоды указаны верно")

        // Проверяем объём запроса
        val blocksRV = srvRequestVolumes.get("media_plans")
        val monthsUntil = dateFrom.until(dateBefore, ChronoUnit.MONTHS) + 1
        if (blocksRV.current < monthsUntil) {
            throw IllegalArgumentException("Количество запрашиваемых месяцев превышает имеющийся объём")
        }
        // Обновляем объём запроса
        srvRequestVolumes.update("media_plans", monthsUntil.unaryMinus().toDouble())

        info("Загрузка медиапланов...")

        // Отправляем запрос к VIMB API
        val response = vimbSrvRequest.request(
            RequestMediaPlans(
                id,
                dateFrom,
                dateBefore,
                advertisers,
                channels
            )
        ) as? ResponseMediaPlans

        // Если не удалось конвертировать ответ в модель медиапланов
        if (response == null) {
            warning("Завершение: загрузка медиапланов: не удалось конвертировать ответ в модель медиапланов")
            return
        }

        info("Загрузка успешно завершена")

        if (response.mediaPlans.count() <= 0) {
            warning("Завершение: загрузка медиапланов: медиапланы не найдены в ответном сообщении")
            return
        }

        // Если была указана коллекция каналов для загрузки медиапланов, тогда ищем только эти каналы, иначе ищем все
        // каналы по указанному направлению продаж
        val dbChannels =
            if (channels.isNullOrEmpty()) srvChannels.findAllBySellingDirectionID(id)
            else srvChannels.findAllByIDs(channels)

        // Если коллекция каналов пустая
        if (dbChannels.count() <= 0) {
            warning("Завершение: загрузка медиапланов: не удалось найти указанные каналы в базе данных")
            return
        }

        // Если была указана коллекция рекламодателей для загрузки медиапланов, тогда ищем только этих рекламодателей,
        // иначе ищем всех рекламодателей по указанному направлению продаж
        val dbAdvertisers =
            if (advertisers.isNullOrEmpty()) srvAdvertisers.findAllBySellingDirectionID(id)
            else srvAdvertisers.findAllByIDs(advertisers).filter { it.sellingDirection.id == id }

        // Если коллекция рекламодателей пустая
        if (dbAdvertisers.count() <= 0) {
            warning("Завершение: загрузка медиапланов: не удалось найти указанных рекламодателей в базе данных")
            return
        }

        // Фильтруем медиапланы, отбрасываем медиапланы, которые невозможно разместить
        val mediaPlans = filter(response.mediaPlans, dbChannels)

        // Если коллекция отфильтрованных медиапланов пустая
        if (mediaPlans.count() <= 0) {
            warning("Завершение: загрузка медиапланов: после фильтрации не осталось медиапланов")
            return
        }

        // Запрашиваем ранги размещения из базы данных
        val dbRanks = srvRanks.findAll()

        // Конвертируем медиапланы vimb в модели базы данных
        val convertedMediaPlans = convert(dbRanks, dbChannels, dbAdvertisers, mediaPlans)

        srvParameters.deleteInvalidLinks(
            mediaPlans.map { it.mediaPlanID },
            mediaPlans.map { it.mediaPlanFilmLinkID }
        )

        srvParameters.deleteInvalid(
            dateFrom,
            dateBefore,
            dbChannels.map { it.id }.toSet(),
            mediaPlans.map { it.mediaPlanID }.toSet(),
            dbAdvertisers.map { it.advertiser.id }.toSet()
        )

        srvMediaPlans.deleteInvalid(
            dateFrom,
            dateBefore,
            dbChannels.map { it.id }.toSet(),
            mediaPlans.map { it.mediaPlanID }.toSet(),
            dbAdvertisers.map { it.advertiser.id }.toSet()
        )

        srvMediaPlans.deleteInvalidLinks(
            dateFrom,
            dateBefore,
            dbChannels.map { it.id }.toSet(),
            dbAdvertisers.map { it.advertiser.id }.toSet(),
            mediaPlans.map { it.mediaPlanID }.toSet(),
            convertedMediaPlans.map(FilmXMediaPlan::id).toSet()
        )

        srvMediaPlans.saveMediaPlansWithFilms(convertedMediaPlans)

        info("Завершение: загрузка медиапланов")
    }

    /**
     * Метод конвертации медиапланов vimb в модель базы данных
     *
     * @param ranks [Iterable] коллекция [Rank] рангов размещения
     * @param channels [Iterable] коллекция [Channel] каналов
     * @param advertisers [Iterable] коллекция [AdvertiserXSellingDirection] сопоставлений рекламодателей и направлений продаж
     * @param mediaPlans [Iterable] коллекция [ResponseMediaPlan] медиапланов
     *
     * @return [Iterable] коллекция [FilmXMediaPlan] сопоставлений роликов и медиапланов
     */
    private fun convert(
        ranks: Iterable<Rank>,
        channels: Iterable<Channel>,
        advertisers: Iterable<AdvertiserXSellingDirection>,
        mediaPlans: Iterable<ResponseMediaPlan>
    ): Iterable<FilmXMediaPlan> = mediaPlans.map {
        FilmXMediaPlan(
            it.mediaPlanFilmLinkID,
            it.mediaPlanFilmLinkDateFrom,
            it.mediaPlanFilmLinkDateBefore,
            MediaPlan(
                it.mediaPlanID,
                it.mediaPlanName,
                it.mediaPlanMonth,
                it.inventoryType == 13.toByte(),
                channels.first { channel -> channel.id == it.channelID },
                Order(
                    it.orderID,
                    it.orderName,
                    it.orderDateFrom,
                    it.orderDateBefore,
                    it.orderIsMultipleSpots,
                    ranks.first { rank -> rank.id == it.rankID },
                    Agreement(
                        it.agreementID,
                        it.agreementName,
                        it.agreementDateFrom,
                        it.agreementDateBefore,
                        advertisers.first { map -> map.advertiser.id == it.advertiserID }
                    )
                )
            ).apply { this.parameters = srvParameters.findByMediaPlanID(it.mediaPlanID) },
            Film(
                it.filmID,
                it.filmName,
                it.filmVersion,
                it.filmDuration,
                it.filmIsInLaw
            )
        )
    }

    /**
     * Метод фильтрации медиапланов, полученных из vimb.
     * Удаление из коллекции медиапланов c плавающим типом размещения, а также медиапланы, в которых указан канал,
     * отсутсвующий в базе данных.
     *
     * @param mediaPlans коллекция всех медиапланов vimb, полученных по заданным критериям.
     * @param channels коллекция каналов vimb, которые есть в vimb.
     *
     * @return коллекция отфильтрованных медиапланов vimb.
     */
    private fun filter(
        mediaPlans: Iterable<ResponseMediaPlan>,
        channels: Iterable<Channel>
    ): List<ResponseMediaPlan> {
        info(
            """
            Начало: фильтрация коллекции медиапланов, полученных из vimb, удаление из коллекции медиапланов с плавающим 
            размещением, количество медиапланов до фильтрации: [${mediaPlans.count()}]
            """
        )

        // Фильтрация медиапланов, полученных из vimb
        val filteredMediaPlans = mediaPlans.filter {
            it.rankID in 1..2 && channels.any { channel -> channel.id == it.channelID }
        }

        info(
            """
            Завершение: фильтрация коллекции медиапланов vimb, количество медиапланов после фильтрации: 
            [${filteredMediaPlans.count()}]
            """
        )

        // Возвращаем результат фильтрации
        return filteredMediaPlans
    }
}