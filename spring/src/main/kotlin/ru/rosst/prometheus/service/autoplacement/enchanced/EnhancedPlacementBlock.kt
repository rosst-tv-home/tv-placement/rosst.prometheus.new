package ru.rosst.prometheus.service.autoplacement.enchanced

import ru.rosst.extensions.spring.AbstractService
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * Модель размещаемого рекламного блока.
 *
 * @param id                [Long]                                      идентификатор рекламного блока.
 * @param date              [LocalDate]                                 плановая дата выхода рекламного блока.
 * @param dateTime          [LocalDateTime]                             плановая дата и время выхода рекламного блока.
 * @param isPrime           [Boolean]                                   признак "рекламный блок в prime".
 * @param isMinute          [Boolean]                                   признак "рекламный блок минутный"
 * @param volumeFree        [Short]                                     свободный объём рекламного блока.
 * @param rating            [Double]                                    прогнозный рейтинг рекламного блока.
 * @param assignedRating    [Double]                                    втянутый рейтинг рекламного блока.
 * @param spots             [MutableList]                               коллекция [EnhancedPlacementSpot] спотов рекламного блока.
 *
 * @property affinity       [EnhancedPlacementAffinityHalfHourPeriod]   временной интервал аффинитивности, к которому принадлежит рекламный блок.
 */
class EnhancedPlacementBlock(
    val isAssignedRatingsPlacement: Boolean,
    val id: Long,
    val date: LocalDate,
    val dateTime: LocalDateTime,
    val deadlineDateTime: LocalDateTime,
    val isPrime: Boolean,
    val isMinute: Boolean,
    val volumeFree: Short,
    val rating: Double,
    private var assignedRating: Double? = null,
    private val spots: MutableList<EnhancedPlacementSpot> = mutableListOf(),
    var hasOtherMediaPlanSpots: Boolean = false,
    var isInvalid: Boolean = false
) : AbstractService() {
    lateinit var affinity: EnhancedPlacementAffinityHalfHourPeriod

    /**
     * Метод получения коллекции спотов рекламного блока.
     * @return [List] коллекция [EnhancedPlacementSpot] спотов рекламного блока.
     */
    fun getSpots(): List<EnhancedPlacementSpot> = spots

    /**
     * Метод получения релевантных спотов - не помеченных как "требуется удаление".
     * @return [Collection] коллекция релевантных [EnhancedPlacementSpot] спотов.
     */
    fun getRelevantSpots(): Collection<EnhancedPlacementSpot> = spots.filterNot(EnhancedPlacementSpot::requireRemoving)

    /**
     * Метод добавления спота в коллекцию спотов рекламного блока.
     * @param film                      [EnhancedPlacementFilm] ролик.
     * @param inPlacement               [Boolean]               признак "спот в размещении".
     * @param isAllowedMultipleSpots    [Boolean]               признак "несколько роликов в один рекламный ролик".
     * @param spotID                    [Long]                  идентификатор спота.
     */
    fun addSpot(
        film: EnhancedPlacementFilm,
        inPlacement: Boolean = false,
        isAllowedMultipleSpots: Boolean = false,
        spotID: Long? = null
    ) {
        // Если явно не передан признак "несколько роликов в один рекламный блок" - выбрасываем исключение
        if (!inPlacement && !isAllowedMultipleSpots && (getRelevantSpots().isNotEmpty() || hasOtherMediaPlanSpots)) {
            throw RuntimeException("Нельзя размещать несколько роликов в один рекламный блок.")
        }

        // Подсчитываем временно занятый обём рекламного блока (по спотам в черновике)
        val temporaryClaimedVolume = getRelevantSpots()
            .filter { !it.inPlacement }
            .sumOf { it.film.timing.duration.toInt() }

        // Если объёма рекламного блока не хватает - выбрасываем исключение
        if (volumeFree - temporaryClaimedVolume < film.timing.duration && !inPlacement) {
            throw RuntimeException("Объёма рекламного блока не хватает для размещения ролика.")
        }

        if (dateTime.toLocalDate() !in film.timing.period.dateFrom..film.timing.period.dateBefore) {
            throw RuntimeException(
                "Рекламный роилк не должен сдесь размещаться: блок ${dateTime}, " +
                "период ${film.timing.period.dateFrom} - ${film.timing.period.dateBefore}"
            )
        }

        // Добавляем спот в коллекцию спотов рекламного блока
        spots += EnhancedPlacementSpot(film, this, inPlacement).apply { this.spotID = spotID }

        // Подсчитываем рейтинг (набранный инвентарь) спота в рекламном блоке
        val calculatedSpotInventory = calculateInventory(film.timing.duration)

        //        info(
        //            "Добавлен ролик ${film.timing.duration}: " +
        //            "[инв: ${film.timing.period.parameters.currentInventoryTotal} += ${calculatedSpotInventory}/" +
        //            "${film.timing.period.parameters.requiredInventoryTotal}]"
        //        )

        // Если рекламный блок в prime - прибавляем к prime инвентарю ролика
        if (this.isPrime) {
            film.currentInventoryPrime += calculatedSpotInventory
        }
        // Или если рекламный блок в off prime - прибавляем к off prime инвентарю ролика
        else {
            film.currentInventoryOffPrime += calculatedSpotInventory
        }
    }

    /**
     * Метод удаления спота из коллекции спотов рекламного блока.
     * @param spot [EnhancedPlacementSpot] спот рекламного блока.
     */
    fun removeSpot(spot: EnhancedPlacementSpot) {
        val foundSpot = spots.find { it == spot } ?: return
        if (foundSpot.inPlacement) foundSpot.requireRemoving = true
        else spots.remove(foundSpot)
        if (foundSpot.block.isPrime) {
            foundSpot.film.currentInventoryPrime -= foundSpot.block.calculateInventory(foundSpot.film.timing.duration)
        } else {
            foundSpot.film.currentInventoryOffPrime -= foundSpot.block.calculateInventory(foundSpot.film.timing.duration)
        }
    }

    /**
     * Метод расчёта инвентаря, который наберёт ролик с указанным хронометражём.
     * @param duration  [Byte]      хронометраж.
     * @return          [Double]    значение инвентаря.
     */
    fun calculateInventory(duration: Byte) =
        when {
            isMinute -> duration.toDouble()
            isAssignedRatingsPlacement -> {
                assignedRating?.times(duration)?.div(20.0) ?: rating * duration / 20.0
            }
            else -> {
                rating * duration / 20.0
            }
        }
}