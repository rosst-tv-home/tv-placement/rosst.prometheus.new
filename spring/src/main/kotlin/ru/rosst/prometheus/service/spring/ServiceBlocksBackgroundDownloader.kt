package ru.rosst.prometheus.service.spring

import kotlinx.coroutines.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.service.ServiceChannels
import ru.rosst.prometheus.service.ServiceSellingDirections
import ru.rosst.prometheus.service.vimb.VimbServiceBlocks
import java.time.LocalDate
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Service
class ServiceBlocksBackgroundDownloader : AbstractService() {
    private lateinit var counterJob: Job

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    @Autowired
    private lateinit var srvVimbBlocks: VimbServiceBlocks

    /**
     *  Метод инициализации счётчика объёмов запросов
     */
    @PostConstruct
    private fun initialize() {
        counterJob = GlobalScope.launch {
            val year = 2021
            val delayMinutes = 4L
            val channels = srvSellingDirections
                .findAllOnlyIDs()
                .associateWith { srvChannels.findAllMeasurableIDsBySellingDirectionID(it).toSet() }
            while (isActive) {
                for (month in 1..12) {
                    for (group in channels) {
                        for (channelsFromGroup in group.value.chunked(25)) {
                            val dateFrom = LocalDate.of(year, month, 1)
                            val dateBefore = dateFrom.plusMonths(1).minusDays(1)
                            srvVimbBlocks.download(
                                group.key,
                                dateFrom,
                                dateBefore,
                                channelsFromGroup.toSet()
                            )
                            delay(1000L * 60L * delayMinutes)
                        }
                    }
                }
            }
        }
    }

    /**
     * Метод уничтожения счётчика объёмов запросов
     */
    @PreDestroy
    private fun terminate() = runBlocking { counterJob.cancelAndJoin() }
}