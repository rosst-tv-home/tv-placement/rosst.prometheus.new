package ru.rosst.prometheus.service.autoplacement.enchanced

/**
 * Модель спота - ролик размещённый в рекламном блоке.
 *
 * @param film              [EnhancedPlacementFilm]     ролик.
 * @param block             [EnhancedPlacementBlock]    рекламный блок.
 * @param inPlacement       [Boolean]                   признак "спот в размещении" - указывает на то, что спот получен из vimb,
 *                                                      т.е. уже размещён в сетке, а не является черновым наброском.
 * @param requireRemoving   [Boolean]                   признак "спот требует удаления".
 * @param spotID            [Long]                      идентификатор спота.
 */
class EnhancedPlacementSpot(
    val film: EnhancedPlacementFilm,
    val block: EnhancedPlacementBlock,
    val inPlacement: Boolean = false,
    var requireRemoving: Boolean = false,
    var spotID: Long? = null
)