package ru.rosst.prometheus.service.autoplacement.models

import ru.rosst.prometheus.entity.PalomarsChannelRating

class Affinity(
    val baseAudienceRating: PalomarsChannelRating,
    val targetAudienceRating: PalomarsChannelRating
) {
    val affinity: Double
        get() = targetAudienceRating.rating / baseAudienceRating.rating * 100
}