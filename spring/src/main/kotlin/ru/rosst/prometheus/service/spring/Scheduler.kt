package ru.rosst.prometheus.service.spring

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import org.springframework.stereotype.Component
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.AutoPlacementParameter
import ru.rosst.prometheus.entity.AutoPlacementRequestDate
import ru.rosst.prometheus.enumerated.AutoPlacementAlgorithm
import ru.rosst.prometheus.repository.RepoRatings
import ru.rosst.prometheus.service.ServiceAutoPlacementParameters
import ru.rosst.prometheus.service.autoplacement.ServiceAutoPlacementByFilms
import ru.rosst.prometheus.service.autoplacement.ServiceAutoPlacementEnhancedV2
import ru.rosst.prometheus.service.vimb.VimbServiceAdvertisers
import ru.rosst.prometheus.service.vimb.VimbServiceChannels
import ru.rosst.prometheus.service.vimb.VimbServiceRanks
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.math.ceil

/**
 * Компонент "планировщик задач" - запускает методы в указанное время.
 *
 * Компоненты взаимодействия с VIMB API:
 * @property vimbSrvRanks               [VimbServiceRanks]                  компонент взаимодействия с рангами VIMB API.
 * @property vimbSrvChannels            [VimbServiceChannels]               компонент взаимодействия с каналами VIMB API.
 * @property vimbSrvAdvertisers         [VimbServiceAdvertisers]            компонент взаимодействия с рекламодателями VIMB API.
 *
 * Прочие компоненты:
 * @property srvAutoPlacementByFilms    [ServiceAutoPlacementByFilms]       компонент автоматического размещения медиапланов по равномерности.
 * @property srvAutoPlacementEnhanced   [ServiceAutoPlacementEnhancedV2]    компонент автоматического размещения медиапланов по affinity.
 * @property srvAutoPlacementParameters [ServiceAutoPlacementParameters]    компонент управления данными таблицы ["auto_placement_parameters"].
 */
@Component
@EnableScheduling
class Scheduler : AbstractService(), SchedulingConfigurer {
    @Autowired
    private lateinit var repoRatings: RepoRatings

    @Autowired
    private lateinit var vimbSrvRanks: VimbServiceRanks

    @Autowired
    private lateinit var vimbSrvChannels: VimbServiceChannels

    @Autowired
    private lateinit var vimbSrvAdvertisers: VimbServiceAdvertisers

    @Autowired
    private lateinit var srvAutoPlacementParameters: ServiceAutoPlacementParameters

    @Autowired
    private lateinit var srvAutoPlacementByFilms: ServiceAutoPlacementByFilms

    @Autowired
    private lateinit var srvAutoPlacementEnhanced: ServiceAutoPlacementEnhancedV2

    /**
     * Задача: инициализации автоматического размещения медиаплана.
     * Время: каждые 13 минут.
     */
    @Scheduled(cron = "0 */13 * * * ?")
    fun initAutoPlacement() {
        // Поиск релевантного медиаплана для автоматического размещения
        info("Поиск релевантного медиаплана для автоматического размещения по TVR.")

        // Все релевантные медиапланы
        val relevantMediaPlans = srvAutoPlacementParameters.getAllRelevantMediaPlans()

        // Если нет релевантных медиапланов
        if (relevantMediaPlans.isEmpty()) {
            return
        }

        // Сгруппированные медиапланы по направлению продаж и месяцу размещения
        val groupedRelevantMediaPlans = relevantMediaPlans.groupBy {
            it.mediaPlan.date to
            it.mediaPlan.channel.region.sellingDirection.id to
            it.mediaPlan.order.rank.id
        }.toSortedMap(compareBy<Pair<Pair<LocalDate, Byte>, Byte>> {
            it.first.first
        }.thenBy {
            it.first.second
        }.thenBy {
            it.second
        })

        // Записи релевантных запросов медиапланов
        val requestDates = srvAutoPlacementParameters
            .getAllRelevantRequestDates()
            .filter {
                groupedRelevantMediaPlans.any { mp ->
                    mp.key.first.first.year == it.month.year &&
                    mp.key.first.first.month == it.month.month &&
                    mp.key.first.second == it.sellingDirection.id &&
                    mp.key.second == it.rank.id
                }
            }

        // Определяем какие медиапланы ещё не размещались
        val placementCandidateGroup = groupedRelevantMediaPlans
            .filter { group ->
                requestDates.none {
                    it.month.year == group.key.first.first.year &&
                    it.month.month == group.key.first.first.month &&
                    it.sellingDirection.id == group.key.first.second &&
                    it.rank.id == group.key.second
                }
            }

        // Группа, которая будет размещаться
        val group: Map.Entry<Pair<Pair<LocalDate, Byte>, Byte>, List<AutoPlacementParameter>>

        if (!placementCandidateGroup.isEmpty()) {
            // Если есть медиапланы, которые ещё не размещались, берём первый из списка
            group = placementCandidateGroup.entries.first()
        } else if (requestDates.isEmpty()) {
            // Если нет записей по размещению медиапланов, тогда берём первый из списка
            group = groupedRelevantMediaPlans.entries.first()
        } else {
            // Определяем какая группа медиапланов не размещалась дольше остальных
            val requiredRequest = requestDates.minByOrNull { it.date }!!
            // Определяем группу медиапланов
            group = groupedRelevantMediaPlans.entries.first { g ->
                requiredRequest.month.year == g.key.first.first.year &&
                requiredRequest.month.month == g.key.first.first.month &&
                requiredRequest.sellingDirection.id == g.key.first.second &&
                requiredRequest.rank.id == g.key.second
            }
        }

        // Если есть медиаплан с размещением по аффинити
        if (group.value.any { it.algorithm == AutoPlacementAlgorithm.AFFINITIES }) {
            // Если есть хотябы один медиаплан не по аффинити
            if (group.value.any { it.algorithm != AutoPlacementAlgorithm.AFFINITIES }) {
                throw IllegalArgumentException("Есть медиапланы с размещением не по аффинити")
            }
            srvAutoPlacementEnhanced.initializeGroupPlacement(group)
        } else {
            for (parameters in group.value) {
                srvAutoPlacementByFilms.initializeScheduledPlacementTask(parameters)
                Thread.sleep(4L * 60L * 1000L)
            }
            val requestDate = srvAutoPlacementParameters.findRequestDate(
                group.key.first.second,
                group.key.second,
                group.key.first.first
            ) ?: AutoPlacementRequestDate(
                LocalDateTime.now(),
                group.key.first.first,
                group.value.first().mediaPlan.channel.region.sellingDirection,
                group.value.first().mediaPlan.order.rank
            )

            requestDate.date = LocalDateTime.now()
            srvAutoPlacementParameters.saveRequestDate(requestDate)
        }

        //Параметры автоматического размещения медиаплана
        //        val parameters = srvAutoPlacementParameters.getRelevantMediaPlan()
        //            ?: return warning("Не найден релевантный медиаплан для инициализации автоматического размещения TVR.")

        //        val parameters = srvAutoPlacementParameters.findByMediaPlanID(18227816)
        //            ?: return warning("Параметры медиаплана с ID 17346072 не найдены.")

        //        info("Начало: размещение релевантного медиаплана.")
        //        info("Параметры: [${parameters.mediaPlan.name}-${parameters.mediaPlan.date}-${parameters.mediaPlan.channel.name}]")
        //
        //        if (parameters.algorithm == AutoPlacementAlgorithm.AFFINITIES) {
        //            srvAutoPlacementEnhanced.initializePlacement(parameters.id)
        //        } else {
        //            srvAutoPlacementByFilms.initializeScheduledPlacementTask(parameters)
        //        }
        //
        //        info("Параметры: [${parameters.mediaPlan.name}-${parameters.mediaPlan.date}-${parameters.mediaPlan.channel.name}]")
        //        info("Завершение: размещение релевантного медиаплана.")
    }

    /**
     * Задача: запрос федеральных каналов из VIMB API.
     * Время: 5, 10, 15, 20 и 25 минут[-ы] каждого часа ежедневно.
     */
    @Scheduled(cron = "0 5,10,15,20,25 * * * ?")
    fun downloadFederalChannels() = vimbSrvChannels.download(21)

    /**
     * Задача: запрос региональных каналов из VIMB API.
     * Время: 30, 35, 40, 45 и 50 минут[-ы] каждого часа ежедневно.
     */
    @Scheduled(cron = "0 30,35,40,45,50 * * * ?")
    fun downloadRegionalChannels() = vimbSrvChannels.download(23)

    /**
     * Задача: запрос федеральных рекламодателей из VIMB API.
     * Время: 7, 12, 17, 22 и 27 минут[-ы] каждого часа ежедневно.
     */
    @Scheduled(cron = "0 7,12,17,22,27 * * * ?")
    fun getFederalAdvertisers() = vimbSrvAdvertisers.download(21)

    /**
     * Задача: запрос региональных рекламодателей из VIMB API.
     * Время: 32, 37, 42, 47 и 52 минут[-ы] каждого часа ежедневно.
     */
    @Scheduled(cron = "0 32,37,42,47,52 * * * ?")
    fun getRegionalAdvertisers() = vimbSrvAdvertisers.download(23)

    /**
     * Задача: запрос рангов из VIMB API.
     * Время: 15:00 ежедневно.
     */
    @Scheduled(cron = "0 0 15 * * ?")
    fun getRanks() = vimbSrvRanks.getRanks()

    /**
     * Задача: очистка таблицы рейтингов для RMPT и заполнение
     * Время: ежедневно
     */
    @Scheduled(cron = "@daily")
    fun recalculateRatings() {
        // Очистка таблицы
        repoRatings.truncate()
        // Заполнение таблицы
        repoRatings.fillTable()
    }

    /**
     * Увеличение пула одновременных задач планировщика до 2.
     */
    override fun configureTasks(
        taskRegistrar: ScheduledTaskRegistrar
    ) = taskRegistrar.setTaskScheduler(
        ThreadPoolTaskScheduler().apply {
            poolSize = ceil(Runtime.getRuntime().availableProcessors() / 2.0).toInt()
            initialize()
        }
    )
}