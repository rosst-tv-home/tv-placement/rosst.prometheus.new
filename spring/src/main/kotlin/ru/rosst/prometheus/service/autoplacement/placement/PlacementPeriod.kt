package ru.rosst.prometheus.service.autoplacement.placement

import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.service.autoplacement.models.Affinity
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import kotlin.math.ceil

/**
 * Недельный период со сгруппированными роликами по хронометражу.
 *
 * @param dateFrom          [LocalDate] дата начала недельного периода.
 * @param dateBefore        [LocalDate] дата окончания недельного периода.
 * @param films             [Iterable]  коллекция [PlacementFilm] роликов, сгруппированных по хронометражу.
 * @param blocksAffinity    [Map]       таблица [Affinity] аффинити [Iterable] коллекция [Block] рекламных блоков.
 */
class PlacementPeriod(
    val orderID: Int,
    val dateFrom: LocalDate,
    val dateBefore: LocalDate,
    val films: Iterable<PlacementFilm>,
    val blocksAffinity: Map<Affinity, Iterable<Block>>
) {
    var isEmulateOneVersionFilmPlacement = false
    var isPreviousIterationInventoryClaimed = false
    var additionalOutputCountPerDay = 0
    var isPrimeInWorkDay = false
    var isSecondQuantile = true
    var isThirdQuantile = false
    var isFourthQuantile = false
    var isSameTimePlacement = false
    var isPreventOutputsCountRule = false

    val requiredTotalInventory: Double = films.sumOf(PlacementFilm::requiredTotalInventory)
    val requiredPrimeInventory: Double = films.sumOf(PlacementFilm::requiredPrimeInventory)
    val requiredOffPrimeInventory: Double = films.sumOf(PlacementFilm::requiredOffPrimeInventory)

    val currentTotalInventory: Double
        get() = currentPrimeInventory + currentOffPrimeInventory
    val currentPrimeInventory: Double
        get() = films.sumOf(PlacementFilm::currentPrimeInventory)
    val currentOffPrimeInventory: Double
        get() = films.sumOf(PlacementFilm::currentOffPrimeInventory)

    private val affinityCollection = blocksAffinity.keys.map(Affinity::affinity).sortedDescending()

    private val middleMedian: Double
        get() = getMedian(affinityCollection)

    private val upperMedian: Double
        get() = getMedian(affinityCollection.subList(0, affinityCollection.indexOfLast { it >= middleMedian } + 1))

    private val lowerMedian: Double
        get() = getMedian(
            affinityCollection.subList(
                affinityCollection.indexOfLast { it >= middleMedian },
                affinityCollection.lastIndex + 1
            )
        )

    val firstQuantile: Map<Affinity, Iterable<Block>>
        get() = blocksAffinity.filter { entry -> entry.key.affinity >= upperMedian }

    val secondQuantile: Map<Affinity, Iterable<Block>>
        get() = blocksAffinity.filter { entry -> entry.key.affinity >= middleMedian && entry.key.affinity < upperMedian }

    val thirdQuantile: Map<Affinity, Iterable<Block>>
        get() = blocksAffinity.filter { entry -> entry.key.affinity >= lowerMedian && entry.key.affinity < middleMedian }

    val fourthQuantile: Map<Affinity, Iterable<Block>>
        get() {
            val minimalAffinity = affinityCollection.minOrNull() ?: return mapOf()
            return blocksAffinity.filter { entry -> entry.key.affinity >= minimalAffinity && entry.key.affinity < lowerMedian }
        }

    fun averageOutputCountPerDay(blocksInDraft: Iterable<Block>): Int {
        val affinity = when {
            isFourthQuantile -> firstQuantile + secondQuantile + thirdQuantile + fourthQuantile
            isThirdQuantile -> firstQuantile + secondQuantile + thirdQuantile
            isSecondQuantile -> firstQuantile + secondQuantile
            else -> firstQuantile
        }
        val flattenBlocks = affinity.values
            .flatten()
            .filter { it.spots.isEmpty() }
            .filter { !blocksInDraft.contains(it) }

        val totalPeriodBlocksInventory = flattenBlocks.sumOf { block ->
            block.assignedRatings.firstOrNull { assign ->
                assign.order.id == orderID
            }?.rating?.times(films.first().duration)?.div(20) ?: block.ratingForecast * films.first().duration / 20
        }

        // Количество дней в периоде
        val periodDaysCount = ChronoUnit.DAYS.between(dateFrom, dateBefore) + 1
        // Количество рекламных блоков в периоде
        val periodBlocksCount = flattenBlocks.size

        var outputCount =
            ceil(requiredTotalInventory / (totalPeriodBlocksInventory / periodBlocksCount) / periodDaysCount).toInt()
        if (outputCount == 0) outputCount = 1

        return outputCount + additionalOutputCountPerDay
    }

    private fun getMedian(affinityCollection: List<Double>): Double =
        if (affinityCollection.size % 2 == 0)
            (affinityCollection[affinityCollection.size / 2] + affinityCollection[affinityCollection.size / 2 - 1]) / 2
        else
            affinityCollection[affinityCollection.size / 2]
}