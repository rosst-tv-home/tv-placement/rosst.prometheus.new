package ru.rosst.prometheus.service.autoplacement.placement

/**
 * Ролик недельного периода.
 *
 * @param linkID                                        [Int]    идентификатор ролика
 * @param duration                                      [Byte]   хронометраж ролика
 * @param primePercent                                  [Double] процент prime
 * @param requiredTotalInventory                        [Double] плановый инвентарь ролика
 *
 * @property requiredPrimeInventory                     [Double] плановый инвентарь ролика в prime
 * @property requiredOffPrimeInventory                  [Double] плановый инвентарь ролика в off prime
 *
 * @property currentTotalInventory                      [Double] весь набранный инвентарь ролика.
 * @property currentPrimeInventory                      [Double] набранный инвентарь ролика в prime.
 * @property currentOffPrimeInventory                   [Double] набранный инвентарь ролика в off prime.
 *
 * @property differenceCurrentRequiredPrimeInventory    [Double] разниза между набранным и плановым инвентарём ролика в prime.
 * @property differenceCurrentRequiredOffPrimeInventory [Double] разница между набранный и плановым инвентарём ролика в off prime.
 */
class PlacementFilm(
    val linkID: Int,
    val duration: Byte,
    val primePercent: Double,
    val requiredTotalInventory: Double
) {
    val requiredPrimeInventory: Double = requiredTotalInventory * primePercent
    val requiredOffPrimeInventory: Double = requiredTotalInventory - requiredPrimeInventory

    val currentTotalInventory: Double
        get() = currentPrimeInventory + currentOffPrimeInventory
    var currentPrimeInventory: Double = 0.0
    var currentOffPrimeInventory: Double = 0.0

    val differenceCurrentRequiredPrimeInventory: Double
        get() = currentPrimeInventory - requiredPrimeInventory
    val differenceCurrentRequiredOffPrimeInventory: Double
        get() = currentOffPrimeInventory - requiredOffPrimeInventory
}