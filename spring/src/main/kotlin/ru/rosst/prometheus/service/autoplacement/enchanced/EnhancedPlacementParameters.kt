package ru.rosst.prometheus.service.autoplacement.enchanced

import kotlinx.coroutines.runBlocking
import ru.rosst.extensions.kotlin.parallelFlatMap
import java.time.LocalDate

/**
 * Модель параметров размещения.
 *
 * @param orderID                       [Int]       идентификатор заказа.
 * @param mediaPlanID                   [Int]       идентификатор медиаплана.
 * @param parametersID                  [Int]       идентификатор параметров.
 * @param inventoryPercentRequired      [Double]    процент от указанного инвентаря в периодах, который нужно набрать.
 * @param periods                       [Iterable]  коллекция [EnhancedPlacementPeriod] периодов.
 *
 * @property isIgnoreQualityInventory   [Boolean] признак "игнорирование аффинитивности".
 * @property isSecondQuantile           [Boolean] признак "можно рассматривать второй квантиль".
 * @property isThirdQuantile            [Boolean] признак "можно рассматривать третий квантиль".
 * @property isFourthQuantile           [Boolean] признак "можно рассматривать четвёртый квантиль".
 * @property isPrimeInWorkDays          [Boolean] признак "можно набирать prime в будние дни".
 * @property isOneVersionEmulation      [Boolean] признак "использовать эмуляцию одной версии ролика" (объединение инвентарей).
 * @property isPeriodInventory          [Boolean] признак ""
 *
 * @property requiredInventoryPrime     [Double] вычисляемый плановый инвентарь в prime.
 * @property requiredInventoryOffPrime  [Double] вычисляемый плановый инвентарь в off prime.
 * @property requiredInventoryTotal     [Double] вычисляемый плановый инвентарь (всего).
 *
 * @property currentInventoryPrime      [Double] вычисляемый набранный инвентарь в prime.
 * @property currentInventoryOffPrime   [Double] вычисляемый набранный инвентарь в off prime.
 * @property currentInventoryTotal      [Double] вычисляемый набранный инвентарь (всего).
 */
class EnhancedPlacementParameters(
    val orderID: Int,
    val mediaPlanID: Int,
    val parametersID: Int,
    val inventoryPercentRequired: Double = 1.0,
    val periods: Iterable<EnhancedPlacementPeriod>
) {
    var isIgnoreQualityInventory = false
    var isSecondQuantile: Boolean = false
    var isThirdQuantile: Boolean = false
    var isFourthQuantile: Boolean = false
    var isPrimeInWorkDays: Boolean = false
    var isOneVersionEmulation: Boolean = false
    var isPeriodInventory: Boolean = false
    var isTotalInventory: Boolean = false

    val requiredInventoryPrime get() = periods.sumOf { it.requiredInventoryPrime }
    val requiredInventoryOffPrime get() = periods.sumOf { it.requiredInventoryOffPrime }
    val requiredInventoryTotal get() = requiredInventoryPrime + requiredInventoryOffPrime

    val currentInventoryPrime get() = periods.sumOf { it.currentInventoryPrime }
    val currentInventoryOffPrime get() = periods.sumOf { it.currentInventoryOffPrime }
    val currentInventoryTotal get() = currentInventoryPrime + currentInventoryOffPrime

    /**
     * Метод получения дат и их количества выходов.
     * @return [Map] таблица из [Int] количества выходов и [Collection] коллекции [LocalDate] дат с этим количеством выходов.
     */
    fun getDatesAndOutputCounts(): Map<Int, Collection<LocalDate>> = runBlocking {
        mutableMapOf<Int, Collection<LocalDate>>().apply {
            periods.flatMap { period ->
                (0 until period.daysCount).map {
                    period.getDateOutputCount(period.dateFrom.plusDays(it)) to period.dateFrom.plusDays(it)
                }
            }.filter { it.first != null }.groupBy { it.first }.forEach { group ->
                this[group.key!!] = group.value.map { it.second }
            }
        }
    }

    /**
     * Метод проверки наличия соседнего спота.
     * @param block [EnhancedPlacementBlock]    рекламный блок, наличие соседних спотов, которого проверяем.
     * @return      [Boolean]                   признак наличия соседнего спота.
     */
    fun hasNeighbourSpot(block: EnhancedPlacementBlock): Boolean = runBlocking {
        periods
            .parallelFlatMap { it.getAffinityHalfHourPeriods() }
            .parallelFlatMap { it.getBlocks() }
            .filter { it.getRelevantSpots().isNotEmpty() }
            .any {
                // Рассматриваемый в коллекции рекламный блок входит в диапозон дат соседних рекламных блоков
                val isInDate = it.date in block.date.minusDays(1)..block.date.plusDays(1)
                // Рассматриваемый в коллекции рекламный блок имеет тоже время что и переданный в метод рекламный блок
                val isInTime = it.dateTime.toLocalTime().withSecond(0) == block.dateTime.toLocalTime().withSecond(0)
                isInDate && isInTime
            }
    }

    /**
     * Метод проверки превышения инвентаря качества.
     * @param isPrime   [Boolean]   признак "инвентарь prime".
     * @param inventory [Double]    инвентарь, который может быть набран.
     * @return          [Boolean]   признак "инвентарь превышен"
     */
    fun isInventoryExceeded(isPrime: Boolean, inventory: Double) =
        if (isPrime) periods.sumOf {
            it.currentInventoryPrime
        } + inventory > periods.sumOf {
            it.requiredInventoryPrime
        } || isTotalInventoryExceeded(inventory)
        else periods.sumOf {
            it.currentInventoryOffPrime
        } + inventory > periods.sumOf {
            it.requiredInventoryOffPrime
        } || isTotalInventoryExceeded(inventory)

    /**
     * Метод проверки превышения всего инвентаря.
     * @param inventory [Double]    инвентарь, который может быть набран.
     * @return          [Boolean]   признак "инвентарь превышен".
     */
    private fun isTotalInventoryExceeded(inventory: Double) =
        periods.sumOf {
            it.currentInventoryTotal
        } + inventory > periods.sumOf {
            it.requiredInventoryTotal
        }
}