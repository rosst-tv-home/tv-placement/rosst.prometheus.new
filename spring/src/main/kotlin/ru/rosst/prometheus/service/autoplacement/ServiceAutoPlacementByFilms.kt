package ru.rosst.prometheus.service.autoplacement

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.enumerated.AutoPlacementAlgorithm
import ru.rosst.prometheus.enumerated.PalomarsTimeDayType
import ru.rosst.prometheus.service.*
import ru.rosst.prometheus.service.autoplacement.models.*
import ru.rosst.prometheus.service.vimb.VimbServiceBlocks
import ru.rosst.prometheus.service.vimb.VimbServiceMediaPlans
import ru.rosst.prometheus.service.vimb.VimbServiceSpots
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.math.ceil

/**
 * Сервис автоматической расстановки.
 *
 * Сервисы взаимодействия с vimb api:
 * @property vimbSrvSpots               [VimbServiceSpots]              сервис загрузки спотов.
 * @property vimbSrvBlocks              [VimbServiceBlocks]             сервис загрузки рекламных блоков.
 * @property vimbSrvMediaPlans          [VimbServiceMediaPlans]         сервис загрузки медиапланов.
 *
 * Сервисы управления данными базы данных:
 * @property srvSpots                   [ServiceSpots]                  сервис управления данными таблицы ["spots"] "споты".
 * @property srvBlocks                  [ServiceBlocks]                 сервис управления данными таблицы ["blocks"] "рекламные блоки".
 * @property srvMediaPlans              [ServiceMediaPlans]             сервис управления данными таблицы ["media_plans"] "медиапланы".
 * @property srvAutoPlacementParameters  [ServiceAutoPlacementParameters] сервис управления данными таблицы ["auto_placement_media_plans"] "параметры автоматической расстановки медиапланов".
 */
@Service
class ServiceAutoPlacementByFilms : AbstractService() {
    @Autowired
    private lateinit var vimbSrvSpots: VimbServiceSpots

    @Autowired
    private lateinit var vimbSrvBlocks: VimbServiceBlocks

    @Autowired
    private lateinit var vimbSrvMediaPlans: VimbServiceMediaPlans

    @Autowired
    private lateinit var srvSpots: ServiceSpots

    @Autowired
    private lateinit var srvBlocks: ServiceBlocks

    @Autowired
    private lateinit var srvMediaPlans: ServiceMediaPlans

    @Autowired
    private lateinit var srvAutoPlacementParameters: ServiceAutoPlacementParameters

    @Autowired
    private lateinit var srvPalomarsChannelsRatings: ServicePalomarsChannelsRatings

    /**
     * Метод инициализации запланированного размещения.
     */
    fun initializeScheduledPlacementTask(parameters: AutoPlacementParameter) {
        // ID направления продаж
        val sellingDirectionID = parameters.mediaPlan.channel.region.sellingDirection.id

        // ID медиаплана
        val mediaPlanID = parameters.mediaPlan.id

        // Дата начала периода
        val dateFrom = parameters.mediaPlan.date.withDayOfMonth(1)

        // Дата окончания периода
        val dateBefore = parameters.mediaPlan.date.plusMonths(1).minusDays(1)

        // коллекция рекламодателей медиаплана
        val advertisers = setOf(parameters.mediaPlan.order.agreement.mapping.advertiser.id)

        // коллекция каналов медиаплана
        val channels = setOf(parameters.mediaPlan.channel.id)

        // Обновляем данные медиаплана
        vimbSrvMediaPlans.download(
            parameters.mediaPlan.channel.region.sellingDirection.id,
            dateFrom,
            dateBefore,
            channels,
            advertisers
        )

        // Обновляем данные канальной сетки медиаплана
        vimbSrvBlocks.download(
            sellingDirectionID,
            dateFrom,
            dateBefore,
            channels
        )

        // Обновляем данные размещённых роликов медиаплана
        vimbSrvSpots.downloadSpots(
            sellingDirectionID,
            dateFrom,
            dateBefore,
            channels,
            advertisers
        )

        // Связи роликов с медиапланом
        val links = requireNotNull(
            srvMediaPlans.findByMediaPlanId(mediaPlanID)
        ) { "Не удалось найти ролики медиаплана №$mediaPlanID" }

        // Медиаплан
        val mediaPlan = links.first().mediaPlan

        // Имеющиеся споты
        val spots = srvSpots.findAllByMediaPlan(mediaPlan).toList()

        // Группируем связи роликов с медиапланом по хронометражам роликов
        val linksGroupedByTiming = links.groupBy { it.film.duration }.toSortedMap(compareByDescending { it })

        // Создаём модели периодов, которые будут использоваться в автоматическом размещении
        val periods = createAutoPlacementPeriods(parameters, mediaPlan, linksGroupedByTiming)

        if (periods.size != 0) {
            // Рассчитываем инвентарь, набранный в Prime и Off Prime
            val currentInventoryPair = getCurrentInventory(mediaPlan, spots)

            // Рассчитываем инвентарь, который требуется в Prime и Off Prime
            val requiredInventoryPair = getRequiredInventory(parameters)

            // Запуск автоматического размещения
            startPlacement(links, requiredInventoryPair, currentInventoryPair, spots, periods)
        } else {
            warning("Не удалось сформировать периоды размещения медиаплана")
        }


        // Устанавливаем новую дату последнего размещения медиаплана
        parameters.lastPlacementDate = LocalDateTime.now()
        // Сохраняем новую дату последнего размещения медиаплана в базу данных
        srvAutoPlacementParameters.save(parameters)
    }


    /**
     * Метод запуска автоматической расстановки.
     *
     * @param links                 [MediaPlan] медиаплан.
     * @param requiredInventoryPair     [Pair]      пара значений планового инвентаря:
     * 1 - [Double] инвентарь в Prime,
     * 2 - [Double] инвентарь в Off Prime.
     * @param currentInventoryPair      [Pair]      пара значений набранного инвентаря:
     * 1 - [Double] инвентарь в Prime,
     * 2 - [Double] инвентарь в Off Prime.
     * @param spots                     [List]      коллекция [Spot] имеющихся спотов.
     * @param periods                   [List]      коллекция [FormattedAutoPlacementPeriod] периодов автоматического размещения.
     */
    private fun startPlacement(
        links: Iterable<FilmXMediaPlan>,
        requiredInventoryPair: Pair<Double, Double>,
        currentInventoryPair: Pair<Double, Double>,
        spots: List<Spot>,
        periods: List<InterfaceFormattedAutoPlacementPeriod>
    ) {
        val mediaPlan = links.first().mediaPlan
        // Инвентарь, необходимый в Prime
        val requiredInventoryPrime = requiredInventoryPair.first
        // Инвентарь, необходимый в Off Prime
        val requiredInventoryOffPrime = requiredInventoryPair.second
        // Инвентарь, набранный в Prime
        var currentInventoryPrime = currentInventoryPair.first
        // Инвентарь набранный в Off Prime
        var currentInventoryOffPrime = currentInventoryPair.second

        // Черновик спотов
        val spotsDraft = mutableMapOf<Block, FilmXMediaPlan>()

        /***********
         * ПРАВИЛА *
         ***********/
        // Предыдущий ролик, который был размещён в сетку
        var previousFilmUsed: FormattedAutoPlacementFilm? = null
        // Количество % смещения от среднего рейтинга блока в периоде
        var allowedRatingOffset = 0.5
        // Признак "можно рассматривать блоки, если есть соседний блок в тоже время со спотом"
        var isAllowedSameTimeUsage = false
        // Признак "можно рассматривать блоки в крайних квантилях"
        var isAllowedExtremeQuantileUsage = false
        // Признак "размещаться одной версией ролика"
        var isAllowedOneVersionPlacement = false
        // Признак "использовать последние сегменты"
        var isAllowedLowerMedianSegmentUsage = false

        val totalPeriodCount = periods
            .filter { it.segments.count() != 0 }
            .count()

        val totalSegmentCount = periods
            .filter { it.segments.count() != 0 }
            .sumOf { it.segments.count() }

        if (totalPeriodCount == 0) return

        val averageSegmentCount = totalSegmentCount / totalPeriodCount

        val averageBlockCount = periods
            .filter { it.segments.count() != 0 }
            .sumOf { it.segments.sumOf { quantile -> quantile.values.count() } } / (4 * averageSegmentCount)

        var iterateCount = ceil(totalPeriodCount * averageSegmentCount * averageBlockCount * 4 * 1.25).toInt()
        iterateCount = 100

        info(
            """
            Всего периодов: $totalPeriodCount; в среднем сегментов: $averageSegmentCount;
            в среднем блоков: $averageBlockCount; в среднем итераций: $iterateCount
        """
        )
        info(
            """
            Включение использования крайних квантилей: ${iterateCount / 7};
            смещение от среднего рейтинга на 100%: ${iterateCount / 6};
            смещение от среднего рейтинга на 150%: ${iterateCount / 5};
            смещение от среднего рейтинга на 200%: ${iterateCount / 4};
            смещение от среднего рейтинга на 500%: ${iterateCount / 3};
            использование тогоже времени: ${ceil(iterateCount / 1.25).toInt()};
        """
        )

        for (i in 0..iterateCount) {
            info("Итерация [$i из $iterateCount]")
            //            if (iterateCount <= 50) {
            //                allowedRatingOffset = 5.0
            //                isAllowedExtremeQuantileUsage = true
            //                isAllowedOneVersionPlacement = true
            //            } else {
            if (!isAllowedExtremeQuantileUsage && i >= iterateCount / 8 && i < iterateCount / 7.5)
                isAllowedExtremeQuantileUsage = true

            if (allowedRatingOffset != 1.0 && i >= iterateCount / 7.5 && i < iterateCount / 7.0)
                allowedRatingOffset = 1.0
            if (allowedRatingOffset != 1.5 && i >= iterateCount / 7.0 && i < iterateCount / 6.5)
                allowedRatingOffset = 1.5
            if (allowedRatingOffset != 2.0 && i >= iterateCount / 6.5 && i < iterateCount / 6.0)
                allowedRatingOffset = 2.0
            if (allowedRatingOffset != 5.0 && i >= iterateCount / 6.0 && i < iterateCount / 5.5)
                allowedRatingOffset = 5.0

            if (!isAllowedLowerMedianSegmentUsage && i >= iterateCount / 5.0)
                isAllowedLowerMedianSegmentUsage = true

            if (!isAllowedOneVersionPlacement && i >= iterateCount / 4.0)
                isAllowedOneVersionPlacement = true

            // if (i == ceil(iterateCount / 1.25).toInt()) isAllowedSameTimeUsage = true
            //            }

            periods.forEach period@{ period ->
                // Определяем среднее количество выходов в периоде
                var averageOutputCountInPeriod = getAverageFilmsOutputCountInPeriod(
                    links, period, spotsDraft, spots
                )

                if (i >= iterateCount / 3.5 && i < iterateCount / 3.0) averageOutputCountInPeriod += 1
                if (i >= iterateCount / 3.0 && i < iterateCount / 2.5) averageOutputCountInPeriod += 2
                if (i >= iterateCount / 2.5 && i < iterateCount / 2.0) averageOutputCountInPeriod += 3
                if (i >= iterateCount / 2.0 && i < iterateCount / 1.5) averageOutputCountInPeriod += 4
                if (i >= iterateCount / 1.5 && i < iterateCount / 1.0) averageOutputCountInPeriod += 5

                // Определяем срдний рейтинг блока в периоде
                val averageBlockRatingInPeriod = getAverageBlockRatingInPeriod(
                    links, period, spots, spotsDraft
                )

                period.segments.forEachIndexed segment@{ segmentIndex, segment ->
                    if (!isAllowUseThatSegment(
                            period.segments,
                            segmentIndex,
                            isAllowedLowerMedianSegmentUsage
                        )
                    ) return@segment
                    segment.values.forEachIndexed quantile@{ index, blocks ->
                        // Отсеиваем блоки квантиля, которые уже используются в черновике или имеющихся спотах
                        val filteredBlocksInQuantile = getUnusedBlocks(blocks, spotsDraft, spots)
                        if (filteredBlocksInQuantile.isEmpty()) return@quantile

                        // Если использование крайних квантилей запрещено, и текущий квантиль является крайним
                        // - переходим к следующему квантилю, если квантили закончились в сегменте - меняется сегмент
                        if (!isAllowUseThatQuantile(
                                index,
                                isAllowedExtremeQuantileUsage,
                                period.parameters.algorithm
                            )
                        ) return@quantile

                        filteredBlocksInQuantile.forEach block@{ block ->
                            // Определяем, нужно ли набирать инвентарь в этом блоке, если нет - переходим к следующему блоку
                            if (!isBlockInventoryCollectPossible(
                                    block,
                                    requiredInventoryPair,
                                    currentInventoryPair
                                )
                            ) return@block

                            // Определяем, не превышает ли погрешность рейтинг блока от среднего рейтинга блока в периоде,
                            // если превышает - переходим к следующему блоку
                            if (period.parameters.algorithm == AutoPlacementAlgorithm.EQUAL_DISTRIBUTION && isAverageRatingOffsetBroken(
                                    block,
                                    mediaPlan,
                                    averageBlockRatingInPeriod,
                                    allowedRatingOffset
                                )
                            ) return@block

                            // Если количество выходов в текущем дне превышает или равно количеству выходов в в день в
                            // рассматриваемом периоде, то переходим к следующему блоку
                            if (getOutputCountInDay(
                                    block.date,
                                    spots,
                                    spotsDraft
                                ) >= averageOutputCountInPeriod
                            ) return@block

                            // Проверяем, есть ли соседние блокие со спотами в тоже время и можно ли в них размещаться,
                            // если есть и нельзя размещаться, то переходим к следующему блоку
                            if (!isNeighboringSpotExistAndAllowedPlacement(
                                    block,
                                    spots,
                                    spotsDraft,
                                    isAllowedSameTimeUsage
                                )
                            ) return@block

                            // Получаем хронометраж роликов в периоде
                            val timing = links.first { it.id == period.films.first().id }.film.duration

                            // Определяем рейтинг блока
                            val blockRating = block.assignedRatings.firstOrNull {
                                it.order.id == mediaPlan.order.id
                            }?.rating?.times(timing)?.div(20.0) ?: block.ratingForecast * timing / 20.0

                            period.films.forEach film@{ film ->
                                // Определяем, использовался ли ролик в прошлой постановке,
                                // Если да и ролик не один, то меняем ролик
                                if (film == previousFilmUsed && period.films.count() != 1) return@film

                                // Определяем набранный инвентарь ролика в периоде
                                val filmPeriodInventory = getFilmInventoryInPeriod(
                                    film,
                                    links.first { it.id == film.id }.film.duration,
                                    mediaPlan,
                                    period,
                                    spots
                                )

                                // Определяем набранный инвентарь роликов в периоде, если бы это была 1 версия ролика
                                val oneVersionFilmPeriodInventory =
                                    getOneVersionFilmInventoryInPeriod(links, period, spots)

                                // Проверяем, можно ли размещаться одной версией ролика в периоде (при этом инвентарь
                                // нескольких роликов в периоде складывается)
                                if (!isAllowedOneVersionPlacement) {
                                    // Если ролик набрал или превысил свой инвентарь в периоде
                                    if (filmPeriodInventory + blockRating > film.inventory) return@block
                                } else {
                                    // Если ролик набрал или превысил свой инвентарь в периоде (в случае имитации одной версии ролика)
                                    if (oneVersionFilmPeriodInventory + blockRating > period.films.sumOf { it.inventory }) return@block
                                }

                                // Если рекламный блок Prime и ролик при постановке превысит инвентарь Prime медиаплана,
                                // или блок Off Prime и ролик при постановке превысит инвентарь off Prime Медиаплана
                                if ((block.isPrime && currentInventoryPrime + blockRating > requiredInventoryPrime) ||
                                    (!block.isPrime && currentInventoryOffPrime + blockRating > requiredInventoryOffPrime)
                                ) return@block

                                // Если блок Prime, прибавляем набранный инвентарь к инвентарю Prime медиаплана
                                if (block.isPrime) currentInventoryPrime += blockRating
                                // Если блок Off Prime, прибавляем набранный инвентарь к инвентарю Off Prime медиаплана
                                else currentInventoryOffPrime += blockRating

                                // Добавляем к черновому инвентарю ролика набранный инвентарь
                                film.inventoryDraft += blockRating

                                // Добавляем спот в черновик
                                spotsDraft[block] = links.first { it.id == film.id }

                                // Запоминаем ролик, который был использован при постановке
                                previousFilmUsed = film

                                // Переходим к следующему квантилю сегмента
                                return@quantile
                            }
                        }
                    }
                }

                // При смене периода - очищаем последний использованный в расстановке ролик
                previousFilmUsed = null
            }
        }

        // Итерируемся по выполненным задачам - получаем коллекцию заблокированных блоков
        val bannedBlocks = spotsDraft.map { (block, link) ->
            vimbSrvSpots.addSpot(block.id, link.id)
        }
            // Берём только пары
            .filterIsInstance<Pair<*, *>>()
            // Берём только пары с Long
            .filter { it.first is Long && it.second is Long }
            // Получаем идентификаторы блоков с запретом на размещение
            .map { it.second as Long }
            .toSet()

        // Если есть идентификаторы блоков с запретом на размещение
        if (!bannedBlocks.isNullOrEmpty()) {
            // Отмечаем в базе данных эти рекламные блоки
            srvBlocks.markBanned(bannedBlocks)
        }
    }

    /**
     * Метод определения набранного инвентаря ролика в периоде, если бы это был 1 ролик.
     *
     * @param links [FilmXMediaPlan] связи роликов и медиаплана.
     * @param period [FormattedAutoPlacementPeriod] рассматриваемый период.
     * @param spots [List] коллекция [Spot] имеющихся спотов.
     */
    private fun getOneVersionFilmInventoryInPeriod(
        links: Iterable<FilmXMediaPlan>,
        period: InterfaceFormattedAutoPlacementPeriod,
        spots: List<Spot>
    ): Double = spots.filter { spot ->
        // Берём споты роликов текущего рассматриваемого периода
        period.films.map { it.id }.contains(spot.filmXMediaPlan.id)
    }.filter {
        // Берём споты за текущий рассматриваемый период
        it.block.date >= period.dateFrom && it.block.date <= period.dateBefore
    }.sumOf { spot ->
        // Суммируем втянутый рейтинг спотов ролика и прибавляем рейтинг в черновике
        spot.block.assignedRatings.first { assignedRating ->
            assignedRating.order.id == links.first().mediaPlan.order.id
        }.rating * links.first { it.id == period.films.first().id }.film.duration / 20.0
    } + period.films.sumOf { it.inventoryDraft }

    /**
     * Метод определения набранного инвентаря ролика в периоде.
     *
     * @param film [FormattedAutoPlacementFilm] текущий рассматриваемый ролик.
     * @param mediaPlan [MediaPlan] медиаплан.
     * @param period [FormattedAutoPlacementPeriod] текущий рассматриваемый период.
     * @param spots [List] коллекция [Spot] имеющихся спотов.
     *
     * @return [Double] набранный инвентарь ролика в пероде.
     */
    private fun getFilmInventoryInPeriod(
        film: FormattedAutoPlacementFilm,
        filmDuration: Byte,
        mediaPlan: MediaPlan,
        period: InterfaceFormattedAutoPlacementPeriod,
        spots: List<Spot>
    ): Double = spots.filter {
        // Берём споты текущего рассматриваемого ролика
        it.filmXMediaPlan.id == film.id
    }.filter {
        // Берём споты за текущий рассматриваемый период
        it.block.date >= period.dateFrom && it.block.date <= period.dateBefore
    }.sumOf {
        // Суммируем втянутый рейтинг спотов ролика и прибавляем рейтинг в черновике
        var rating = it.block.assignedRatings.firstOrNull { assignedRating ->
            assignedRating.order.id == mediaPlan.order.id
        }?.rating?.times(filmDuration)?.div(20.0)
        if (rating == null) {
            warning("Не найден втянутый рейтинг блока ${it.block.id} для заказа ${mediaPlan.order.id}")
            rating = it.block.ratingForecast * filmDuration / 20.0
        }
        rating
    } + film.inventoryDraft

    /**
     * Метод определения наличия соседних спотов в тоже время, и возможности размещения в текущем рассматриваемом блоке.
     *
     * @param block [Block] текущий рассматриваемый блок.
     * @param spots [List] коллекция [Spot] спотов.
     * @param spotsDraft [MutableList] таблица из [Block] блоков и [FilmXMediaPlan] связей роликов и медиапланов.
     * @param isAllowedSameTimeUsage [Boolean] признак "можно ли размещаться рядом с соседними спотами".
     *
     * @return [Boolean] можно ли размещаться в текущем рассматриваемом блоке.
     */
    private fun isNeighboringSpotExistAndAllowedPlacement(
        block: Block,
        spots: List<Spot>,
        spotsDraft: MutableMap<Block, FilmXMediaPlan>,
        isAllowedSameTimeUsage: Boolean
    ): Boolean {
        // Определяем, есть ли соседний рекламный блок с тем же временем в коллекции имеющихся спотов
        val isBlockUsedInSpots = spots.any { spot ->
            // Определяем, есть ли блок спереди (более поздняя дата)
            val isBlockInFront = spot.block.date == block.date.plusDays(1)

            // Определяем, есть ли блок сзади (более ранняя дата)
            val isBlockBehind = spot.block.date == block.date.minusDays(1)

            // Определяем, равное ли время между блоками
            val isSameTime = spot.block.time.toLocalTime() == block.time.toLocalTime()

            return@any isSameTime && (isBlockInFront || isBlockBehind)
        }

        // Если в коллекции имеющихся спотов есть соседний рекламный блок с тем же временем,
        // что и рассматриваемый блок в текущей итерации,
        // то переходим к следующему блоку
        if (isBlockUsedInSpots && !isAllowedSameTimeUsage) return false

        // Определяем, есть ли соседний рекламный блок с тем же временем в черновике
        val isBlockUsedInDraft = spotsDraft.keys.any { draftBlock ->
            // Определяем, есть ли блок спереди (более поздняя дата)
            val isBlockInFront = draftBlock.date == block.date.plusDays(1)

            // Определяем, есть ли блок сзади (более ранняя дата)
            val isBlockBehind = draftBlock.date == block.date.minusDays(1)

            // Определяем, равное ли время между блоками
            val isSameTime = draftBlock.time.toLocalTime() == block.time.toLocalTime()

            return@any isSameTime && (isBlockInFront || isBlockBehind)
        }

        // Если в черновике есть соседний рекламный блок с тем же временем,
        // что и рассматриваемый блок в текущей итерации,
        // то переходим к следующему блоку
        if (isBlockUsedInDraft && !isAllowedSameTimeUsage) return false

        return true
    }

    /**
     * Метод рассчёта количества выходов в указанный день.
     *
     * @param date [LocalDate] дата, в которой нужно определить имеющееся количество выходов.
     * @param spots [List] коллекция [Spot] имеющихся спотов.
     * @param spotsDraft [MutableMap] таблица из [Block] блоков и [FilmXMediaPlan] связей роликов и медиапланов.
     *
     * @return [Int] количество выходов в указанном дне.
     */
    private fun getOutputCountInDay(
        date: LocalDate,
        spots: List<Spot>,
        spotsDraft: MutableMap<Block, FilmXMediaPlan>
    ): Int = spots.filter { it.block.date == date }.size + spotsDraft.keys.filter { it.date == date }.size

    /**
     * Метод определения среднего рейтинга рекламного блока в периоде.
     *
     * @param links [FilmXMediaPlan] связи роликов и медиаплана.
     * @param period [FormattedAutoPlacementPeriod] период.
     * @param spots [List] коллекция [Spot] имеющихся спотов.
     * @param draft [MutableMap] таблица из [Block] блоков и [FilmXMediaPlan] связей роликов и медиапланов.
     *
     * @return [Double] средний рейтинг рекламного блока в периоде.
     */
    private fun getAverageBlockRatingInPeriod(
        links: Iterable<FilmXMediaPlan>,
        period: InterfaceFormattedAutoPlacementPeriod,
        spots: List<Spot>,
        draft: MutableMap<Block, FilmXMediaPlan>
    ): Double {
        val mediaPlan = links.first().mediaPlan
        // Получаем неиспользуемые блоки в периоде
        val unusedBlocks = getUnusedBlocks(period.blocks.toList(), draft, spots)

        // Получаем хронометраж роликов в периоде
        val timing = links.first { it.id == period.films.first().id }.film.duration

        // Суммарный рейтинг блоков в периоде
        val blocksRatings = unusedBlocks.sumOf { block ->
            block.assignedRatings.firstOrNull { it.order.id == mediaPlan.order.id }?.rating?.times(timing)?.div(20.0)
                ?: block.ratingForecast * timing / 20.0
        }

        return blocksRatings / unusedBlocks.size
    }

    /**
     * Определение среднего количества выходов в день в периоде.
     *
     * @param links [FilmXMediaPlan] связи роликов и медиаплана.
     * @param period [FormattedAutoPlacementPeriod] период автоматического размещения.
     * @param spotsDraft [MutableMap] таблица-черновик из [Block] блоков и [FilmXMediaPlan] связей роликов и медиапланов.
     * @param spots [List] коллекция [Spot] спотов.
     */
    private fun getAverageFilmsOutputCountInPeriod(
        links: Iterable<FilmXMediaPlan>,
        period: InterfaceFormattedAutoPlacementPeriod,
        spotsDraft: MutableMap<Block, FilmXMediaPlan>,
        spots: List<Spot>
    ): Int {
        val mediaPlan = links.first().mediaPlan
        // Получаем неиспользуемые блоки в периоде
        val unusedBlocks = getUnusedBlocks(period.blocks.toList(), spotsDraft, spots)

        // Получаем хронометраж роликов в периоде
        val timing = links.first { it.id == period.films.first().id }.film.duration

        // Плановый инвентарь в периоде
        val requiredPeriodInventory = period.films.sumOf { it.inventory }
        // Свободный инвентарь в периоде
        val freePeriodInventory = unusedBlocks.sumOf { block ->
            block.assignedRatings.firstOrNull { it.order.id == mediaPlan.order.id }?.rating?.times(timing)?.div(20.0)
                ?: block.ratingForecast * timing / 20.0
        }
        // Количество дней в периоде
        val periodDaysCount = ChronoUnit.DAYS.between(period.dateFrom, period.dateBefore) + 1
        // Количество рекламных блоков в периоде
        val periodBlocksCount = unusedBlocks.size

        return ceil(requiredPeriodInventory / (freePeriodInventory / periodBlocksCount) / periodDaysCount).toInt()
    }

    /**
     * Метод определения смещения рейтинга рекламного блока от среднего рейтинга рекламного блока в периоде.
     *
     * @param block [Block] рекламный блок.
     * @param mediaPlan [MediaPlan] медиаплан.
     * @param averageBlockRatingInPeriod [Double] средний рейтинг рекламного блока в периоде.
     * @param allowedRatingOffset [Double] количество % допустимого смещения рейтинга блока от среднего рейтинга блока
     * в периоде.
     *
     * @return [Boolean] признак "смещение рейтинга не превышает погрешность".
     */
    private fun isAverageRatingOffsetBroken(
        block: Block,
        mediaPlan: MediaPlan,
        averageBlockRatingInPeriod: Double,
        allowedRatingOffset: Double
    ): Boolean {
        // Определяем допустимое смещение рейтинга блока от среднего рейтинга блока в периоде
        val offsetRating = allowedRatingOffset * averageBlockRatingInPeriod
        // Определяем рейтинг текущего рассматриваемого блока
        val blockRating = block.assignedRatings.firstOrNull {
            it.order == mediaPlan.order
        }?.rating ?: block.ratingForecast

        // Возвращаем результат
        return blockRating > (averageBlockRatingInPeriod + offsetRating) || blockRating < (averageBlockRatingInPeriod - offsetRating)
    }

    /**
     * Метод определения, нужно ли набирать инвентарь в этом блоке.
     *
     * @param block [Block] рекламный блок.
     * @param requiredInventoryPair [Pair] пара значений планового инвентаря:
     * 1 - [Double] инвентарь в Prime,
     * 2 - [Double] инвентарь в Off Prime.
     * @param currentInventoryPair [Pair] пара значений набранного инвентаря:
     * 1 - [Double] инвентарь в Prime,
     * 2 - [Double] инвентарь в Off Prime.
     *
     * @return [Boolean] признак "нужно ли набирать инвентарь в этом блоке"
     */
    private fun isBlockInventoryCollectPossible(
        block: Block,
        requiredInventoryPair: Pair<Double, Double>,
        currentInventoryPair: Pair<Double, Double>
    ): Boolean {
        // Если блок Prime
        return if (block.isPrime) {
            // Плановый инвентарь Prime больше набранного инвентаря Prime
            requiredInventoryPair.first > currentInventoryPair.first
        } else {
            // Плановый инвентарь Off Prime больше набранного инвентаря Off Prime
            requiredInventoryPair.second > currentInventoryPair.second
        }
    }

    /**
     * Метод фильтрации блоков. Отсеиваются блоки, которые используются в черновике или в имеющихся спотах.
     *
     * @param blocks [List] коллекция [Block] блоков.
     * @param draftBlocks [MutableMap] таблица [Block] блоков и [FilmXMediaPlan] связей роликов с медиапланом.
     * @param spots [List] коллекция [Spot] имеющихся спотов.
     *
     * @return [List] коллекция [Block] блоков, подходящих для размещения.
     */
    private fun getUnusedBlocks(
        blocks: Iterable<Block>,
        draftBlocks: MutableMap<Block, FilmXMediaPlan>,
        spots: List<Spot>
    ): List<Block> {
        // Блоки в черновике
        val blocksInDraft = draftBlocks.keys
        // Блоки в имеющихся спотах
        val blocksInSpots = spots.map(Spot::block)

        // Отсеиваем блоки квантиля, которые используются в черновике или в спотах
        return blocks.filter { block -> !blocksInDraft.contains(block) && !blocksInSpots.contains(block) }
    }

    /**
     * Метод определения возможности использования квантиля для автоматического размещения.
     *
     * @param index [Int] индекс квантиля.
     * @param isAllowedExtremeQuantileUsage [Boolean] признак "можно рассматривать блоки в крайних квантилях"
     *
     * @return [Boolean] true - можно использовать, false - нельзя.
     */
    private fun isAllowUseThatQuantile(
        index: Int,
        isAllowedExtremeQuantileUsage: Boolean,
        algorithm: AutoPlacementAlgorithm
    ): Boolean {
        return if (algorithm != AutoPlacementAlgorithm.EQUAL_DISTRIBUTION) true
        else (index == 0 || index == 3) && isAllowedExtremeQuantileUsage
    }

    /**
     * Метод рассчёта планового инвентаря в Prime и Off Prime.
     *
     * @param parameter [AutoPlacementParameter] параметры автоматического размещения.
     *
     * @return [Pair] пара значений: 1 - [Double] инвентарь в Prime, 2 - [Double] инвентарь в Off Prime
     */
    private fun getRequiredInventory(parameter: AutoPlacementParameter): Pair<Double, Double> {
        // Инвентарь в Prime
        var inventoryPrime = 0.0
        // Инвентарь в Off Prime
        var inventoryOffPrime = 0.0

        // Считаем инвентарь
        parameter.periods.forEach { period ->
            val prime = period.inventory * period.percentPrime
            val offPrime = period.inventory - prime
            inventoryPrime += prime
            inventoryOffPrime += offPrime
        }

        // Возвращаем рассчитанный инвентарь в виде пары <Prime, Off Prime>
        return inventoryPrime to inventoryOffPrime
    }

    /**
     * Метод возвращает набранный инвентарь в Prime и Off Prime.
     *
     * @param mediaPlan [MediaPlan] медиаплан.
     * @param spots [List] коллекция [Spot] спотов медиаплана.
     *
     * @return [Pair] пара значений: 1 - [Double] инвентарь в Prime, 2 - [Double] инвентарь в Off Prime
     */
    private fun getCurrentInventory(mediaPlan: MediaPlan, spots: List<Spot>): Pair<Double, Double> {
        // Набранный инвентарь в Prime
        var currentPrimeInventory = 0.0
        // Набранный инвентарь в Off Prime
        var currentOffPrimeInventory = 0.0

        // Считаем набранный инвентарь
        spots.forEach { spot ->
            // Определяем рейтинг спота
            val rating = spot.block.assignedRatings.firstOrNull { assignedRating ->
                assignedRating.order.id == mediaPlan.order.id
            }?.rating ?: 0.0

            // Определяем набранный инвентарь
            val inventory = rating * spot.filmXMediaPlan.film.duration / 20

            // Если блок Prime, то прибавляем рассчитанный рейтинг к Prime, иначе в Off Prime
            if (spot.block.isPrime) {
                currentPrimeInventory += inventory
            } else {
                currentOffPrimeInventory += inventory
            }
        }

        // Возвращаем рассчитанный инвентарь в виде пары <Prime, Off Prime>
        return currentPrimeInventory to currentOffPrimeInventory
    }

    /**
     * Метод формирования моделей периодов для автоматического размещения.
     *
     * @param parameter [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     * @param mediaPlan [MediaPlan] медиаплан.
     * @param linksGroupedByTiming [SortedMap] отсортированная таблица по хронометражу из:
     * 1 - [Byte] значение хронометража,
     * 2 - [List] коллекция [FilmXMediaPlan] связей роликов с медиапланом.
     *
     * @return [List] коллекция [FormattedAutoPlacementPeriod] моделей периодов, которые будут использоваться в
     * автоматическом размещении.
     */
    private fun createAutoPlacementPeriods(
        parameter: AutoPlacementParameter,
        mediaPlan: MediaPlan,
        linksGroupedByTiming: SortedMap<Byte, List<FilmXMediaPlan>>
    ): MutableList<InterfaceFormattedAutoPlacementPeriod> {
        // коллекция моделей периодов, которые будут использоваться в автоматическом размещении
        val formattedPeriods: MutableList<InterfaceFormattedAutoPlacementPeriod> = mutableListOf()

        // Формируем коллекция моделей периодов, которые будут использоваться в автоматическом размещении
        linksGroupedByTiming.keys.forEach timing@{ timing ->
            // Рекламные блоки, подходящие под размещение рассматриваемого хронометража
            val suitableTimingBlocks = srvBlocks.findAllByMediaPlan(mediaPlan, timing.toInt()).toList()

            // Если коллекция блоков пустая - переходим к следующему хронометражу
            if (suitableTimingBlocks.isNullOrEmpty()) return@timing

            parameter.periods.filter { period ->
                period.dateFrom !== null && period.dateBefore !== null
            }.forEach period@{ period ->
                // Ролики периода заполняемого хронометража
                val periodFilms =
                    period.films.filter { film -> film.filmXMediaPlan.film.duration == timing && film.inventoryPercent > 0.0 }

                // Если в рассматриваемом периоде нет хронометража,
                // который сейчас заполняется - переходим к следующему периоду
                if (periodFilms.isNullOrEmpty()) return@period

                // Создаём модель, которая будет использоваться в автоматическом размещении
                val formattedAutoPlacementPeriod =
                    if (parameter.algorithm == AutoPlacementAlgorithm.EQUAL_DISTRIBUTION)
                        FormattedAutoPlacementPeriod(
                            parameter,
                            mediaPlan,
                            parameter.times.toList(),
                            period.dateFrom!!,
                            period.dateBefore!!,
                            periodFilms.map {
                                FormattedAutoPlacementFilm(
                                    it.filmXMediaPlan.id,
                                    period.inventory * it.inventoryPercent
                                )
                            }
                        ).apply {
                            blocks = suitableTimingBlocks.filter { block ->
                                // Если время блока в промежутке ограничения времени, то выбираем этот блок
                                times.any { timePeriod ->
                                    block.time.toLocalTime() in timePeriod.timeFrom..timePeriod.timeBefore &&
                                    ((block.weekDay in 6..7 && timePeriod.isWeekend) || (block.weekDay in 1..5 && !timePeriod.isWeekend))
                                } && block.date in dateFrom..dateBefore
                            }
                        }
                    else
                        FormattedAutoPlacementPeriodTVR(
                            parameter,
                            period.dateFrom!!, period.dateBefore!!,
                            mediaPlan, parameter.times,
                            periodFilms.map { film ->
                                FormattedAutoPlacementFilm(
                                    film.filmXMediaPlan.id,
                                    film.period!!.inventory * film.inventoryPercent
                                )
                            },
                            convertBlocks(
                                suitableTimingBlocks.filter { block ->
                                    // Если время блока в промежутке ограничения времени, то выбираем этот блок
                                    parameter.times.any { timePeriod ->
                                        block.time.toLocalTime() in timePeriod.timeFrom..timePeriod.timeBefore &&
                                        ((block.weekDay in 6..7 && timePeriod.isWeekend) || (block.weekDay in 1..5 && !timePeriod.isWeekend))
                                    } && block.date in period.dateFrom!!..period.dateBefore!!
                                },
                                getAffinity(parameter)
                            ).map { map -> map.key.affinity to map.value }
                        )

                // Добавляем модель в коллекция
                formattedPeriods.add(formattedAutoPlacementPeriod)
            }
        }

        return formattedPeriods
    }

    /**
     * Метод конвертации коллекции рекламных блоков в таблицу tvr -> коллекция рекламных блоков.
     *
     * @param blocks        [Iterable]      коллекция [Block] рекламных блоков.
     * @param audiences     [Iterable]      коллекция [Affinity] TVR.
     *
     * @return              [MutableMap]    таблица из [Affinity] TVR и [List] коллекции [Block] рекламных блоков.
     */
    private fun convertBlocks(
        blocks: Iterable<Block>,
        audiences: Iterable<Affinity>
    ): MutableMap<Affinity, List<Block>> {
        // Таблица <Аудитория, Коллекция блоков>
        val audienceToBlocks = mutableMapOf<Affinity, List<Block>>()

        // Сортировка TVR по убыванию
        val audienceDescendingTVR = audiences.sortedByDescending { it.affinity }

        // Формируем таблицу
        audienceDescendingTVR.forEach { audience ->
            // Добавляем в таблицу аудиторию с TVR и принадлежащие рекламные блоки
            audienceToBlocks[audience] = blocks
                // Отсеиваем рекламные блоки, которые не входят в текущие временные параметры TVR аудитории
                .filter { block ->
                    // Отсеиваем рекламные блоки, которые не входят в временной период TVR аудитории и
                    // не соотвествуют дню недели
                    block.time.toLocalTime() in audience.baseAudienceRating.timeFrom..audience.baseAudienceRating.timeBefore &&
                    (
                    (block.weekDay.toInt() in 6..7 && audience.baseAudienceRating.dayType == PalomarsTimeDayType.DAY_OFF) ||
                    (block.weekDay.toInt() in 1..5 && audience.baseAudienceRating.dayType == PalomarsTimeDayType.WORKDAY)
                    )
                }
                // Сортируем блоки по убыванию прогнозного рейтинга
                .sortedBy { it.ratingForecast }
        }

        // Возвращаем таблицу <Аудитория, Коллекция рекламных блоков>
        return audienceToBlocks.filter { !it.value.isNullOrEmpty() }.toMutableMap()
    }

    private fun getAffinity(parameters: AutoPlacementParameter): MutableList<Affinity> {
        val affinity = mutableListOf<Affinity>()

        val baseAudience = parameters.mediaPlan.channel.palomarsChannel?.baseAudiences?.last()
        if (baseAudience == null) {
            error("Не удалось получить базовую аудиторию канала: ${parameters.mediaPlan.channel.palomarsChannel?.name}")
            return affinity
        }

        val ratings = parameters.mediaPlan.channel.palomarsChannel?.ratings
        if (ratings.isNullOrEmpty()) {
            error("Не удалось найти хоть какие-то рейтинги канала: ${parameters.mediaPlan.channel.palomarsChannel?.name}")
            return affinity
        }

        val targetAudienceRatings = ratings.filter { rating ->
            rating.audience == parameters.audience && rating.month == parameters.audienceDate
        }

        val baseAudienceRatings = ratings.filter { rating ->
            rating.audience == baseAudience.audience && rating.month == parameters.audienceDate
        }

        targetAudienceRatings.forEach { targetRating ->
            val foundBaseAudience = baseAudienceRatings.firstOrNull { baseRating ->
                baseRating.timeFrom == targetRating.timeFrom && baseRating.timeBefore == targetRating.timeBefore &&
                baseRating.dayType == targetRating.dayType
            } ?: return@forEach warning("Не удалось найти БА канала к ЦА чтобы посчитать affinity")
            affinity.add(Affinity(foundBaseAudience, targetRating))
        }

        return affinity
    }

    private fun isAllowUseThatSegment(
        segments: List<Map<Int, Iterable<Block>>>,
        index: Int,
        isAllowedLowerMedianSegmentUsage: Boolean
    ): Boolean {
        if (isAllowedLowerMedianSegmentUsage) return true
        return index <= segments.size / 2
    }
}