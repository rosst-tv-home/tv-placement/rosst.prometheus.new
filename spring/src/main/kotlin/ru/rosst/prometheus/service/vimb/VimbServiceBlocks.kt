package ru.rosst.prometheus.service.vimb

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.models.blocks.request.RequestBlocks
import ru.rosst.prometheus.models.blocks.response.ResponseBlock
import ru.rosst.prometheus.models.blocks.response.ResponseBlocks
import ru.rosst.prometheus.service.ServiceBlocks
import ru.rosst.prometheus.service.ServiceChannels
import ru.rosst.prometheus.service.ServiceRequestVolumes
import ru.rosst.prometheus.service.ServiceSellingDirections
import java.time.LocalDate
import java.time.temporal.ChronoUnit

/**
 * Сервис взаимодействия с рекламными блоками через vimb api.
 *
 * @property vimbSrvRequest         [VimbServiceRequest]        сервис формирования запросов к vimb api.
 *
 * Сервисы управления данными таблиц базы данных:
 * @property srvBlocks              [ServiceBlocks]             сервис управления данными таблицы ["blocks"] базы данных.
 * @property srvChannels            [ServiceChannels]           сервис управления данными таблицы ["channels"] базы данных.
 * @property srvSellingDirections   [ServiceSellingDirections]  сервис управления данными таблицы ["selling_directions"] базы данных.
 */
@Component
class VimbServiceBlocks : AbstractService() {
    @Autowired
    private lateinit var srvRequestVolumes: ServiceRequestVolumes

    @Autowired
    private lateinit var vimbSrvRequest: VimbServiceRequest

    @Autowired
    private lateinit var srvBlocks: ServiceBlocks

    @Autowired
    private lateinit var srvChannels: ServiceChannels

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    /**
     * Метод загрузки рекламных блоков.
     * Максимальный вес запроса: 366 дней
     * Время перезарядки: 2700 секунд - 45 минут
     * Восстановление: 366 / 2700 = 0.1355... дней в секунду
     *
     * @param id        [Byte]      идентификатор направления продаж.
     * @param from      [LocalDate] дата начала периода рекламных блоков.
     * @param before    [LocalDate] дата окончания периода рекламных блоков.
     * @param channels  [Set]       коллекция [Int] идентификаторов каналов (не обязательно).
     */
    fun download(id: Byte, from: LocalDate, before: LocalDate, channels: Set<Int>? = null) {
        info("Начало: загрузка рекламных блоков - [н/п: $id] [дата: $from - $before] [каналы: ${channels?.size ?: 0} шт.]")

        // Если дата окончания периода больше даты начала периода
        if (before < from) {
            error("Дата окончания периода [$before] не может быть больше даты начала периода [$from]")
            error("Завершение: загрузка рекламных блоков - [н/п: $id] [дата: $from - $before] [каналы: ${channels?.size ?: 0} шт.]")
            return
        }

        // Проверяем объём запроса
        val blocksRV = srvRequestVolumes.get("blocks")
        val daysUntil = from.until(before, ChronoUnit.DAYS)
        if (blocksRV.current < daysUntil) {
            throw IllegalArgumentException("Количество запрашиваемых дней превышает имеющийся объём")
        }
        // Обновляем объём запроса
        srvRequestVolumes.update("blocks", daysUntil.unaryMinus().toDouble())

        // Направление продаж
        val sellingDirection = srvSellingDirections.findById(id)

        // Каналы
        val channelsList =
            if (channels.isNullOrEmpty()) srvChannels.findAllBySellingDirectionIDLight(sellingDirection.id)
            else srvChannels.findAllByIDsLight(channels)

        // Если коллекция каналов пустая
        if (channelsList.isEmpty()) {
            error("Коллекция каналов не может быть пустой.")
            error("Ошибка: идентификаторы каналов не указаны в аргументах или не найдены в базе данных.")
            error("Завершение: загрузка рекламных блоков.")
            return
        }

        // Отправка запроса в vimb api
        val response = vimbSrvRequest.request(
            RequestBlocks(
                sellingDirectionID = sellingDirection.id,
                dateFrom = from,
                dateBefore = before,
                channels = channelsList.map { it.id }.toSet()
            ), true
        ) as? ResponseBlocks

        // Если не удалось сформировать\\отправить запрос к vimb api или преобразовать данные в модель ответа
        if (response == null) {
            error("Ошибка: не удалось сформировать\\отправить запрос к vimb api или переобразовать данные в модель ответа.")
            error("Завершение: загрузка рекламных блоков.")
            return
        }

        // Если в ответе из vimb api рекламных блоков нет
        if (response.blocks.isEmpty()) {
            warning("Предупреждение: рекламные блоки, полученные из vimb api не найдены.")
            info("Завершение: загрузка рекламных блоков.")
            return
        }

        // Фильтруем рекламные блоки, полученные из vimb api
        val filteredBlocks = filterBlocks(response.blocks)
        if (filteredBlocks.isEmpty()) {
            warning("Предупреждение: рекламных блоков, полученных из vimb api, после фильтрации не осталось.")
            info("Завершение: загрузка рекламных блоков.")
            return
        }

        // Конвертация моделей рекламных блоков vimb api в модели базы данных.
        val convertedBlocks = convertBlocks(filteredBlocks, channelsList)

        // Отмечаем блоки с запретом на размещение
        srvBlocks
            .findAllByIds(convertedBlocks.map(Block::id).toSet())
            .filter { it.isBanned }
            .forEach { block -> convertedBlocks.firstOrNull { it.id == block.id }?.isBanned = true }

        // Сохранение рекламных блоков в базу данных
        srvBlocks.saveBlocks(convertedBlocks, true, from, before, channelsList)

        info("Завершение: загрузка рекламных блоков.")
    }

    /**
     * Метод фильтрации рекламных блоков, полученных из vimb api.
     *
     * @param blocks    [Iterable]  коллекция [ResponseBlock] рекламных блоков из vimb api.
     *
     * @return          [List]      отфильтрованная коллекция [ResponseBlock] рекламных блоков.
     */
    private fun filterBlocks(blocks: Iterable<ResponseBlock>): List<ResponseBlock> {
        info("Начало: фильтрация рекламных блоков - [${blocks.count()} шт.]")
        val filteredBlocks = blocks
            // Отбрасываем рекламные блоки, относящиеся к программам с флагом "специальный проект"
            .filter { !it.isSpecialProject }
        info("Завершение: фильтрация рекламных блоков - [${filteredBlocks.size} шт.]")
        return filteredBlocks
    }

    /**
     * Метод конвертации рекламных блоков vimb api в модели базы данных.
     *
     * @param blocks        [List] коллекция [ResponseBlock] рекламных блоков, полученная из vimb api.
     * @param channelsLight [List] коллекция [ChannelLight] каналов.
     *
     * @return              [List] коллекция [Block] конвертированных рекламных блоков.
     */
    private fun convertBlocks(
        blocks: List<ResponseBlock>,
        channelsLight: List<ChannelLight>
    ): List<Block> {
        val channels = srvChannels.findAllByIDs(channelsLight.map { it.id })
        info("Начало: конвертация рекламных блоков vimb api в модели базы данных.")
        val convertedBlocks = runBlocking {
            blocks.parallelMap {
                Block(
                    it.blockID,
                    it.tnsBlockID,
                    it.blockDate,
                    it.blockDate.atStartOfDay().plusSeconds(it.blockSeconds),
                    it.blockDeadlineDateTime,
                    it.blockDayOfWeek,
                    it.blockIsPrime,
                    it.blockIsMinute,
                    it.blockAuctionState,
                    (it.blockVolume - (it.booking?.sumOf { booking -> booking.bookedVolume.toInt() } ?: 0)).toShort(),
                    it.blockForecastRating ?: 0.0,
                    Issue(
                        it.issueID,
                        it.blockDate.atStartOfDay().plusSeconds(it.issueSeconds.toLong()),
                        it.blockDate.atStartOfDay().plusSeconds(it.issueSeconds.toLong() + it.issueDuration.toLong()),
                        Program(
                            it.programID,
                            it.programName ?: "Название не определено",
                            channels.first { c -> c.id == it.channelID }
                        )
                    )
                )
            }
        }
        info("Завершение: конвертация рекламных блоков vimb api в модели базы данных.")
        return convertedBlocks
    }
}