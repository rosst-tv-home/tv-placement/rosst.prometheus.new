package ru.rosst.prometheus.service.vimb

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Advertiser
import ru.rosst.prometheus.entity.AdvertiserXSellingDirection
import ru.rosst.prometheus.entity.SellingDirection
import ru.rosst.prometheus.models.advertisers.request.RequestAdvertisers
import ru.rosst.prometheus.models.advertisers.response.ResponseAdvertiser
import ru.rosst.prometheus.models.advertisers.response.ResponseAdvertisers
import ru.rosst.prometheus.service.ServiceAdvertisers
import ru.rosst.prometheus.service.ServiceSellingDirections

/**
 * Компонент загрузки рекламодателей из VIMB API.
 *
 * Сервисы взаимодействия с VIMB API:
 * @property vimbSrvRequest         [VimbServiceRequest]        сервис отправки запросов к VIMB API.
 *
 * Сервисы управления данными в базе данных:
 * @property srvAdvertisers         [ServiceAdvertisers]        сервис управления рекламодателями.
 * @property srvSellingDirections   [ServiceSellingDirections]  сервис управления направлениями продаж.
 */
@Component
class VimbServiceAdvertisers : AbstractService() {
    @Autowired
    private lateinit var vimbSrvRequest: VimbServiceRequest

    @Autowired
    private lateinit var srvAdvertisers: ServiceAdvertisers

    @Autowired
    private lateinit var srvSellingDirections: ServiceSellingDirections

    /**
     * Метод загрузки рекламодателей из VIMB API.
     *
     * @param sellingDirectionID [Byte] идентификатор направления продаж.
     */
    fun download(sellingDirectionID: Byte) {
        info("Начало: загрузка рекламодателей по направлению продаж ID: $sellingDirectionID.")

        // Проверяем, есть ли направление продаж в базе данных
        val sellingDirection = srvSellingDirections.findById(sellingDirectionID)

        // Отправляем запрос в VIMB API
        val response = vimbSrvRequest.request(
            RequestAdvertisers(sellingDirectionID),
            true
        ) as? ResponseAdvertisers ?: return

        // Конвертируем модели рекламодателей VIMB в модели базы данных.
        val convertedAdvertisers = convertAdvertisers(response.advertisers, sellingDirection)

        // Сохраняем рекламодателей в базу данных
        srvAdvertisers.save(convertedAdvertisers)

        info("Завершение: загрузка рекламодателей по направлению продаж ID: $sellingDirectionID.")
    }

    /**
     * Метод конвертации моделей рекламодателей в модели базы данных.
     *
     * @param advertisers       [Iterable]          коллекция [ResponseAdvertiser] рекламодателей.
     * @param sellingDirection  [SellingDirection]  направление продаж.
     *
     * @return                  [Iterable] коллекция [AdvertiserXSellingDirection] рекламодателей.
     */
    private fun convertAdvertisers(
        advertisers: Iterable<ResponseAdvertiser>,
        sellingDirection: SellingDirection
    ): Iterable<AdvertiserXSellingDirection> {
        info("Начало: конвертация рекламодателей.")
        info("Параметры: рекламодателей до конвертации ${advertisers.count()} шт.")
        val convertedAdvertisers = runBlocking {
            advertisers.parallelMap { AdvertiserXSellingDirection(Advertiser(it.id, it.name), sellingDirection) }
        }
        info("Параметры: рекламодателей после конвертации ${convertedAdvertisers.count()} шт.")
        info("Завершение: конвертация рекламодателей.")
        return convertedAdvertisers
    }
}