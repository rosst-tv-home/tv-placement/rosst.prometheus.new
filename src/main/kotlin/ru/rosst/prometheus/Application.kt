package ru.rosst.prometheus

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * Главный класс приложения
 */
@SpringBootApplication
class Application

/**
 * Точка входа приложения
 *
 * @param args [Array] коллекция [String] аргументов запуска приложения
 */
fun main(args: Array<String>) {
    // Вызываем метод запуска приложения с переданными аргументами
    runApplication<Application>(*args)
}
