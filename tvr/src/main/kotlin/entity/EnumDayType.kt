package entity

/**
 * Модель перечесляемого типа дня.
 *
 * @property WORKDAY    [EnumDayType.WORKDAY] рабочий.
 * @property HOLIDAY    [EnumDayType.HOLIDAY] праздник.
 * @property DAY_OFF    [EnumDayType.DAY_OFF] выходной.
 */
enum class EnumDayType {
    WORKDAY,
    HOLIDAY,
    DAY_OFF
}