package entity

/**
 * Модель канала.
 *
 * @property name       [String]        название канала.
 * @property months     [MutableList]   изменяемая коллекция [Month] даты.
 */
class Channel(
    val name: String,
    val months: MutableList<Month> = mutableListOf()
)