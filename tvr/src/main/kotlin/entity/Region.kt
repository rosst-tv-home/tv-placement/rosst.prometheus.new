package entity

/**
 * Модель региона.
 *
 * @property name       [String]        назваание региона.
 * @property channels   [MutableList]   изеняеаая коллекция [Channel] каналов.
 */
class Region(
    val name: String,
    val channels: MutableList<Channel> = mutableListOf()
)