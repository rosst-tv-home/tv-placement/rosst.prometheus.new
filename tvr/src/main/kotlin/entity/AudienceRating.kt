package entity

/**
 * Модель tvr аудитории.
 *
 * @property name       [String]  название целевой аудитории.
 * @property rating     [Double]  рейтинг целевой аудитории.
 */
class AudienceRating(
    val name: String,
    val rating: Double = 0.0
)