package entity

/**
 * Модель типа дня.
 *
 * @property type     [EnumDayType] тип дня.
 * @property times    [MutableList] изменяемая коллекция [Time] временных интервалов.
 */
class DayType(
    val type: EnumDayType,
    val times: MutableList<Time> = mutableListOf()
)