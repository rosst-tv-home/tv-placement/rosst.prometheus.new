package entity

import java.time.YearMonth

/**
 * Модель месяца.
 *
 * @property date       [YearMonth]     объект года и месяца.
 * @property dayTypes   [MutableList]   изменяемя коллекция [DayType] типов дней.
 */
class Month(
    val date: YearMonth,
    val dayTypes: MutableList<DayType> = mutableListOf()
)