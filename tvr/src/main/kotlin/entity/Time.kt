package entity

import java.time.LocalTime

/**
 * Модель временного периода.
 *
 * @property timeFrom       [LocalTime]     время начала периода.
 * @property timeBefore     [LocalTime]     время окончания периода.
 * @property audiencesRatings      [MutableList]   изменяемая коллекция [AudienceRating] tvr целевых аудиторий.
 */
class Time(
    val timeFrom: LocalTime,
    val timeBefore: LocalTime,
    val audiencesRatings: MutableList<AudienceRating> = mutableListOf()
)