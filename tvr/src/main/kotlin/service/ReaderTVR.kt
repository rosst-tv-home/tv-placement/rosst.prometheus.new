package service

import entity.*
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.InputStream
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.YearMonth
import java.time.ZoneId

/**
 * Модуль парсинга документа выгрузки TVR ["Excel file"] из Palomars.
 *
 * @author Пакалин Сергей
 * @version 0.0.1
 * @since 21.04.2020
 */
object ReaderTVR {
    /**
     * Метод чтения документа выгрузки TVR файла в формате xlsx ["Excel"].
     *
     * @param inputStream   [InputStream]   файл в формате xlsx ["Excel"].
     *
     * @return              [Iterable]      коллекция [Region] регионов.
     */
    fun read(inputStream: InputStream): Iterable<Region> {
        // Книга excel
        val book = XSSFWorkbook(inputStream)
        // Коллекция строк
        val rows = book.getSheetAt(0).rowIterator().asSequence().toList()
        // Коллекция регионов
        val regions = mutableListOf<Region>()
        // Коллекция целевых аудиторий
        val audiences = mutableListOf<String>()

        // Итерация по строкам EXCEL документа
        rows.forEachIndexed { rowIndex, row ->
            // Если индекс 1 - это заголовки - пропускаем и переходим к следующей строке
            if (rowIndex == 0) return@forEachIndexed

            // Коллекция ячеек строки
            val cells = row.cellIterator().asSequence().toList()

            // Если индекс 0 - целевая аудитория
            if (rowIndex == 1) {
                // Итерация по ячейкам (название целевых аудиторий)
                cells.forEach { audiences.add(it.stringCellValue) }

                // Переход к следующей строке
                return@forEachIndexed
            }

            var region: Region? = null
            var channel: Channel? = null
            var month: Month? = null
            var dayType: DayType?
            var time: Time? = null

            // Итерация по ячейкам строки
            cells.forEachIndexed cells@{ cellIndex, cell ->
                when (cellIndex) {
                    // Регион
                    0 -> {
                        // Ищем регоин в коллекции регоинов или создаём новый
                        region = regions.firstOrNull {
                            it.name == cell.stringCellValue
                        } ?: Region(cell.stringCellValue).apply {
                            // Добавляем регион в коллекцию регионов
                            regions.add(this)
                        }
                    }
                    // Канал
                    1 -> {
                        // Ищем канал, если он есть в коллекции каналов в регионе или создаём новый
                        channel = region?.channels?.firstOrNull {
                            it.name == cell.stringCellValue
                        } ?: Channel(cell.stringCellValue).apply {
                            // Добавляем канал в коллекцию каналов региона
                            region?.channels?.add(this)
                        }
                    }
                    // Месяц
                    2 -> {
                        // Конвертируем Date в LocalDateTime
                        val dateTime = LocalDateTime.ofInstant(
                            cell.dateCellValue.toInstant(),
                            ZoneId.systemDefault()
                        )

                        // Создаём YearMonth из LocalDateTime
                        val yearMonth = YearMonth.of(dateTime.year, dateTime.month)

                        // Создаём объект месяца или ищем уже имеющийся
                        month = channel?.months?.firstOrNull {
                            it.date == yearMonth
                        } ?: Month(yearMonth).apply {
                            // Добавляем месяц в коллекцию месяцев канала
                            channel?.months?.add(this)
                        }
                    }
                    // Временной интервал
                    3 -> {
                        // Конвертируем строковое представление временных интервалов в LocalTime объекты
                        val transformedTime = transformTime(cell.stringCellValue)

                        // Создаём временной интервал
                        time = Time(transformedTime.first, transformedTime.second)
                    }
                    // Тип дня
                    4 -> {
                        // Определяем тип дня
                        val transformedDayType = transformDayType(cell.stringCellValue)

                        // Создаём тип дня либо находим его, если он есть в коллекции месяцев
                        dayType = month?.dayTypes?.firstOrNull {
                            it.type == transformedDayType
                        } ?: DayType(transformedDayType).apply {
                            // Добавляем тип дня в коллекцию месяцев
                            month?.dayTypes?.add(this)
                        }

                        dayType?.times?.add(time!!)
                    }
                    5 -> return@cells
                    // TVR целевой аудитории
                    else -> {
                        // Создаём tvr целевой аудитории
                        AudienceRating(
                            audiences[cellIndex - 6],
                            cell.numericCellValue
                        ).apply {
                            // Добавляем tvr целевой аудитории во временной интервал
                            time?.audiencesRatings?.add(this)
                        }
                    }
                }
            }
        }

        // Возвращаем данные файла EXCEL
        return regions
    }

    /**
     * Нормализации времени.
     *
     * @param time  [Int] часы.
     *
     * @return      [Int] нормализованные часы.
     */
    private fun transformHours(time: Int): Int = when (time) {
        25 -> 1
        24 -> 0
        26 -> 2
        27 -> 3
        28 -> 4
        else -> time
    }

    /**
     * Метод определения и конвертации типа дня.
     *
     * @param dayType       [String]        строковое представление типа дня.
     *
     * @return              [EnumDayType]   тип дня.
     */
    private fun transformDayType(dayType: String): EnumDayType = when (dayType) {
        "Выходной" -> EnumDayType.DAY_OFF
        "Праздник" -> EnumDayType.HOLIDAY
        "Рабочий" -> EnumDayType.WORKDAY
        else -> throw IllegalArgumentException("Ошибка: тип дня \"$dayType\" не определён.")
    }

    /**
     * Конвертация временых интервалов.
     *
     * @param stringTime    [String]    строковое представление временного интервала.
     *
     * @return              [Pair]      пара [LocalTime] времени начала периода и [LocalTime] времени окончания периода.
     */
    private fun transformTime(stringTime: String): Pair<LocalTime, LocalTime> {
        // Массив временных периодов
        val stringifyTimes = stringTime.split("-").map { it.trim() }

        // Массив часов, минут и секунд времени начала периода
        val splitTimeFrom = stringifyTimes[0].split(":")
        // Время начала периода
        val timeFrom = LocalTime.of(
            transformHours(splitTimeFrom[0].toInt()),
            splitTimeFrom[1].toInt(),
            splitTimeFrom[2].toInt()
        )

        // Массив часов, минут и секунд времени окончания периода
        val splitTimeBefore = stringifyTimes[1].split(":")
        // Время окончания периода
        val timeBefore = LocalTime.of(
            transformHours(splitTimeBefore[0].toInt()),
            splitTimeBefore[1].toInt(),
            splitTimeBefore[2].toInt()
        )

        return timeFrom to timeBefore
    }
}