package service.report.entity

import java.time.LocalDate

class PlacementMediaPlan(
    val name: String,
    val type: String,
    val date: LocalDate
) {
    val line = mutableListOf<PlacementLine>()
}