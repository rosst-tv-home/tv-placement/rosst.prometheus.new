package service.report.entity

import java.time.LocalTime

class PlacementLine(
    val programName: String,
    val time: LocalTime
) {
    val days = mutableListOf<PlacementBlock>()
    var issues = mutableListOf<Long>()
}