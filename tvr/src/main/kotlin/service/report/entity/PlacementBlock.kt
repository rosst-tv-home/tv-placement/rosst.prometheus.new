package service.report.entity

import java.time.LocalDate

class PlacementBlock(
    val day: LocalDate,
    val isBlocks: Boolean,
    val affinity: Double,
    val isSpots: Boolean,
    val weekend: Boolean,
    var spotsComment: String? = null
)