package service.report

import org.apache.commons.codec.binary.Hex
import org.apache.poi.ss.usermodel.BorderStyle
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.xssf.usermodel.*
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCfType
import org.openxmlformats.schemas.spreadsheetml.x2006.main.STCfvoType
import service.report.entity.PlacementMediaPlan
import java.awt.Color
import java.io.ByteArrayOutputStream
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

object ServiceReportPlacement {
    private var book: XSSFWorkbook? = null
    private lateinit var sheet: XSSFSheet

    fun createBook() {
        book = XSSFWorkbook()
    }

    fun removeBook() {
        book = null
    }

    fun getResult(): ByteArray {
        // Вызываем автоматический подгон ширины колонок в каждом листе
        autoSizeAllColumns()

        val bos = ByteArrayOutputStream()
        book!!.write(bos)
        bos.flush()
        book!!.close()
        return bos.toByteArray()
    }

    fun createReport(params: PlacementMediaPlan) {
        // Если книга не создана - исключение
        if (book == null) throw RuntimeException("EXCEL кника не инициализирована.")
        // Создаём лист
        createSheet(params)
        // Создаём заголовки
        createHeaders(params)
        // Заполняем таблицу данными
        setData(params)
        // Применяем цветовую шкалу
        bockSheerFormatted(params)
    }

    /**
     * Метод создания листа.
     *
     * @param params [PlacementMediaPlan] параметры медиаплана.
     */
    private fun createSheet(params: PlacementMediaPlan) {
        // Название медиаплана
        val sheetMediaPlanName = params.name.uppercase(Locale.getDefault())
        // Создаём лист
        sheet = book!!.createSheet(sheetMediaPlanName)
        // Фиксируем строку заголовков
        sheet.createFreezePane(2, 1)
    }

    /**
     * Метод создания заголовков листа.
     *
     * @param params [PlacementMediaPlan] параметры медиаплана.
     */
    private fun createHeaders(params: PlacementMediaPlan) {
        // Стиль заголовка
        val headerCellStyle = book!!.createCellStyle().apply {
            alignment = HorizontalAlignment.CENTER
            setFont(book!!.createFont().apply { bold = true })
        }

        // Стиль выходного дня
        val weekendCellStyle = book!!.createCellStyle().apply {
            alignment = HorizontalAlignment.CENTER
            setFont(book!!.createFont().apply {
                bold = true
                setColor(XSSFColor(Color(255, 0, 0), DefaultIndexedColorMap()))
            })
        }

        // Строка заголовков
        val headerRow = sheet.createRow(0)

        // Заголовок "Программа"
        headerRow.createCell(0, CellType.STRING).apply {
            cellStyle = headerCellStyle
            setCellValue("Программа")
        }
        // Заголовок "Время"
        headerRow.createCell(1, CellType.STRING).apply {
            cellStyle = headerCellStyle
            setCellValue("Время")
        }

        // Заголовки "Номер дня месяца"
        params.line.maxByOrNull { it.days.size }?.days?.forEachIndexed { index, day ->
            // Создаём ячейку номера дня месяца
            val monthDayCell = headerRow.createCell(2 + index, CellType.NUMERIC)
            // Применяем стиль
            monthDayCell.cellStyle = headerCellStyle
            // Устанавливаем значение номера дня месяца в ячейку
            monthDayCell.setCellValue(day.day.dayOfMonth.toDouble())

            // Если не выходной день - переходим к следующему
            if (!day.weekend) return@forEachIndexed

            // Окрашиваем день в красный цвет
            monthDayCell.cellStyle = weekendCellStyle
        }
    }

    /**
     * Заполняем таблицу данными.
     *
     * @param params [PlacementMediaPlan] параметры медиаплана.
     */
    private fun setData(params: PlacementMediaPlan) {
        // Стиль времени
        val timeCellStyle = book!!.createCellStyle().apply { alignment = HorizontalAlignment.CENTER }

        // Формируем сетку
        params.line
            // Сортируем рекламные блоки по времени
            .sortedBy { it.time }
            // Итерируемся
            .forEachIndexed { lineIndex, line ->
                // Создаём строку выпуска
                val row = sheet.createRow(1 + lineIndex)

                // Создаём ячейку названия программы
                val programNameCell = row.createCell(0, CellType.STRING)
                // Устанавливаем значение ячейки названия программы
                programNameCell.setCellValue(line.programName)

                // Создаём ячейку времени выхода программы
                row.createCell(1, CellType.STRING).apply {
                    // Применяем стиль
                    cellStyle = timeCellStyle
                    // Устанавливаем значение ячейки времени выхода программы
                    setCellValue(line.time.format(DateTimeFormatter.ofPattern("HH:mm")))
                }

                val blockCellStyle = book!!.createCellStyle().apply {
                    borderBottom = BorderStyle.THIN
                    borderLeft = BorderStyle.THIN
                    borderTop = if (lineIndex == 0) BorderStyle.NONE else BorderStyle.THIN
                    borderRight = BorderStyle.THIN
                    alignment = HorizontalAlignment.CENTER
                    verticalAlignment = VerticalAlignment.CENTER
                }

                val spotCellStyle = book!!.createCellStyle().apply {
                    borderBottom = BorderStyle.THIN
                    borderLeft = BorderStyle.THIN
                    borderTop = if (lineIndex == 0) BorderStyle.NONE else BorderStyle.THIN
                    borderRight = BorderStyle.THIN
                    alignment = HorizontalAlignment.CENTER
                    verticalAlignment = VerticalAlignment.CENTER
                    setFont(book!!.createFont().apply {
                        bold = true
                        fontHeightInPoints = 14
                    })
                }

                // Формируем рекламные блоки этой программы в это время
                line.days.forEachIndexed daysForEach@{ index, day ->
                    if (!day.isBlocks) return@daysForEach
                    // Создаём ячейку рекламного блока
                    val blockCell = row.createCell(2 + index, CellType.NUMERIC)
                    // Устанавливаем значение ячейки рекламного блока (значение affinity)
                    blockCell.setCellValue(day.affinity)

                    // Если текущий рассматриваемый день содержит споты - окрашиваем текст блока в зелёный
                    if (day.isSpots) {
                        addComment(blockCell, day.spotsComment!!)
                        blockCell.cellStyle = spotCellStyle
                    } else {
                        // Применяем стиль к ячейке рекламного блока
                        blockCell.cellStyle = blockCellStyle
                    }
                }
            }
    }

    /**
     * Метод добавления коментария в ячейку.
     *
     * @param cell [XSSFCell] ячейка.
     * @param comment [String] комментарий.
     */
    private fun addComment(cell: XSSFCell, comment: String) {
        val helper = book!!.creationHelper
        val anchor = helper.createClientAnchor().apply {
            setCol1(cell.columnIndex + 1)
            setCol2(cell.columnIndex + 20)
            row1 = cell.rowIndex + 1
            row2 = cell.rowIndex + 10
        }
        val drawing = sheet.createDrawingPatriarch()
        val commentDrawing = drawing.createCellComment(anchor).apply {
            author = "Программатик"
            string = helper.createRichTextString(comment)
        }
        cell.cellComment = commentDrawing
    }

    /**
     * Метод получения групп типа-дня, для применения цветовой шкалы.
     *
     * @param params [PlacementMediaPlan] параметры медиаплана.
     */
    private fun bockSheerFormatted(params: PlacementMediaPlan) {
        // Список рабочих дней
        val workDays = listOf(
            DayOfWeek.MONDAY, DayOfWeek.THURSDAY,
            DayOfWeek.WEDNESDAY, DayOfWeek.TUESDAY,
            DayOfWeek.FRIDAY
        )
        // Список выходных дней
        val weekends = listOf(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY)
        // Группа из типа-дня
        val groups: MutableList<MutableList<LocalDate>> = mutableListOf()

        params.line.maxByOrNull { it.days.size }?.days?.forEachIndexed { index, day ->
            // Первый день?
            if (index == 0) {
                groups.add(mutableListOf(day.day))
                return@forEachIndexed
            }
            val lastGroup = groups.last()
            // Какой тип дня?
            when (day.day.dayOfWeek) {
                // Выходны?
                DayOfWeek.SATURDAY, DayOfWeek.SUNDAY -> {
                    if (weekends.contains(lastGroup.first().dayOfWeek)) {
                        lastGroup.add(day.day)
                        return@forEachIndexed
                    }

                    groups.add(mutableListOf(day.day))
                    return@forEachIndexed
                }
                // Будни?
                else -> {
                    if (workDays.contains(lastGroup.first().dayOfWeek)) {
                        lastGroup.add(day.day)
                        return@forEachIndexed
                    }

                    groups.add(mutableListOf(day.day))
                    return@forEachIndexed
                }
            }
        }

        val references = groups.map { group ->
            val first = group.first()
            val last = group.last()


            // Получаем первую строку
            val firstRow = sheet.getRow(1)
            // Получаем первую ячейку в строке
            val cellFirst =
                firstRow.getCell(2 + groups.flatten().indexOf(first))
                    ?: firstRow.createCell(2 + groups.flatten().indexOf(first))
            // Получаем последнюю строку
            val lastRow = sheet.getRow(params.line.size)
            // Получаем последнюю ячейку в строке
            val cellSecond = lastRow.getCell(2 + groups.flatten().indexOf(last))
                ?: lastRow.createCell(2 + groups.flatten().indexOf(last))
            "${cellFirst.reference}:${cellSecond.reference}"
        }
        setColorScale(references)

    }

    /**
     * Метод применения цветовой шкалы.
     *
     * @param groups [List<String>] получения групп типа-дня.
     */
    private fun setColorScale(groups: List<String>) {
        groups.forEach { reference ->
            val colorCase = sheet.ctWorksheet.addNewConditionalFormatting()
            colorCase.sqref = mutableListOf(reference)

            val myCFrule = colorCase.addNewCfRule()
            myCFrule.type = STCfType.COLOR_SCALE
            myCFrule.priority = 1
            val colorScaledefn = myCFrule.addNewColorScale()
            val minvalue = colorScaledefn.addNewCfvo()

            minvalue.type = STCfvoType.MIN
            minvalue.`val` = "0"

            val midvalue = colorScaledefn.addNewCfvo()
            midvalue.type = STCfvoType.PERCENTILE
            midvalue.`val` = "50"

            val maxvalue = colorScaledefn.addNewCfvo()
            maxvalue.type = STCfvoType.MAX
            maxvalue.`val` = "0"

            val lowColor = colorScaledefn.addNewColor()
            val midColor = colorScaledefn.addNewColor()
            val maxColor = colorScaledefn.addNewColor()

            lowColor.rgb = Hex.decodeHex("F93E47")
            midColor.rgb = Hex.decodeHex("FFF176")
            maxColor.rgb = Hex.decodeHex("529F73")
        }
    }

    /**
     * Метод автоматического подгона ширины колонок таблицы.
     */
    private fun autoSizeAllColumns() {
        // Итерируемся по каждому листу книги
        book!!.sheetIterator().forEach { sheet ->
            // Берём первую строку листа и итерируемся по ячейкам
            sheet.getRow(0).cellIterator().forEach { cell ->
                // Подгоняем ширину колонки
                sheet.autoSizeColumn(cell.columnIndex)
            }
        }
    }
}