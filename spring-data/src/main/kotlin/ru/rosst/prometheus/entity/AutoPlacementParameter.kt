package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import ru.rosst.prometheus.enumerated.AutoPlacementAlgorithm
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*

/**
 * Таблица ["auto_placement_parameters"] "параметры автоматического размещения".
 *
 * @property id                         [Int]                       идентификатор.
 *
 * @param mediaPlan                     [MediaPlan]                 медиаплан.
 * @param algorithm                     [AutoPlacementAlgorithm]    алгоритм автоматического размещения.
 * @param lastPlacementDate             [LocalDateTime]             дата последней автоматической размещения.
 * @param times                         [Set]                       коллекция [AutoPlacementTime] временных периодов автоматического размещения.
 * @param periods                       [Set]                       коллекция [AutoPlacementPeriod] периодов автоматического размещения.
 * @param audience                      [Audience]                  аудитория.
 * @param audienceDate                  [LocalDate]                 дада аудитории.
 * @param inventoryPercentRequired      [Double]                    процент планового инвентаря.
 * @param isPlacementActive             [Boolean]                   признак "автоматическое размещение".
 * @param isAssignedRatingsPlacement    [Boolean]                   признак "размещение по втянутым рейтингам".
 */
@Table(name = "auto_placement_parameters")
@Entity
class AutoPlacementParameter(
    @OneToOne(optional = false)
    @JoinColumn(name = "media_plan_id", unique = true, foreignKey = ForeignKey(name = "fk_media_plan_id"))
    val mediaPlan: MediaPlan,
    @Column(name = "algorithm_type", nullable = false, columnDefinition = "text")
    @Enumerated(EnumType.STRING)
    var algorithm: AutoPlacementAlgorithm,
    @Column(name = "last_placement_date", nullable = false)
    var lastPlacementDate: LocalDateTime = LocalDateTime.now(),
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(
        mappedBy = "parameter",
        fetch = FetchType.EAGER,
        cascade = [CascadeType.MERGE, CascadeType.REFRESH]
    )
    var times: Set<AutoPlacementTime> = hashSetOf(),
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "parameter", fetch = FetchType.EAGER, cascade = [CascadeType.MERGE, CascadeType.REFRESH])
    val periods: Set<AutoPlacementPeriod> = hashSetOf(),
    @ManyToOne
    @JoinColumn(name = "audience_id", foreignKey = ForeignKey(name = "fk_audience_id"))
    var audience: Audience? = null,
    @Column(name = "audience_date", nullable = true)
    var audienceDate: LocalDate? = null,
    @Column(name = "inventory_percent_required", nullable = false)
    var inventoryPercentRequired: Double = 1.0,
    @Column(name = "is_placement_active", nullable = false)
    var isPlacementActive: Boolean,
    @Column(name = "is_assigned_ratings_placement", nullable = false)
    var isAssignedRatingsPlacement: Boolean
) : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is AutoPlacementParameter) return false
        if (other.mediaPlan != this.mediaPlan) return false
        return true
    }

    override fun hashCode(): Int = mediaPlan.hashCode()
}