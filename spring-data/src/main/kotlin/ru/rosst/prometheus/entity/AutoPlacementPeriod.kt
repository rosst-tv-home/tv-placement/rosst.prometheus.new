package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

/**
 * Таблица ["auto_placement_periods"] "периоды автоматического размещения".
 *
 * @param       dateFrom        [LocalDate]                 дата начала периода.
 * @param       dateBefore      [LocalDate]                 дата окончания периода.
 * @param       inventory       [Double]                    инвентарь, необходимый набрать в периоде.
 * @param       percentPrime    [Double]                    процент prime от инвентаря, необходимый набрать в периоде.
 * @param       films           [Set]                       коллекция [AutoPlacementFilm] роликов.
 *
 * @property    id              [Int]                       идентификатор.
 * @property    parameter       [AutoPlacementParameter]    параметры автоматического размещения медиаплана.
 */
@Table(name = "auto_placement_periods")
@Entity
class AutoPlacementPeriod(
    @Column(name = "date_from")
    val dateFrom: LocalDate? = null,
    @Column(name = "date_before")
    val dateBefore: LocalDate? = null,
    @Column(name = "inventory", nullable = false)
    val inventory: Double = 0.0,
    @Column(name = "percent_prime", nullable = false)
    val percentPrime: Double = 0.0,
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "period", fetch = FetchType.EAGER, cascade = [CascadeType.MERGE, CascadeType.REFRESH])
    val films: Set<AutoPlacementFilm> = hashSetOf()
) : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0

    @ManyToOne
    @JoinColumn(name = "auto_placement_parameters_id")
    var parameter: AutoPlacementParameter? = null

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is AutoPlacementPeriod) return false
        if (other.id != this.id) return false
        return true
    }

    override fun hashCode(): Int = id
}