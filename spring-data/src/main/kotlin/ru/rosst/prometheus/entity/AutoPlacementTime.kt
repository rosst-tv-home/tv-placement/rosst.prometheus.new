package ru.rosst.prometheus.entity

import java.io.Serializable
import java.time.LocalTime
import javax.persistence.*

/**
 * Модель временного периода, относящегося к параметрам автоматического размещения медиаплана.
 * Верменной период указывает на время начала и окончания (включительно), которое можно рассматривать
 * программатику для выбора рекламных блоков для расстановки.
 *
 * @property id         [Int]                       идентификатор.
 * @property parameter  [AutoPlacementParameter]    параметры автоматического размещения медиаплана.
 *
 * @param timeFrom      [LocalTime]                 время начала временного периода.
 * @param timeBefore    [LocalTime]                 время окончания временного периода (включительно).
 */
@Table(name = "auto_placement_times")
@Entity
class AutoPlacementTime(
    @Column(name = "time_from", nullable = false)
    val timeFrom: LocalTime,
    @Column(name = "time_before", nullable = false)
    val timeBefore: LocalTime,
    @Column(name = "is_weekend", nullable = false)
    val isWeekend: Boolean
) : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0

    @ManyToOne
    @JoinColumn(name = "auto_placement_parameters_id")
    var parameter: AutoPlacementParameter? = null

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is AutoPlacementTime) return false
        if (other.id != this.id) return false
        return true
    }

    override fun hashCode(): Int = id
}