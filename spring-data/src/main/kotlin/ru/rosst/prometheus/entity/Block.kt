package ru.rosst.prometheus.entity

import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*

/**
 * Модель таблицы ["blocks"] рекламные блоки.
 *
 * @param id                    [Long]          идентификатор.
 * @param tnsId                 [Long]          идентификатор в tns.
 * @param date                  [LocalDate]     дата.
 * @param time                  [LocalDateTime] плановая дата и время выхода.
 * @param deadlineDateTime      [LocalDateTime] дата и время дедлайна.
 * @param weekDay               [Byte]          день недели.
 * @param isPrime               [Boolean]       признак "prime".
 * @param isMinute              [Boolean]       признак "минутный".
 * @param auctionState          [Byte]          статус аукциона: 0 - нет, 1 - планируется, 2 - действует, 3 - закончен.
 * @param volumeFree            [Short]         свободный объём для размещения в fix.
 * @param ratingForecast        [Double]        прогнозный рейтинг.
 * @param issue                 [Issue]         выпуск.
 * @param isBanned              [Boolean]       признак "блок заблокирован для размещения".
 *
 * @property spots              [List]          коллекция [Spot] спотов.
 * @property assignedRatings    [List]          коллекция [BlockAssignedRating] втянутых рейтингов.
 */
@Table(name = "blocks")
@Entity
class Block(
    @Id
    val id: Long,
    @Column(name = "tns_id", nullable = true)
    val tnsId: Long?,
    @Column(nullable = false)
    val date: LocalDate,
    @Column(nullable = false)
    val time: LocalDateTime,
    @Column(name = "deadline_date_time")
    val deadlineDateTime: LocalDateTime,
    @Column(name = "week_day", nullable = false)
    val weekDay: Byte,
    @Column(name = "is_prime", nullable = false)
    val isPrime: Boolean,
    @Column(name = "is_minute", nullable = false)
    val isMinute: Boolean,
    @Column(name = "auction_state", nullable = false)
    val auctionState: Byte,
    @Column(name = "volume_free", nullable = false)
    val volumeFree: Short,
    @Column(name = "rating_forecast", nullable = false)
    val ratingForecast: Double,
    @ManyToOne(cascade = [CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "issue_id", nullable = false, foreignKey = ForeignKey(name = "fk_issue_id"))
    val issue: Issue,
    @Column(name = "is_banned", nullable = false)
    var isBanned: Boolean = false
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "block")
    @LazyCollection(LazyCollectionOption.FALSE)
    val spots: List<Spot> = listOf()

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "block")
    @LazyCollection(LazyCollectionOption.FALSE)
    val assignedRatings: List<BlockAssignedRating> = listOf()
}