package ru.rosst.prometheus.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.rosst.prometheus.entity.PalomarsChannelXBaseAudience.PrimaryKey
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

/**
 * Модель таблицы ["palomars_channels_x_ифыу_audiences"] сопоставления palomars каналов с базовыми аудиториями.
 *
 * @property palomarsChannel    [PalomarsChannel]   palomars канал.
 * @property audience           [Audience]          базовая аудитория.
 * @property date               [LocalDate]         дата, с которой действует базовая аудитория palomars канала.
 *
 * @property primaryKey         [PrimaryKey]        составной главный ключ таблицы.
 */
@Table(name = "palomars_channels_x_base_audiences")
@Entity
class PalomarsChannelXBaseAudience(
    @MapsId("palomars_channel_id")
    @ManyToOne(optional = false, cascade = [CascadeType.MERGE])
    @JoinColumn(
        name = "palomars_channel_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_palomars_channel_id")
    )
    val palomarsChannel: PalomarsChannel,
    @MapsId("audience_id")
    @ManyToOne(optional = false, cascade = [CascadeType.MERGE])
    @JoinColumn(name = "audience_id", nullable = false, foreignKey = ForeignKey(name = "fk_audience_id"))
    val audience: Audience,
    @Column(name = "date", nullable = false, insertable = false, updatable = false)
    val date: LocalDate
) {
    @JsonIgnore
    @EmbeddedId
    val primaryKey = PrimaryKey(palomarsChannel.id, audience.id, date)

    /**
     * Модель составного главного ключа таблицы ["palomars_channels_x_audiences"] "сопоставление базовой аудитории с каналом palomars".
     *
     * @param palomarsChannel   [Int]       идентификатор Palomars канала.
     * @param audience          [Int]       идентификатор аудитории.
     * @param date              [LocalDate] дата, с которой действует базовая аудтория Palomars канала.
     */
    @Embeddable
    class PrimaryKey(
        @Column(name = "palomars_channel_id", nullable = false)
        val palomarsChannel: Int,
        @Column(name = "audience_id", nullable = false)
        val audience: Int,
        @Column(name = "date", nullable = false)
        val date: LocalDate
    ) : Serializable {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as PrimaryKey

            if (palomarsChannel != other.palomarsChannel) return false
            if (audience != other.audience) return false
            if (date != other.date) return false

            return true
        }

        override fun hashCode(): Int {
            var result = palomarsChannel.hashCode()
            result = 31 * result + audience.hashCode()
            result = 31 * result + date.hashCode()
            return result
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PalomarsChannelXBaseAudience

        if (primaryKey != other.primaryKey) return false

        return true
    }

    override fun hashCode(): Int = primaryKey.hashCode()
}