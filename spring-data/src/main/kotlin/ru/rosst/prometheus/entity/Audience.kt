package ru.rosst.prometheus.entity

import javax.persistence.*

/**
 * Модель таблицы ["audiences"] аудитории.
 *
 * @param       name    [String]    название аудитории.
 *
 * @property    id      [Int]       идентификатор аудитории.
 */
@Table(name = "audiences")
@Entity
class Audience(
    @Column(name = "name", unique = true, nullable = false, columnDefinition = "text")
    val name: String
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Audience

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int = name.hashCode()
}