package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["advertisers"] рекламодатели.
 *
 * @param id            [Int]       идентификатор.
 * @param name          [String]    название.
 *
 * @property mapping    [List]      коллекция [AdvertiserXSellingDirection] сопоставлений с направлениями продаж.
 */
@Table(name = "advertisers")
@Entity
class Advertiser(
    @Id
    val id: Int,
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val name: String
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "advertiser")
    val mapping: List<AdvertiserXSellingDirection> = emptyList()
}