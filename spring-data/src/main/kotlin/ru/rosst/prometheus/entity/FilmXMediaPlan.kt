package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDate
import javax.persistence.*

/**
 * Модель таблицы ["films_x_media_plans"] сопоставления роликов с медиапланами.
 *
 * @param id            [Int]       идентификатор.
 * @param dateFrom      [LocalDate] дата начала периода размещения ролика.
 * @param dateBefore    [LocalDate] дата окончания периода размещения ролика.
 * @param mediaPlan     [MediaPlan] медиаплан.
 * @param film          [Film]      ролик.
 *
 * @property spots      [List]      коллекция [Spot] спотов.
 */
@Table(name = "films_x_media_plans")
@Entity
class FilmXMediaPlan(
    @Id
    val id: Int,
    @Column(name = "date_from", nullable = false)
    val dateFrom: LocalDate,
    @Column(name = "date_before", nullable = false)
    val dateBefore: LocalDate,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "media_plan_id", nullable = false, foreignKey = ForeignKey(name = "fk_media_plan_id"))
    val mediaPlan: MediaPlan,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "film_id", nullable = false, foreignKey = ForeignKey(name = "fk_film_id"))
    val film: Film
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "filmXMediaPlan")
    val spots: List<Spot> = emptyList()
}