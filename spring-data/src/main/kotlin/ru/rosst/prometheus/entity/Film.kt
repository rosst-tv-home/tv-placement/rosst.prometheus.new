package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["films"] ролики.
 *
 * @param       id          [Int]       идентификатор.
 * @param       name        [String]    название.
 * @param       versionName [String]    название версии.
 * @param       duration    [Byte]      хронометраж.
 * @param       isInLaw     [Boolean]   признак "соответвует закону о рекламе".
 *
 * @property    mapping     [List]      коллекция [FilmXMediaPlan] сопоставлений с медиапланами.
 */
@Table(name = "films")
@Entity
class Film(
    @Id
    val id: Int,
    @Column(nullable = false, columnDefinition = "text")
    val name: String,
    @Column(name = "version_name", nullable = false, columnDefinition = "text")
    val versionName: String,
    @Column(name = "duration", nullable = false)
    val duration: Byte,
    @Column(name = "is_in_law", nullable = false)
    val isInLaw: Boolean
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "film")
    val mapping: List<FilmXMediaPlan> = emptyList()
}