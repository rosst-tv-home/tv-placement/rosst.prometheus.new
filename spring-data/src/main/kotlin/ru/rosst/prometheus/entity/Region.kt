package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["regions"] регионы.
 *
 * @param id                [Short]             идентификатор.
 * @param name              [String]            название.
 * @param sellingDirection  [SellingDirection]  направление продаж.
 *
 * @property channels       [List]              коллекция [Channel] каналов.
 */
@Entity
@Table(name = "regions")
class Region(
    @Id
    val id: Short,
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val name: String,
    @ManyToOne
    @JoinColumn(
        name = "selling_direction_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_selling_direction_id")
    )
    val sellingDirection: SellingDirection
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "region")
    val channels: List<Channel> = emptyList()
}