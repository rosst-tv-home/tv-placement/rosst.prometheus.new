package ru.rosst.prometheus.entity

interface ChannelLight {
    val id: Int
    val name: String
    val palomarsChannelID: Int?
}