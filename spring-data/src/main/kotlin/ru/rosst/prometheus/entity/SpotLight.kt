package ru.rosst.prometheus.entity

class SpotLight(
    val spotID: Long,
    val isAssignedRatingsPlacement: Boolean,
    val isMinute: Boolean,
    val ratingForecast: Double,
    val rating: Double,
    val duration: Byte
)