package ru.rosst.prometheus.entity

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Модель таблицы ["request_dates"] "даты последних запросов".
 *
 * @param name          [String]        название запроса.
 * @param nextDate      [LocalDateTime] следующая дата запроса.
 * @param previousDate  [LocalDateTime] предыдущая дата запроса.
 */
@Table(name = "request_dates")
@Entity
class RequestDate(
    @Id
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val name: String,
    @Column(name = "previous_date", nullable = false)
    var previousDate: LocalDateTime,
    @Column(name = "next_date", nullable = false)
    var nextDate: LocalDateTime
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is RequestDate) return false
        if (other.name != this.name) return false
        return true
    }

    override fun hashCode(): Int = name.hashCode()
}