package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDate
import javax.persistence.*

/**
 * Модель таблицы ["orders"] заказы.
 *
 * @param id                    [Int]           идентификатор.
 * @param name                  [String]        название.
 * @param dateFrom              [LocalDate]     дата начала периода.
 * @param dateBefore            [LocalDate]     дата окончания периода.
 * @param rank                  [Rank]          ранг типа размещения.
 * @param agreement             [Agreement]     сделка.
 *
 * @property mediaPlans         [List]          коллекция [MediaPlan] медиапланов.
 * @property assignedRatings    [List]          коллекция [BlockAssignedRating] втянутых рейтингов рекламных блоков, относящихся к заказу.
 */
@Table(name = "orders")
@Entity
class Order(
    @Id
    val id: Int,
    @Column(nullable = false, columnDefinition = "text")
    val name: String,
    @Column(name = "date_from", nullable = false)
    val dateFrom: LocalDate,
    @Column(name = "date_before", nullable = false)
    val dateBefore: LocalDate,
    @Column(name = "is_multiple_spots", nullable = true)
    val isMultipleSpots: Boolean?,
    @ManyToOne
    @JoinColumn(name = "rank_id", nullable = false, foreignKey = ForeignKey(name = "fk_rank_id"))
    val rank: Rank,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "agreement_id", nullable = false, foreignKey = ForeignKey(name = "fk_agreement"))
    val agreement: Agreement
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "order")
    val mediaPlans: List<MediaPlan> = emptyList()

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "order")
    val assignedRatings: List<BlockAssignedRating> = emptyList()
}