package ru.rosst.prometheus.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

@Table(name = "ratings")
@Entity
class Rating(
    rmptID: Int,
    month: LocalDate,
    isPrime: Boolean,
    @Column(name ="rating", nullable = false)
    val rating: Double
) : Serializable {
    @EmbeddedId
    val primaryKey = PrimaryKey(rmptID, month, isPrime)

    @Embeddable
    class PrimaryKey(
        @Column(name = "rmpt_id", nullable = false)
        val rmptID: Int,
        @Column(name = "month", nullable = false)
        val month: LocalDate,
        @Column(name = "is_prime", nullable = false)
        val isPrime: Boolean
    ) : Serializable {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as PrimaryKey

            if (rmptID != other.rmptID) return false
            if (month != other.month) return false
            if (isPrime != other.isPrime) return false

            return true
        }

        override fun hashCode(): Int {
            var result = rmptID
            result = 31 * result + month.hashCode()
            result = 31 * result + isPrime.hashCode()
            return result
        }
    }
}