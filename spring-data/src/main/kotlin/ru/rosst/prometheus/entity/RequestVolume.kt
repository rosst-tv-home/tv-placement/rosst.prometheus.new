package ru.rosst.prometheus.entity

import org.springframework.data.annotation.LastModifiedDate
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Модель таблицы ["request_volumes"] "объёмы запросов"
 *
 * @param name              [String]    наименование запроса
 * @param current           [Double]    текущий объём
 * @param maximum           [Double]    максимальный объём
 * @param simulatedMaximum  [Double]    искуственный максимальный объём
 * @param coolDownSeconds   [Short]     время перезарядки объёма в секундах
 */
@Entity
@Table(name = "request_volumes")
data class RequestVolume(
    @Id
    @Column(unique = true, nullable = false, updatable = false)
    val name: String,
    @Column(nullable = false)
    var current: Double,
    @Column(nullable = false, updatable = false)
    val maximum: Double,
    @Column(name = "simulated_maximum", nullable = false, updatable = false)
    val simulatedMaximum: Double,
    @Column(name = "cool_down_seconds", nullable = false, updatable = false)
    val coolDownSeconds: Short,
    @Column(name = "last_modified_date", nullable = false)
    val lastModifiedDate: LocalDateTime = LocalDateTime.now()
)
