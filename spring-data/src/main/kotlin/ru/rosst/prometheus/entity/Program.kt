package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["programs"] программы.
 *
 * @param id        [Int]       идентификатор.
 * @param name      [String]    название.
 * @param channel   [Channel]   канал.
 *
 * @property issues [List]      коллекция [Issue] выпусков.
 */
@Table(name = "programs")
@Entity
class Program(
    @Id
    val id: Int,
    @Column(nullable = false, columnDefinition = "text")
    val name: String,
    @ManyToOne
    @JoinColumn(name = "channel_id", nullable = false, foreignKey = ForeignKey(name = "fk_channel_id"))
    val channel: Channel
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "program")
    val issues: List<Issue> = emptyList()
}