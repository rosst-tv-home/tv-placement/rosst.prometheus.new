package ru.rosst.prometheus.entity

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Модель таблицы ["users"] пользователи.
 *
 * @param login     [String] логин.
 * @param name      [String] имя.
 * @param surname   [String] фамилия.
 * @param password  [String] пароль.
 */
@Table(name = "users")
@Entity
class User(
    @Id
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val login: String,
    @Column(nullable = false, columnDefinition = "text")
    val name: String,
    @Column(nullable = false, columnDefinition = "text")
    val surname: String,
    @Column(nullable = false, columnDefinition = "text")
    val password: String
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is User) return false
        if (other.login != this.login) return false
        return true
    }

    override fun hashCode() = login.hashCode()
}