package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDate
import javax.persistence.*

/**
 * Модель таблицы ["agreements"] сделки.
 *
 * @param id                [Int]                           идентификатор.
 * @param name              [String]                        название.
 * @param dateFrom          [LocalDate]                     дата начала периода.
 * @param dateBefore        [LocalDate]                     дата окончания периода.
 * @param mapping           [AdvertiserXSellingDirection]   сопоставление рекламодателя и направления продаж.
 *
 * @property orders         [List]                          коллекция [Order] заказов.
 */
@Table(name = "agreements")
@Entity
class Agreement(
    @Id
    @Column(name = "id", unique = true, nullable = false)
    val id: Int,
    @Column(name = "name", nullable = false, columnDefinition = "text")
    val name: String,
    @Column(name = "date_from", nullable = false)
    val dateFrom: LocalDate,
    @Column(name = "date_before", nullable = false)
    val dateBefore: LocalDate,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumns(
        JoinColumn(name = "advertiser_id", nullable = false, foreignKey = ForeignKey(name = "fk_advertiser_id")),
        JoinColumn(
            name = "selling_direction_id",
            nullable = false,
            foreignKey = ForeignKey(name = "fk_selling_direction_id")
        )
    )
    val mapping: AdvertiserXSellingDirection
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "agreement")
    val orders: List<Order> = emptyList()
}