package ru.rosst.prometheus.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.rosst.prometheus.entity.AutoPlacementRequestDate.PrimaryKey
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import javax.persistence.*

/**
 * Модель таблицы ["auto_placement_request_dates"] "записи запросов размещения медиапланов"
 *
 * @param date                  [LocalDate]         дата и время размещения
 * @param month                 [LocalDate]         месяц медиапланов
 * @param sellingDirection      [SellingDirection]  направление продаж
 * @param rank                  [Rank]              ранг типа размещения
 *
 * @property primaryKey [PrimaryKey] составной главный ключ таблицы
 */
@Table(name = "auto_placement_request_dates")
@Entity
class AutoPlacementRequestDate(
    @Column(name = "date", nullable = false)
    var date: LocalDateTime,
    @Column(name = "month", nullable = false, insertable = false, updatable = false)
    val month: LocalDate,
    @MapsId("selling_direction_id")
    @ManyToOne
    @JoinColumn(
        name = "selling_direction_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_selling_direction_id")
    )
    val sellingDirection: SellingDirection,
    @MapsId("rank_id")
    @ManyToOne
    @JoinColumn(
        name = "rank_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_rank_id")
    )
    val rank: Rank
) : Serializable {
    @JsonIgnore
    @EmbeddedId
    val primaryKey = PrimaryKey(month, sellingDirection.id, rank.id)

    /**
     * Модель составного главного ключа таблицы ["auto_placement_request_dates"] "записи запросов размещения медиапланов"
     *
     * @param date                  [LocalDate] дата и время размещения
     * @param month                 [LocalDate] месяц медиапланов
     * @param sellingDirectionID    [Byte]      идентификатор направления продаж
     * @param rankID                [Byte]      идентификатор ранга типа размещения
     */
    @Embeddable
    class PrimaryKey(
        @Column(name = "month", nullable = false)
        val month: LocalDate,
        @Column(name = "selling_direction_id", nullable = false)
        val sellingDirectionID: Byte,
        @Column(name = "rank_id", nullable = false)
        val rankID: Byte
    ) : Serializable {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as PrimaryKey

            if (month != other.month) return false
            if (sellingDirectionID != other.sellingDirectionID) return false
            if (rankID != other.rankID) return false

            return true
        }

        override fun hashCode(): Int {
            var result = month.hashCode()
            result = 31 * result + sellingDirectionID
            result = 31 * result + rankID
            return result
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AutoPlacementRequestDate

        if (primaryKey != other.primaryKey) return false

        return true
    }

    override fun hashCode(): Int = primaryKey.hashCode()
}