package ru.rosst.prometheus.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["palomars_channels"] "каналы Palomars".
 *
 * @param       name            [String]    название канала.
 * @param       channel         [Channel]   канал VIMB.
 *
 * @property    ratings         [Set]       коллекция уникальных [PalomarsChannelRating] месяцев.
 * @property    id              [Int]       идентификатор.
 */
@Table(name = "palomars_channels")
@Entity
class PalomarsChannel(
    @Column(name = "name", unique = true, nullable = false, columnDefinition = "text")
    val name: String,
    @JsonIgnore
    @OneToOne(optional = false)
    @JoinColumn(
        name = "channel_id",
        unique = true,
        nullable = false,
        foreignKey = ForeignKey(name = "fk_palomars_channel_channel")
    )
    val channel: Channel
) {
    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "palomarsChannel")
    @LazyCollection(LazyCollectionOption.FALSE)
    val ratings: List<PalomarsChannelRating> = emptyList()

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "palomarsChannel")
    @LazyCollection(LazyCollectionOption.FALSE)
    val baseAudiences: Set<PalomarsChannelXBaseAudience> = emptySet()

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is PalomarsChannel) return false
        if (other.id != this.id) return false

        return true
    }

    override fun hashCode(): Int = id
}