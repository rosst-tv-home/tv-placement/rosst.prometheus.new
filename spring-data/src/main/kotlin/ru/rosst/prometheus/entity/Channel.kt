package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["channels"] каналы.
 *
 * @param id                    [Int]               идентификатор.
 * @param name                  [String]            название.
 * @param isDisabled            [Boolean]           признак "не активен".
 * @param region                [Region]            регион.
 *
 * @property mediaPlans         [List]              коллекция [MediaPlan] медиапланов.
 * @property programs           [List]              коллекция [Program] программ.
 * @property palomarsChannel    [PalomarsChannel]   канал palomars.
 */
@Table(name = "channels")
@Entity
class Channel(
    @Id
    val id: Int,
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val name: String,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "region_id", nullable = false, foreignKey = ForeignKey(name = "fk_region_id"))
    val region: Region,
    @Column(name = "is_disabled", nullable = false)
    val isDisabled: Boolean
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "channel")
    val mediaPlans: List<MediaPlan> = emptyList()

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "channel")
    val programs: List<Program> = emptyList()

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToOne(mappedBy = "channel")
    val palomarsChannel: PalomarsChannel? = null
}
