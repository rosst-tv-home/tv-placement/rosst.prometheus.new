package ru.rosst.prometheus.entity

import ru.rosst.prometheus.entity.BlockAssignedRating.PrimaryKey
import java.io.Serializable
import javax.persistence.*

/**
 * Модель таблицы ["blocks_assigned_ratings"] втянутые рейтинги рекламных блоков в заказах.
 *
 * @param order         [Order]         заказ.
 * @param block         [Block]         рекламный блок.
 * @param rating        [Double]        втянутый рейтинг.
 *
 * @property primaryKey [PrimaryKey]    составной главный ключ.
 */
@Table(name = "blocks_assigned_ratings")
@Entity
class BlockAssignedRating(
    @MapsId("order_id")
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false, foreignKey = ForeignKey(name = "fk_order_id"))
    val order: Order,
    @MapsId("block_id")
    @ManyToOne
    @JoinColumn(name = "block_id", nullable = false, foreignKey = ForeignKey(name = "fk_block_id"))
    val block: Block,
    @Column(name = "rating", nullable = false)
    val rating: Double
) : Serializable {
    @EmbeddedId
    val primaryKey = PrimaryKey(order.id, block.id)

    /**
     * Модель составного главного ключа таблицы ["blocks_assigned_ratings"] втянутых рейтиногов рекламных блоков в заказах.
     *
     * @param order_id [Int]    идентификатор заказа.
     * @param block_id [Long]   идентификатор рекламного блока.
     */
    @Embeddable
    class PrimaryKey(
        @Column(name = "order_id", nullable = false)
        val order_id: Int,
        @Column(name = "block_id", nullable = false)
        val block_id: Long
    ) : Serializable {
        override fun equals(other: Any?): Boolean {
            if (other === this) return true
            if (other !is PrimaryKey) return false
            if (other.order_id != this.order_id) return false
            if (other.block_id != this.block_id) return false
            return true
        }

        override fun hashCode(): Int {
            var result = order_id
            result = 31 * result + block_id.hashCode()
            return result
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is BlockAssignedRating) return false
        if (other.primaryKey != this.primaryKey) return false
        return true
    }

    override fun hashCode(): Int = primaryKey.hashCode()
}