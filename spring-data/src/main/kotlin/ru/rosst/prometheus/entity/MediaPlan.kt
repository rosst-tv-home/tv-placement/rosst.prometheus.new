package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDate
import javax.persistence.*

/**
 * Модель таблицы ["media_plans"] медиапланы.
 *
 * @param id            [Int]       идентификатор.
 * @param name          [String]    название.
 * @param date          [LocalDate] дата размещения.
 * @param isMinute      [Boolean]   признак медиаплана "минутное размещение"
 * @param channel       [Channel]   канал.
 * @param order         [Order]     заказ.
 *
 * @property mapping    [List]      коллекция [FilmXMediaPlan] сопоставлений с роликами.
 */
@Table(name = "media_plans")
@Entity
class MediaPlan(
    @Id
    val id: Int,
    @Column(nullable = false, columnDefinition = "text")
    val name: String,
    @Column(name = "date", nullable = false)
    val date: LocalDate,
    @Column(name = "is_minute", nullable = false)
    val isMinute: Boolean = false,
    @ManyToOne
    @JoinColumn(name = "channel_id", nullable = false, foreignKey = ForeignKey(name = "fk_channel_id"))
    val channel: Channel,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "order_id", nullable = false, foreignKey = ForeignKey(name = "fk_order_id"))
    val order: Order
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "mediaPlan")
    val mapping: Set<FilmXMediaPlan> = setOf()

    @OneToOne(optional = true, orphanRemoval = true, cascade = [CascadeType.REMOVE], mappedBy = "mediaPlan")
    var parameters: AutoPlacementParameter? = null
}