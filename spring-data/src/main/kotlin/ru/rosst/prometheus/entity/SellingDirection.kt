package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*

/**
 * Модель таблицы ["selling_directions"] направления продаж.
 *
 * @param id            [Byte]      идентификатор.
 * @param name          [String]    название.
 *
 * @property regions    [List]      коллекция [Region] регионов.
 * @property agreements [List]      коллекция [Agreement] сделок.
 * @property mapping    [List]      коллекция [AdvertiserXSellingDirection] сопоставлений с рекламодателями.
 */
@Entity
@Table(name = "selling_directions")
class SellingDirection(
    @Id
    val id: Byte,
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val name: String
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "sellingDirection")
    val regions: List<Region> = emptyList()

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "sellingDirection")
    val mapping: List<AdvertiserXSellingDirection> = emptyList()
}