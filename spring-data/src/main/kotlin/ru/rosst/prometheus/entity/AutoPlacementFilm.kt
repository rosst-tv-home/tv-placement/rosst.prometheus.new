package ru.rosst.prometheus.entity

import java.io.Serializable
import javax.persistence.*

/**
 * Таблица ["auto_placement_films"] "ролики автоматического размещения и их инвентарь".
 *
 * @param filmXMediaPlan          [FilmXMediaPlan]        связь ролика и медиаплана.
 * @param inventoryPercent  [Double]                процент инвентаря ролика от инвентаря периода.
 *
 * @property id             [Int]                   идентификатор.
 * @property period         [AutoPlacementPeriod]   период автоматического размещения.
 */
@Table(name = "auto_placement_films")
@Entity
class AutoPlacementFilm(
    @ManyToOne(optional = false)
    @JoinColumn(name = "link_id", foreignKey = ForeignKey(name = "fk_link_id"))
    val filmXMediaPlan: FilmXMediaPlan,
    @Column(name = "inventory_percent", nullable = false)
    val inventoryPercent: Double
) : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0

    @ManyToOne
    @JoinColumn(name = "auto_placement_periods_id")
    var period: AutoPlacementPeriod? = null

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is AutoPlacementFilm) return false
        if (other.id != this.id) return false
        return true
    }

    override fun hashCode(): Int = id
}