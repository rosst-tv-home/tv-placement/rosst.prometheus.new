package ru.rosst.prometheus.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import ru.rosst.prometheus.entity.AdvertiserXSellingDirection.PrimaryKey
import java.io.Serializable
import javax.persistence.*

/**
 * Модель таблицы ["advertisers_x_selling_directions"] сопоставления рекламодателей и направлений продаж.
 *
 * @param advertiser        [Advertiser]        рекламодатель.
 * @param sellingDirection  [SellingDirection]  направление продаж.
 *
 * @property primaryKey     [PrimaryKey]        составной главный ключ.
 */
@Table(name = "advertisers_x_selling_directions")
@Entity
class AdvertiserXSellingDirection(
    @MapsId("advertiser_id")
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "advertiser_id", nullable = false, foreignKey = ForeignKey(name = "fk_advertiser_id"))
    val advertiser: Advertiser,
    @MapsId("selling_direction_id")
    @ManyToOne
    @JoinColumn(
        name = "selling_direction_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_selling_direction_id")
    )
    val sellingDirection: SellingDirection
) {
    @JsonIgnore
    @EmbeddedId
    val primaryKey = PrimaryKey(advertiser.id, sellingDirection.id)

    /**
     * Модель составного главного ключа таблицы ["advertisers_x_selling_directions"] сопоставления рекламодателей и направлений продаж.
     *
     * @param advertiser_id         [Int]   идентификатор рекламодателя.
     * @param selling_direction_id  [Byte]  идентификатор направления продаж.
     */
    @Embeddable
    class PrimaryKey(
        @Column(name = "advertiser_id", nullable = false)
        val advertiser_id: Int,
        @Column(name = "selling_direction_id", nullable = false)
        val selling_direction_id: Byte
    ) : Serializable {
        override fun equals(other: Any?): Boolean {
            if (other === this) return true
            if (other !is PrimaryKey) return false
            if (other.advertiser_id != this.advertiser_id) return false
            if (other.selling_direction_id != this.selling_direction_id) return false
            return true
        }

        override fun hashCode(): Int {
            var result = advertiser_id
            result = 31 * result + selling_direction_id
            return result
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is AdvertiserXSellingDirection) return false
        if (other.primaryKey != this.primaryKey) return false
        return true
    }

    override fun hashCode(): Int = primaryKey.hashCode()
}