package ru.rosst.prometheus.entity

import ru.rosst.prometheus.enumerated.PalomarsTimeDayType
import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.*

@Table(name = "palomars_channels_ratings")
@Entity
class PalomarsChannelRating(
    @ManyToOne(optional = false, cascade = [CascadeType.MERGE])
    @JoinColumn(
        name = "palomars_channel_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_palomars_channel_id")
    )
    val palomarsChannel: PalomarsChannel,
    @ManyToOne(optional = false, cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "audience_id", nullable = false, foreignKey = ForeignKey(name = "fk_audiences_id"))
    var audience: Audience,
    @Column(name = "month", nullable = false)
    val month: LocalDate,
    @Column(name = "time_from", nullable = false)
    val timeFrom: LocalTime,
    @Column(name = "time_before", nullable = false)
    val timeBefore: LocalTime,
    @Column(name = "day_type", nullable = false, columnDefinition = "text")
    @Enumerated(EnumType.STRING)
    val dayType: PalomarsTimeDayType,
    @Column(name = "rating", nullable = false)
    var rating: Double
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0
}