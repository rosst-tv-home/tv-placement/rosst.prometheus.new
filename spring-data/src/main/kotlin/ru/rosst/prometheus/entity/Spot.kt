package ru.rosst.prometheus.entity

import javax.persistence.*

/**
 * Модель таблицы ["spots"] споты.
 *
 * @param id                [Int]               идентификатор.
 * @param isHumanPlacement  [Boolean]           признак "размещён человеком".
 * @param block             [Block]             рекламный блок.
 * @param filmXMediaPlan    [FilmXMediaPlan]    сопоставление ролика с медиапланом.
 */
@Table(name = "spots")
@Entity
class Spot(
    @Id
    val id: Long,
    @Column(name = "is_human_placement", nullable = false)
    val isHumanPlacement: Boolean,
    @ManyToOne
    @JoinColumn(name = "block_id", nullable = false, foreignKey = ForeignKey(name = "fk_block_id"))
    val block: Block,
    @ManyToOne
    @JoinColumn(
        name = "film_x_media_plan_id",
        nullable = false,
        foreignKey = ForeignKey(name = "fk_film_x_media_plan_id")
    )
    val filmXMediaPlan: FilmXMediaPlan
)