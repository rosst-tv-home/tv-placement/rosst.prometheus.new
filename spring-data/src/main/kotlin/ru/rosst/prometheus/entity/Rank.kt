package ru.rosst.prometheus.entity

import ru.rosst.prometheus.entity.Rank.RankType.*
import javax.persistence.*

/**
 * Модель таблицы ["ranks"] ранги типов размещения.
 *
 * @param id    [Byte]      идентификатор.
 * @param name  [String]    название.
 */
@Table(name = "ranks")
@Entity
class Rank(
    @Id
    val id: Byte,
    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false, columnDefinition = "text")
    val name: RankType
) {
    /**
     * Типы рангов.
     *
     * @property FIX            [RankType.FIX]          фиксированное размещение.
     * @property SUPER_FIX      [RankType.SUPER_FIX]    супер фиксированное размещение.
     * @property FLOATING       [RankType.FLOATING]     плавающее размещение.
     * @property LOW_PRIORITY   [RankType.LOW_PRIORITY] низкоприоритетное размещение.
     * @property BONUS          [RankType.BONUS]        бонусное размещение.
     */
    enum class RankType(val placementTypeName: String) {
        FIX("Фикс"),
        SUPER_FIX("Суперфикс"),
        FLOATING("Плавание"),
        LOW_PRIORITY("Низкоприоритет"),
        BONUS("Бонус")
    }
}