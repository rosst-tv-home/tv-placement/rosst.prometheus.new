package ru.rosst.prometheus.entity

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.LocalDateTime
import javax.persistence.*

/**
 * Модель таблицы ["issues"] выпуски.
 *
 * @param       id              [Long]          идентифиатор.
 * @param       dateTimeFrom    [LocalDateTime] дата и время начала.
 * @param       dateTimeBefore  [LocalDateTime] дата и время окончания.
 * @param       program         [Program]       программа.
 *
 * @property    blocks          [List]          коллекция [Block] рекламных блоков.
 */
@Table(name = "issues")
@Entity
class Issue(
    @Id
    val id: Long,
    @Column(name = "date_time_from", nullable = false)
    val dateTimeFrom: LocalDateTime,
    @Column(name = "date_time_before", nullable = false)
    val dateTimeBefore: LocalDateTime,
    @ManyToOne(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @JoinColumn(name = "program_id", nullable = false, foreignKey = ForeignKey(name = "fk_program_id"))
    val program: Program
) {
    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "issue")
    val blocks: List<Block> = emptyList()
}