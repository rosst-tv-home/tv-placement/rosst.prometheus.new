package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.Issue

@Repository
interface RepositoryIssues : JpaRepository<Issue, Long>