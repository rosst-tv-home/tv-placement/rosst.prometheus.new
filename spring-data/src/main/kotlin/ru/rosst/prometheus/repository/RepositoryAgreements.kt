package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.Agreement

/**
 * Репозиторий взаимодействия с таблицей ["agreements"] "сделки".
 */
@Repository
interface RepositoryAgreements : JpaRepository<Agreement, Int>