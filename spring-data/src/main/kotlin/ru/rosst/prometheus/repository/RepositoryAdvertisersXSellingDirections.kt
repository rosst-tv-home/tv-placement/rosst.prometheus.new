package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.AdvertiserXSellingDirection
import ru.rosst.prometheus.entity.SellingDirection

/**
 * Репозиторий взаимодействия с таблицей ["advertisers_x_selling_directions"] сопоставления рекламодателей и направлений продаж.
 */
@Repository
interface RepositoryAdvertisersXSellingDirections :
    JpaRepository<AdvertiserXSellingDirection, AdvertiserXSellingDirection.PrimaryKey> {
    /**
     * Запрос связей рекламодателей и направлений продаж по идентификаторам рекламодателей.
     *
     * @param advertisers   [Set]   коллекция уникальных [Int] идентификаторов рекламодателей.
     *
     * @return              [List]  коллекция [AdvertiserXSellingDirection] связей рекламодателей и направлений продаж.
     */
    @Query("from AdvertiserXSellingDirection link where link.advertiser.id in :advertisers")
    fun findAllByAdvertisersIDs(@Param("advertisers") advertisers: Set<Int>): List<AdvertiserXSellingDirection>

    /**
     * Запрос связей рекламодателей и направлений продаж по идентификатору направления продаж.
     *
     * @param id    [Byte] идентификатор направления продаж.
     *
     * @return      [List] коллекция [AdvertiserXSellingDirection] связей рекламодателей и направлений продаж.
     */
    @Query("from AdvertiserXSellingDirection link where link.sellingDirection.id = :selling_direction_id")
    fun findAllBySellingDirectionID(@Param("selling_direction_id") id: Byte): List<AdvertiserXSellingDirection>

    /**
     * Запрос удаления недействительных связей рекламодателей и направлений продаж.
     * Удаляются связи по указанному направлению продаж, которых нет в коллекции действительных связей рекламодателей и направлений продаж.
     *
     * @param sellingDirection  [SellingDirection]  направление продаж.
     * @param links             [Iterable]          коллекция [AdvertiserXSellingDirection] связей рекламодателей и направлений продаж.
     *
     * @return                  [Int]               количество удалённых записей.
     */
    @Query("delete from AdvertiserXSellingDirection link where link.sellingDirection = :selling_direction and link not in :links")
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("selling_direction") sellingDirection: SellingDirection,
        @Param("links") links: Iterable<AdvertiserXSellingDirection>
    ): Int
}