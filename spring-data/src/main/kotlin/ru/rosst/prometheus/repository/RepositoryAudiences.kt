package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.rosst.prometheus.entity.Audience

interface RepositoryAudiences : JpaRepository<Audience, Int> {
    /**
     * Запрос поиска аудиторий по названиям.
     *
     * @param names [Iterable] коллекция [String] названий аудиторий.
     *
     * @return [Iterable] коллекция [Audience] аудиторий.
     */
    fun findByNameIn(names: Iterable<String>): Iterable<Audience>

    fun findByName(name: String): Audience?
}