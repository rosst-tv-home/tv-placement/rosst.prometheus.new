package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.AutoPlacementFilm
import javax.transaction.Transactional

@Repository
interface RepositoryParametersFilms : JpaRepository<AutoPlacementFilm, Int> {
    @Query("""delete from AutoPlacementFilm apf where apf.period is null""")
    @Modifying
    @Transactional
    fun deleteInvalid(): Int

    /**
     * Запрос удаления параметров недействительных роликов.
     *
     * @param mediaPlans    [Set] коллекция [Int] действительных идентификаторов медиапланов.
     * @param links         [Set] коллекция [Int] действительных идентификаторов связей роликов с медиапланами.
     */
    @Query(
        """
        delete from AutoPlacementFilm film
        where film.filmXMediaPlan.id in (
            select nested_film.filmXMediaPlan.id
            from AutoPlacementFilm nested_film
            join nested_film.period period
            join period.parameter parameter
            where parameter.mediaPlan.id in :media_plans
            and nested_film.filmXMediaPlan.id not in :links
        )
    """
    )
    @Modifying
    @Transactional
    fun deleteInvalidLinks(
        @Param("media_plans")
        mediaPlans: Set<Int>,
        @Param("links")
        links: Set<Int>
    ): Int
}