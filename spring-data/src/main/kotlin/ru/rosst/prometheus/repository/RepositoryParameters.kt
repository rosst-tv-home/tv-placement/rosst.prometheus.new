package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.AutoPlacementParameter
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["auto_placement_parameters"] "параметры автоматического размещения медиапланов".
 */
@Repository
interface RepositoryParameters : JpaRepository<AutoPlacementParameter, Int> {
    /**
     * Метод поиска релевантных для размещения медиапланов
     * @return [List] коллекция [AutoPlacementParameter] параметров автоматического размещения медиапланов
     */
    @Query(
        """
        select *
        from auto_placement_parameters auto_placement_parameter
        join auto_placement_periods auto_placement_period on auto_placement_parameter.id = auto_placement_period.auto_placement_parameters_id
        where auto_placement_period.id in (
            select auto_placement_period_nested.id
            from auto_placement_periods auto_placement_period_nested
            where auto_placement_period_nested.auto_placement_parameters_id = auto_placement_parameter.id
            and auto_placement_period_nested.date_before > NOW()
            order by auto_placement_period_nested.date_before desc
            limit 1
        )
        and auto_placement_parameter.is_placement_active = true
        order by auto_placement_parameter.last_placement_date
        """, nativeQuery = true
    )
    fun getAllRelevantMediaPlans(): List<AutoPlacementParameter>

    /**
     * Метод поиска релевантного медиаплана для инициализации размещения. Ищется медиаплан, который размещался ранее
     * всех медиапланов, т.е. медиаплан, который был размещён, от текущей даты, гараздо раньше всех остальных
     * медиапланов.
     *
     * @return [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    @Query(
        """
        select *
        from auto_placement_parameters auto_placement_parameter
        join auto_placement_periods auto_placement_period on auto_placement_parameter.id = auto_placement_period.auto_placement_parameters_id
        where auto_placement_period.id in (
            select auto_placement_period_nested.id
            from auto_placement_periods auto_placement_period_nested
            where auto_placement_period_nested.auto_placement_parameters_id = auto_placement_parameter.id
            and auto_placement_period_nested.date_before > NOW()
            order by auto_placement_period_nested.date_before desc
            limit 1
        )
        and auto_placement_parameter.is_placement_active = true
        order by auto_placement_parameter.last_placement_date
        limit 1""", nativeQuery = true
    )
    fun getRelevantMediaPlan(): AutoPlacementParameter?

    /**
     * Запрос поиска параметров автоматического размещения по идентификатору медиаплана.
     *
     * @param id [Int] идентификатор медиаплана.
     *
     * @return [AutoPlacementParameter] параметры автоматического размещекния медиаплана.
     */
    fun findByMediaPlanId(id: Int): AutoPlacementParameter?

    @Query(
        "select count(media_plan_id) = 1 from auto_placement_parameters where media_plan_id = :id limit 1",
        nativeQuery = true
    )
    fun isExists(id: Int): Boolean

    @Query(
        "select is_placement_active = true from auto_placement_parameters where media_plan_id = :id limit 1",
        nativeQuery = true
    )
    fun isActive(id: Int): Boolean

    @Query(
        "select sum(inventory) from auto_placement_parameters ap join auto_placement_periods app on ap.id = app.auto_placement_parameters_id where media_plan_id = :id ",
        nativeQuery = true
    )
    fun requiredInventory(id: Int): Double?

    @Query("select inventory_percent_required from auto_placement_parameters where media_plan_id = :id", nativeQuery = true)
    fun requiredInventoryPercent(id: Int): Double?

    /**
     * Запрос удаления параметров автоматического размещения по идентификатору медиаплана.
     *
     * @param mediaPlanID [Int] идентификатор медиаплана.
     */
    @Modifying
    @Transactional
    fun deleteByMediaPlanId(mediaPlanID: Int)

    @Query(
        """
        delete from AutoPlacementParameter app where app.mediaPlan.id in (
            select nestedMediaPlan.id
            from MediaPlan nestedMediaPlan
            join nestedMediaPlan.order ord
            join ord.agreement agreement
            where nestedMediaPlan.id not in :media_plans
            and nestedMediaPlan.date >= :date_from
            and nestedMediaPlan.date <= :date_before
            and nestedMediaPlan.channel.id in :channels 
            and agreement.mapping.advertiser.id in :advertisers
        )
    """
    )
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Set<Int>,
        @Param("media_plans")
        mediaPlans: Set<Int>,
        @Param("advertisers")
        advertisers: Set<Int>
    ): Int
}