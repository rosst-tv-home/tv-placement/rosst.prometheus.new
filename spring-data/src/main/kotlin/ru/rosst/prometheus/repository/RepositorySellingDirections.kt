package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.SellingDirection

/**
 * Репозиторий взаимодействия с таблицей ["selling_directions"] "направления продаж".
 */
@Repository
interface RepositorySellingDirections : JpaRepository<SellingDirection, Byte> {
    @Query("select id from selling_directions", nativeQuery = true)
    fun findAllOnlyIDs(): List<Byte>
}