package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.FilmXMediaPlan
import java.time.LocalDate
import javax.transaction.Transactional

/**
 * Репозиторий взаимодействия с таблицей ["media_plan_x_films"] "связь медиаплана с роликом".
 */
@Repository
interface RepositoryMediaPlanFilmLinks : JpaRepository<FilmXMediaPlan, Int> {
    /**
     * Поиск связей медиапланов с роликами по коллекции идентификаторов.
     *
     * @param links [List] коллекция идентификаторов связей медиапланов с роликами.
     *
     * @return      [List] коллекция [FilmXMediaPlan] связей медиапланов с роликами.
     */
    fun findAllByIdIn(links: Iterable<Int>): List<FilmXMediaPlan>

    /**
     * Поиск связей медиапланов с роликами по идентификатору медиаплана.
     *
     * @param id    [Int]       идентификатор медиаплана.
     *
     * @return      [Iterable]  коллекция [FilmXMediaPlan] связей медиапланов с роликами.
     */
    @Query("select link from FilmXMediaPlan link where link.mediaPlan.id = :id")
    fun findAllByMediaPlanId(@Param("id") id: Int): List<FilmXMediaPlan>?

    /**
     * Запрос удаления недейлствиетельных связей роликов и медиапланов.
     *
     * @return [Int] кол-во удалённых записей.
     */
    @Query(
        """
            delete from FilmXMediaPlan link where link in (
                select nested_link
                from FilmXMediaPlan nested_link
                join nested_link.mediaPlan media_plan
                join media_plan.order ord
                join ord.agreement agreement
                where media_plan.id in :media_plans
                and media_plan.date >= :date_from
                and media_plan.date <= :date_before
                and media_plan.channel.id in :channels
                and agreement.mapping.advertiser.id in :advertisers
                and nested_link.id not in :links
            )
        """
    )
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Iterable<Int>,
        @Param("advertisers")
        advertisers: Iterable<Int>,
        @Param("media_plans")
        mediaPlans: Iterable<Int>,
        @Param("links")
        links: Iterable<Int>
    ): Int
}