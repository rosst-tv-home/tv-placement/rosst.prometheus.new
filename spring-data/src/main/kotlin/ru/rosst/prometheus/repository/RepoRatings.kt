package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.Rating
import java.time.LocalDate

@Repository
interface RepoRatings : JpaRepository<Rating, Rating.PrimaryKey> {
    /**
     *  Запрос очистки таблицы "ratings"
     */
    @Modifying
    @Transactional
    @Query("truncate table ratings", nativeQuery = true)
    fun truncate()

    /**
     * Запрос наполнения таблицы "ratings"
     */
    @Modifying
    @Transactional
    @Query(
        """
        insert into ratings (rmpt_id, month, is_prime, rating)
        select rmpt_id,
               date_trunc('month', b.date)            as month,
               b.is_prime,
               (sum(b.rating_forecast) / count(b.id)) as rating
        from regions r
                 left join channels c on r.id = c.region_id
                 left join programs p on c.id = p.channel_id
                 left join issues i on p.id = i.program_id
                 left join blocks b on i.id = b.issue_id
        where r.is_measurable
          and c.rmpt_id is not null
          and b.rating_forecast is not null
          and b.rating_forecast > 0.0
        group by rmpt_id, month, b.is_prime
        """, nativeQuery = true
    )
    fun fillTable()

    @Query("select r from Rating r where r.primaryKey.rmptID in (:ids) and r.primaryKey.month in (:dates)")
    @Transactional
    fun getRatings(@Param("ids") ids: Set<Int>, @Param("dates") dates: Set<LocalDate>): Iterable<Rating>
}