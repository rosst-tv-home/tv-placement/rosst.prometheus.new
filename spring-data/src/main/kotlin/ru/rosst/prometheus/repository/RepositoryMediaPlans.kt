package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.MediaPlan
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["media_plans"] "медиапланы".
 */
@Repository
interface RepositoryMediaPlans : JpaRepository<MediaPlan, Int> {
    /**
     * Запрос удаления недействительных медиапланов. Удаляются медиапланы, которых нет в коллекции действительных
     * медиапланов по указанным каналам, рекламодателям и периоду.
     *
     * @param dateFrom      [LocalDate] дата начала периода.
     * @param dateBefore    [LocalDate] дата окончания периода.
     * @param channels      [Set]       коллекция уникальных [Int] идентификаторов действительных каналов.
     * @param mediaPlans    [Set]       коллекция уникальных [Int] идентификаторов действительных медиапланов.
     * @param advertisers   [Set]       коллекция уникальных [Int] идентификаторов действительных рекламодателей.
     *
     * @return              [Int]       количество удалённых недействительных медиапланов.
     */
    @Query(
        """
        delete from MediaPlan mediaPlan where mediaPlan.id in (
            select nestedMediaPlan.id
            from MediaPlan nestedMediaPlan
            join nestedMediaPlan.order ord
            join ord.agreement agreement
            where nestedMediaPlan.id not in :media_plans
            and nestedMediaPlan.date >= :date_from
            and nestedMediaPlan.date <= :date_before
            and nestedMediaPlan.channel.id in :channels 
            and agreement.mapping.advertiser.id in :advertisers
        )
    """
    )
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Set<Int>,
        @Param("media_plans")
        mediaPlans: Set<Int>,
        @Param("advertisers")
        advertisers: Set<Int>
    ): Int

    /**
     * Запрос медиапланов по указанным каналам, рекламодателям, направлению продаж и периоду.
     *
     * @param sellingDirection  [Byte]      идентификатор направления продаж.
     * @param dateFrom          [LocalDate] дата начала периода.
     * @param dateBefore        [LocalDate] дата окончания периода.
     * @param channels          [Set]       коллекция уникальных [Int] идентификаторов каналов.
     * @param advertisers       [Set]       коллекция уникальных [Int] идентификаторов рекламодателей.
     *
     * @return [List] коллекция [MediaPlan] медиапланов.
     */
    @Query(
        """
        select media_plan
        from MediaPlan media_plan
        join media_plan.channel channel
        join media_plan.order ord
        join ord.agreement agreement
        join agreement.mapping.sellingDirection sellingDirection
        join agreement.mapping.advertiser advertiser
        where sellingDirection.id = :selling_direction
        and media_plan.date >= :date_from
        and media_plan.date <= :date_before
        and channel.id in :channels
        and advertiser.id in :advertisers
    """
    )
    fun findAllBySellingDirectionChannelsAdvertisersAndPeriod(
        @Param("selling_direction")
        sellingDirection: Byte,
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Set<Int>,
        @Param("advertisers")
        advertisers: Set<Int>
    ): List<MediaPlan>
}