package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.PalomarsChannel

/**
 * Репозиторий взаимодействия с таблицей ["palomars_channels"] "каналы Palomars".
 */
@Repository
interface RepositoryPalomarsChannels : JpaRepository<PalomarsChannel, Int> {
    /**
     * Запрос поиска канала Palomars по имени.
     *
     * @param name  [String]            название канала Palomars.
     *
     * @return      [PalomarsChannel]   канал Palomars.
     */
    @Query("select palomars_channel from PalomarsChannel palomars_channel where palomars_channel.name = :name")
    fun findByName(@Param("name") name: String): PalomarsChannel?

    /**
     * Запрос поиска каналов Palomars по именам.
     *
     * @param names  [Iterable] коллекция    [String] названий каналов Palomars.
     *
     * @return      [Iterable]   коллекция   [PalomarsChannel] каналов Palomars.
     */
    @Query("select palomars_channel from PalomarsChannel palomars_channel where palomars_channel.name in :names")
    fun findByNames(@Param("names") names: Iterable<String>): Iterable<PalomarsChannel>
}