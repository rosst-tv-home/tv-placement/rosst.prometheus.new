package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.*
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["spots"] "споты".
 */
@Repository
interface RepositorySpots : JpaRepository<Spot, Long> {
    /**
     * Запрос спотов медиаплана.
     *
     * @param mediaPlan [MediaPlan] медиаплан.
     *
     * @return          [List]      коллекция [Spot] спотов.
     */
    @Query("select spot from Spot spot join spot.filmXMediaPlan link where link.mediaPlan = :media_plan")
    fun findAllByMediaPlan(@Param("media_plan") mediaPlan: MediaPlan): List<Spot>

    /**
     * Метод удаления недействительных спотов. Во входные параметры передаются все параметры, по которым были запрошены
     * споты у VIMB API. В запросе к базе данных запрашиваются споты с такими же параметрами как и запрос к VIMB API.
     * Коллекция действительных спотов может быть пустая, а может и нет. Если коллекция не пуста, то все споты,
     * указанные в этой коллекции будут исключены из запроса удаления спотов из базы данных.
     *
     * @param spots         [Iterable]  коллекция [Spot] действительных спотов.
     * @param dateFrom      [LocalDate] дата начала периода.
     * @param dateBefore    [LocalDate] дата окончания периода.
     * @param channels      [Iterable]  коллекция [Channel] каналов.
     * @param advertisers   [Iterable]  коллекция [Advertiser] рекламодателей.
     *
     * @return [Int] количество удалённых спотов.
     */
    @Query(
        """
        delete from Spot spot where spot in (
            select spot_nested
            from Spot spot_nested
            join spot_nested.block block
            join spot_nested.filmXMediaPlan link
            join link.mediaPlan media_plan
            join media_plan.order ord
            join ord.agreement agreement
            join block.issue issue
            join issue.program program
            where (spot_nested not in :spots or :spots is null)
            and block.date >= :date_from
            and block.date <= :date_before
            and program.channel in :channels
            and agreement.mapping.advertiser in :advertisers
        )
    """
    )
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("spots")
        spots: Iterable<Spot>? = null,
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Iterable<Channel>,
        @Param("advertisers")
        advertisers: Iterable<Advertiser>
    ): Int

    @Query(
        """
        select distinct new ru.rosst.prometheus.entity.SpotLight(s.id, app.isAssignedRatingsPlacement, b.isMinute, b.ratingForecast, bar.rating, f.duration)
        from Spot s
        join s.block b on s.block.id = b.id
         join s.filmXMediaPlan fxmp on s.filmXMediaPlan.id = fxmp.id
         join fxmp.film f on fxmp.film.id = f.id
         join fxmp.mediaPlan mp on mp.id = fxmp.mediaPlan.id
         join mp.parameters app on mp.id = app.mediaPlan.id
        join b.assignedRatings bar on b.id = bar.block.id and mp.order.id = bar.order.id
         where mp.id = :id
    """
    )
    fun spotsLight(id: Int): List<SpotLight>?
}