package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.Region
import ru.rosst.prometheus.entity.SellingDirection

/**
 * Репозиторий взаимодействия с таблицей ["regions"] "регионы".
 */
@Repository
interface RepositoryRegions : JpaRepository<Region, Short> {
    /**
     * Запрос удаления недействительных регионов.
     * Удаляются регионы, которых нет в коллекции действительных регионов по указанному направлению продаж.
     *
     * @param regions           [Iterable]          коллекция действительных [Region] регионов.
     * @param sellingDirection  [SellingDirection]  направление продаж.
     *
     * @return                  [Int]               количество удалённых регионов.
     */
    @Query("delete from Region region where region.sellingDirection = :selling_direction and region not in :regions")
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("selling_direction")
        sellingDirection: SellingDirection,
        @Param("regions")
        regions: Iterable<Region>
    ): Int
}