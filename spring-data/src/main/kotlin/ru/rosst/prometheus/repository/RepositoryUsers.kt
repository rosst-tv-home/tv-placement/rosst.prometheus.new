package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.User

/**
 * Репозиторий взаимодействия с таблицей ["users"] "пользователи".
 */
@Repository
interface RepositoryUsers : JpaRepository<User, String> {
    /**
     * Запрос поиска пользователя по логину.
     *
     * @param login [String]    логин.
     * @return      [User]      пользователь.
     */
    fun findByLogin(@Param("login") login: String): User?
}