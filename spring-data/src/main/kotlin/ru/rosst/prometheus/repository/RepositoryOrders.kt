package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.Advertiser
import ru.rosst.prometheus.entity.Channel
import ru.rosst.prometheus.entity.Order
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["orders"] "заказы".
 */
@Repository
interface RepositoryOrders : JpaRepository<Order, Int> {
    /**
     * Запрос удаления недействительных заказов.
     * Удаляются заказы, которых нет в коллекции действительных заказов по указанным каналам, рекламодателям и периоду.
     *
     * @param dateFrom      [LocalDate] дата начала периода медиапланов.
     * @param dateBefore    [LocalDate] дата окончания периода медиапланов.
     * @param orders        [Iterable]  коллекция действительных заказов.
     * @param channels      [Iterable]  коллекция действительных каналов.
     * @param advertisers   [Iterable]  коллекция действительных рекламодателей.
     *
     * @return              [Int]       количество удаленных заказов.
     */
    @Query(
        """
        delete from Order ord
        where ord in (
            select ord_nested
            from MediaPlan media_plan
            join media_plan.order ord_nested
            join ord_nested.agreement agreement
            where ord_nested not in :orders
            and ord_nested.dateFrom >= :date_from
            and ord_nested.dateBefore <= :date_before
            and media_plan.channel in :channels
            and agreement.mapping.advertiser in :advertisers
        )
    """
    )
    @Modifying
    @Transactional
    fun deleteInvalid(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("orders")
        orders: Iterable<Order>,
        @Param("channels")
        channels: Iterable<Channel>,
        @Param("advertisers")
        advertisers: Iterable<Advertiser>
    ): Int

    /**
     * Запрос заказов по идентификаторам.
     *
     * @param orders    [Set]   коллекция уникальных [Int] идентификаторов.
     *
     * @return          [List]  коллекция [Order] заказов.
     */
    fun findAllByIdIn(orders: Set<Int>): List<Order>
}