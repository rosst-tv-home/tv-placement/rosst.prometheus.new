package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.Advertiser

/**
 * Репозиторий взаимодействия с таблицей ["advertisers"] рекламодатели.
 */
@Repository
interface RepositoryAdvertisers : JpaRepository<Advertiser, Int>