package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.PalomarsChannelXBaseAudience

/**
 * Репозиторий взаимодействия с таблицей ["palomars_channel_x_audience"] "сопоставление базовой аудитории с каналом palomars".
 */
@Repository
interface RepositoryPalomarsChannelsXBaseAudiences :
    JpaRepository<PalomarsChannelXBaseAudience, PalomarsChannelXBaseAudience.PrimaryKey>