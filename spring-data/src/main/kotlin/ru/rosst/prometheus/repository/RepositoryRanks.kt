package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.Rank

/**
 * Репозиторий взаимодействия с таблицей ["ranks"] "ранги".
 */
@Repository
interface RepositoryRanks : JpaRepository<Rank, Byte>