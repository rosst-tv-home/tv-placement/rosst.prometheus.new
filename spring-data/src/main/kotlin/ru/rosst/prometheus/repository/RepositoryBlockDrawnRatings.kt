package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.*
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["blocks_drawn_ratings"] "втянутые рейтинги блоков" базы данных.
 */
@Repository
interface RepositoryBlockDrawnRatings : JpaRepository<BlockAssignedRating, BlockAssignedRating.PrimaryKey> {
    /**
     * Удаление недействительных втянутых рейтингов рекламных блоков.
     *
     * @param dateFrom          [LocalDate] дата начала периода действительных втянутых рейтингов рекламных блоков.
     * @param dateBefore        [LocalDate] дата окончания периода действительных втянутых рейтингов рекламных блоков.
     * @param channels          [Iterable]  коллекция [Channel] каналов втянутых рейтингов рекламных блоков.
     * @param advertisers       [Iterable]  коллекция [Advertiser] рекламодателей втянутых рейтингов рекламных блоков.
     * @param assignedRatings   [Iterable]  коллекция действительных [BlockAssignedRating] втянутых рейтингов рекламных блоков.
     *
     * @return                  [Int]       количество удалённых записей.
     */
    @Query(
        """
            delete from BlockAssignedRating bdr
            where bdr.primaryKey in (
                select bdr_nested.primaryKey
                from BlockAssignedRating bdr_nested
                join bdr_nested.block block
                join block.issue issue
                join issue.program program
                join bdr_nested.order ord
                join ord.agreement agreement
                where bdr_nested not in :block_drawn_ratings
                and block.date >= :date_from
                and block.date <= :date_to
                and program.channel in :channels
                and agreement.mapping.advertiser in :advertisers
            )
        """
    )
    @Modifying
    @Transactional
    fun setInvalid(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_to")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Iterable<Channel>,
        @Param("advertisers")
        advertisers: Iterable<Advertiser>,
        @Param("block_drawn_ratings")
        assignedRatings: Iterable<BlockAssignedRating>
    ): Int

    /**
     * Удаление недействительных втянутых рейтингов рекламных блоков.
     *
     * @param dateFrom          [LocalDate] дата начала периода действительных втянутых рейтингов рекламных блоков.
     * @param dateBefore        [LocalDate] дата окончания периода действительных втянутых рейтингов рекламных блоков.
     * @param channels          [Iterable]  коллекция [Channel] каналов втянутых рейтингов рекламных блоков.
     * @param advertisers       [Iterable]  коллекция [Advertiser] рекламодателей втянутых рейтингов рекламных блоков.
     *
     * @return                  [Int]       количество удалённых записей.
     */
    @Query(
        """
            delete from BlockAssignedRating bdr
            where bdr.primaryKey in (
                select bdr_nested.primaryKey
                from BlockAssignedRating bdr_nested
                join bdr_nested.block block
                join block.issue issue
                join issue.program program
                join bdr_nested.order ord
                join ord.agreement agreement
                where block.date >= :date_from
                and block.date <= :date_before
                and program.channel in :channels
                and agreement.mapping.advertiser in :advertisers
            )
        """
    )
    @Modifying
    @Transactional
    fun setInvalid(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Iterable<Channel>,
        @Param("advertisers")
        advertisers: Iterable<Advertiser>
    ): Int

    /**
     * Поиск втянутых рейтингов рекламных блоков по рекламным блокам и заказу.
     *
     * @param order     [Order]     заказ.
     * @param blocks    [Iterable]  коллекция [Block] рекламных блоков.
     *
     * @return          [List]      коллекция [BlockAssignedRating] втянутых рейтингов.
     */
    @Query("select bdr from BlockAssignedRating bdr where bdr.order = :ord and bdr.block in :blocks")
    fun findAllByBlocksAndOrder(
        @Param("ord")
        order: Order,
        @Param("blocks")
        blocks: Iterable<Block>
    ): List<BlockAssignedRating>
}