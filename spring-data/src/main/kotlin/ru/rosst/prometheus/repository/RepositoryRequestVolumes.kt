package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.RequestVolume
import javax.transaction.Transactional

/**
 * Репозиторий взаимодействия с таблицей ["request_volumes"] "объёмы запросов"
 */
@Repository
interface RepositoryRequestVolumes : JpaRepository<RequestVolume, String> {
    /**
     * Запрос поиска данных об объёме запроса по его наименованию
     * @param name [String] наименование запроса
     * @return [RequestVolume] найденный объём запроса
     */
    @Transactional
    @Query("select * from request_volumes where name = :name limit 1", nativeQuery = true)
    fun get(@Param("name") name: String): RequestVolume?

    /**
     * Запрос обновления текущего объёма запроса
     * @param name [String] наименование запроса
     * @param value [Double] значение увеличения/уменьшения текущего объёма
     */
    @Modifying
    @Transactional
    @Query(
        """
        update request_volumes set
        current = current + :value,
        last_modified_date = now()
        where name = :name
        """, nativeQuery = true
    )
    fun update(@Param("name") name: String, @Param("value") value: Double)
}