package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.Block
import ru.rosst.prometheus.entity.Channel
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["blocks"] "рекламные блоки" базы данных.
 */
@Repository
interface RepositoryBlocks : JpaRepository<Block, Long> {
    /**
     * Запрос поиска и удаления недействительных рекламных блоков.
     *
     * @param blocks        [Iterable]  коллекция [Block] действительных рекламных блоков.
     * @param dateFrom      [LocalDate] дата начала периода действительных рекламных блоков.
     * @param dateBefore    [LocalDate] дата окончания периода действительных рекламных блоков.
     * @param channels      [Iterable]  коллекция [Channel] каналов действительных рекламных блоков.
     *
     * @return              [Int]       количество удалённых недействительных релкамных блоков.
     *
     * P.S. выборку блоков нужно делать по столбцу block.date, так как это
     * действительная дата блока, время блока может указывать на утро следующего месяца,
     * что все равно является принадлежностью блока к предыдущему месяцу.
     */
    @Query(
        """
            delete from blocks
            where date >= :date_from
            and date <= :date_before
            and id in (
                select b.id
                from blocks b
                         join issues i on i.id = b.issue_id
                         join programs p on i.program_id = p.id
                where p.channel_id in (:channels)
                  and b.date >= :date_from
                  and b.date <= :date_before
                  and b.id not in (:blocks)
            )
        """, nativeQuery = true
    )
    @Modifying
    @Transactional
    fun removeInvalidBlocks(
        @Param("blocks")
        blocks: Set<Long>,
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channels")
        channels: Set<Int>
    ): Int

    /**
     * Поиск блоков по коллекции идентификаторов.
     * @param blocks [Iterable] коллекция [Long] идентификаторов блоков.
     * @return коллекция блоков.
     */
    fun findAllByIdIn(blocks: Iterable<Long>): MutableSet<Block>

    /**
     * Метод поиска рекламных блоков, принадлежащих медиаплану.
     *
     * @param dateFrom      [LocalDate] дата начала периода блоков.
     * @param dateBefore    [LocalDate] дата окончания периода блоков.
     * @param channel       [Channel]   канал.
     *
     * @return              [Iterable]  коллекция [Block] рекламных блоков.
     */
    @Query(
        """
        select block
        from Block block
        join block.issue issue
        join issue.program program
        where block.ratingForecast is not null
        and program.channel = :channel
        and block.date >= :date_from
        and block.date <= :date_before
    """
    )
    fun findAllByMediaPlan(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channel")
        channel: Channel
    ): Iterable<Block>

    /**
     * Метод поиска рекламных блоков, принадлежащих медиаплану и удовлетворяющие переданным параметрам.
     *
     * @param dateFrom          [LocalDate] дата начала периода размещения.
     * @param dateBefore        [LocalDate] дата окончания периода размещения.
     * @param channelId         [Int]       канал, на котором будет проходить размещение.
     * @param minimalFreeVolume [Short]     минимальное значение объёма.
     *
     * @return                  [Iterable]  коллекция [Block] рекламных блоков.
     */
    @Query(
        """
            select block
            from Block block
            join block.issue issue
            join issue.program program
            where block.deadlineDateTime > CURRENT_TIMESTAMP
            and block.ratingForecast is not null
            and block.volumeFree >= :minimal_timing
            and program.channel.id = :channel_id
            and block.date >= :date_from
            and block.date <= :date_before
        """
    )
    @Transactional
    fun findAllByMediaPlan(
        @Param("date_from")
        dateFrom: LocalDate,
        @Param("date_before")
        dateBefore: LocalDate,
        @Param("channel_id")
        channelId: Int,
        @Param("minimal_timing")
        minimalFreeVolume: Short
    ): Iterable<Block>

    /**
     * Запрос отметки рекламных блоков с запретом на размещение.
     * @param blocks [Set] коллекция [Long] идентификаторов рекламных блоков с запретом на размещение.
     */
    @Query("update Block block set block.isBanned = true where block.id in :blocks")
    @Modifying
    @Transactional
    fun markBanned(@Param("blocks") blocks: Set<Long>)
}