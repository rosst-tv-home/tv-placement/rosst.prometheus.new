package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import ru.rosst.prometheus.entity.Channel
import ru.rosst.prometheus.entity.ChannelLight
import ru.rosst.prometheus.entity.Region

/**
 * Репозиторий взаимодействия с таблицей ["channels"] "каналы".
 */
@Repository
interface RepositoryChannels : JpaRepository<Channel, Int> {
    /**
     * Запрос удаления недействительных каналов.
     * Удаляются каналы, которых нет в коллекции действительных каналов по указанной коллекции регионов.
     *
     * @param channels  [Iterable]  коллекция действительных [Channel] каналов.
     * @param regions   [Iterable]  коллекция действительных [Region] регионов.
     *
     * @return          [Int]       количество удалённых каналов.
     */
    @Query(
        """
        delete from Channel channel
        where channel not in :channels
        and channel.region in (select region from Region region where region in :regions)
    """
    )
    @Modifying
    @Transactional
    fun deleteInvalid(@Param("channels") channels: Iterable<Channel>, @Param("regions") regions: Iterable<Region>): Int

    /**
     * Запрос коллекции каналов по идентификатору направления продаж.
     *
     * @param id    [Byte] идентификатор направления продаж.
     *
     * @return      [List] коллекция [Channel] каналов.
     */
    @Query(
        """
        select channel 
        from Channel channel
        join channel.region region
        where region.sellingDirection.id = :selling_direction_id
    """
    )
    fun findAllBySellingDirectionID(@Param("selling_direction_id") id: Byte): List<Channel>

    @Query(
        "select c.id, c.name, pc.id as palomarsChannelID from channels c " +
        "join regions r on c.region_id = r.id " +
        "join selling_directions sd on r.selling_direction_id = sd.id " +
        "left join palomars_channels pc on c.id = pc.channel_id " +
        "where sd.id = :selling_direction_id and c.is_disabled = false and r.id not in (593, 958)", nativeQuery = true
    )
    fun findAllBySellingDirectionIDLight(@Param("selling_direction_id") id: Byte): List<ChannelLight>

    @Query(
        "select c.id, c.name, pc.id as palomarsChannelID from channels c " +
        "left join palomars_channels pc on c.id = pc.channel_id " +
        "where c.id in :channels", nativeQuery = true
    )
    fun findAllByIDsLight(@Param("channels") channelIDs: Iterable<Int>): List<ChannelLight>

    /**
     * Метод запроса идентификаторов измеряемых каналов по указанному направлению продаж
     */
    @Query(
        """
            select c.id
            from channels c
            join regions r on r.id = c.region_id
            where r.selling_direction_id = :id
            and r.is_measurable = true
            and c.is_disabled = false
        """, nativeQuery = true
    )
    fun findAllMeasurableIDsBySellingDirectionID(@Param("id") id: Byte): Iterable<Int>
}