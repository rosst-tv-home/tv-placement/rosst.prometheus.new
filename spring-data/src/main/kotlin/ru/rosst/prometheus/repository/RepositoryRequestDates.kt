package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.RequestDate

/**
 * Репозиторий взаимодействия с таблицей ["request_dates"] "даты последних запросов".
 */
@Repository
interface RepositoryRequestDates : JpaRepository<RequestDate, String> {
    /**
     * Запрос поиска даты последнего запроса по названию.
     *
     * @param name  [String]        название запроса.
     *
     * @return      [RequestDate]   дата последнего запроса.
     */
    fun findFirstByName(name: String): RequestDate?
}