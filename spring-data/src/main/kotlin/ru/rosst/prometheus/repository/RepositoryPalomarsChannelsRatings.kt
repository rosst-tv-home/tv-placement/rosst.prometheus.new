package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import ru.rosst.prometheus.entity.Audience
import ru.rosst.prometheus.entity.Channel
import ru.rosst.prometheus.entity.PalomarsChannel
import ru.rosst.prometheus.entity.PalomarsChannelRating
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["palomars_audiences"] "TVR".
 */
@Repository
interface RepositoryPalomarsChannelsRatings : JpaRepository<PalomarsChannelRating, Int> {

    /**
     * Запрос поиска известных рейтингов.
     *
     * @param months    [Iterable] коллекция [LocalDate] дат.
     * @param channels  [Iterable] коллекция [Int] идентификаторов каналов Palomars.
     * @param audiences [Iterable] коллекция [Int] идентификаторов аудиторий.
     *
     * @return [Iterable] коллекция известных [PalomarsChannelRating] рейтингов.
     */
    @Query(
        """
        select rating from PalomarsChannelRating rating
        where rating.month in :months
        and rating.palomarsChannel.id in :channels
        and rating.audience.id in :audiences
    """
    )
    fun findExistingRatings(
        @Param("months")
        months: Iterable<LocalDate>,
        @Param("channels")
        channels: Iterable<Int>,
        @Param("audiences")
        audiences: Iterable<Int>
    ): Iterable<PalomarsChannelRating>

    /**
     * Запрос поиска аудиторий канала.
     *
     * @param channel [Channel] канал.
     *
     * @return [Iterable] коллекция [PalomarsChannelRating] рейтингов.
     */
    @Query(
        """
        select rating from PalomarsChannelRating rating
        join rating.palomarsChannel p_channel
        where p_channel.channel = :channel
    """
    )
    fun findChannelAudiences(@Param("channel") channel: Channel): Iterable<PalomarsChannelRating>

    /**
     * Запрос поиска рейтинга канала по аудитории.
     *
     * @param audience  [Audience]          аудитория.
     * @param channel   [PalomarsChannel]   palomars канал.
     * @param date      [LocalDate]         дата.
     *
     * @return [Iterable] коллекция [PalomarsChannelRating] рейтингов.
     */
    @Query(
        """
        select rating from PalomarsChannelRating rating
        where rating.audience = :audience
        and rating.palomarsChannel = :channel
        and rating.month = :date
    """
    )
    fun findChannelAudienceRating(
        @Param("audience") audience: Audience,
        @Param("channel") channel: PalomarsChannel,
        @Param("date") date: LocalDate
    ): Iterable<PalomarsChannelRating>
}
