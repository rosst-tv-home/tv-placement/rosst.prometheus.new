package ru.rosst.prometheus.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import ru.rosst.prometheus.entity.AutoPlacementRequestDate
import java.time.LocalDate

/**
 * Репозиторий взаимодействия с таблицей ["auto_placement_request_dates"] "записи последних запросов размещения медиапланов"
 */
interface RepositoryAutoPlacementRequestDates :
    JpaRepository<AutoPlacementRequestDate, AutoPlacementRequestDate.PrimaryKey> {

    /**
     * Метод запроса релевантных записей запросов размещения медиапланов
     * @return [List] коллекция [AutoPlacementRequestDate] записей последних запросов размещения медиапланов
     */
    @Query(
        """
    select request
    from AutoPlacementRequestDate request
    where date_trunc('month', request.month) >= date_trunc('month', current_date)
    """
    )
    fun getAllRelevantRequestDates(): List<AutoPlacementRequestDate>

    /**
     * Метод запроса поиска записи запроса размещения медиапланов
     *
     * @param sellingDirectionID [Byte] идентификатор направления продаж
     * @param rankID [Byte] идентификатор ранга типа размещения
     * @param month [LocalDate] месяц медиапланов
     *
     * @return запись запроса размещения медиапланов
     */
    fun findBySellingDirectionIdAndRankIdAndMonth(
        sellingDirectionID: Byte,
        rankID: Byte,
        month: LocalDate
    ): AutoPlacementRequestDate?
}