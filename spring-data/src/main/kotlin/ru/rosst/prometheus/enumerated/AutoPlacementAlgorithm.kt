package ru.rosst.prometheus.enumerated

/**
 * Алгоритм автоматического размещения.
 *
 * @property AFFINITIES          по высокорейтинговым рекламным блокам.
 * @property EQUAL_DISTRIBUTION  равномерное распределение.
 */
enum class AutoPlacementAlgorithm {
    AFFINITIES,
    EQUAL_DISTRIBUTION
}