package ru.rosst.prometheus.enumerated

import ru.rosst.prometheus.enumerated.PalomarsTimeDayType.*

/**
 * Модель перечесляемого типа дня.
 *
 * @property WORKDAY    [PalomarsTimeDayType.WORKDAY] рабочий.
 * @property HOLIDAY    [PalomarsTimeDayType.HOLIDAY] праздник.
 * @property DAY_OFF    [PalomarsTimeDayType.DAY_OFF] выходной.
 */
enum class PalomarsTimeDayType {
    WORKDAY,
    HOLIDAY,
    DAY_OFF
}