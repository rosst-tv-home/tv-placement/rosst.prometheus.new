package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.PalomarsChannel
import ru.rosst.prometheus.repository.RepositoryPalomarsChannels

/**
 * Сервис управления данными таблицы ["palomars_channels"] "каналы palomars".
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoPalomarsChannels [RepositoryPalomarsChannels] репозиторий взаимодействия с таблицей ["palomars_channels"] "каналы Palomars".
 */
@Service
class ServicePalomarsChannels : AbstractService() {
    @Autowired
    private lateinit var repoPalomarsChannels: RepositoryPalomarsChannels

    /**
     * Метод сохранения каналов Palomars.
     *
     * @param channels [Iterable] коллекция [PalomarsChannel] каналов Palomars.
     */
    fun save(channels: Iterable<PalomarsChannel>) {
        info("Начало: сохранение/обновление каналов Palomars.")
        repoPalomarsChannels.saveAll(channels)
        repoPalomarsChannels.flush()
        info("Завершение: сохранение/обновление каналов Palomars.")
    }

    /**
     * Метод поиска канала Palomars по названию.
     *
     * @param name [String] название канала Palomars.
     *
     * @return [PalomarsChannel] канал Palomars.
     */
    fun findByName(name: String): PalomarsChannel? {
        info("Начало: поиск канала Palomars по названию.")
        val channel = repoPalomarsChannels.findByName(name)
        info("Завершение: поиск канала Palomars по названию.")
        return channel
    }

    /**
     * Метод поиска каналов Palomars по названиям.
     *
     * @param names [Iterable]  коллекция [String] названий каналов Palomars.
     *
     * @return      [Iterable]  коллекция [PalomarsChannel] каналов Palomars.
     */
    fun findByNames(names: Iterable<String>): Iterable<PalomarsChannel> {
        info("Начало: поиск каналов Palomars по названию.")
        val channels = repoPalomarsChannels.findByNames(names)
        info("Завершение: поиска каналов Palomars по названию")
        return channels
    }

    fun findByID(id: Int): PalomarsChannel? {
        return repoPalomarsChannels.findByIdOrNull(id)
    }
}