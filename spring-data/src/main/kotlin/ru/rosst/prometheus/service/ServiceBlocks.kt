package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.repository.RepositoryBlockDrawnRatings
import ru.rosst.prometheus.repository.RepositoryBlocks
import java.time.LocalDate

/**
 * Сервис управления данными таблицы ["blocks"] "рекламные блоки" базы данных.
 *
 * Репозитории взаимодействия с данными таблиц базы данных:
 * @property repoBlocks             [RepositoryBlocks]              репозиторий взаимодействия с данными таблицы ["blocks"] "рекламные блоки".
 * @property repoBlockDrawnRatings  [RepositoryBlockDrawnRatings]   репозиторий взаимодействия с данными таблицы ["blocks_assigned_ratings"] "втянутые рейтинги рекламных блоков".
 */
@Component
class ServiceBlocks : AbstractService() {
    @Autowired
    private lateinit var repoBlocks: RepositoryBlocks

    @Autowired
    private lateinit var repoBlockDrawnRatings: RepositoryBlockDrawnRatings

    /**
     * Метод запроса сохранения и\или обновления рекламных блоков в базу(-е) данных.
     *
     * @param blocks        [Iterable]  коллекция [Block] рекламных блоков.
     * @param setInvalid    [Boolean]   признак "найти и удалить недействительные рекламные блоки в базе данных".
     * @param dateFrom      [LocalDate] дата начала периода недействительных рекламных блоков (должна быть такая же, как при запросе к vimb api).
     * @param dateBefore    [LocalDate] дата окончания периода недействительных рекламных блоков (должна быть такая же, как при запросе к vimb api).
     * @param channels      [Iterable]  коллекция [Channel] каналов недействительных рекламных блоков (должна быть такая же, как при запросе к vimb api).
     */
    fun saveBlocks(
        blocks: Iterable<Block>,
        setInvalid: Boolean = false,
        dateFrom: LocalDate? = null,
        dateBefore: LocalDate? = null,
        channels: List<ChannelLight>? = null
    ) {
        info("Начало: сохранение и\\или обновление рекламных блоков.")

        // Если требуется найти и удалить недействительные рекламные блоки
        if (setInvalid) {
            // Если дополнительные параметры не были указаны
            if (dateFrom == null || dateBefore == null || channels == null || channels.count() == 0) {
                warning("Предупреждение: установлен флаг поиска и удаления недействительных рекламных блоков в базе данных...")
                warning("Предупреждение: , но не указаны дополнительные параметры, такие как:...")
                warning("Предупреждение: дата начала, окончания и коллекция каналов недействительных рекламных блоков.")
                warning("Предупреждение: сохранение и\\или обновление рекламных блоков продолжится без поиска и удаления недействительных рекламных блоков.")
            } else {
                // Ищем и удаляем недействительные рекламные блоки
                removeInvalidBlocks(blocks, dateFrom, dateBefore, channels)
            }
        }

        blocks
            .chunked(Short.MAX_VALUE - 1)
            .forEach { repoBlocks.saveAll(it) }

        repoBlocks.flush()

        info("Завершение: сохранение и\\или обновление рекламных блоков.")
    }

    /**
     * Метод сохранения/обновления данных о втянутых рейтингах рекламных блоков в базе данных. Тут же вызывается метод
     * поиска и отметки недействительных записей о втянутых рейтингах рекламных блоков, если переданы все требующиеся
     * параметры.
     *
     * @param assignedRatings коллекция втянутых рейтингов рекламных блоков.
     * @param isSetInvalid признак "требуется поиск и отметка недействительных записей о втянутых рейтингах рекламных
     * блоков".
     * @param dateFrom дата начала периода втянутых рейтингов рекламных блоков.
     * @param dateBefore дата окончания периода втянутых рейтингов рекламных блоков.
     * @param channels коллекция каналов втянутых рейтингов рекламных блоков.
     *
     * @return [Unit] ничего.
     */
    fun saveDrawnRatings(
        assignedRatings: Iterable<BlockAssignedRating>?,
        isSetInvalid: Boolean = false,
        dateFrom: LocalDate? = null,
        dateBefore: LocalDate? = null,
        channels: Collection<Channel>? = null,
        advertisers: Collection<Advertiser>? = null
    ) {
        // Проверяем, требуется ли поиск и отметка недействительных записей о втянутых рейтингах рекламных блоков
        //        if (
        //            isSetInvalid && dateFrom != null && dateBefore != null &&
        //            !channels.isNullOrEmpty() && !advertisers.isNullOrEmpty()
        //        ) {
        //            // Вызываем метод поиска и отметки недействительных записей
        //            setInvalidBlockDrawnRatings(assignedRatings, dateFrom, dateBefore, channels, advertisers)
        //        }

        info("Начало: сохранение и/или обновление втянутых рейтингов блоков...")
        if (assignedRatings != null && assignedRatings.count() > 0) {
            repoBlockDrawnRatings.saveAll(assignedRatings)
            repoBlockDrawnRatings.flush()
        }
        info("Завершение: сохранение и/или обновление втянутых рейтингов блоков.")
    }

    /**
     * Метод поиска и отметки недействительных записей втянутых рейтингов рекламных блоков в базе данных.
     * @param assignedRatings коллекция действительных втянутых рейтингов рекламных блоков.
     * @param dateFrom дата начала периода действительных втянутых рейтингов рекламных блоков.
     * @param dateBefore дата окончания периода действительных втянутых рейтингов рекламных блоков.
     * @param channels коллекция каналов втянутых рейтингов рекламных блоков.
     * @return [Unit] ничего.
     */
    private fun setInvalidBlockDrawnRatings(
        assignedRatings: Iterable<BlockAssignedRating>?,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Iterable<Channel>,
        advertisers: Iterable<Advertiser>
    ) {
        info("Начало: поиск и отметка недействительных записей втянутых рейтингов рекламных блоков в базе данных.")
        val invalidBlockDrawnRatingsCount = if (assignedRatings == null || assignedRatings.count() < 1) {
            repoBlockDrawnRatings.setInvalid(dateFrom, dateBefore, channels, advertisers)
        } else {
            repoBlockDrawnRatings.setInvalid(dateFrom, dateBefore, channels, advertisers, assignedRatings)
        }
        info(
            """
            Завершение: поиск и отметка недействительных записей втянутых
            рейтингов рекламных блоков в базе данных: $invalidBlockDrawnRatingsCount шт.
        """
        )
    }

    /**
     * Метод запроса поиска и удаления недействительных рекламных блоков в базе данных.
     *
     * @param blocks        [Iterable]  коллекция дейсвтительных [Block] рекламных блоков.
     * @param dateFrom      [LocalDate] дата начала периода недействительных рекламных блоков.
     * @param dateBefore    [LocalDate] дата окончания периода недействительных рекламных блоков.
     * @param channels      [Iterable]  коллекция действительных [Channel] каналов рекламных блоков.
     */
    private fun removeInvalidBlocks(
        blocks: Iterable<Block>,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: List<ChannelLight>
    ): Int {
        info("Начало: поиск и удаление недействительных рекламных блоков.")

        // Количество удалённых недействительных рекламных блоков
        val tasks = channels.map { channel ->
            channel to blocks.filter { it.issue.program.channel.id == channel.id }
        }.map { pair ->
            repoBlocks.removeInvalidBlocks(
                pair.second.map { it.id }.toSet(),
                dateFrom,
                dateBefore,
                setOf(pair.first.id)
            )
        }
        val count = tasks.sumOf { it }

        info("Выполнение: удалено недействительных рекламных блоков [$count].")
        info("Завершение: поиск и удаление недействительных рекламных блоков.")
        return count
    }

    fun findAllByIds(blocks: Set<Long>): MutableSet<Block> = blocks
        .chunked(Short.MAX_VALUE - 1)
        .flatMap { repoBlocks.findAllByIdIn(it) }
        .toMutableSet()

    fun findAllByMediaPlan(mediaPlan: MediaPlan): Iterable<Block> {
        return repoBlocks.findAllByMediaPlan(
            mediaPlan.date,
            mediaPlan.date.plusMonths(1).minusDays(1),
            mediaPlan.channel
        )
    }

    /**
     * Поиск подходящих для размещения рекламных блоков.
     * @param mediaPlan медиаплан.
     * @param minimalTiming минимальный хронометраж роликов медиаплана.
     * @return [Iterable] коллекция [Block] рекламных блоков.
     */
    fun findAllByMediaPlan(mediaPlan: MediaPlan, minimalTiming: Int): Iterable<Block> {
        return repoBlocks.findAllByMediaPlan(
            mediaPlan.date,
            mediaPlan.date.plusMonths(1).minusDays(1),
            mediaPlan.channel.id,
            minimalTiming.toShort()
        )
    }

    /**
     * Метод запроса отметки заблокированных для размещения рекламных блоков.
     * @param blocks [Set] коллекция [Long] идентификаторов рекламных блоков, заблокированных для размещения.
     */
    fun markBanned(blocks: Set<Long>) {
        info("Начало: отметка рекламных блоков с запретом на размещение.")
        repoBlocks.markBanned(blocks)
        info("Завершение: отметка рекламных блоков с запретом на размещение.")
    }
}