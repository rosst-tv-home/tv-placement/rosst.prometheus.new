package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.repository.*
import java.time.LocalDate

/**
 * Сервис управления данными таблицы ["auto_placement_parameters"] "параметры автоматического размещения" базы данных.
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoParameters                 [RepositoryParameters]                  репозиторий взаимодействия с таблицей ["auto_placement_parameters"] "параметры автоматического размещения медиаплана".
 * @property repoParametersFilms            [RepositoryParametersFilms]             репозиторий взаимодействия с таблицей ["auto_placement_films"] "параметры роликов".
 * @property repoParametersTimes            [RepositoryParametersTimes]             репозиторий взаимодействия с таблицей ["auto_placement_times"] "параметры временных интервалов".
 * @property repoParametersPeriods          [RepositoryParametersPeriods]           репозиторий взаимодействия с таблицей ["auto_placement_periods"] "параметры периодов".
 * @property repoAutoPlacementRequestDates  [RepositoryAutoPlacementRequestDates]   репозиторий взаимодействия с таблицей ["auto_placement_request_dates"] "записи последних запросов размещения медиапланов"
 */
@Service
class ServiceAutoPlacementParameters : AbstractService() {
    @Autowired
    private lateinit var repoParameters: RepositoryParameters

    @Autowired
    private lateinit var repoParametersFilms: RepositoryParametersFilms

    @Autowired
    private lateinit var repoParametersTimes: RepositoryParametersTimes

    @Autowired
    private lateinit var repoParametersPeriods: RepositoryParametersPeriods

    @Autowired
    private lateinit var repoAutoPlacementRequestDates: RepositoryAutoPlacementRequestDates

    /**
     * Метод сохранения параметров автоматического размещения медиаплана.
     *
     * @param entities переменное число [AutoPlacementParameter] параметров автоматического размещения медиапланов.
     * @return [Unit] ничего.
     */
    fun save(entities: AutoPlacementParameter): AutoPlacementParameter {
        info("Начало: сохранение и/или обновление коллекции параметров автоматического размещения.")
        val parameters = repoParameters.save(entities)
        info("Завершение: сохранение и/или обновление коллекции параметров автоматического размещения.")
        return parameters
    }

    fun findAll(): Iterable<AutoPlacementParameter> = repoParameters.findAll()

    fun findById(id: Int): AutoPlacementParameter? = repoParameters.findByIdOrNull(id)

    /**
     * Метод сохранения параметров ролика.
     *
     * @param film  [AutoPlacementFilm] параметры ролика.
     *
     * @return      [AutoPlacementFilm] сохранённые параметры ролика.
     */
    fun saveFilm(film: AutoPlacementFilm): AutoPlacementFilm {
        info("Начало: сохранение параметров ролика.")
        val savedFilm = repoParametersFilms.save(film)
        info("ЗАвершение: сохранение параметров ролика.")
        return savedFilm
    }

    /**
     * Метод сохранения параметров временных интервалов.
     *
     * @param times [Iterable] коллекция [AutoPlacementTime] параметров временных интервалов.
     *
     * @return      [Iterable] коллекция [AutoPlacementTime] сохранённых параметров временных интервалов.
     */
    fun saveTimes(times: Iterable<AutoPlacementTime>): Iterable<AutoPlacementTime> {
        info("Начало: сохранение параметров временных интервалов.")
        info("Параметры: ${times.count()} шт. параметров временных интервалов.")
        val savedTimes = repoParametersTimes.saveAll(times)
        info("Завершение: сохранение параметров временных интервалов.")
        return savedTimes
    }

    /**
     * Метод сохранения параметров периода.
     *
     * @param period   [AutoPlacementPeriod] параметры периода.
     *
     * @return         [AutoPlacementPeriod] сохранённые параметры периода.
     */
    fun savePeriod(period: AutoPlacementPeriod): AutoPlacementPeriod {
        info("Начало: сохранение параметров периода.")
        val savedPeriod = repoParametersPeriods.save(period)
        info("Завершение: сохранение параметров периода.")
        return savedPeriod
    }

    /**
     * Метод поиска релевантного медиаплана для инициализации размещения. Ищется медиаплан, который размещался ранее
     * всех медиапланов, т.е. медиаплан, который был размещён, от текущей даты, гараздо раньше всех остальных
     * медиапланов.
     *
     * @return [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    fun getRelevantMediaPlan(): AutoPlacementParameter? = repoParameters.getRelevantMediaPlan()

    /**
     *  Метод поиска релевантных для размещения медиапланов
     *  @return [List] коллекцияь [AutoPlacementParameter] параметров автоматического размещения медиапланов
     */
    fun getAllRelevantMediaPlans(): List<AutoPlacementParameter> = repoParameters.getAllRelevantMediaPlans()

    /**
     * Метод поиска релевантных записей запросов размещения медиапланов
     * @return [List] коллекция [AutoPlacementRequestDate] записей последних запросов размещения медиапланов
     */
    fun getAllRelevantRequestDates() = repoAutoPlacementRequestDates.getAllRelevantRequestDates()

    /**
     * Метод запроса поиска записи запроса размещения медиапланов
     *
     * @param sellingDirectionID [Byte] идентификатор направления продаж
     * @param rankID [Byte] идентификатор ранга типа продаж
     * @param month [LocalDate] месяц медиапланов
     *
     * @return запись запроса размещения медиапланов
     */
    fun findRequestDate(
        sellingDirectionID: Byte,
        rankID: Byte,
        month: LocalDate
    ) = repoAutoPlacementRequestDates.findBySellingDirectionIdAndRankIdAndMonth(
        sellingDirectionID,
        rankID,
        month
    )


    fun saveRequestDate(requestDate: AutoPlacementRequestDate) {
        repoAutoPlacementRequestDates.saveAndFlush(requestDate)
    }

    /**
     * Метод поиска параметров автоматического размещения по идентификатору медиаплана.
     *
     * @param id [Int] идентификатор медиаплана.
     *
     * @return [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    fun findByMediaPlanID(id: Int): AutoPlacementParameter? {
        info("Начало: поиск параметров автоматического размещения медиаплана.")
        info("Параметры: [$id] идентификатор медиаплана.")
        val parameters = repoParameters.findByMediaPlanId(id)
        info("Завершение: поиск параметров автоматического размещения медиаплана.")
        return parameters
    }

    /**
     * Метод поиска параметров автоматического размещения по идентификатору медиаплана.
     *
     * @param id [Int] идентификатор медиаплана.
     *
     * @return [AutoPlacementParameter] параметры автоматического размещения медиаплана.
     */
    fun isExists(id: Int): Boolean {
        info("Начало: поиск параметров автоматического размещения медиаплана.")
        info("Параметры: [$id] идентификатор медиаплана.")
        val isExists = repoParameters.isExists(id)
        info("Завершение: поиск параметров автоматического размещения медиаплана.")
        return isExists
    }

    fun isActive(id: Int): Boolean {
        info("Начало: поиск признака активности размещения.")
        info("Параметры: [$id] идентификатор медиаплана.")
        val isActive = repoParameters.isActive(id)
        info("Завершение: поиск признака активности размещения.")
        return isActive
    }

    fun requiredInventory(id: Int): Double {
        val inventory = repoParameters.requiredInventory(id) ?: return 0.0;
        return inventory
    }

    fun requiredInventoryPercent(id: Int): Double {

        val inventory = repoParameters.requiredInventoryPercent(id)?: return 0.0;

        return inventory
    }


    /**
     * Метод удаления параметров по идентификатору.
     *
     * @param id [Int] идентификатор параметров.
     */
    fun deleteByID(id: Int) {
        info("Начало: удаление параметров автоматического размещения.")
        repoParameters.deleteById(id)
        repoParameters.flush()
        info("Завершение: удаление параметров автоматического размещения.")
    }

    /**
     * Метод удаления параметров по идентификатору медиаплана.
     *
     * @param id [Int] идентификатор медиаплана.
     */
    fun deleteByMediaPlanID(id: Int) {
        info("Начало: удаление параметров автоматического размещения.")
        repoParameters.deleteByMediaPlanId(id)
        info("Завершение: удаление параметров автоматического размещения.")
    }

    fun deleteInvalid(
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Iterable<Int>,
        mediaPlans: Iterable<Int>,
        advertisers: Iterable<Int>
    ) {
        info("Начало: удаление параметров недействительных медиапланов.")
        val count = repoParameters.deleteInvalid(
            dateFrom,
            dateBefore,
            channels.toSet(),
            mediaPlans.toSet(),
            advertisers.toSet()
        )
        repoParametersFilms.deleteInvalid()
        info("Удалено $count параметров недейсвтительных медиапланов.")
        info("Завершение: удаление параметров недейтсвтиельных медиапланов.")
    }

    /**
     * Метод запроса удаления параметров недействительных роликов медиапланов.
     */
    fun deleteInvalidLinks(
        mediaPlans: Iterable<Int>,
        links: Iterable<Int>
    ) {
        info("Начало: удаления параметров недействительных роликов медиапланов.")
        val count = repoParametersFilms.deleteInvalidLinks(mediaPlans.toSet(), links.toSet())
        info("Удалено $count параметров недействительных роликов медиапланов.")
        info("Завершение: удаление параметров недействительных роликов медиапланов.")
    }
}