package ru.rosst.prometheus.service

import kotlinx.coroutines.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.RequestVolume
import ru.rosst.prometheus.repository.RepositoryRequestVolumes
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Service
class ServiceRequestVolumes : AbstractService() {
    private lateinit var counterJob: Job

    @Autowired
    private lateinit var repoRequestVolumes: RepositoryRequestVolumes

    /**
     *  Метод инициализации счётчика объёмов запросов
     */
    @PostConstruct
    private fun initializeCounter() {
        info("Начало: инициализация счётчика объёмов запросов...")
        counterJob = GlobalScope.launch(Dispatchers.IO) {
            val delaySeconds = 15L
            while (isActive) {
                delay(1000L * delaySeconds)

                recalculateRequestVolume(delaySeconds, get("blocks"))
                recalculateRequestVolume(delaySeconds, get("media_plans"))
            }
        }
        info("Завершение: инициализация счётчика объёмов запросов...")
    }

    /**
     * Метод уничтожения счётчика объёмов запросов
     */
    @PreDestroy
    private fun terminateCounter() = runBlocking {
        info("Начало: уничтожение счётчика объёмов...")
        counterJob.cancelAndJoin()
        info("Завершение: уничтожение счётчика объёмов")
    }

    private fun recalculateRequestVolume(delaySeconds: Long, rv: RequestVolume) {
        if (rv.current == rv.simulatedMaximum) return;
        val secondsUntil = rv.lastModifiedDate.until(LocalDateTime.now(), ChronoUnit.SECONDS)
        val multiplier = if (secondsUntil > delaySeconds + 2L) secondsUntil else delaySeconds
        val calculatedValue = rv.simulatedMaximum / rv.coolDownSeconds * multiplier
        val value = if (rv.current + calculatedValue > rv.simulatedMaximum)
            rv.simulatedMaximum - rv.current
        else calculatedValue
        update(rv.name, value)
    }

    /**
     * Метод получения объёма запроса по его наименования
     * @param name  [String]        наименование запроса
     * @return      [RequestVolume] объём запроса
     */
    fun get(name: String): RequestVolume = repoRequestVolumes.get(name) ?: create(name)

    /**
     * Метод обновления объёма запроса по его наименованию
     * @param name  [String] наименование запроса
     * @param value [Double] значение увеличения/уменьшения текущего объёма
     */
    fun update(name: String, value: Double) = repoRequestVolumes.update(name, value)

    /**
     * Метод создания модели объёма, в случае, если она не была найдена в базе данных
     * @param name  [String]        наименование запроса
     * @return      [RequestVolume] модель объёма запроса
     */
    private fun create(name: String): RequestVolume = when (name) {
        "blocks" -> repoRequestVolumes.saveAndFlush(RequestVolume(name, 366.0, 366.0, 360.0, 2700))
        "media_plans" -> repoRequestVolumes.saveAndFlush(RequestVolume(name, 12.0, 12.0, 12.0, 600))
        else -> throw Exception("Не удалось создать модель объёма запроса с наименованием: [$name]")
    }
}