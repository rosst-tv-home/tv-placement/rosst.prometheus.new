package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.*
import ru.rosst.prometheus.repository.RepositorySpots
import java.time.LocalDate

/**
 * Сервис управления данными таблицы ["spots"] "споты".
 *
 * Репозитории взаимодействия с данными таблиц базы данных:
 * @property repoSpots [RepositorySpots] репозиторий взаимодействия с данными таблицы ["spots"] "споты".
 */
@Service
class ServiceSpots : AbstractService() {
    @Autowired
    private lateinit var repoSpots: RepositorySpots

    /**
     * Метод сохранения/обновления спотов.
     *
     * @param spots         [Collection]    коллекция действительных [Spot] спотов.
     * @param dateFrom      [LocalDate]     дата начала периода.
     * @param dateBefore    [LocalDate]     дата окончания периода.
     * @param channels      [Collection]    коллекция [Channel] каналов.
     * @param advertisers   [Collection]    коллекция [Advertiser] рекламодателей.
     */
    fun save(
        spots: Collection<Spot>? = null,
        dateFrom: LocalDate? = null,
        dateBefore: LocalDate? = null,
        channels: Collection<Channel>? = null,
        advertisers: Collection<Advertiser>? = null
    ) {
        // Если переданы параметры запроса к VIMB API, значит требуется найти и удалить недействительные споты
        if (dateFrom != null && dateBefore != null && !channels.isNullOrEmpty() && !advertisers.isNullOrEmpty()) {
            deleteInvalid(spots, dateFrom, dateBefore, channels, advertisers)
        }

        // Если коллекция спотов пуста, то сохранение в базу данных не требуется - завершение метода
        if (spots.isNullOrEmpty()) return

        // Рекламных блоков очень много, база данных не может сохранить такой объём за раз
        // (только в случае работающего batch). Поэтому вручную разбиваем коллекция на несколько частей
        // и сохраняем в базу
        info("Начало: сохранение/обновление спотов.")
        if (spots.count() >= Short.MAX_VALUE - 1) {
            spots.chunked(Short.MAX_VALUE.toInt() - 1).forEach {
                repoSpots.saveAll(it)
                repoSpots.flush()
            }
        } else {
            repoSpots.saveAll(spots)
            repoSpots.flush()
        }
        info("Завершение: сохранение/обновление спотов.")
    }

    /**
     * Метод удаления недействительных спотов.
     *
     * @see RepositorySpots.deleteInvalid
     *
     * @param spots         [Collection]    коллекция действительных [Spot] спотов.
     * @param dateFrom      [LocalDate]     дата начала периода.
     * @param dateBefore    [LocalDate]     дата окончания периода.
     * @param channels      [Collection]    коллекция действительных [Channel] каналов.
     * @param advertisers   [Collection]    коллекция действительных [Advertiser] рекламодателей.
     */
    private fun deleteInvalid(
        spots: Collection<Spot>? = null,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Collection<Channel>,
        advertisers: Collection<Advertiser>
    ) {
        //        if (blocks.count() >= Short.MAX_VALUE - 1) {
        //            blocks.chunked(Short.MAX_VALUE.toInt() - 1).forEach {
        //                repoBlocks.saveAll(it)
        //                repoBlocks.flush()
        //            }
        //        }
        var invalidSpotsCount = 0
        info("Начало: удаление недействительных спотов.")
        if (channels.count() >= 10) {
            channels.chunked(10).forEach {
                invalidSpotsCount += repoSpots.deleteInvalid(
                    spots?.filter { s -> it.any { c -> c.id == s.filmXMediaPlan.mediaPlan.channel.id } },
                    dateFrom, dateBefore, it, advertisers
                )
            }
        }
        info("Завершение: удаление недействительных спотов: $invalidSpotsCount шт.")
    }

    /**
     * Метод поиска спотов по медиаплану.
     *
     * @see RepositorySpots.findAllByMediaPlan
     *
     * @param mediaPlan [MediaPlan] медиаплан.
     *
     * @return          [Iterable]  коллекция [Spot] спотов.
     */
    fun findAllByMediaPlan(mediaPlan: MediaPlan): Iterable<Spot> {
        info("Начало: поиск действительных спотов медиаплана.")
        val spots = repoSpots.findAllByMediaPlan(mediaPlan)
        info("Завершение: поиск действительных спотов медиаплана: ${spots.count()} шт.")
        return spots
    }

    fun spotsLight(id: Int): List<SpotLight> {
        val respSpotsList = repoSpots.spotsLight(id)
        if (respSpotsList == null || respSpotsList.count() == 0) {
            return listOf(
                SpotLight(
                    0,
                    isAssignedRatingsPlacement = false,
                    isMinute = false,
                    ratingForecast = 0.0,
                    rating = 0.0,
                    duration = 0
                )
            )
        }
        return respSpotsList
    }

}