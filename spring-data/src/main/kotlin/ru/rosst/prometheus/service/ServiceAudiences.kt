package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Audience
import ru.rosst.prometheus.repository.RepositoryAudiences

/**
 * Сервис управления данными таблицы ["audiences"] "аудитории".
 *
 * @property repoAudiences [RepositoryAudiences] репозиторий взаимодействия с таблицей ["audiences"] "аудитории".
 */
@Service
class ServiceAudiences : AbstractService() {
    @Autowired
    private lateinit var repoAudiences: RepositoryAudiences

    /**
     * Метод запроса всех аудиторий.
     *
     * @return [Iterable] коллекция [Audience] аудиторий.
     */
    fun findAll(): Iterable<Audience> = repoAudiences.findAll()

    fun findByID(id: Int): Audience? = repoAudiences.findByIdOrNull(id)

    fun findByName(name: String): Audience? = repoAudiences.findByName(name)
}