package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.SellingDirection
import ru.rosst.prometheus.repository.RepositorySellingDirections

/**
 * Сервис управления данными таблицы ["selling_directions"] "направления продаж".
 *
 * Репозитории управления таблицами базы данных:
 * @property repoSellingDirections [RepositorySellingDirections] репозиторий взаимодействия с таблицей ["selling_directions"] "направления продаж".
 */
@Service
class ServiceSellingDirections : AbstractService() {
    @Autowired
    private lateinit var repoSellingDirections: RepositorySellingDirections

    /**
     * Метод поиска направления продаж по его идентификатору.
     *
     * @param id    [Byte]                      идентификатор направления продаж.
     *
     * @return      [SellingDirection]          направление продаж.
     * @throws      [IllegalArgumentException]  если направление продаж не было найдено.
     */
    fun findById(id: Byte): SellingDirection {
        info("Начало: поиск в базе данных направление продаж.")

        // Поиск направления продаж в базе данных по идентификатору
        var sellingDirection = repoSellingDirections.findByIdOrNull(id)

        // Если направление продаж не было найдено - пытаемся восстановить, иначе возвращаем найденное
        if (sellingDirection == null) {
            warning("Направление продаж с ID: $id не было найдено в базе данных.")
            warning("Начало: Восстановление направлений продаж по умолчанию.")
            restoreDefaultSellingDirections()
            info("Завершение: Восстановление направлений продаж")
        } else return sellingDirection

        // Пытаемся снова найти направление продаж после восстановления, если не нашли - выбрасываем искючение
        sellingDirection = requireNotNull(
            repoSellingDirections.findByIdOrNull(id)
        ) { "Направление продаж с ID: $id не было найдено в базе данных, даже после восстановления." }

        info("Завершение: поиск в базе данных направление продаж.")

        return sellingDirection
    }

    /**
     * Поиск всех направлений продаж.
     *
     * @return [List] коллекция [SellingDirection] направлений продаж.
     */
    fun findAll(): List<SellingDirection> {
        info("Начало: поиск всех направлений продаж.")

        var sellingDirections = repoSellingDirections.findAll()

        // Если коллекция пустой - пытаемся восстановить, иначе возвращаем полученный коллекция
        if (sellingDirections.isNullOrEmpty()) {
            warning("Не удалось получить коллекция направлений продаж из базы данных, таблица пуста.")
            warning("Начало: Восстановление направлений продаж по умолчанию.")
            restoreDefaultSellingDirections()
            info("Завершение: Восстановление направлений продаж")
        } else return sellingDirections

        // Пытаемся снова найти направление продаж после восстановления, если не нашли - выбрасываем искючение
        sellingDirections = repoSellingDirections.findAll()

        if (sellingDirections.isEmpty()) throw IllegalArgumentException(
            "Не удалось получить коллекция направлений продаж, даже после восстановления."
        )

        info("Завершение: поиск всех направлений продаж: ${sellingDirections.size} шт.")
        return sellingDirections
    }

    fun findAllOnlyIDs(): List<Byte> = repoSellingDirections.findAllOnlyIDs()

    /**
     * Метод восстановления направлений продаж по умолчанию.
     */
    private fun restoreDefaultSellingDirections() {
        // Поиск всех направлений продаж, имеющихся в базе данных
        val sellingDirections = repoSellingDirections.findAll()

        // Если в базе данных нет ни одного направления
        if (sellingDirections.isNullOrEmpty()) {
            // Сохраняем направления продаж по умолчанию в базу данных
            repoSellingDirections.saveAll(DEFAULT_SELLING_DIRECTIONS)
            // Принудительно закидываем изменения в базу данных и завершаем выполнение метода
            return repoSellingDirections.flush()
        }

        // Если коллекция не пустая, проверяем каждое направление продаж по отдельности,
        // в случае, если одного из направлений нет - сохраняем его в базу данных

        sellingDirections.firstOrNull {
            it.id == 21.toByte()
        } ?: repoSellingDirections.saveAndFlush(
            DEFAULT_SELLING_DIRECTIONS.first { it.id == 21.toByte() }
        )

        sellingDirections.firstOrNull {
            it.id == 23.toByte()
        } ?: repoSellingDirections.saveAndFlush(
            DEFAULT_SELLING_DIRECTIONS.first { it.id == 23.toByte() }
        )
    }

    /**
     * @property DEFAULT_SELLING_DIRECTIONS [List] коллекция [SellingDirection] направлений продаж по умолчанию.
     */
    companion object {
        val DEFAULT_SELLING_DIRECTIONS = listOf(
            SellingDirection(21, "Федеральное ТВ"),
            SellingDirection(23, "Региональное ТВ")
        )
    }
}