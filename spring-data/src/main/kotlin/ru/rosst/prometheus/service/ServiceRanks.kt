package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Rank
import ru.rosst.prometheus.repository.RepositoryRanks

/**
 * Сервис управления данными таблицы ["ranks"] "ранги размещения" в базе данных.
 *
 * Репозитории взаимодействия с таблицами:
 * @property repoRanks [RepositoryRanks] репозиторий взаимодействия с таблицей ["ranks"] "ранги размещения" в базе данных
 */
@Service
class ServiceRanks : AbstractService() {
    @Autowired
    private lateinit var repoRanks: RepositoryRanks

    /**
     * Метод сохранения/обновления рангов размещения в базе данных
     *
     * @param ranks [Iterable] коллекция [Rank] рангов размещения
     */
    fun saveAll(ranks: Iterable<Rank>) {
        info("Начало: сохранение/обновление рангов размещения в базе данных")
        info("Параметры: рангов размещения для сохранения/обновления: [${ranks.count()}]")

        repoRanks.saveAll(ranks)
        repoRanks.flush()

        info("Завершение: сохранение/обновление рангов размещения в базе данных")
    }

    /**
     * Метод поиска всех рангов в базе данных.
     *
     * @return [List] коллекция [Rank] рангов.
     */
    fun findAll(): List<Rank> {
        info("Начало: запрос всех рангов размещения из базы данных")

        // Запрашиваем все ранги размещения из базы данных
        val ranks = repoRanks.findAll()

        info("Параметры: найдено рангов размещения в базе данных: [${ranks.count()}]")
        info("Завершение: запрос всех рангов размещения из базы данных")

        // Возвращаем результат
        return ranks
    }
}