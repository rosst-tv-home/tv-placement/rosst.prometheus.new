package ru.rosst.prometheus.service

import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.kotlin.parallelForEach
import ru.rosst.extensions.kotlin.parallelMap
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Audience
import ru.rosst.prometheus.entity.Channel
import ru.rosst.prometheus.entity.PalomarsChannel
import ru.rosst.prometheus.entity.PalomarsChannelRating
import ru.rosst.prometheus.repository.RepositoryAudiences
import ru.rosst.prometheus.repository.RepositoryPalomarsChannelsRatings
import java.time.LocalDate


/**
 * Сервис управление данными таблицы ["palomars_audiences"] "TVR" в базе данных.
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoPalomarsChannelsRatings  [RepositoryPalomarsChannelsRatings]   репозиторий взаимодействия с таблицей ["palomars_audiences"] "TVR".
 */
@Service
class ServicePalomarsChannelsRatings : AbstractService() {
    @Autowired
    private lateinit var repoAudiences: RepositoryAudiences

    @Autowired
    private lateinit var repoPalomarsChannelsRatings: RepositoryPalomarsChannelsRatings

    /**
     * Метод сохранения\обновления рейтингов.
     *
     * @param ratings [Iterable] коллекция [PalomarsChannelRating] рейтингов.
     */
    fun save(ratings: Iterable<PalomarsChannelRating>) {
        info("Начало: сохранение/обновление TVR Palomars.")

        // Ищем все известные аудитории и сохраняем неизвестные
        val existedAudiences = saveAudiences(runBlocking { ratings.parallelMap { it.audience } })

        // Заменяем неизвестные аудитории на известные
        val ratingsWithReplacedAudiences = replaceAudiences(ratings, existedAudiences)

        // Ищем известные рейтинги
        val existingRatings = findExistingRatings(ratingsWithReplacedAudiences)

        // Заменяем рейтинги
        val replacedRatings = replaceRatingsWithExistingRatings(
            ratingsWithReplacedAudiences,
            existingRatings
        )

        // Сохраняем рейтинги
        repoPalomarsChannelsRatings.saveAll(replacedRatings)

        info("Завершение: сохранение/обновление TVR Palomars.")
    }

    /**
     * Метод замены рейтингов на известные рейтинги.
     *
     * @param ratings           [Iterable] коллекция [PalomarsChannelRating] новых рейтингов.
     * @param existingRatings   [Iterable] коллекция [PalomarsChannelRating] известных рейтингов.
     *
     * @return                  [Iterable] коллекция [PalomarsChannelRating] заменённых рейтингов.
     */
    private fun replaceRatingsWithExistingRatings(
        ratings: Iterable<PalomarsChannelRating>,
        existingRatings: Iterable<PalomarsChannelRating>
    ): Iterable<PalomarsChannelRating> = runBlocking {
        // Коллекция рейтингов, которые нужно будет добавить как новые в базу данных
        val ratingsToAdd = mutableListOf<PalomarsChannelRating>()

        // Заменяем значения рейтингов в известных рейтингах, если есть
        ratings.parallelForEach { rating ->
            // Ищем рейтинг в коллекции известных рейтингов
            val foundRating = existingRatings.firstOrNull { existingRating ->
                rating.audience.name == existingRating.audience.name &&
                rating.palomarsChannel.id == existingRating.palomarsChannel.id &&
                rating.month == existingRating.month &&
                rating.dayType == existingRating.dayType &&
                rating.timeFrom == existingRating.timeFrom &&
                rating.timeBefore == existingRating.timeBefore
            }

            // Если мы нашли рейтинг в коллекции известных рейтингов
            if (foundRating != null) {
                // Обновляем значение рейтинга
                foundRating.rating = rating.rating
                // Переходим к следующему рейтингу
                return@parallelForEach
            }

            // Иначе добавляем рейтинг в коллекцию рейтингов для сохранения в базу данных
            ratingsToAdd.add(rating)
        }

        // Добавляем известные рейтинги с обновлёнными значениями рейтингов (если они были обновлены)
        ratingsToAdd.addAll(existingRatings)

        // Возвращаем коллекцию рейтингов, которую необходимо сохранить в базу данных
        return@runBlocking ratingsToAdd
    }

    /**
     * Метод запроса известных рейтингов.
     *
     * @param ratings   [Iterable] коллекция новых [PalomarsChannelRating] рейтингов.
     *
     * @return          [Iterable] коллекция известных [PalomarsChannelRating] рейтингов.
     */
    private fun findExistingRatings(
        ratings: Iterable<PalomarsChannelRating>
    ): Iterable<PalomarsChannelRating> = runBlocking {
        // Коллекция дат
        val months = mutableSetOf<LocalDate>()
        // Коллекция каналов Palomars
        val channels = mutableSetOf<Int>()
        // Коллекция аудиторий
        val audiences = mutableSetOf<Int>()

        ratings.parallelForEach { rating ->
            // Добавляем год-месяц в коллекцию дат
            months.add(rating.month)
            // Добавляем идентификатор канала Palomars в коллекцию каналов
            channels.add(rating.palomarsChannel.id)
            // Добавляем идентификатор аудитории в коллекцию аудиторий
            audiences.add(rating.audience.id)
        }

        // Ищем известные рейтинги по этим каналам-месяцам-аудиториям и возвращаем коллекцию
        repoPalomarsChannelsRatings.findExistingRatings(months, channels, audiences)
    }

    /**
     * Метод замены известных аудиторий в рейтингах.
     *
     * @param ratings   [Iterable] коллекция [PalomarsChannelRating] рейтингов.
     * @param audiences [Iterable] коллекция известных [Audience] аудиторий.
     *
     * @return          [Iterable] коллекция [PalomarsChannelRating] рейтингов, с замененными аудиториями.
     */
    private fun replaceAudiences(
        ratings: Iterable<PalomarsChannelRating>,
        audiences: Iterable<Audience>
    ): Iterable<PalomarsChannelRating> = runBlocking {
        ratings.parallelMap { rating ->
            rating.audience = audiences.first { it.name == rating.audience.name }
            return@parallelMap rating
        }
    }

    /**
     * Метод запроса сохранения аудиторий.
     *
     * @param audiences [Iterable] коллекция [Audience] аудиторий.
     *
     * @return          [Iterable] коллекция сохранённых [Audience] аудиторий.
     */
    private fun saveAudiences(audiences: Iterable<Audience>): MutableList<Audience> {
        // Коллекция сохранённых аудиторий
        val existingAudiences = mutableListOf<Audience>()

        // Добавляем в коллекцию все изместные аудтории
        existingAudiences.addAll(findExistingAudiences(audiences))

        // Отсеиваем из коллекции аудторий известные аудитории
        val notExistingAudiences =
            audiences.filterNot { existingAudiences.any { existing -> existing.name == it.name } }.toSet()

        // Сохраняем все не известные аудитории и добавляем их в коллекцию известных
        existingAudiences.addAll(repoAudiences.saveAll(notExistingAudiences))

        // Возвращаем коллекцию известных аудиторий
        return existingAudiences
    }

    /**
     * Метод запроса известных аудиторий.
     *
     * @param audiences [Iterable] коллекция [Audience] аудиторий, для исключения.
     *
     * @return          [Iterable] коллекция [Audience] аудиторий, которые есть в базе данных.
     */
    private fun findExistingAudiences(audiences: Iterable<Audience>) = repoAudiences.findByNameIn(
        runBlocking { audiences.parallelMap { it.name } }.toSet()
    )

    /**
     * Метод запроса поиска аудиторий канала.
     *
     * @param channel [Channel] канал.
     *
     * @return [Iterable] коллекция [PalomarsChannelRating] рейтингов.
     */
    fun findChannelAudiences(channel: Channel) = repoPalomarsChannelsRatings.findChannelAudiences(channel)

    /**
     * Метод запроса поиска рейтинга канала по аудитории.
     *
     * @param audience  [Audience]          аудитория.
     * @param channel   [PalomarsChannel]   palomars канал.
     * @param date      [LocalDate]         дата.
     *
     * @return [Iterable] коллекция [PalomarsChannelRating] рейтингов.
     */
    fun findChannelAudienceRating(
        audience: Audience,
        channel: PalomarsChannel,
        date: LocalDate
    ) = repoPalomarsChannelsRatings.findChannelAudienceRating(audience, channel, date)
}
