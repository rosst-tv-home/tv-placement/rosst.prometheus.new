package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.RequestDate
import ru.rosst.prometheus.repository.RepositoryRequestDates

/**
 * Сервис управления данными таблицы ["request_dates"] "даты последних запросов".
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoRequestDates [RepositoryRequestDates] репозиторий взаимодейсвтия с таблицей ["request_dates"] "даты последних запросов".
 */
@Service
class ServiceRequestDates : AbstractService() {
    @Autowired
    private lateinit var repoRequestDates: RepositoryRequestDates

    /**
     * Метод поиска даты последнего запроса по названию.
     *
     * @param name  [String]        название запроса.
     *
     * @return      [RequestDate]   дата последнего запроса.
     */
    fun findFetchDateByName(name: String): RequestDate? = repoRequestDates.findFirstByName(name)

    /**
     * Метод сохранения/обновления даты запроса.
     *
     * @param requestDate [RequestDate] дата последнего запроса.
     */
    fun saveAndFlush(requestDate: RequestDate) {
        info("Начало: сохранение/обновление даты запроса.")
        info("Параметры: название запроса: \"${requestDate.name}\".")
        repoRequestDates.saveAndFlush(requestDate)
        info("Завершение: сохранение/обновление даты запроса.")
    }
}