package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.PalomarsChannelXBaseAudience
import ru.rosst.prometheus.repository.RepositoryPalomarsChannelsXBaseAudiences

/**
 * Сервис управления данными таблицы ["palomars_channels_x_base_audiences"] "сопоставление базовой аудитории с каналом palomars".
 *
 * @property repoPalomarsChannelsXBaseAudiences [RepositoryPalomarsChannelsXBaseAudiences] репозиторий взаимодействия с данными таблицы ["palomars_channel_x_audience"] "сопоставление базовой аудитории с каналом palomars".
 */
@Service
class ServicePalomarsChannelsXBaseAudiences : AbstractService() {
    @Autowired
    private lateinit var repoPalomarsChannelsXBaseAudiences: RepositoryPalomarsChannelsXBaseAudiences

    /**
     * Метод запроса всех сопоставлений.
     *
     * @return [Iterable] коллекция [PalomarsChannelXBaseAudience] сопоставлений каналов Palomars с базовыми аудиториями.
     */
    fun findAll(): Iterable<PalomarsChannelXBaseAudience> = repoPalomarsChannelsXBaseAudiences.findAll()

    /**
     * Метод запроса поиска сопоставления.
     *
     * @param id    [PalomarsChannelXBaseAudience.PrimaryKey]   составной главный ключ сопоставления.
     *
     * @return      [PalomarsChannelXBaseAudience]              сопоставление канала Palomars с базовой аудиторией.
     */
    fun findOne(id: PalomarsChannelXBaseAudience.PrimaryKey): PalomarsChannelXBaseAudience? =
        repoPalomarsChannelsXBaseAudiences.findByIdOrNull(id)

    /**
     * Метод запроса сохранения сопоставления.
     *
     * @param palomarsChannelXBaseAudience  [PalomarsChannelXBaseAudience] сопоставление канала Palomars с базовой аудторией.
     *
     * @return                          [PalomarsChannelXBaseAudience] сохранённое сопоставление канала Palomars с базовой аудторией.
     */
    fun save(palomarsChannelXBaseAudience: PalomarsChannelXBaseAudience): PalomarsChannelXBaseAudience =
        repoPalomarsChannelsXBaseAudiences.save(palomarsChannelXBaseAudience)

    /**
     * Метод запроса удаления сопоставления.
     *
     * @param id [PalomarsChannelXBaseAudience.PrimaryKey] составной главный ключ сопоставления palomars канала и аудитории.
     */
    fun deleteByID(id: PalomarsChannelXBaseAudience.PrimaryKey) = repoPalomarsChannelsXBaseAudiences.deleteById(id)
}