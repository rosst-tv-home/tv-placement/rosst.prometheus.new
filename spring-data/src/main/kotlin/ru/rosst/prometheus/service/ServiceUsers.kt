package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.User
import ru.rosst.prometheus.repository.RepositoryUsers

/**
 * Сервис управления данными таблицы ["users"] "пользователи".
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoUsers [RepositoryUsers] репозиторий взаимодействия с таблицей ["users"] "пользователи".
 */
@Service
class ServiceUsers : AbstractService() {
    @Autowired
    private lateinit var repoUsers: RepositoryUsers

    /**
     * Метод сохранения пользователя.
     *
     * @param user  [User] пользователь.
     */
    fun save(user: User) {
        info("Начало: сохранение пользователя.")
        repoUsers.saveAndFlush(user)
        info("Завершение: сохранение пользователя.")
    }

    /**
     * Метод поиска пользователя по его логину.
     *
     * @see RepositoryUsers.findByLogin
     *
     * @param login [String]    логин.
     *
     * @return      [User]      пользователь.
     */
    fun findByLogin(login: String): User? {
        info("Начало: поиск пользователя по логину.")
        val user = repoUsers.findByLogin(login)
        info("Завершение: поиск пользователя по логину: ${user ?: "не найден"}.")
        return user
    }
}