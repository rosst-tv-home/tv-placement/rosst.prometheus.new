package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.FilmXMediaPlan
import ru.rosst.prometheus.entity.MediaPlan
import ru.rosst.prometheus.entity.Order
import ru.rosst.prometheus.repository.RepositoryMediaPlanFilmLinks
import ru.rosst.prometheus.repository.RepositoryMediaPlans
import ru.rosst.prometheus.repository.RepositoryOrders
import java.time.LocalDate

/**
 * Сервис управления данными таблиц ["media_plans"] "медиапланы", ["orders"] "заказы", ["media_plans_x_films"] "связи медиапланов и роликов".
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoOrders [RepositoryOrders] репозиторий взаимодействия с таблицей ["orders"] "заказы".
 * @property repoMediaPlanFilmLinks [RepositoryMediaPlanFilmLinks] репозиторий связей медиапланов и роликов.
 */
@Service
class ServiceMediaPlans : AbstractService() {
    @Autowired
    private lateinit var repoOrders: RepositoryOrders

    @Autowired
    private lateinit var repoMediaPlans: RepositoryMediaPlans

    @Autowired
    private lateinit var repoMediaPlanFilmLinks: RepositoryMediaPlanFilmLinks

    /**
     * Сохранение или обновление связей медиапланов и роликов, а вместе с ними
     * и сделки, заказы, сами медиапланы и ролики.
     *
     * @param filmsLinks [Iterable] коллекция [FilmXMediaPlan] связей медиапланов и роликов.
     */
    fun saveMediaPlansWithFilms(filmsLinks: Iterable<FilmXMediaPlan>) {
        info("Начало: сохранение и обновление связей медиапланов с роликами")
        repoMediaPlanFilmLinks.saveAll(filmsLinks)
        repoMediaPlanFilmLinks.flush()
        info("Завершение: сохранение и обновление связей медиапланов с роликами")
    }

    /**
     * Метод удаления недействительных медиапланов.
     *
     * @param dateFrom      [LocalDate] дата начала периода медиапланов.
     * @param dateBefore    [LocalDate] дата окончания периода медиапланов.
     * @param channels      [Iterable]  коллекция [Int] идентификаторов каналов медиапланов.
     * @param mediaPlans    [Iterable]  коллекция [Int] идентификаторов действительных медиапланов.
     * @param advertisers   [Iterable]  колекция [Int] идентификуаторов рекламодателей.
     */
    fun deleteInvalid(
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Iterable<Int>,
        mediaPlans: Iterable<Int>,
        advertisers: Iterable<Int>
    ) {
        info("Начало: удаление недействительных медиапланов.")
        val count = repoMediaPlans.deleteInvalid(
            dateFrom,
            dateBefore,
            channels.toSet(),
            mediaPlans.toSet(),
            advertisers.toSet()
        )
        info("Удалено $count шт. недействительных медиапланов.")
        info("Завершение: удаление недействительных медиапланов.")
    }

    /**
     * Метод удаления недействительных связей роликов и медиапланов.
     */
    fun deleteInvalidLinks(
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Iterable<Int>,
        advertisers: Iterable<Int>,
        mediaPlans: Iterable<Int>,
        links: Iterable<Int>
    ) {
        info("Начало: удаление недействительных связей роликов и медиапланов.")
        val count = repoMediaPlanFilmLinks.deleteInvalid(dateFrom, dateBefore, channels, advertisers, mediaPlans, links)
        info("Удалено $count шт. недействительных связей роликов и медиапланов.")
        info("Завершение: удаление недействительных связей роликов и медиапланов.")
    }

    fun findLinksByIds(links: Set<Int>): List<FilmXMediaPlan> = repoMediaPlanFilmLinks.findAllByIdIn(links)

    fun findAllOrdersByIds(orders: Set<Int>): List<Order> = repoOrders.findAllByIdIn(orders)

    fun findByMediaPlanId(mediaPlanId: Int): List<FilmXMediaPlan>? =
        repoMediaPlanFilmLinks.findAllByMediaPlanId(mediaPlanId)

    /**
     * Метод поиска медиапланов по параметрам.
     *
     * @param sellingDirection  [Byte]      идентификатор направления продаж.
     * @param dateFrom          [LocalDate] дата начала периода медиапланов.
     * @param dateBefore        [LocalDate] дата окончания периода медиапланов.
     * @param channels          [Iterable]  коллекция [Int] идентификаторов каналов медиапланов.
     * @param advertisers       [Iterable]  коллекция [Int] идентификаторов рекламодателей медиапланов.
     *
     * @return [Iterable] коллекция [MediaPlan] медиапланов.
     */
    fun findAllBySellingDirectionChannelsAdvertisersAndPeriod(
        sellingDirection: Byte,
        dateFrom: LocalDate,
        dateBefore: LocalDate,
        channels: Iterable<Int>,
        advertisers: Iterable<Int>
    ) = repoMediaPlans.findAllBySellingDirectionChannelsAdvertisersAndPeriod(
        sellingDirection,
        dateFrom,
        dateBefore,
        channels.toSet(),
        advertisers.toSet()
    )
}