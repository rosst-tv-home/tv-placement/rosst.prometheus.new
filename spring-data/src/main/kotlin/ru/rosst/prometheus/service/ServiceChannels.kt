package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Channel
import ru.rosst.prometheus.repository.RepositoryChannels
import ru.rosst.prometheus.repository.RepositoryRegions

/**
 * Сервис управления каналами в базе данных.
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoRegions    [RepositoryRegions]     репозиторий взаимодействия с таблицей ["regions"] "регионы".
 * @property repoChannels   [RepositoryChannels]    репозиторий взаимодействия с таблицей ["channel"] "каналы".
 */
@Service
class ServiceChannels : AbstractService() {
    @Autowired
    private lateinit var repoRegions: RepositoryRegions

    @Autowired
    private lateinit var repoChannels: RepositoryChannels

    fun findAll(): MutableList<Channel> = repoChannels.findAll()

    /**
     * Метод сохранения/обновления каналов и регионов в базе данных.
     *
     * @param channels [Iterable] коллекция [Channel] каналов.
     */
    fun save(channels: Iterable<Channel>) {
        // Удаляем недействительные каналы и регионы из базы данных
        deleteInvalid(channels)

        info("Начало: сохранение/обновление каналов и регионов.")

        // Сохраняем и обновляем коллекция каналов в базе данных
        repoChannels.saveAll(channels)
        repoChannels.flush()

        info("Завершение: сохранение/обновление каналов и регионов.")
    }

    /**
     * Метод удаления недействительных регионов и каналов.
     *
     * @param channels [Iterable] коллекция действительных [Channel] каналов.
     */
    private fun deleteInvalid(channels: Iterable<Channel>) {
        info("Начало: удаление недействительных каналов и регионов.")

        // Формируем коллекцию регионов из коллекции каналов
        val regions = channels.map { it.region }.toSet()

        // Удаляем недействительные регионы
        val invalidRegionsCount = repoRegions.deleteInvalid(channels.first().region.sellingDirection, regions)
        info("$invalidRegionsCount шт. недействительных регионов было удалено.")

        // Удаляем недействительные каналы
        val invalidChannelsCount = repoChannels.deleteInvalid(channels, regions)
        info("$invalidChannelsCount шт. недействительных каналов было удалено.")

        info("Завершение: удаление недействительных каналов и регионов.")
    }

    /**
     * Метод поиска каналов по идентификатору направления продаж.
     *
     * @param id    [Byte] идентификатор направления продаж.
     *
     * @return      [List] коллекция [Channel] каналов.
     */
    fun findAllBySellingDirectionID(id: Byte): List<Channel> {
        info("Начало: поиск каналов по направлению продаж")

        val channels = repoChannels.findAllBySellingDirectionID(id)

        info("Найдено: ${channels.count()} шт. каналов.")
        info("Завершение: поиск каналов по направлению продаж.")

        return channels
    }

    fun findAllBySellingDirectionIDLight(id: Byte) = repoChannels.findAllBySellingDirectionIDLight(id)

    fun findAllByIDsLight(channelIDs: Iterable<Int>) = repoChannels.findAllByIDsLight(channelIDs)

    /**
     * Метод поиска каналов по идентификаторам.
     *
     * @param channelIDs    [Iterable]  коллекция [Int] идентификаторов каналов.
     *
     * @return              [List]      коллекция [Channel] каналов.
     */
    fun findAllByIDs(channelIDs: Iterable<Int>): List<Channel> {
        info("Начало: поиск каналов по идентификаторам...")
        info("Параметры: ${channelIDs.count()} шт. идентификаторов")

        val channels = repoChannels.findAllById(channelIDs)

        info("Найдено: ${channels.count()} шт. каналов")
        info("Завершение: поиск каналов по идентифиекатору.")

        return channels
    }

    /**
     * @see RepositoryChannels.findAllMeasurableIDsBySellingDirectionID
     */
    fun findAllMeasurableIDsBySellingDirectionID(id: Byte) = repoChannels.findAllMeasurableIDsBySellingDirectionID(id)
}