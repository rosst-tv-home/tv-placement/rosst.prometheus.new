package ru.rosst.prometheus.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.rosst.extensions.spring.AbstractService
import ru.rosst.prometheus.entity.Advertiser
import ru.rosst.prometheus.entity.AdvertiserXSellingDirection
import ru.rosst.prometheus.repository.RepositoryAdvertisersXSellingDirections

/**
 * Сервис управления данными таблицы ["advertisers"] "рекламодатели".
 *
 * Репозитории взаимодействия с таблицами базы данных:
 * @property repoAdvertisersXSellingDirections [RepositoryAdvertisersXSellingDirections] репозиторий взимодействия с таблицей ["advertisers_x_selling_directions"] "связи рекламодателей и направлений продаж".
 */
@Service
class ServiceAdvertisers : AbstractService() {
    @Autowired
    private lateinit var repoAdvertisersXSellingDirections: RepositoryAdvertisersXSellingDirections

    /**
     * Метод поиска связей реклмодателей с направлениями продаж по коллекции идентификаторов рекламодателей.
     *
     * @param advertiserIDs [Iterable]  коллекция [Int] идентификаторов рекламодателей.
     *
     * @return              [List]      коллекция [AdvertiserXSellingDirection] связей рекламодателей и направлений продаж.
     */
    fun findAllByIDs(advertiserIDs: Iterable<Int>): List<AdvertiserXSellingDirection> {
        info("Начало: поиск связей рекламодателей и направлений продаж по коллекции идентификаторов рекламодателей...")
        info("Параметры: ${advertiserIDs.count()} шт. идентификаторов рекламодателей...")

        val advertisers = repoAdvertisersXSellingDirections.findAllByAdvertisersIDs(advertiserIDs.toSet())

        info("Найдено ${advertisers.size} шт. связей рекламодателей и направлений продаж.")
        info("Завершение: поиск связей рекламодателей и направлений продаж по коллекции идентификаторов рекламодателей.")

        return advertisers
    }

    /**
     * Метод поиска рекламодателей по идентификатору направления продаж.
     *
     * @param id    [Byte] идентификатор направления продаж.
     *
     * @return      [List] коллекция [Advertiser] рекламодателей.
     */
    fun findAllBySellingDirectionID(id: Byte): List<AdvertiserXSellingDirection> {
        info("Начало: поиск рекламодателей по направлению продаж.")
        info("Параметры: Идентификатор направления продаж: $id")

        val advertisers = repoAdvertisersXSellingDirections.findAllBySellingDirectionID(id)

        info("Найдено: ${advertisers.size} шт. рекламодателей.")
        info("Завершение: поиск рекламодателей по направлению продаж.")

        return advertisers
    }

    /**
     * Метод сохранения и/или обновления связей рекламодателей и направлений продаж.
     *
     * @param entities [Iterable] коллекция [AdvertiserXSellingDirection] связей рекламодателей и направлений продаж.
     */
    fun save(entities: Iterable<AdvertiserXSellingDirection>) {
        // Если коллекция пустая - завершаем выполнение метода
        if (entities.count() == 0) {
            info("Завершение: сохранение и/или обновление коллекции связей рекламодателей и направлений продаж.")
            info("Причина: коллекция сущностей пустая.")
            return
        }

        // Вызов метода удаления недействительных связей рекламодателей и направлений продаж
        deleteInvalid(entities)

        info("Начало: сохранение и/или обновление коллекции связей рекламодателей и направлений продаж.")

        repoAdvertisersXSellingDirections.saveAll(entities)

        info("Завершение: сохранение и/или обновление коллекции связей рекламодателей и направлений продаж.")
    }

    /**
     * Метод удаления недействительных связей рекламодателей и направлений продаж.
     *
     * @param entities [Iterable] коллекция [AdvertiserXSellingDirection] связей рекламодателей и направлений продаж.
     */
    private fun deleteInvalid(entities: Iterable<AdvertiserXSellingDirection>) {
        info("Начало: удаление недействительных связей рекламодателей и направлений продаж.")

        val count = repoAdvertisersXSellingDirections.deleteInvalid(
            entities.first().sellingDirection,
            entities
        )

        info("Удалено $count шт. недействительных связей рекламодателей и направлений продаж.")
        info("Завершение: удаление недействительных записей рекламодателей и направлений продаж.")
    }
}