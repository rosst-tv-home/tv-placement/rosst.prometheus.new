plugins {
    kotlin("plugin.jpa")
}

dependencies {
    api(project(":extensions"))
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.4")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.5.4")
}