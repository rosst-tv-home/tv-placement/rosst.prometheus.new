dependencies {
    implementation("javax.xml.soap:javax.xml.soap-api:1.4.0")
    implementation("com.sun.xml.messaging.saaj:saaj-impl:2.0.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.12.4")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.12.4")
}