package ru.rosst.prometheus.models.blocks.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import ru.rosst.prometheus.converters.deserializers.DeserializerBoolean
import ru.rosst.prometheus.converters.deserializers.DeserializerDateTime
import ru.rosst.prometheus.converters.deserializers.DeserializerDateWithDashes
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * Модель рекламного блока.
 *
 * @param blockID               [Long]              идентификатор рекламного блока.
 * @param blockVolume           [Short]             объём рекламного блока.
 * @param tnsBlockID            [Long]              идентификатор рекламного блока в Palomars.
 * @param blockDate             [LocalDate]         плановая дата выхода рекламного блока.
 * @param blockDeadlineDateTime [LocalDateTime]     дата и время дедлайна рекламного блока.
 * @param blockSeconds          [Int]               кол-во секунд от плановой даты выхода рекламного блока - т.е. плановое время выхода.
 * @param blockIsPrime          [Boolean]           признак рекламного блока "Prime Time".
 * @param blockIsMinute         [Boolean]           признак рекламного блока "минутный".
 * @param blockForecastRating   [Double]            прогнозный рейтинг рекламного блока.
 * @param blockDayOfWeek        [Byte]              день недели рекламного блока.
 * @param blockAuctionState     [Byte]              статус аукциона рекламного блока.
 * @param booking               [List]              коллекция [ResponseBooking] занятых объёмов рекламного блока.
 * @param issueID               [Long]              идентификатор выпуска.
 * @param issueSeconds          [Int]               кол-во секунд от плановой даты выпуска (выхода рекламного блока).
 * @param issueDuration         [Short]             длительность выпуска в секундах.
 * @param programID             [Int]               идентификатор программы.
 * @param programName           [String]            название программы.
 * @param programShortName      [String]            короткое название программы.
 * @param channelID             [Int]               идентификатор канала.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ResponseBlock(
    @JacksonXmlProperty(isAttribute = true, localName = "BlockID")
    val blockID: Long,
    @JacksonXmlProperty(isAttribute = true, localName = "VM")
    val blockVolume: Short,
    @JacksonXmlProperty(localName = "TNSBlockFactID")
    val tnsBlockID: Long?,
    @JacksonXmlProperty(localName = "BlockDate")
    @JsonDeserialize(using = DeserializerDateWithDashes::class)
    val blockDate: LocalDate,
    @JacksonXmlProperty(localName = "DLDate")
    @JsonDeserialize(using = DeserializerDateTime::class)
    val blockDeadlineDateTime: LocalDateTime,
    @JacksonXmlProperty(localName = "BlockTime")
    val blockSeconds: Long,
    @JacksonXmlProperty(localName = "IsPrime")
    @JsonDeserialize(using = DeserializerBoolean::class)
    val blockIsPrime: Boolean,
    @JacksonXmlProperty(localName = "IsSpecialProject")
    @JsonDeserialize(using = DeserializerBoolean::class)
    val isSpecialProject: Boolean,
    @JacksonXmlProperty(localName = "NoRating")
    @JsonDeserialize(using = DeserializerBoolean::class)
    val blockIsMinute: Boolean,
    @JacksonXmlProperty(localName = "ForecastRateBase")
    val blockForecastRating: Double?,
    @JacksonXmlProperty(localName = "WeekDay")
    val blockDayOfWeek: Byte,
    @JacksonXmlProperty(localName = "BlkAuc")
    val blockAuctionState: Byte,
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Booked")
    val booking: List<ResponseBooking>?,
    @JacksonXmlProperty(localName = "IssID")
    val issueID: Long,
    @JacksonXmlProperty(localName = "IssTime")
    val issueSeconds: Int,
    @JacksonXmlProperty(localName = "IssDuration")
    val issueDuration: Int,
    @JacksonXmlProperty(localName = "ProgID")
    val programID: Int,
    @JacksonXmlProperty(localName = "PrgName")
    val programName: String?,
    @JacksonXmlProperty(localName = "PrgNameShort")
    val programShortName: String?,
    @JacksonXmlProperty(localName = "CnlID")
    val channelID: Int
)