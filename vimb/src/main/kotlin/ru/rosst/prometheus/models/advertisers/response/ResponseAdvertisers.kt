package ru.rosst.prometheus.models.advertisers.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос рекламодателей.
 *
 * @param advertisers [List] коллекция [ResponseAdvertiser] рекламодателей.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "responseGetCustomersWithAdvertisers")
class ResponseAdvertisers(
    @JacksonXmlProperty(localName = "Advertisers")
    @JacksonXmlElementWrapper
    val advertisers: List<ResponseAdvertiser>
)