package ru.rosst.prometheus.models.spots.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос спотов.
 *
 * @param spots             [List] коллекция [ResponseSpot] спотов.
 * @param assignedRatings   [List] коллекция [ResponseAssignedRating] втянутых рейтингов.
 */
@JacksonXmlRootElement(localName = "ResponseSpot")
class ResponseSpots(
    @JacksonXmlElementWrapper
    @JacksonXmlProperty(localName = "SpotList")
    val spots: List<ResponseSpot>? = null,
    @JacksonXmlElementWrapper
    @JacksonXmlProperty(localName = "OrdBlocks")
    val assignedRatings: List<ResponseAssignedRating>? = null
)