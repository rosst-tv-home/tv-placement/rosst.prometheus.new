package ru.rosst.prometheus.models.blocks.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос блоков.
 *
 * @param blocks [List] коллекция [ResponseBlock] рекламных блоков.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "responseProgramBreaksV2")
class ResponseBlocks(
    @JacksonXmlElementWrapper
    @JacksonXmlProperty(localName = "BreakList")
    val blocks: List<ResponseBlock> = emptyList()
)