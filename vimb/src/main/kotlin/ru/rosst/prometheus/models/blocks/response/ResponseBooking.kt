package ru.rosst.prometheus.models.blocks.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель занятых объёмов рекламного блока.
 *
 * @param rankID        [Byte]  ранг типа размещения.
 * @param bookedVolume  [Short] занятый объём.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "Booked")
class ResponseBooking(
    @JacksonXmlProperty(isAttribute = true, localName = "RankID")
    val rankID: Byte,
    @JacksonXmlProperty(isAttribute = true, localName = "VM")
    val bookedVolume: Short
)