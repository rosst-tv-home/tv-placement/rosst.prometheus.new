package ru.rosst.prometheus.models.spots.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import ru.rosst.prometheus.converters.deserializers.DeserializerSpotDate
import java.time.LocalDate

/**
 * Модель спота.
 *
 * @param spotID            [Long]      идентификатор спота.
 * @param blockID           [Long]      идентификатор рекламного блока.
 * @param linkID            [Int]       идентификатор ролика в медиаплане.
 * @param date              [LocalDate] дата размещения спота.
 * @param seconds           [Int]       кол-во секунд от начала даты размещения спота.
 * @param isHumanPlacement  [Boolean]   признак "спот установлен человеком".
 * @param rankID            [Byte]      номер ранга размещения спота.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "s")
class ResponseSpot(
    @JacksonXmlProperty(localName = "SpotID")
    val spotID: Long,
    @JacksonXmlProperty(localName = "BlockID")
    val blockID: Long,
    @JacksonXmlProperty(localName = "CommInMplID")
    val linkID: Int,
    @JacksonXmlProperty(localName = "SptDateL")
    @JsonDeserialize(using = DeserializerSpotDate::class)
    val date: LocalDate,
    @JacksonXmlProperty(localName = "SpotBroadcastTime")
    val seconds: Long,
    @JacksonXmlProperty(localName = "IsHumanBeing")
    val isHumanPlacement: Boolean,
    @JacksonXmlProperty(localName = "RankID")
    val rankID: Byte
)