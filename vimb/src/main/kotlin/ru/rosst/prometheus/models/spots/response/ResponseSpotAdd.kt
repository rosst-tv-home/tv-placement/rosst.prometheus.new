package ru.rosst.prometheus.models.spots.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос добавления спота.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "AddSpotResultDTO")
class ResponseSpotAdd(
    @JacksonXmlProperty(localName = "Result")
    val code: Byte,
    @JacksonXmlProperty(localName = "newSptID")
    val spotID: Long,
    @JacksonXmlProperty(localName = "Position")
    val position: Byte?,
    @JacksonXmlProperty(localName = "ErrorMessage")
    val error: String?
)