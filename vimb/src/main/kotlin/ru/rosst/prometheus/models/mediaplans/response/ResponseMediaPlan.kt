package ru.rosst.prometheus.models.mediaplans.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import ru.rosst.prometheus.converters.deserializers.DeserializerBoolean
import ru.rosst.prometheus.converters.deserializers.DeserializerDate
import ru.rosst.prometheus.converters.deserializers.DeserializerMonth
import java.time.LocalDate

/**
 * Модель медиаплана.
 *
 * @param advertiserID                  [Int]       идентификатор рекламодателя.
 * @param channelID                     [Int]       идентификатор канала.
 * @param agreementID                   [Int]       идентификатор сделки.
 * @param agreementName                 [String]    название сделки.
 * @param agreementDateFrom             [LocalDate] дата начала сделки.
 * @param agreementDateBefore           [LocalDate] дата окончания сделки.
 * @param orderID                       [Int]       идентификатор заказа.
 * @param orderName                     [String]    название заказа.
 * @param orderDateFrom                 [LocalDate] дата начала заказа.
 * @param orderDateBefore               [LocalDate] дата окончания заказа.
 * @param orderIsMultipleSpots          [Boolean]   признак "больше одного ролика в блок".
 * @param mediaPlanID                   [Int]       идентификатор медиаплана.
 * @param mediaPlanName                 [String]    название медиаплана.
 * @param mediaPlanMonth                [LocalDate] месяц медиаплана.
 * @param mediaPlanFilmLinkID           [Int]       идентификатор связи медиаплана с роликом.
 * @param mediaPlanFilmLinkDateFrom     [LocalDate] дата начала размещения ролика.
 * @param mediaPlanFilmLinkDateBefore   [LocalDate] дата окончания размещения ролика.
 * @param filmID                        [Int]       идентификатор роилка.
 * @param filmName                      [String]    название ролика.
 * @param filmVersion                   [String]    название версии ролика.
 * @param filmDuration                  [Byte]      хронометраж ролика.
 * @param filmIsInLaw                   [Boolean]   признак ролика "соответсвует закону о рекламе".
 * @param rankID                        [Byte]      ранг типа размещения заказа.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "m")
class ResponseMediaPlan(
    @JacksonXmlProperty(localName = "AdtID")
    val advertiserID: Int,
    @JacksonXmlProperty(localName = "MplCnlID")
    val channelID: Int,
    @JacksonXmlProperty(localName = "AgrID")
    val agreementID: Int,
    @JacksonXmlProperty(localName = "AgrName")
    val agreementName: String,
    @JacksonXmlProperty(localName = "ContractBeg")
    @JsonDeserialize(using = DeserializerDate::class)
    val agreementDateFrom: LocalDate,
    @JacksonXmlProperty(localName = "ContractEnd")
    @JsonDeserialize(using = DeserializerDate::class)
    val agreementDateBefore: LocalDate,
    @JacksonXmlProperty(localName = "OrdID")
    val orderID: Int,
    @JacksonXmlProperty(localName = "OrdName")
    val orderName: String,
    @JacksonXmlProperty(localName = "ordBegDate")
    @JsonDeserialize(using = DeserializerDate::class)
    val orderDateFrom: LocalDate,
    @JacksonXmlProperty(localName = "ordEndDate")
    @JsonDeserialize(using = DeserializerDate::class)
    val orderDateBefore: LocalDate,
    @JacksonXmlProperty(localName = "Multiple")
    @JsonDeserialize(using = DeserializerBoolean::class)
    val orderIsMultipleSpots: Boolean?,
    @JacksonXmlProperty(localName = "MplID")
    val mediaPlanID: Int,
    @JacksonXmlProperty(localName = "MplName")
    val mediaPlanName: String,
    @JacksonXmlProperty(localName = "MplMonth")
    @JsonDeserialize(using = DeserializerMonth::class)
    val mediaPlanMonth: LocalDate,
    @JacksonXmlProperty(localName = "CommInMplID")
    val mediaPlanFilmLinkID: Int,
    @JacksonXmlProperty(localName = "DateFrom")
    @JsonDeserialize(using = DeserializerDate::class)
    val mediaPlanFilmLinkDateFrom: LocalDate,
    @JacksonXmlProperty(localName = "DateTo")
    @JsonDeserialize(using = DeserializerDate::class)
    val mediaPlanFilmLinkDateBefore: LocalDate,
    @JacksonXmlProperty(localName = "FilmID")
    val filmID: Int,
    @JacksonXmlProperty(localName = "FilmName")
    val filmName: String,
    @JacksonXmlProperty(localName = "FilmVersion")
    val filmVersion: String,
    @JacksonXmlProperty(localName = "FilmDur")
    val filmDuration: Byte,
    @JacksonXmlProperty(localName = "ffoaLawAcc")
    @JsonDeserialize(using = DeserializerBoolean::class)
    val filmIsInLaw: Boolean,
    @JacksonXmlProperty(localName = "RankID")
    val rankID: Byte,
    @JacksonXmlProperty(localName = "InventoryUnits")
    val inventoryType: Byte?
)