package ru.rosst.prometheus.models.errors.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

class ResponseErrorDetails(
    @JacksonXmlProperty(localName = "ErrorDescription")
    val description: ResponseErrorDescription
)