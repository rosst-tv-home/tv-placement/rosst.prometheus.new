package ru.rosst.prometheus.models.channels.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import ru.rosst.prometheus.converters.deserializers.DeserializerBoolean

/**
 * Модель канала.
 *
 * @param id            [Int]       идентификатор.
 * @param name          [String]    название.
 * @param isDisabled    [Boolean]   признак "не активен".
 * @param regionID      [Short]     идентификатор региона.
 * @param regionName    [String]    название региона.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ResponseChannel(
    @JacksonXmlProperty(localName = "ID")
    val id: Int,
    @JacksonXmlProperty(localName = "ShortName")
    val name: String,
    @JacksonXmlProperty(localName = "IsDisabled")
    @JsonDeserialize(using = DeserializerBoolean::class)
    val isDisabled: Boolean,
    @JacksonXmlProperty(localName = "bcpCentralID")
    val regionID: Short,
    @JacksonXmlProperty(localName = "bcpName")
    val regionName: String
)