package ru.rosst.prometheus.models.advertisers.request

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель запроса рекламодателей.
 *
 * @param sellingDirectionID [Byte] идентификатор направления продаж.
 */
@JacksonXmlRootElement(localName = "GetCustomersWithAdvertisers")
class RequestAdvertisers(
    @JacksonXmlProperty(localName = "SellingDirectionID")
    val sellingDirectionID: Byte
)