package ru.rosst.prometheus.models.ranks.request

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Метод запроса рангов.
 */
@JacksonXmlRootElement(localName = "GetRanks")
class RequestRanks