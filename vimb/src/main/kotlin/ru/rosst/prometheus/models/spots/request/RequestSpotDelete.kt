package ru.rosst.prometheus.models.spots.request

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель запроса удаления спота.
 *
 * @param spotID [Long] идентификатор спота.
 */
@JacksonXmlRootElement(localName = "DeleteSpot")
class RequestSpotDelete(
    @JacksonXmlProperty(localName = "SpotID")
    val spotID: Long
)