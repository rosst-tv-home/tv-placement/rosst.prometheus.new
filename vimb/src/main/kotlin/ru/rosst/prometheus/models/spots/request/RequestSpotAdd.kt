package ru.rosst.prometheus.models.spots.request

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель запроса добавления спота.
 *
 * @param blockID           [Long]      идентификатор рекламного блока.
 * @param linkID            [Int]       идентификатор связи ролика с медиапланом.
 * @param position          [Byte]      позиция спота в рекламном блоке.
 * @param isFixedPosition   [Boolean]   признак "жёсткое позиционирование".
 * @param auctionBid        [Short]     ставка аукциона.
 */
@JacksonXmlRootElement(localName = "AddSpot")
class RequestSpotAdd(
    @JacksonXmlProperty(localName = "BlockID")
    val blockID: Long,
    @JacksonXmlProperty(localName = "FilmID")
    val linkID: Int,
    @JacksonXmlProperty(localName = "Position")
    val position: Byte = 0,
    @JacksonXmlProperty(localName = "FixedPosition")
    val isFixedPosition: Boolean = false,
    @JacksonXmlProperty(localName = "AuctionBidValue")
    val auctionBid: Short = 0
)