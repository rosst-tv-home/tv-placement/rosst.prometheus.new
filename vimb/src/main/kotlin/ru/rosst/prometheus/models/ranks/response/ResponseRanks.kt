package ru.rosst.prometheus.models.ranks.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос рангов.
 *
 * @param ranks [List] коллекция [ResponseRank] рангов.
 */
@JacksonXmlRootElement(localName = "responseGetRanks")
class ResponseRanks(
    @JacksonXmlElementWrapper
    @JacksonXmlProperty(localName = "Ranks")
    val ranks: List<ResponseRank>
)