package ru.rosst.prometheus.models.advertisers.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель рекламодателя.
 *
 * @param id    [Int]       идентификатор.
 * @param name  [String]    название.
 */
@JacksonXmlRootElement(localName = "item")
class ResponseAdvertiser(
    @JacksonXmlProperty(localName = "ID")
    val id: Int,
    @JacksonXmlProperty(localName = "Name")
    val name: String
)