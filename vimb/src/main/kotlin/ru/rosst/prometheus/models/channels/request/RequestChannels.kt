package ru.rosst.prometheus.models.channels.request

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель запроса каналов.
 *
 * @property sellingDirectionID идентификатор направления продаж [Byte].
 */
@JacksonXmlRootElement(localName = "GetChannels")
class RequestChannels(
    @JacksonXmlProperty(localName = "SellingDirectionID")
    val sellingDirectionID: Byte = 21
)