package ru.rosst.prometheus.models.spots.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель втянутого рейтинга.
 *
 * @param orderID   [Int]       идентификатор заказа.
 * @param blockID   [Long]      идентификатор рекламного блока.
 * @param rating    [Double]    втянутый рейтинг рекламного блока в заказе.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "obl")
class ResponseAssignedRating(
    @JacksonXmlProperty(localName = "OrdID")
    val orderID: Int,
    @JacksonXmlProperty(localName = "BlockID")
    val blockID: Long,
    @JacksonXmlProperty(localName = "Rate")
    val rating: Double
)