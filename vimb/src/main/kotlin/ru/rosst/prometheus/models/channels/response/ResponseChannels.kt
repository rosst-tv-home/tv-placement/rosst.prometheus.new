package ru.rosst.prometheus.models.channels.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос каналов.
 *
 * @param channels [List] коллекция [ResponseChannel] каналов.
 */
@JacksonXmlRootElement(localName = "responseGetChannels")
class ResponseChannels(
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "Channel")
    val channels: List<ResponseChannel>
)