package ru.rosst.prometheus.models.blocks.request

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import ru.rosst.prometheus.converters.serializers.SerializerDate
import java.time.LocalDate

/**
 * Модель запроса блоков.
 *
 * @param sellingDirectionID    [Byte]      идентификатор направления продаж.
 * @param includeProperties     [Byte]      признак "как заполнять тег ProMaster".
 * @param includeForecast       [Byte]      признак "как заполнять тег BlockForecast".
 * @param roundRatingValue      [Byte]      признак "до какого знака после запятой округлять рейтинг".
 * @param dateFrom              [LocalDate] дата начала периода.
 * @param dateBefore            [LocalDate] дата окончания периода.
 * @param lightMode             [Byte]      признак "ответ от сервера в облегчённом режиме".
 * @param channels              [Set]       коллекция [Int] идентификаторов каналов.
 * @param protocolVersion       [Byte]      формат ответа от сервера.
 */
@JacksonXmlRootElement(localName = "GetProgramBreaks")
class RequestBlocks(
    @JacksonXmlProperty(localName = "SellingDirectionID")
    val sellingDirectionID: Byte,
    @JacksonXmlProperty(localName = "InclProgAttr")
    val includeProperties: Byte = 0,
    @JacksonXmlProperty(localName = "InclForecast")
    val includeForecast: Byte = 0,
    @JacksonXmlProperty(localName = "AudRatDec")
    val roundRatingValue: Byte = 9,
    @JacksonXmlProperty(localName = "StartDate")
    @JsonSerialize(using = SerializerDate::class)
    val dateFrom: LocalDate,
    @JacksonXmlProperty(localName = "EndDate")
    @JsonSerialize(using = SerializerDate::class)
    val dateBefore: LocalDate,
    @JacksonXmlProperty(localName = "LightMode")
    val lightMode: Byte = 0,
    @JacksonXmlElementWrapper(localName = "CnlList")
    @JacksonXmlProperty(localName = "Cnl")
    val channels: Set<Int>,
    @JacksonXmlProperty(localName = "ProtocolVersion")
    val protocolVersion: Byte = 2
)