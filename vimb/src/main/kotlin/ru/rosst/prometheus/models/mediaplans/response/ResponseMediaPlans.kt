package ru.rosst.prometheus.models.mediaplans.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа на запрос медиапланов.
 *
 * @param mediaPlans [List] коллекция [ResponseMediaPlan] медиапланолв.
 */
@JacksonXmlRootElement(localName = "responseMPlans")
class ResponseMediaPlans(
    @JacksonXmlElementWrapper
    @JacksonXmlProperty(localName = "MPlansList")
    val mediaPlans: List<ResponseMediaPlan>
)