package ru.rosst.prometheus.models.spots.request

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import ru.rosst.prometheus.converters.serializers.SerializerDate
import java.time.LocalDate

/**
 * Модель запроса спотов.
 *
 * @param sellingDirectionID        [Byte]      идентификатор направления продаж.
 * @param dateFrom                  [LocalDate] дата начала периода.
 * @param dateBefore                [LocalDate] дата окончания периода.
 * @param includeAssignedRatings    [Byte]      режим заполнения втянутых рейтингов.
 * @param channels                  [Set]       коллекция [Int] идентификаторов каналов.
 * @param advertisers               [Set]       коллекция [Int] идентификаторов рекламодателей.
 */
@JacksonXmlRootElement(localName = "GetSpots")
class RequestSpots(
    @JacksonXmlProperty(localName = "SellingDirectionID")
    val sellingDirectionID: Byte = 21,
    @JacksonXmlProperty(localName = "StartDate")
    @JsonSerialize(using = SerializerDate::class)
    val dateFrom: LocalDate,
    @JacksonXmlProperty(localName = "EndDate")
    @JsonSerialize(using = SerializerDate::class)
    val dateBefore: LocalDate,
    @JacksonXmlProperty(localName = "InclOrdBlocks")
    val includeAssignedRatings: Byte = 2,
    @JacksonXmlElementWrapper(localName = "ChannelList")
    @JacksonXmlProperty(localName = "Cnl")
    val channels: Set<Int> = emptySet(),
    @JacksonXmlElementWrapper(localName = "AdtList")
    @JacksonXmlProperty(localName = "AdtID")
    val advertisers: Set<Int> = emptySet()
)