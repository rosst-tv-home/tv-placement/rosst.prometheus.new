package ru.rosst.prometheus.models.ranks.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ранга.
 *
 * @param id    [Byte]      идентификатор ранга.
 * @param name  [String]    название ранга.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "Rank")
class ResponseRank(
    @JacksonXmlProperty(localName = "ID")
    val id: Byte,
    @JacksonXmlProperty(localName = "Name")
    val name: String
)