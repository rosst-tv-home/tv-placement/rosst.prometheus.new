package ru.rosst.prometheus.models.errors.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Модель ответа сообщения об ошибке.
 *
 * @property code           [String]                    сторона, на которой возникла ошибка: soap:Client - клиент, soap:Server - сервер.
 * @property string         [String]                    описание ошибки.
 * @property actor          [String]                    адрес по которому возникла ошибка.
 * @property details    [ResponseErrorDescription]  детали ошибки.
 */
@JacksonXmlRootElement(localName = "Fault")
class ResponseError(
    @JacksonXmlProperty(localName = "faultcode")
    val code: String,
    @JacksonXmlProperty(localName = "faultstring")
    val string: String,
    @JacksonXmlProperty(localName = "faultactor")
    val actor: String,
    @JacksonXmlProperty(localName = "detail")
    val details: ResponseErrorDetails
) : Exception()