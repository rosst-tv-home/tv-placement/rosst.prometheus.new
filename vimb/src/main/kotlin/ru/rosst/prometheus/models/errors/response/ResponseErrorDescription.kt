package ru.rosst.prometheus.models.errors.response

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

/**
 * Модель описания ошибки.
 *
 * @param code      [Int]       код ошибки.
 * @param message   [String]    описание кода ошибки.
 */
class ResponseErrorDescription(
    @JacksonXmlProperty(localName = "Code")
    val code: Int,
    @JacksonXmlProperty(localName = "Message")
    val message: String
)