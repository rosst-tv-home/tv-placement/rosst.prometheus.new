package ru.rosst.prometheus.models.mediaplans.request

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import ru.rosst.prometheus.converters.serializers.SerializerMonth
import java.time.LocalDate

/**
 * Модель запроса медиапланов.
 *
 * @param sellingDirectionID    [Byte]      идентификатор направления продаж.
 * @param dateFrom              [LocalDate] дата начала периода.
 * @param dateBefore            [LocalDate] дата окончания периода.
 * @param advertisers           [Set]       коллекция [Int] идентификаторов рекламодателей.
 * @param channels              [Set]       коллекция [Int] идентификаторов каналов.
 */
@JacksonXmlRootElement(localName = "GetMPlans")
class RequestMediaPlans(
    @JacksonXmlProperty(localName = "SellingDirectionID")
    val sellingDirectionID: Byte,
    @JsonSerialize(using = SerializerMonth::class)
    @JacksonXmlProperty(localName = "StartMonth")
    val dateFrom: LocalDate,
    @JsonSerialize(using = SerializerMonth::class)
    @JacksonXmlProperty(localName = "EndMonth")
    val dateBefore: LocalDate,
    @JacksonXmlElementWrapper(localName = "AdtList")
    @JacksonXmlProperty(localName = "AdtID")
    val advertisers: Set<Int>?,
    @JacksonXmlElementWrapper(localName = "ChannelList")
    @JacksonXmlProperty(localName = "Cnl")
    val channels: Set<Int>?,
    @JacksonXmlProperty(localName = "IncludeEmpty")
    val isIncludeMediaPlanWithoutFilms: Boolean = false
)