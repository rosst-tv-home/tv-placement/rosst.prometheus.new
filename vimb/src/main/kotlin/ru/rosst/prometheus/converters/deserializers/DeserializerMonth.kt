package ru.rosst.prometheus.converters.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter

/**
 * Конвертер [String] формата ["yyyyMM"] в [LocalDate].
 */
class DeserializerMonth : StdDeserializer<LocalDate>(LocalDate::class.java) {
    /**
     * Метод конвертации [String] формата ["yyyyMM"] в [LocalDate].
     *
     * @param parser    [JsonParser]                api чтения xml.
     * @param context   [DeserializationContext]    контекст конвертации.
     *
     * @return          [LocalDate]                 дата.
     */
    override fun deserialize(
        parser: JsonParser?,
        context: DeserializationContext?
    ): LocalDate = YearMonth.parse(
        parser?.text, DateTimeFormatter.ofPattern("yyyyMM")
    ).atDay(1)
}