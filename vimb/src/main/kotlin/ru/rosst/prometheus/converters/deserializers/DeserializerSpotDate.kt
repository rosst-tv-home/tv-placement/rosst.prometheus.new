package ru.rosst.prometheus.converters.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.time.LocalDate

/**
 * Конвертер даты спота из [Int] в [LocalDate].
 */
class DeserializerSpotDate : StdDeserializer<LocalDate>(LocalDate::class.java) {
    /**
     * Метод конвертации даты спота из [Int] в [LocalDate].
     *
     * @param parser    [JsonParser]                api чтения xml.
     * @param context   [DeserializationContext]    контекст конвертации.
     *
     * @return          [LocalDate]                 дата.
     */
    override fun deserialize(parser: JsonParser?, context: DeserializationContext?): LocalDate {
        // Определяем наличие значения в теге
        val value = requireNotNull(parser?.text?.toIntOrNull()) { "Конвертация невозможна. Дата спота не найдена." }
        // Высчитываем год
        val year: Int = (value / 512)
        // Высчитываем месяц
        val month: Int = ((value - year * 512) / 32)
        // Высчитываем день
        val day: Int = (value - (year * 512 + month * 32))
        // Возвараем объект даты
        return LocalDate.of(year, month, day)
    }
}