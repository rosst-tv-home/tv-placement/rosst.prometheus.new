package ru.rosst.prometheus.converters.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Конвертер [LocalDate] в [String] формата ["yyyyMMdd"].
 */
class SerializerDate : StdSerializer<LocalDate>(LocalDate::class.java) {
    /**
     * Метод конвертации [LocalDate] в [String] формата ["yyyyMMdd"].
     *
     * @param value     [LocalDate]     дата.
     * @param generator [JsonGenerator] api для записи xml.
     */
    override fun serialize(value: LocalDate?, generator: JsonGenerator?, provider: SerializerProvider?) {
        generator?.writeString(value?.format(DateTimeFormatter.ofPattern("yyyyMMdd")))
    }
}