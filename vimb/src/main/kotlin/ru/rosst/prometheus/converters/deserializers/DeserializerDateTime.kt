package ru.rosst.prometheus.converters.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Конвертер [String] формата ["yyyy-MM-dd'T'HH:mm:ss"] в [LocalDateTime].
 */
class DeserializerDateTime : StdDeserializer<LocalDateTime>(LocalDateTime::class.java) {
    /**
     * Метод конвертации [String] формата ["yyyy-MM-dd'T'HH:mm:ss"] в [LocalDateTime].
     *
     * @param parser    [JsonParser]                api чтения xml.
     * @param context   [DeserializationContext]    контекст конвертации.
     *
     * @return          [LocalDateTime]             дата и время.
     */
    override fun deserialize(
        parser: JsonParser?,
        context: DeserializationContext?
    ): LocalDateTime = LocalDateTime.parse(
        parser?.text,
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
    )
}