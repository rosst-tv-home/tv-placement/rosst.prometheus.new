package ru.rosst.prometheus.converters.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer

/**
 * Конвертер [Byte] в [Boolean].
 */
class DeserializerBoolean : StdDeserializer<Boolean>(Any::class.java) {
    /**
     * Метод конвертации [Byte] в [Boolean].
     *
     * @param parser    [JsonParser]                api чтения XML.
     * @param context   [DeserializationContext]    контекст конвертации.
     *
     * @return          [Boolean]                   булево значение.
     */
    override fun deserialize(parser: JsonParser?, context: DeserializationContext?): Boolean {
        return requireNotNull(parser?.text?.toByteOrNull()) {
            "Ошибка конвертации. Значение не должно быть пустым."
        } == 1.toByte()
    }
}