package ru.rosst.prometheus.converters.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Конвертер [String] формата ["yyyyMMdd"] в [LocalDate].
 */
class DeserializerDate : StdDeserializer<LocalDate>(LocalDate::class.java) {
    /**
     * Метод конвертации [String] формата ["yyyyMMdd"] в [LocalDate].
     *
     * @param parser    [JsonParser]                api чтения xml.
     * @param context   [DeserializationContext]    контекст конвертации.
     *
     * @return          [LocalDate]                 дата.
     */
    override fun deserialize(
        parser: JsonParser?,
        context: DeserializationContext?
    ): LocalDate = LocalDate.parse(
        parser?.text,
        DateTimeFormatter.ofPattern("yyyyMMdd")
    )
}