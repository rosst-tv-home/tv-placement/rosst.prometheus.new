package ru.rosst.prometheus.services

import java.io.InputStream
import java.security.KeyStore
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * Сервис инициализации сертификата.
 */
object Certificate {
    /**
     * Метод инициализации сертификата.
     *
     * @param certificateFileInputStream    [InputStream]   поток ввода файла сертификата.
     * @param certificatePassword           [String]        пароль сертификата.
     */
    internal fun setSSLContext(certificateFileInputStream: InputStream, certificatePassword: String) {
        // Получаем экземпляр хранилища сертификатов
        val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())

        // Загружаем сертификат в хранилище
        keyStore.load(certificateFileInputStream, certificatePassword.toCharArray())

        // Получаем экземпляр менеджера сертификатов
        val keyManager = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())

        // Добавляем хранилище сертификатов в менеджер сертификатов
        keyManager.init(keyStore, certificatePassword.toCharArray())

        // Получаем экземпляр контекста запроса
        val context = SSLContext.getInstance("SSL")

        // Переопределям методы проверки сертификатов
        val trustAllCertificates = arrayOf<TrustManager>(object : X509TrustManager {
            override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
            override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
            override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
        })

        // Инициализируем контекст запроса
        context.init(keyManager.keyManagers, trustAllCertificates, SecureRandom())
        SSLContext.setDefault(context)
    }
}