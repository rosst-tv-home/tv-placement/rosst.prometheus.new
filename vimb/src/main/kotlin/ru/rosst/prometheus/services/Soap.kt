package ru.rosst.prometheus.services

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.w3c.dom.Node
import ru.rosst.prometheus.models.advertisers.request.RequestAdvertisers
import ru.rosst.prometheus.models.advertisers.response.ResponseAdvertisers
import ru.rosst.prometheus.models.blocks.request.RequestBlocks
import ru.rosst.prometheus.models.blocks.response.ResponseBlocks
import ru.rosst.prometheus.models.channels.request.RequestChannels
import ru.rosst.prometheus.models.channels.response.ResponseChannels
import ru.rosst.prometheus.models.errors.response.ResponseError
import ru.rosst.prometheus.models.mediaplans.request.RequestMediaPlans
import ru.rosst.prometheus.models.mediaplans.response.ResponseMediaPlans
import ru.rosst.prometheus.models.ranks.request.RequestRanks
import ru.rosst.prometheus.models.ranks.response.ResponseRanks
import ru.rosst.prometheus.models.spots.request.RequestSpotAdd
import ru.rosst.prometheus.models.spots.request.RequestSpots
import ru.rosst.prometheus.models.spots.response.ResponseSpotAdd
import ru.rosst.prometheus.models.spots.response.ResponseSpots
import java.io.*
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.zip.GZIPInputStream
import javax.xml.soap.MessageFactory
import javax.xml.soap.SOAPConnectionFactory
import javax.xml.soap.SOAPException
import javax.xml.soap.SOAPMessage
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import kotlin.reflect.KClass

/**
 * Сервис формирования и отправки запросов к VIMB API.
 *
 * @param SOAP_URL                              [String]        ссылка на метод. По умолчанию: "VIMBWebApplication2"
 * @param SOAP_HTTP_URL                         [String]        ссылка на сервер. По умолчанию: "https://vimb-svc.vitpc.com:435/VIMBService.asmx"
 * @param SOAP_LOCAL_NAME                       [String]        название метода. По умолчанию: "GetVimbInfoStream"
 * @param VIMB_CERTIFICATE_FILE_INPUT_STREAM    [InputStream]   поток чтения файла сертификата.
 * @param VIMB_CERTIFICATE_PASSWORD             [String]        пароль сертификата.
 * @Param XML_MAPPER                            [ObjectMapper]  API сериализации и десериализации объектов и xml.
 */
class Soap(
    private val SOAP_URL: String = "VIMBWebApplication2",
    private val SOAP_HTTP_URL: String = "https://vimb-svc.vitpc.com:435/VIMBService.asmx",
    private val SOAP_LOCAL_NAME: String = "GetVimbInfoStream",
    private val VIMB_CERTIFICATE_FILE_INPUT_STREAM: InputStream,
    private val VIMB_CERTIFICATE_PASSWORD: String,
    private val XML_MAPPER: ObjectMapper = XmlMapper().registerKotlinModule()
) {
    /**
     * Метод SOAP запроса к серверу VIMB.
     *
     * @param requestModel  [Any] объект модели запроса.
     *
     * @return              [Any] модель ответа на запрос.
     */
    fun getVimbInfoStream(requestModel: Any): Any {
        // Инициализируем сертификат
        Certificate.setSSLContext(VIMB_CERTIFICATE_FILE_INPUT_STREAM, VIMB_CERTIFICATE_PASSWORD)

        // Создаём экземпляр SOAP подключения
        val connectionFactory = SOAPConnectionFactory.newInstance()
        val connection = connectionFactory.createConnection()

        // Создаём экземпляр SOAP сообщения
        val messageFactory = MessageFactory.newInstance()
        val message = messageFactory.createMessage()

        // Добавляем в SOAP сообщение необходимые заголовки
        message.mimeHeaders.addHeader("SOAPAction", "$SOAP_URL/$SOAP_LOCAL_NAME")
        message.mimeHeaders.addHeader("Content-Type", "text/xml; charset=utf-8")

        // Добавляем в тело SOAP сообщения необходимые теги
        message.soapBody
            .addChildElement(SOAP_LOCAL_NAME, "", SOAP_URL)
            .addChildElement("InputXML")
            .addTextNode(XML_MAPPER.writeValueAsString(requestModel))

        // Сохраняем изменения сообщения
        message.saveChanges()

        // Debug message
        println("==========REQUEST MESSAGE==========")
        message.writeTo(System.out)
        println("\n==================================")

        // Переменная, которая будет хранить ответ от сервера VIMB
        val response: SOAPMessage

        // Пытаемся отправить запрос на сервер VIMB
        try {
            // Отправляем запрос на сервер VIMB
            response = connection.call(message, SOAP_HTTP_URL)
        } catch (exception: SOAPException) {
            throw exception
        } finally {
            // Закрываем соединение
            connection.close()
        }

        // Проверяем является ли ответ сообщением об ошибке
        try {
            // Метод выбросить сформированное исключение
            // если от сервера vimb пришёл ответ с ошибкой
            isFaultMessage(response)
        } catch (exception: Exception) {
            throw exception
        }

        // Получаем тело ответа (base64 строка)
        val base64 = response.soapBody.firstChild.firstChild.textContent

        // Debug message
        println("==========RESPONSE BASE64 MESSAGE==========")
        println(base64)
        println("===========================================")

        // Выбираем модель объекта ответа, конвертируем xml в объект
        // и возвращаем полученный экземпляр класса
        when (requestModel) {
            // Если запрос коллекции каналов
            is RequestChannels -> return decompress(base64, ResponseChannels::class)
            // Если запрос коллекции рекламодателей и заказчиков
            is RequestAdvertisers -> return decompress(base64, ResponseAdvertisers::class)
            // Если запрос рангов
            is RequestRanks -> return decompress(base64, ResponseRanks::class)
            // Если запрос коллекции спотов
            is RequestSpots -> return decompress(base64, ResponseSpots::class)
            // Если запрос коллекции медиапланов
            is RequestMediaPlans -> return decompress(base64, ResponseMediaPlans::class)
            // Если запрос коллекции блоков
            is RequestBlocks -> return decompress(base64, ResponseBlocks::class)
            // Если запрос добавления спота
            is RequestSpotAdd -> return decompress(base64, ResponseSpotAdd::class)
            // Иначе ошибка
            else -> throw Exception("Неизвестная модель объекта ответа от VIMB API.")
        }
    }

    /**
     * Метод проверки ответа от сервера VIMB на наличие сообщения об ошибке.
     *
     * @param message   [SOAPMessage]   ответ, пришедший из VIMB API.
     *
     * @exception       [Exception]     сообщение об ошибке, в случае ошибки.
     */
    private fun isFaultMessage(message: SOAPMessage) {
        // Если название первого тега тела сообщения - ответа не равно "soap:Fault" - прекращаем выполнение метода
        if (message.soapBody.firstChild.nodeName != "soap:Fault") return

        // Копируем объект DOM Node (тело сообщения ответа)
        val node = message.soapBody.firstChild.cloneNode(true)

        // Конвертируем объект DOM Node в XML строку
        val xml = nodeToString(node)

        println("==========RESPONSE ERROR BASE64 MESSAGE==========")
        println(xml)
        println("=================================================")

        // Конвертируем XML строку в объект модели ErrorResponse::class
        val errorResponse = XML_MAPPER.readValue(xml, ResponseError::class.java)

        // Выбрасываем исключение с сообщением об ошибке из объекта модели ErrorResponse::class
        throw errorResponse
    }

    /**
     * Метод конвертирует DOM Node объект в строку.
     *
     * @param node  [Node]      объект DOM, который нужно конвертировать в строку.
     *
     * @return      [String]    объект [Node] DOM конвертированный в строку.
     */
    private fun nodeToString(node: Node): String {
        // Экземпляр StringWriter, который будет хранить результат трансформации DOM Node объекта в строку
        val stringWriter = StringWriter()

        // Создание экземпляра трансформера
        val transformerFactory = TransformerFactory.newInstance()
        val transformer = transformerFactory.newTransformer()

        // Трансформация Node объекта в строку и запись результата в StringWriter
        transformer.transform(DOMSource(node), StreamResult(stringWriter))

        // Возвращаем получившуюся строку
        return stringWriter.toString()
    }

    /**
     * Метод конвертирует Base64 строку в объект модели ответа.
     *
     * @param base64        [String]    base64 строка.
     * @param responseModel [KClass]    модель ответа, в которую нужно конвертировать строку.
     *
     * @return              [T]         модель ответа.
     */
    private fun <T : Any> decompress(base64: String, responseModel: KClass<T>): Any = BufferedReader(
        InputStreamReader(
            GZIPInputStream(ByteArrayInputStream(Base64.getDecoder().decode(base64))),
            StandardCharsets.UTF_8
        )
    ).use { return@decompress XML_MAPPER.readValue(it, responseModel.java) }
}